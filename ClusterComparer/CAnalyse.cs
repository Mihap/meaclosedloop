﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClusterComparer
{
  public class CAnalyse
  {
    // Начальные данные кластеризации по:
    private ClustData baseFreqClustData; // частотной характеристике
    private ClustData baseSprdClustData; // пространственной характеристике

    // Данные кластеризации после отсеивания несовпадающих пачек
    public ClustData freqClustData;
    public ClustData sprdClustData;

    // Матрица пересечений: aij = Ai && Bj / Ai || Bj
    public double[,] crossMatrix;

    // Матрица значений: aij = Ai && Bj
    public int[,] valueMatrix;

    // Допустимая разность времени начала одинаковых пачек
    private static int delta;

    // Кол-во совпавших
    public int sameBurstsCount;

    public List<long> notSameBursts;

    private double th = 0.2;

    public CAnalyse(string freqFileName, string sprdFileName, bool freqPoints, bool sprdPoints, bool _withDuration, int _delta)
    {
      delta = _delta;
      baseFreqClustData = CFileReader.Read(freqFileName, freqPoints, false);
      baseSprdClustData = CFileReader.Read(sprdFileName, sprdPoints, _withDuration);
      baseSprdClustData.withDuration = _withDuration;
      valueMatrix = FindSameBursts(baseFreqClustData.RemoveSmallClusters(th), baseSprdClustData.RemoveSmallClusters(th), out freqClustData, out sprdClustData, out notSameBursts);
      crossMatrix = FillCrossMatrix(valueMatrix, freqClustData, sprdClustData);
      sameBurstsCount = valueMatrix.Cast<int>().Sum();
    }

    /// <summary>
    /// Отсеивание всех несовпадающих пачек
    /// </summary>
    /// <param name="freqData">Изначальное кластерное распределение по частотной хар-ке</param>
    /// <param name="sprdData">Изначальное кластерное распределение по пространственной хар-ке</param>
    /// <param name="freqDataOut">Выходное кластерное распределение с исключенными пачками по частотной хар-ке</param>
    /// <param name="sprdDataOut">Выходное кластерное распределение с исключенными пачками по пространственной хар-ке</param>
    /// <returns>Возвращает матрицу с кол-вом совпадающих пачек между
    /// i-ым кластером 1ого типа и j-ым 2ого (в изначальной нумерации)</returns>
    public static int[,] FindSameBursts(ClustData freqData, ClustData sprdData, out ClustData freqDataOut, out ClustData sprdDataOut, out List<long> differBursts)
    {
      differBursts = new List<long>();
      // Инизиализация матрицы
      int[,] resultMatrix = new int[freqData.Count, sprdData.Count];
      for (int i = 0; i < freqData.Count; i++)
      {
        for (int j = 0; j < sprdData.Count; j++)
        {
          resultMatrix[i, j] = 0;
        }
      }

      // Инициализация словарей
      freqDataOut = new ClustData();
      sprdDataOut = new ClustData();
      foreach (int key in sprdData.Keys)
      {
        List<long> valueList = new List<long>();
        sprdDataOut.Add(key, valueList);
      }
      foreach (int key in freqData.Keys)
      {
        List<long> valueList = new List<long>();
        freqDataOut.Add(key, valueList);
      }

      // Для случая, когда пачки по пространственной хар-ке заданы диапазоном
      if (sprdData.withDuration)
      {
        foreach (KeyValuePair<int, List<long>> sprdKvp in sprdData)
        {
          for (int i = 0; i < sprdKvp.Value.Count; i += 2)
          {
            List<KeyValuePair<int, long>> sameTimesList = new List<KeyValuePair<int, long>>();
            long t1 = sprdKvp.Value[i];
            long t2 = sprdKvp.Value[i + 1];

            foreach (KeyValuePair<int, List<long>> freqKvp in freqData)
            {
              foreach (long freqTime in freqKvp.Value)
              {
                if (((t1 - delta) < freqTime) && ((t1 + 12500 + delta) > freqTime))
                {
                  // Добавляем пачки, входящие в диапазон, в список
                  sameTimesList.Add(new KeyValuePair<int, long>(freqKvp.Key, freqTime));
                }
              }
            }

            if (sameTimesList.Count > 0)
            {
              // Сортируем и берём самую раннюю пачку
              sameTimesList.Sort(new KvpComparer());
              freqDataOut[sameTimesList[0].Key].Add(sameTimesList[0].Value);
              sprdDataOut[sprdKvp.Key].Add(t1);

              // Обновляем матрицу пересечений
              int sk = sprdKvp.Key;
              resultMatrix[sameTimesList[0].Key, sk]++;
            }
            else
            {
              differBursts.Add(t1);
            }
          }
        }
      }
      // Если известно только время начала пачки
      else
      {
        foreach (KeyValuePair<int, List<long>> freqKvp in freqData)
        {
          foreach (long freqTime in freqKvp.Value)
          {
            List<long> sameTimesList = new List<long>();
            List<int> keysList = new List<int>();

            foreach (KeyValuePair<int, List<long>> sprdKvp in sprdData)
            {
              foreach (long sprdTime in sprdKvp.Value)
              {
                if (Math.Abs(sprdTime - freqTime) < delta)
                {
                  sameTimesList.Add(sprdTime);
                  keysList.Add(sprdKvp.Key);
                }
              }
            }

            if (sameTimesList.Count > 0)
            {
              long sprdTime = 0;
              int sprdKey = 0;
              if (sameTimesList.Count == 1)
              {
                sprdTime = sameTimesList[0];
                sprdKey = keysList[0];
              }
              else
              {
                // Ищем самую близкую пачку из списка
                double dif = delta;
                for (int i = 0; i < sameTimesList.Count; i++)
                {
                  if (dif > Math.Abs(sameTimesList[i] - freqTime))
                  {
                    dif = Math.Abs(sameTimesList[i] - freqTime);
                    sprdTime = sameTimesList[i];
                    sprdKey = keysList[i];
                  }
                }
              }
              // Добавляем эти пачки в новые словари
              if (freqDataOut.ContainsKey(freqKvp.Key))
              {
                freqDataOut[freqKvp.Key].Add(freqTime);
              }

              if (sprdDataOut.ContainsKey(sprdKey))
              {
                sprdDataOut[sprdKey].Add(sprdTime);
              }

              // Обновляем матрицу пересечений
              int sk = sprdKey;
              resultMatrix[freqKvp.Key, sk]++;

            }
            else
            {
              differBursts.Add(freqTime);
            }
          }

        }
      }
      return resultMatrix;
    }

    /// <summary>
    /// Заполнение матрицы пересечений
    /// </summary>
    /// <param name="matrix">Матрица количеств пересечений</param>
    /// <param name="freqData">Кластерное распределение по частотной хар-ке</param>
    /// <param name="sprdData">Кластерное распределение по пространственной хар-ке</param>
    /// <returns></returns>
    private double[,] FillCrossMatrix(int[,] matrix, ClustData freqData, ClustData sprdData)
    {
      double[,] result = new double[freqData.Count, sprdData.Count];
      bool from1 = sprdData.Keys.ElementAt<int>(0) == 1;
      {
        for (int i = 0; i < freqData.Count; i++)
        {
          int freqKey = freqData.Keys.ElementAt<int>(i);

          for (int j = 0; j < sprdData.Count; j++)
          {
            if (matrix[i, j] == 0)
            {
              result[i, j] = 0;
            }
            else
            {
              int sprdKey = sprdData.Keys.ElementAt<int>(j);
              double sprdCount = sprdData[sprdKey].Count;
              result[i, j] = (float)matrix[i, j] / (freqData[freqKey].Count + sprdData[sprdKey].Count - matrix[i, j]);
            }
          }
        }
      }
      return result;
    }

    /// <summary>
    /// Формирование матрицы для отрисовки (исключение пустых кластеров)
    /// </summary>
    /// <param name="matrix">Матрица количеств пересечений</param>
    /// <param name="drawMatrix">Выходная матрицы отрисовки</param>
    /// <returns>Список элементов матрицы</returns>
    public List<matrixElInfo> fillDrawMatrix(int[,] matrix, ClustData freqData, ClustData sprdData, out double[,] drawMatrix)
    {
      drawMatrix = new double[freqData.Count - freqData.EmptyClustersCount, sprdData.Count - sprdData.EmptyClustersCount];
      List<matrixElInfo> elemList = new List<matrixElInfo>();

      int m = 0;
      int n = 0;
      {
        for (int i = 0; i < freqData.Count; i++)
        {
          int freqKey = freqData.Keys.ElementAt<int>(i);
          if (freqData[freqKey].Count == 0)
            continue;

          n = 0;
          for (int j = 0; j < sprdData.Count; j++)
          {
            int sprdKey = sprdData.Keys.ElementAt<int>(j);
            if (sprdData[sprdKey].Count == 0)
              continue;
            if (matrix[i, j] == 0)
            {
              drawMatrix[m, n] = 0;
            }
            else
            {
              double sprdCount = sprdData[sprdKey].Count;
              drawMatrix[m, n] = (float)matrix[i, j] / (freqData[freqKey].Count + sprdData[sprdKey].Count - matrix[i, j]);
            }
            elemList.Add(new matrixElInfo() { x = m, y = n, Value = drawMatrix[m, n++], Count = matrix[i,j]});
          }
          m++;
        }
      }
      return elemList;
    }

  }



  public class matrixElInfo
  {
    public int x { get; set; }
    public int y { get; set; }
    public double Value { get; set; }
    public double Count { get; set; }
  }

  public class KvpComparer : IComparer<KeyValuePair<int, long>>
  {
    public int Compare(KeyValuePair<int, long> x, KeyValuePair<int, long> y)
    {
      if (x.Value > y.Value)
      {
        return 1;
      }
      else if (x.Value < y.Value)
      {
        return -1;
      }
      else
      {
        return 0;
      }
    }
  }
}
