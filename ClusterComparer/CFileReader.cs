﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ClusterComparer
{
  static class CFileReader
  {
    // Чтение данных из файла формата:
    //PatternNum1 Time;Time;Time;Time .....
    //PatternNum2 Time;Time;Time;Time .....
    //PatternNum3 Time;Time;Time;Time .....
    //Time изм в отсчётах: points = true; в секундах: points = false
    public static ClustData Read(string fileName, bool points, bool withDuration)
    {
      ClustData result = new ClustData();
      char[] separator1 = { ' ' };
      char[] separator2 = { ';' };
      char[] separator3 = { '-' };

      using (StreamReader sr = new StreamReader(fileName))
      {
        while (true)
        {
          try
          {
            string[] line = sr.ReadLine().Split(separator1);
            int id = int.Parse(line[0]);
            List<long> timeList = new List<long>();
            string[] timeArr = { "" };
            if (line.Length > 1)
            {
              timeArr = line[1].Split(separator2);
            }
            foreach (string time in timeArr)
            {
              if (withDuration)
              {
                string[] time12 = time.Split(separator3);
                double t1;
                double t2;
                if (double.TryParse(time12[0], out t1) && double.TryParse(time12[1], out t2))
                {
                  if (!points)
                  {
                    t1 *= 25000;
                    t2 *= 25000;
                  }
                  timeList.Add((long)t1);
                  timeList.Add((long)t2);
                }
              }
              else
              {
                double t;
                if (double.TryParse(time, out t))
                {
                  // Преобразование из секунд в отсчёты
                  if (!points)
                  {
                    t *= 25000;
                  }
                  timeList.Add((long)t);
                }
              }
            }
            result.Add(id, timeList);
          }
          catch (Exception)
          {
            break;
          }
        }
      }

      return result;
    }
  }
}
