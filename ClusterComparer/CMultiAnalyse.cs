﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MEAClosedLoop;

namespace ClusterComparer
{
  public class CMultiAnalyse
  {
    // Словарь со сравнитильным анализом по каждому каналу
    public Dictionary<int, CAnalyse> analyseResultDict;

    public List<chInfo> chInfoList;

    public CMultiAnalyse(string folderName, string sprdFileName, bool points, bool withDuration, int delta)
    {
      analyseResultDict = new Dictionary<int, CAnalyse>();
      chInfoList = new List<chInfo>();
      Read(folderName, sprdFileName, points, withDuration, delta);
    }

    private void Read(string folderName, string sprdFileName, bool points, bool withDuration, int delta)
    {
      // Пусть файлы именуются в виде xxx_channelNum.txt
      List<string> fileNames = System.IO.Directory.EnumerateFiles(folderName).ToList();
      char[] separator = { '.', '_' };
      foreach (string path in fileNames)
      {
        string fileName = System.IO.Path.GetFileName(path);
        string[] splittedString = fileName.Split(separator);
        if (splittedString.Length == 3 && splittedString[2] == "txt")
        {
          CAnalyse ca = new CAnalyse(path, sprdFileName, points, points, withDuration, delta);
          int chNum = Int32.Parse(splittedString[1]);
          analyseResultDict.Add(chNum, ca);

          if (ca.sameBurstsCount != 0)
          {
            chInfoList.Add(new chInfo()
            {
              Num = MEA.IDX2NAME[chNum],
              BurstsCount = ca.sameBurstsCount,
              MaxEl = ca.crossMatrix.Cast<double>().Max(),
              Sum = ca.crossMatrix.Cast<double>().Sum()
            });
          }
        }
      }
    }

  }

  public class chInfo
  {
    public int Num { get; set; }

    public double Sum { get; set; }

    public double MaxEl { get; set; }

    public int BurstsCount { get; set; }
  }
}
