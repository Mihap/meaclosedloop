﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClusterComparer
{
  public class ClustData : Dictionary<int, List<long>>
  {
    public bool withDuration = false;

    /// <summary>
    /// Удаление кластеров, на которые приходится меньше некоторого процента th от общего
    /// количества пачечных событий
    /// </summary>
    /// <param name="th">[0,1]. Порог</param>
    /// <returns>Новое распределение</returns>
    public ClustData RemoveSmallClusters(double th)
    {
      ClustData result = new ClustData();

      int burstsThreshold = (int)(BurstsCount * (1 - th));
      int current = 0; // текущее количество пачек
      int n = 0; // новое значение ключа (переопределяем нумерацию кластеров с нуля)
      foreach(KeyValuePair<int, List<long>> kvp in this.OrderByDescending(x => x.Value.Count))
      {
        result.Add(n++, kvp.Value);
        current += kvp.Value.Count;
        if (current >= burstsThreshold)
          break;
      }

      return result;
    }

    /// <summary>
    /// Перемешивает элементы кластерного распределения, сохраняя размерность кластеров
    /// </summary>
    /// <returns>Распределение с перемешаннами пачками</returns>
    public ClustData Shuffle()
    {
      ClustData result = new ClustData();

      List<long> allBursts = new List<long>();
      foreach (List<long> list in Values)
      {
        allBursts.AddRange(list);
      }

      Random rand = new Random(System.Environment.TickCount);
      for (int i = 0; i < allBursts.Count; i++)
      {
        long tmp = allBursts[i];
        allBursts.RemoveAt(i);
        allBursts.Insert(rand.Next(0, allBursts.Count), tmp);
      }

      foreach(KeyValuePair<int, List<long>> kvp in this)
      {
        List<long> list = new List<long>();
        for(int i = 0; i < kvp.Value.Count; i++)
        {
          list.Add(allBursts[0]);
          allBursts.RemoveAt(0);
        }
        result.Add(kvp.Key, list);
      }

      return result;
    }

    /// <summary>
    /// Количество пачечных событий
    /// </summary>
    public int BurstsCount
    {
      get
      {
        int cnt = 0;
        foreach (List<long> list in Values)
        {
          cnt += list.Count;
        }
        if (withDuration)
        {
          cnt = cnt / 2;
        }
        return cnt;
      }
      
    }

    /// <summary>
    /// Количество пустых кластеров
    /// </summary>
    public int EmptyClustersCount
    {
      get
      {
        int cnt = 0;
        foreach (List<long> list in Values)
        {
          if (list.Count == 0)
          {
            cnt++;
          }
        }
        return cnt;
      }
    }

  }
}
