﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClusterComparer
{
  /// <summary>
  /// Логика взаимодействия для FAnalyseResult.xaml
  /// </summary>
  public partial class FAnalyseResult : Window
  {
    CAnalyse ca;
    double[,] drawMatrix;

    public FAnalyseResult(CAnalyse _ca)
    {
      InitializeComponent();
      ca = _ca;
      Update();
    }

    public void Update()
    {
      // Заполнение полей со статистикой анализа
      freqTextBlock1.Text += ca.crossMatrix.GetLength(0);
      sprdTextBlock1.Text += ca.crossMatrix.GetLength(1);
      freqTextBlock2.Text += ca.freqClustData.BurstsCount;
      sprdTextBlock2.Text += ca.sprdClustData.BurstsCount;
      freqTextBlock3.Text += Math.Round((double)ca.sameBurstsCount * 100 / ca.freqClustData.BurstsCount, 1) + "% (" + ca.sameBurstsCount +")";
      sprdTextBlock3.Text += Math.Round((double)ca.sameBurstsCount * 100 / ca.sprdClustData.BurstsCount, 1) + "% (" + ca.sameBurstsCount + ")";
      freqTextBlock4.Text += ca.freqClustData.EmptyClustersCount;
      sprdTextBlock4.Text += ca.sprdClustData.EmptyClustersCount;
      maxValTextBlock.Text += "Сумма элементов матрицы: " + Math.Round(ca.crossMatrix.Cast<double>().Sum(), 2);

      // Заполнение матрицы для отрисовки
      List<matrixElInfo> elemList = ca.fillDrawMatrix(ca.valueMatrix, ca.freqClustData, ca.sprdClustData, out drawMatrix);
      // Заполнение покластерного списка пересечений
      dataGrid.ItemsSource = elemList;
    }

    private void drawButton_Click(object sender, RoutedEventArgs e)
    {
      Draw(drawMatrix, ca.freqClustData, ca.sprdClustData, "Пересечение пачечных событий в паттернах"); 
    }

    private void drawRandomMatrix_Click(object sender, RoutedEventArgs e)
    {
      ClustData data1;
      ClustData data2;
      List<long> dif;
      ClustData shData1 = ca.freqClustData.Shuffle();
      ClustData shData2 = ca.sprdClustData.Shuffle();
      int[,] matrix = CAnalyse.FindSameBursts(shData1, shData2, out data1, out data2, out dif);

      double[,] randMatr;
      List<matrixElInfo> elemList = ca.fillDrawMatrix(matrix, shData1, shData2, out randMatr);
      //dataGrid.ItemsSource = elemList;
      Draw(randMatr, shData1, shData2, "Пересечение паттернов с перемешанными событиями");
    }

    private void exportButton_Click(object sender, RoutedEventArgs e)
    {
      string fileName;
      using(SaveFileDialog dialog = new SaveFileDialog())
      {
        switch(dialog.ShowDialog())
        {
          case System.Windows.Forms.DialogResult.OK:
            fileName = dialog.FileName;
            break;
          default:
            return;
        }
      }
      using (StreamWriter sw = new StreamWriter(fileName, false))
      {
        ca.notSameBursts.Sort();
        foreach (long t in ca.notSameBursts)
        {
          sw.Write("{0} ", (double)t / 25000);
        }
      }
    }

   

    private void Draw(double[,] matrix, ClustData freq, ClustData sprd, string title)
    {
      //var matrix1 = new double[matrix.GetLength(1), matrix.GetLength(0)];

      //for(int i = 0; i < matrix.GetLength(0); i++)
      //{
      //  for(int j = 0; j < matrix.GetLength(1); j++)
      //  {
      //    matrix1[j, i] = matrix[i, j];
      //  }
      //}
      //matrix = matrix1;

      Window gridWindow = new Window();
      gridWindow.Title = title;

      Grid grid = new Grid();
      grid.ShowGridLines = true;

      ColumnDefinition col1 = new ColumnDefinition();
      col1.MaxWidth = 40;
      grid.ColumnDefinitions.Add(col1);
      for (int i = 0; i < matrix.GetLength(0); i++)
      {
        ColumnDefinition col = new ColumnDefinition();
        grid.ColumnDefinitions.Add(col);
      }

      RowDefinition row1 = new RowDefinition();
      row1.MaxHeight = 30;
      grid.RowDefinitions.Add(row1);
      for (int i = 0; i < matrix.GetLength(1); i++)
      {
        RowDefinition row = new RowDefinition();
        grid.RowDefinitions.Add(row);
      }

      int n = 1;
      foreach(int key in freq.Keys)
      {
        if(freq[key].Count != 0)
        {
          TextBlock tb = new TextBlock();
          tb.TextAlignment = TextAlignment.Center;
          tb.VerticalAlignment = VerticalAlignment.Center;
          tb.FontSize = 14;
          tb.Text = freq[key].Count.ToString();
          Grid.SetColumn(tb, n++);
          Grid.SetRow(tb, 0);
          grid.Children.Add(tb);
        }
      }
      n = 1;
      foreach (int key in sprd.Keys)
      {
        if (sprd[key].Count != 0)
        {
          TextBlock tb = new TextBlock();
          tb.TextAlignment = TextAlignment.Center;
          tb.VerticalAlignment = VerticalAlignment.Center;
          tb.FontSize = 14;
          tb.Text = sprd[key].Count.ToString();
          Grid.SetColumn(tb, 0);
          Grid.SetRow(tb, n++);
          grid.Children.Add(tb);
        }
      }

      double max = matrix.Cast<double>().Max();
      for (int i = 1; i < matrix.GetLength(0) + 1; i++)
      {
        for (int j = 1; j < matrix.GetLength(1) + 1; j++)
        {
          Rectangle r = new Rectangle();
          double k = matrix[i - 1, j - 1] / max;
          r.Fill = new SolidColorBrush(Color.FromRgb((byte)(255 * k), 0, (byte)(255 - 255 * k)));
          Grid.SetColumn(r, i);
          Grid.SetRow(r, j);
          grid.Children.Add(r);
        }
      }

      gridWindow.Content = grid;
      gridWindow.Show();
    }
  }
}
