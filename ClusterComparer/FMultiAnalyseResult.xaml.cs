﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClusterComparer
{
  /// <summary>
  /// Логика взаимодействия для FMultiAnalyseResult.xaml
  /// </summary>
  public partial class FMultiAnalyseResult : Window
  {
    CMultiAnalyse cma;
    MEAClosedLoop.UIForms.ChannelComboBox selectControl;

    public FMultiAnalyseResult(CMultiAnalyse _cma)
    {
      InitializeComponent();
      cma = _cma;
      selectControl = new MEAClosedLoop.UIForms.ChannelComboBox();
      wfHost.Child = selectControl;
      selectControl.OnSelectedChannelsChanged += channelChanged;
      chDataGrid.ItemsSource = cma.chInfoList;
    }

    public FMultiAnalyseResult(CAnalyse ca)
    {
      InitializeComponent();
      FAnalyseResult ar = new FAnalyseResult(ca);
      object content = ar.Content;
      ar.Content = null;
      Grid.SetColumn(content as UIElement, 1);
      mainGrid.Children.Add(content as UIElement);
    }

    void channelChanged()
    {
      int chNum = selectControl.chIDList.First();

      if (!cma.analyseResultDict.ContainsKey(chNum))
      {
        wrongChLabel.Content = "Такого канала нет";
        return;
      }

      foreach (UIElement control in mainGrid.Children)
      {
        if (Grid.GetColumn(control) == 1)
        {
          mainGrid.Children.Remove(control);
          break;
        }
      }

      wrongChLabel.Content = "";
      CAnalyse ca = cma.analyseResultDict[chNum];
      FAnalyseResult ar = new FAnalyseResult(ca);
      object content = ar.Content;
      ar.Content = null;
      Grid.SetColumn(content as UIElement, 1);
      mainGrid.Children.Add(content as UIElement);
    }

  }
}
