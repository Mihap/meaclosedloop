﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using System.Windows.Forms;

namespace ClusterComparer
{
  /// <summary>
  /// Логика взаимодействия для MainWindow.xaml
  /// </summary>
  public partial class MainWindow : Window
  {
    CAnalyse analyseResult;

    public MainWindow()
    {
      InitializeComponent();
    }

    private void sprdOpnButton_Click(object sender, RoutedEventArgs e)
    {
      Microsoft.Win32.OpenFileDialog dialog = new Microsoft.Win32.OpenFileDialog();
      switch (dialog.ShowDialog())
      {
        case true:
          break;
        default:
          return;
      }
      sprdFileTextBox.Text = dialog.FileName;
      if (freqFileTextBox.Text.Length > 1)
      {
        actionButton.IsEnabled = true;
      }
    }

    private void freqOpnButton_Click(object sender, RoutedEventArgs e)
    {
      if (!(bool)multiSelectChckBox.IsChecked)
      {
        Microsoft.Win32.OpenFileDialog dialog = new Microsoft.Win32.OpenFileDialog();
        switch (dialog.ShowDialog())
        {
          case true:
            break;
          default:
            return;
        }
        freqFileTextBox.Text = dialog.FileName;
        if (sprdFileTextBox.Text.Length > 1)
        {
          actionButton.IsEnabled = true;
        }
      }
      else
      {
        using (FolderBrowserDialog dialog = new FolderBrowserDialog())
        {
          switch (dialog.ShowDialog())
          {
            case System.Windows.Forms.DialogResult.OK:
              break;
            default:
              return;
          }
          freqFileTextBox.Text = dialog.SelectedPath;
          if (sprdFileTextBox.Text.Length > 1)
          {
            actionButton.IsEnabled = true;
          }

        }
      }
    }

    private void actionButton_Click(object sender, RoutedEventArgs e)
    {
      if (!(bool)multiSelectChckBox.IsChecked)
      {
        analyseResult = new CAnalyse(freqFileTextBox.Text, sprdFileTextBox.Text,
          (bool)isPointsRadioButtonFreq.IsChecked, (bool)isPointsRadioButton.IsChecked, (bool)burstBeginEndRadioButton.IsChecked,
          Int32.Parse(deltaTextBox.Text));
        FAnalyseResult fa = new FAnalyseResult(analyseResult);
        fa.Show();
      }
      else
      {
        CMultiAnalyse cma = new CMultiAnalyse(freqFileTextBox.Text, sprdFileTextBox.Text,
          (bool)isPointsRadioButton.IsChecked, (bool)burstBeginEndRadioButton.IsChecked,
          Int32.Parse(deltaTextBox.Text));
        FMultiAnalyseResult fma = new FMultiAnalyseResult(cma);
        fma.Show();
      }
    }
  }
}
