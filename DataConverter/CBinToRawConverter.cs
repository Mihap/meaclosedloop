﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading.Tasks;

namespace DataConverter
{
  using TFltDataPacket = Dictionary<int, System.Double[]>;
  using TKvpII = KeyValuePair<int, System.Int16[]>;
  using TKvpIUi = KeyValuePair<int, System.UInt16[]>;
  using TRawDataPacket = Dictionary<int, ushort[]>;

  class CBinToRawConverter
  {
    private string InputFileName;
    private string OutputFileName;
    private object FileWriteLock = new object();
    private long FilePosition = 2;
    public int FileDuration;
    public int Length;
    private bool GotLength = false;

    public CBinToRawConverter() { }
    public CBinToRawConverter(string input, string output)
    {
      this.InputFileName = input;
      this.OutputFileName = output;
    }

    public bool Write(TRawDataPacket data)
    {
      try
      {
        lock (FileWriteLock)
        {
          using (BinaryWriter bw = new BinaryWriter(File.Open(OutputFileName, FileMode.Append, FileAccess.Write)))
          {
            for (int j = 0; j < data.Values.First().Count(); j++)
            {
              for (int i = 0; i < data.Count; i++)
                bw.Write(data[i][j]);
            }
          }
        }
        return true;
      }
      catch (Exception e)
      {
        return false;
      }
    }

    public TRawDataPacket Read()
    {
      try
      {
        using (BinaryReader br = new BinaryReader(File.Open(InputFileName, FileMode.Open, FileAccess.Read)))
        {
          if (!GotLength)
          {
            GotLength = true;
            Length = (int)br.BaseStream.Length;
          }
          br.BaseStream.Position = FilePosition;
          FileDuration = br.ReadInt32();
          byte[] binData = br.ReadBytes(FileDuration);
          FilePosition = br.BaseStream.Position;
          TRawDataPacket DataPacket = BinaryToRawData(binData);
          return DataPacket;
        }
      }
      catch (Exception e)
      {
        return null;
      }
    }

    private static TRawDataPacket BinaryToRawData(Byte[] data)
    {
      MemoryStream ms = new MemoryStream(data, false);
      BinaryFormatter formatter = new BinaryFormatter();
      TKvpII[] FltDataArray= (TKvpII[])formatter.Deserialize(ms);
      TKvpIUi[] RawDataArray = Array.ConvertAll<TKvpII, TKvpIUi>(FltDataArray, new Converter<TKvpII, TKvpIUi>(FltKvpToRaw));
      TRawDataPacket result = RawDataArray.ToDictionary((keyItem) => keyItem.Key, (valueItem) => valueItem.Value);
      return result;
    }

    private static TKvpIUi FltKvpToRaw(TKvpII kvpi)
    {
      return new TKvpIUi(kvpi.Key, Array.ConvertAll<System.Int16, System.UInt16>(kvpi.Value, new Converter<System.Int16, System.UInt16>(IntToUInt)));
    }

    private static System.UInt16 IntToUInt(System.Int16 x)
    {
      return (System.UInt16)( x + System.UInt16.MaxValue / 4);
    }

    public void Convert()
    {
      TRawDataPacket DataPacket = Read();
      while (DataPacket != null)
      {
        Write(DataPacket);
        Read();
      }
    }

  }
}
