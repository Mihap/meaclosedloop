﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace DataConverter
{
  using TRawDataPacket = Dictionary<int, ushort[]>;
  class CNoiseFiller
  {
    int blockSize = 2500;
    int[] elArray;
    int[] emptyChannelsArr;
    string[] filesToRead;
    string fileToRead;
    string fileToSave;
    int numOfCh;
    bool[] chID;
    bool isFileEnded = false;
    //ushort noiseValue;

    public string currentFile;
    public int progress = 0;
    public int fullProgressValue = 0;
    const double NORM_KOEF = 100;

    private object FileLockObject = new object();

    public CNoiseFiller(string path1, string path2)
    {
      fileToRead = path1;
      fileToSave = path2;
    }

    public CNoiseFiller(string[] pathList, string path2)
    {
      fileToSave = path2;
      filesToRead = pathList;
    }

    // Записывает в новый файл дополненные(если какие-то каналы остутсвуют)
    // шумом данные из нескольких файлов
    public void ConvertFilesList(ushort noiseValue)
    {
      using (BinaryWriter bw = new BinaryWriter(File.Open(fileToSave, FileMode.Create, FileAccess.Write)))
      { }

      int[] fullChList = new int[60];
      for (int i = 0; i < 60; i++)
      {
        fullChList[i] = i;
      }

      foreach (string fileName in filesToRead)
      {
        currentFile = fileName;
   
        int startOfData = ParseHeader(fileName);
        bool isFileNeedConvertion = true;

        if (startOfData != 0)
        {
          Array.Sort(elArray);
          chID = ConvertIDArr();
          emptyChannelsArr = GetEmptyChannels();
        }
        else
        {
          elArray = fullChList;
          numOfCh = 60;
          isFileNeedConvertion = false;
        }


        using (BinaryReader br = new BinaryReader(File.Open(fileName, FileMode.Open, FileAccess.Read)))
        {
          double fpValue = ((double)br.BaseStream.Length * (double)int.MaxValue * NORM_KOEF / long.MaxValue);
          fullProgressValue = (int)fpValue;

          br.BaseStream.Position = startOfData;
          while (true)
          {
            TRawDataPacket readData = ReadRawDataPack(br);
            if (isFileEnded)
              break;

            progress = (int)((double)(br.BaseStream.Position * int.MaxValue) * NORM_KOEF / long.MaxValue);
            if(progress < 0)
            {

            }

            if (isFileNeedConvertion)
            {
              TRawDataPacket convData = ConvRawDataPack(readData, noiseValue);
              WriteRawDataPack(convData);
            }
            else
            {
              WriteRawDataPack(readData);
            }
            
          }
        }
        fullProgressValue = 0;
        progress = 0;
        isFileEnded = false;
      }
    }

    // Записывает в новый файл дополненные шумом данные
    public void Convert(ushort noiseValue)
    {
      int startOfData = ParseHeader(fileToRead);
      if(startOfData == 0)
      {
        throw new Exception("WRONG format of Header!");
      }

      Array.Sort(elArray);
      chID = ConvertIDArr();
      emptyChannelsArr = GetEmptyChannels();

      using (BinaryWriter bw = new BinaryWriter(File.Open(fileToSave, FileMode.Create, FileAccess.Write)))
      { }

      using (BinaryReader br = new BinaryReader(File.Open(fileToRead, FileMode.Open, FileAccess.Read)))
      {
        double fpValue = ((double)br.BaseStream.Length * (double)int.MaxValue * NORM_KOEF / long.MaxValue);
        fullProgressValue = (int)fpValue;

        br.BaseStream.Position = startOfData;
        while (true)
        {
          TRawDataPacket readData = ReadRawDataPack(br);
          if (isFileEnded)
            break;
          long progress2 = (long)((double)(br.BaseStream.Position * (double)int.MaxValue) * NORM_KOEF / long.MaxValue);
          progress = (int)((double)(br.BaseStream.Position * (double)int.MaxValue) * NORM_KOEF / long.MaxValue);
          if (progress < 0)
          {

          }
          TRawDataPacket convData = ConvRawDataPack(readData, noiseValue);
          WriteRawDataPack(convData);
        }
      }
    }

    // Чтение одного TRawDataPacket из файла
    private TRawDataPacket ReadRawDataPack(BinaryReader binReader)
    {

      TRawDataPacket data;
      data = new TRawDataPacket(numOfCh);
      for (int k = 0; k < numOfCh; ++k)
      {
        data[elArray[k]] = new ushort[blockSize];
      }

      try
      {
        //int count = 0;
        lock (FileLockObject)
        {
          for (int j = 0; j < blockSize; j++)
          {
            for (int k = 0; k < numOfCh; k++)
            {
              ushort tmpData = binReader.ReadUInt16();
              int key = elArray[k];
              data[key][j] = tmpData;
            }
          }
        }
        return data;
      }
      catch (EndOfStreamException e)
      {
        isFileEnded = true;
        return new TRawDataPacket();
        // System.Windows.Forms.MessageBox.Show("EOF! " + e.Message);
      }
    }

    // Возвращает массив ushort с заданным значением шума
    private ushort[] GetFillerData(ushort noiseValue)
    {
      ushort[] arrOut = new ushort[blockSize];
      for (int i = 0; i < blockSize; i++)
      {
        arrOut[i] = noiseValue;
      }
      return arrOut;
    }

    // Возвращает новый TRawDataPacket, дополненный шумом по отсутствующим каналам
    private TRawDataPacket ConvRawDataPack(TRawDataPacket dataIn, ushort noiseValue)
    {
      TRawDataPacket dataOut = new TRawDataPacket();
      dataOut = dataIn;
      for (int i = 0; i < (60 - numOfCh); i++)
      {
        ushort[] filler = GetFillerData(noiseValue);
        int key = emptyChannelsArr[i];
        dataOut.Add(key, filler);
      }
      return dataOut;
    }

    // Запись одного TRawDataPacket в файл
    private void WriteRawDataPack(TRawDataPacket data)
    {
      //try
      //{
        lock (FileLockObject)
        {
          using (BinaryWriter bw = new BinaryWriter(File.Open(fileToSave, FileMode.Append, FileAccess.Write)))
          {
            for (int j = 0; j < data.Values.First().Count(); j++)
            {
              for (int i = 0; i < data.Count; i++)
              {
                bw.Write(data[i][j]);
              }
            }
          }
        }
      //  return true;
      //}
      //catch (Exception e)
      //{
      //  return false;
      //}
    }

    // Возвращает массив с true и false на 60 каналах в зависимости от наличия
    private bool[] ConvertIDArr()
    {
      numOfCh = elArray.Length;
      bool[] IDArr = new bool[60];
      for (int i = 0; i < 60; i++)
      {
        IDArr[i] = false;
      }
      for (int i = 0; i < numOfCh; i++)
      {
        IDArr[elArray[i]] = true;
      }
      return IDArr;
    }

    // Возвращает массив с ID пустых каналов
    private int[] GetEmptyChannels()
    {
      int[] arrOut = new int[60 - numOfCh];
      bool[] fullArr = new bool[60];
      for(int i = 0; i < 60; i++)
      {
        fullArr[i] = true;
      }

      for (int i = 0; i < numOfCh; i++)
      {
        fullArr[elArray[i]] = false;
      }

      int j = 0;
      for (int i = 0; i < 60; i++)
      {
        if(fullArr[i])
        {
          arrOut[j] = i;
          j++;
        }
      }

      return arrOut;
    }

    // Извлекает список используемых каналов из заголовка, возвращает позицию с началом данных
    public int ParseHeader(string path)
    {
      int startOfData = 0;
      using (StreamReader strReader = new StreamReader(path))
      {
        char[] header = new char[11];
        strReader.Read(header, 0, 11);
        int position = 11;
        int CRLF = 2;
        int START_CHANNEL_LIST = 13;

        if (new string(header).CompareTo("MC_DataTool") == 0)
        {
          string s;
          s = strReader.ReadLine();           // Rest of the first line
          position += s.Length + CRLF;
          s = strReader.ReadLine();           // "Version" line
          position += s.Length + CRLF;
          s = strReader.ReadLine();           // "MC_REC file" line
          position += s.Length + CRLF;

          // Sample Rate
          s = strReader.ReadLine();
          position += s.Length + CRLF;

          // ADC Zero Level
          s = strReader.ReadLine();
          position += s.Length + CRLF;

          // Voltage (uV/bit) 
          s = strReader.ReadLine();
          position += s.Length + CRLF;

          // List of Channels
          s = strReader.ReadLine().Substring(START_CHANNEL_LIST);
          position += s.Length + START_CHANNEL_LIST + CRLF;
          string[] elNumbers = s.Split(new string[] { ";El_" }, MAX_CHANNELS, StringSplitOptions.RemoveEmptyEntries);
          elArray = Array.ConvertAll(elNumbers, el => NAME2IDX[Int32.Parse(el)]);

          // End Of Header
          if (strReader.ReadLine() == "EOH")
          {
            startOfData = position + 5;
          }
          else
          {
            throw new Exception("Wrong file format!");
          }
          return startOfData;
        }
        else
        {
          return 0;
        }
      }
    }

    #region NAME/ID matrix

    public const int MAX_CHANNELS = 60;

    // Range of correct electrode names. But some names are invalid within this range (19, 20, 29, etc.)
    public const int ELECTRODE_FIRST = 12;
    public const int ELECTRODE_LAST = 87;

    public static readonly int[] NAME2IDX = { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,   // 0
                                              -1, -1, 20, 18, 15, 14, 11,  9, -1, -1,   // 1
                                              -1, 23, 21, 19, 16, 13, 10,  8,  6, -1,   // 2
                                              -1, 25, 24, 22, 17, 12,  7,  5,  4, -1,   // 3
                                              -1, 28, 29, 27, 26,  3,  2,  0,  1, -1,   // 4
                                              -1, 31, 30, 32, 33, 56, 57, 59, 58, -1,   // 5
                                              -1, 34, 35, 37, 42, 47, 52, 54, 55, -1,   // 6
                                              -1, 36, 38, 40, 43, 46, 49, 51, 53, -1,   // 7
                                              -1, -1, 39, 41, 44, 45, 48, 50, -1, -1 }; // 8


    public static readonly int[] IDX2NAME = {     47, 48, 46, 45, 38, 37,
                                              28, 36, 27, 17, 26, 16, 35, 25,
                                              15, 14, 24, 34, 13, 23, 12, 22,
                                              33, 21, 32, 31, 44, 43, 41, 42,
                                              52, 51, 53, 54, 61, 62, 71, 63,
                                              72, 82, 73, 83, 64, 74, 84, 85,
                                              75, 65, 86, 76, 87, 77, 66, 78,
                                                  67, 68, 55, 56, 58, 57 };
  }
  #endregion

}