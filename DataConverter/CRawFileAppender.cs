﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading.Tasks;

namespace DataConverter
{
  using TRawDataPacket = Dictionary<int, ushort[]>;

  class CRawFileAppender
  {
    private const int START_CHANNEL_LIST = 13;
    private const int CRLF = 2;                     // Length of CRLF in Windows = 2
    private int startOfData;
    private string[] InputFileNames;
    private string OutputFileName;
    private object FileWriteLock = new object();
    public int Length;
    public static long normK = 10000000000;

    public CRawFileAppender() { }
    public CRawFileAppender(string[] input, string output)
    {
      this.InputFileNames = input;
      this.OutputFileName = output;
      this.Length = LengthCounter(input);
    }

    public bool ReadWrite(ref int current)
    {
      try
      {
        lock (FileWriteLock)
        {
          using (BinaryWriter bw = new BinaryWriter(File.Open(OutputFileName, FileMode.Create, FileAccess.Write)))
          {
            foreach (string inputFileName in InputFileNames)
            {
              // TESTING MODE
              //if (!ParseHeader(inputFileName))
              {
                startOfData = 0;
              }


              using (BinaryReader br = new BinaryReader(File.OpenRead(inputFileName)))
              {
                try
                {
                  br.BaseStream.Position = startOfData;
                  while (true)
                  {
                    ushort tmpData = br.ReadUInt16();
                    bw.Write(tmpData);
                    if ((br.BaseStream.Position % normK) == 0)
                    {
                      current++;
                    }
                  }
                }
                catch (EndOfStreamException)
                { }

              }
            }
          }
        }
        return true;
      }
      catch (Exception e)
      {
        return false;
      }
    }

    private int LengthCounter(string[] fileNames)
    {
      long result = 0;
      foreach (string fileName in fileNames)
      {
        using (BinaryReader br = new BinaryReader(File.OpenRead(fileName)))
        {
          result += br.BaseStream.Length;
        }
      }
      return (int)(result / normK);
    }

    private bool ParseHeader(string fileName)
    {
      startOfData = 0;
      using (StreamReader strReader = new StreamReader(fileName))
      {
        char[] header = new char[11];
        strReader.Read(header, 0, 11);
        int position = 11;

        if (new string(header).CompareTo("MC_DataTool") == 0)
        {
          string s;
          s = strReader.ReadLine();           // Rest of the first line
          position += s.Length + CRLF;
          s = strReader.ReadLine();           // "Version" line
          position += s.Length + CRLF;
          s = strReader.ReadLine();           // "MC_REC file" line
          position += s.Length + CRLF;

          // Sample Rate
          s = strReader.ReadLine();
          position += s.Length + CRLF;

          // ADC Zero Level
          s = strReader.ReadLine();
          position += s.Length + CRLF;

          // Voltage (uV/bit) 
          s = strReader.ReadLine();
          position += s.Length + CRLF;

          // List of Channels
          s = strReader.ReadLine().Substring(START_CHANNEL_LIST);
          position += s.Length + START_CHANNEL_LIST + CRLF;

          // End Of Header
          if (strReader.ReadLine() == "EOH")
          {
            startOfData = position + 5;
          }
          else
          {
            throw new Exception("Wrong file format!");
          }
          return true;
        }
        else
        {
          return false;
        }
      }
    }

  }

  
}
