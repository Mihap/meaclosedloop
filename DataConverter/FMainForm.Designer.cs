﻿namespace DataConverter
{
  partial class FMainForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.ConvertingProgress = new System.Windows.Forms.ProgressBar();
      this.BinToRawConvertButton = new System.Windows.Forms.Button();
      this.Convertions = new System.Windows.Forms.GroupBox();
      this.AppendRawButton = new System.Windows.Forms.Button();
      this.fillEmptyGroupBox = new System.Windows.Forms.GroupBox();
      this.multiSelectCheckBox = new System.Windows.Forms.CheckBox();
      this.fillEmptyButton = new System.Windows.Forms.Button();
      this.label3 = new System.Windows.Forms.Label();
      this.noiseValueTextBox = new System.Windows.Forms.TextBox();
      this.saveFileButton = new System.Windows.Forms.Button();
      this.label2 = new System.Windows.Forms.Label();
      this.saveFileTextBox = new System.Windows.Forms.TextBox();
      this.opnFileButton = new System.Windows.Forms.Button();
      this.label1 = new System.Windows.Forms.Label();
      this.opnFileTextBox = new System.Windows.Forms.TextBox();
      this.currFileLabel = new System.Windows.Forms.Label();
      this.Convertions.SuspendLayout();
      this.fillEmptyGroupBox.SuspendLayout();
      this.SuspendLayout();
      // 
      // ConvertingProgress
      // 
      this.ConvertingProgress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.ConvertingProgress.Location = new System.Drawing.Point(12, 274);
      this.ConvertingProgress.Name = "ConvertingProgress";
      this.ConvertingProgress.Size = new System.Drawing.Size(1027, 23);
      this.ConvertingProgress.TabIndex = 0;
      // 
      // BinToRawConvertButton
      // 
      this.BinToRawConvertButton.Location = new System.Drawing.Point(6, 19);
      this.BinToRawConvertButton.Name = "BinToRawConvertButton";
      this.BinToRawConvertButton.Size = new System.Drawing.Size(183, 23);
      this.BinToRawConvertButton.TabIndex = 1;
      this.BinToRawConvertButton.Text = "Convert Bin To Raw";
      this.BinToRawConvertButton.UseVisualStyleBackColor = true;
      this.BinToRawConvertButton.Click += new System.EventHandler(this.BinToRawConvertButton_Click);
      // 
      // Convertions
      // 
      this.Convertions.Controls.Add(this.AppendRawButton);
      this.Convertions.Controls.Add(this.BinToRawConvertButton);
      this.Convertions.Location = new System.Drawing.Point(12, 13);
      this.Convertions.Name = "Convertions";
      this.Convertions.Size = new System.Drawing.Size(528, 245);
      this.Convertions.TabIndex = 2;
      this.Convertions.TabStop = false;
      this.Convertions.Text = "Convertions";
      // 
      // AppendRawButton
      // 
      this.AppendRawButton.Location = new System.Drawing.Point(6, 71);
      this.AppendRawButton.Name = "AppendRawButton";
      this.AppendRawButton.Size = new System.Drawing.Size(183, 23);
      this.AppendRawButton.TabIndex = 2;
      this.AppendRawButton.Text = "Append Raw Files";
      this.AppendRawButton.UseVisualStyleBackColor = true;
      this.AppendRawButton.Click += new System.EventHandler(this.AppendRawButton_Click);
      // 
      // fillEmptyGroupBox
      // 
      this.fillEmptyGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.fillEmptyGroupBox.Controls.Add(this.multiSelectCheckBox);
      this.fillEmptyGroupBox.Controls.Add(this.fillEmptyButton);
      this.fillEmptyGroupBox.Controls.Add(this.label3);
      this.fillEmptyGroupBox.Controls.Add(this.noiseValueTextBox);
      this.fillEmptyGroupBox.Controls.Add(this.saveFileButton);
      this.fillEmptyGroupBox.Controls.Add(this.label2);
      this.fillEmptyGroupBox.Controls.Add(this.saveFileTextBox);
      this.fillEmptyGroupBox.Controls.Add(this.opnFileButton);
      this.fillEmptyGroupBox.Controls.Add(this.label1);
      this.fillEmptyGroupBox.Controls.Add(this.opnFileTextBox);
      this.fillEmptyGroupBox.Location = new System.Drawing.Point(546, 13);
      this.fillEmptyGroupBox.Name = "fillEmptyGroupBox";
      this.fillEmptyGroupBox.Size = new System.Drawing.Size(483, 245);
      this.fillEmptyGroupBox.TabIndex = 3;
      this.fillEmptyGroupBox.TabStop = false;
      this.fillEmptyGroupBox.Text = "Filling Empty Channels With Noise";
      // 
      // multiSelectCheckBox
      // 
      this.multiSelectCheckBox.AutoSize = true;
      this.multiSelectCheckBox.Location = new System.Drawing.Point(193, 19);
      this.multiSelectCheckBox.Name = "multiSelectCheckBox";
      this.multiSelectCheckBox.Size = new System.Drawing.Size(114, 17);
      this.multiSelectCheckBox.TabIndex = 10;
      this.multiSelectCheckBox.Text = "MultiSelect of Files";
      this.multiSelectCheckBox.UseVisualStyleBackColor = true;
      // 
      // fillEmptyButton
      // 
      this.fillEmptyButton.Location = new System.Drawing.Point(193, 190);
      this.fillEmptyButton.Name = "fillEmptyButton";
      this.fillEmptyButton.Size = new System.Drawing.Size(84, 28);
      this.fillEmptyButton.TabIndex = 8;
      this.fillEmptyButton.Text = "Convert";
      this.fillEmptyButton.UseVisualStyleBackColor = true;
      this.fillEmptyButton.Click += new System.EventHandler(this.fillEmptyButton_Click);
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(6, 136);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(64, 13);
      this.label3.TabIndex = 7;
      this.label3.Text = "Noise Value";
      // 
      // noiseValueTextBox
      // 
      this.noiseValueTextBox.Location = new System.Drawing.Point(76, 133);
      this.noiseValueTextBox.Name = "noiseValueTextBox";
      this.noiseValueTextBox.Size = new System.Drawing.Size(61, 20);
      this.noiseValueTextBox.TabIndex = 6;
      this.noiseValueTextBox.Text = "0";
      // 
      // saveFileButton
      // 
      this.saveFileButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
      this.saveFileButton.Location = new System.Drawing.Point(401, 93);
      this.saveFileButton.Name = "saveFileButton";
      this.saveFileButton.Size = new System.Drawing.Size(75, 23);
      this.saveFileButton.TabIndex = 5;
      this.saveFileButton.Text = "Select";
      this.saveFileButton.UseVisualStyleBackColor = true;
      this.saveFileButton.Click += new System.EventHandler(this.saveFileButton_Click);
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(6, 79);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(163, 13);
      this.label2.TabIndex = 4;
      this.label2.Text = "Path for new Converted Raw File";
      // 
      // saveFileTextBox
      // 
      this.saveFileTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.saveFileTextBox.Location = new System.Drawing.Point(6, 95);
      this.saveFileTextBox.Name = "saveFileTextBox";
      this.saveFileTextBox.ReadOnly = true;
      this.saveFileTextBox.Size = new System.Drawing.Size(387, 20);
      this.saveFileTextBox.TabIndex = 3;
      // 
      // opnFileButton
      // 
      this.opnFileButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
      this.opnFileButton.Location = new System.Drawing.Point(400, 36);
      this.opnFileButton.Name = "opnFileButton";
      this.opnFileButton.Size = new System.Drawing.Size(75, 23);
      this.opnFileButton.TabIndex = 2;
      this.opnFileButton.Text = "Select";
      this.opnFileButton.UseVisualStyleBackColor = true;
      this.opnFileButton.Click += new System.EventHandler(this.opnFileButton_Click);
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(6, 23);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(100, 13);
      this.label1.TabIndex = 1;
      this.label1.Text = "Raw File to Convert";
      // 
      // opnFileTextBox
      // 
      this.opnFileTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.opnFileTextBox.Location = new System.Drawing.Point(6, 39);
      this.opnFileTextBox.Name = "opnFileTextBox";
      this.opnFileTextBox.ReadOnly = true;
      this.opnFileTextBox.Size = new System.Drawing.Size(387, 20);
      this.opnFileTextBox.TabIndex = 0;
      // 
      // currFileLabel
      // 
      this.currFileLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.currFileLabel.AutoSize = true;
      this.currFileLabel.Location = new System.Drawing.Point(543, 261);
      this.currFileLabel.Name = "currFileLabel";
      this.currFileLabel.Size = new System.Drawing.Size(0, 13);
      this.currFileLabel.TabIndex = 11;
      this.currFileLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
      // 
      // FMainForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(1042, 309);
      this.Controls.Add(this.currFileLabel);
      this.Controls.Add(this.fillEmptyGroupBox);
      this.Controls.Add(this.Convertions);
      this.Controls.Add(this.ConvertingProgress);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
      this.Name = "FMainForm";
      this.Text = "Data Converter";
      this.Convertions.ResumeLayout(false);
      this.fillEmptyGroupBox.ResumeLayout(false);
      this.fillEmptyGroupBox.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.ProgressBar ConvertingProgress;
    private System.Windows.Forms.Button BinToRawConvertButton;
    private System.Windows.Forms.GroupBox Convertions;
    private System.Windows.Forms.Button AppendRawButton;
    private System.Windows.Forms.GroupBox fillEmptyGroupBox;
    private System.Windows.Forms.Button opnFileButton;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.TextBox opnFileTextBox;
    private System.Windows.Forms.Button saveFileButton;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.TextBox saveFileTextBox;
    private System.Windows.Forms.Button fillEmptyButton;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.TextBox noiseValueTextBox;
    private System.Windows.Forms.CheckBox multiSelectCheckBox;
    private System.Windows.Forms.Label currFileLabel;
  }
}

