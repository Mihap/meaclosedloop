﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using System.Threading.Tasks;



namespace DataConverter
{
  using TRawDataPacket = Dictionary<int, ushort[]>;
  public partial class FMainForm : Form
  {
    private string[] filesList;

    public FMainForm()
    {
      InitializeComponent();
    }

    private void BinToRawConvertButton_Click(object sender, EventArgs e)
    {
      string Input, Output;
      using (OpenFileDialog dialog = new OpenFileDialog())
      {
        switch (dialog.ShowDialog())
        {
          case System.Windows.Forms.DialogResult.OK:
            break;
          default:
            return;
        }
        Input = dialog.FileName;

      }
      using (SaveFileDialog dialog = new SaveFileDialog())
      {
        switch (dialog.ShowDialog())
        {
          case System.Windows.Forms.DialogResult.OK:
            break;
          default:
            return;
        }
        Output = dialog.FileName;
      }
      MessageBox.Show("You select:\nInput: " + Input + "\nOutput: " + Output);

      Task t = new Task(new Action(() => this.BinToRaw(Input, Output)));
      t.Start();
    }
    private void BinToRaw(string InPath, string OutPath)
    {
      CBinToRawConverter conv = new CBinToRawConverter(InPath, OutPath);
      TRawDataPacket DataPacket = conv.Read();
      int Total = conv.Length;
      int Current = 0;
      ConvertingProgress.BeginInvoke(new Action<int>(x => ConvertingProgress.Maximum = x), Total);

      while (DataPacket != null)
      {
        conv.Write(DataPacket);
        DataPacket = conv.Read();
        Current += conv.FileDuration;
        ConvertingProgress.BeginInvoke(new Action<int>(x => ConvertingProgress.Value = x), Current);
      }
      ConvertingProgress.BeginInvoke(new Action<int>(x => ConvertingProgress.Value = x), Total);
      MessageBox.Show("Success!");
    }

    private void AppendRawButton_Click(object sender, EventArgs e)
    {
      string Output;
      string[] Input;
      using (OpenFileDialog dialog = new OpenFileDialog())
      {
        dialog.Multiselect = true;
        switch (dialog.ShowDialog())
        {
          case System.Windows.Forms.DialogResult.OK:
            break;
          default:
            return;
        }
        Input = dialog.FileNames;
      }

      using (SaveFileDialog dialog = new SaveFileDialog())
      {
        switch (dialog.ShowDialog())
        {
          case System.Windows.Forms.DialogResult.OK:
            break;
          default:
            return;
        }
        Output = dialog.FileName;
      }
      string s = "";
      foreach (string fileName in Input) s += fileName + "\n";
      MessageBox.Show("You select:\nInput: " + s + "Output: " + Output);

      Task t = new Task(new Action(() => this.AppendRawFiles(Input, Output)));
      t.Start();
    }

    private void AppendRawFiles(string[] InPath, string OutPath)
    {
      CRawFileAppender ap = new CRawFileAppender(InPath, OutPath);

      int Total = ap.Length;
      int Current = 0;
      ConvertingProgress.BeginInvoke(new Action<int>(x => ConvertingProgress.Maximum = x), Total);
      ap.ReadWrite(ref Current);

      while (Current < Total - 1)
      {
        ConvertingProgress.BeginInvoke(new Action<int>(x => ConvertingProgress.Value = x), Current);
      }
      ConvertingProgress.BeginInvoke(new Action<int>(x => ConvertingProgress.Value = x), Total);
      MessageBox.Show("Success!");
    }

    private void opnFileButton_Click(object sender, EventArgs e)
    {
      using (OpenFileDialog dialog = new OpenFileDialog())
      {
        if (multiSelectCheckBox.Checked)
        {
          dialog.Multiselect = true;
        }
        switch (dialog.ShowDialog())
        {
          case System.Windows.Forms.DialogResult.OK:
            break;
          default:
            return;
        }
        if (!multiSelectCheckBox.Checked)
        {
          opnFileTextBox.Text = dialog.FileName;
        }
        else
        {
          filesList = dialog.FileNames;
          string s = "";
          foreach (string path in dialog.FileNames)
          {
            s += System.IO.Path.GetFileName(path) + ";";
          }
          opnFileTextBox.Text = s;
        }
      }
    }

    private void saveFileButton_Click(object sender, EventArgs e)
    {
      using (SaveFileDialog dialog = new SaveFileDialog())
      {
        switch (dialog.ShowDialog())
        {
          case System.Windows.Forms.DialogResult.OK:
            break;
          default:
            return;
        }
        saveFileTextBox.Text = dialog.FileName;
      }
    }

    private void fillEmptyButton_Click(object sender, EventArgs e)
    {
      ushort noiseValue;
      if (!UInt16.TryParse(noiseValueTextBox.Text, out noiseValue))
      {
        noiseValue = 0;
      }

      string Output = saveFileTextBox.Text;

      if (!multiSelectCheckBox.Checked)
      {
        string Input = opnFileTextBox.Text;
        CNoiseFiller nf = new CNoiseFiller(Input, Output);

        Task t = new Task(new Action(() => nf.Convert(noiseValue)));
        t.Start();

        while (nf.fullProgressValue == 0) { }
        ConvertingProgress.Maximum = nf.fullProgressValue;

        Task t2 = new Task(new Action(() => progressBarAct(nf, false)));
        t2.Start();
      }
      else
      {
        CNoiseFiller nf = new CNoiseFiller(filesList, Output);

        Task t = new Task(new Action(() => nf.ConvertFilesList(noiseValue)));
        t.Start();

        Task t2 = new Task(new Action(() => progressBarAct(nf, true, filesList.Length)));
        t2.Start();
      }

    }

    void progressBarAct(CNoiseFiller nf, bool multiselect, int numOfFiles = 0)
    {
      if (!multiselect)
      {
        while (nf.progress < nf.fullProgressValue - 1)
        {
          ConvertingProgress.BeginInvoke(new Action<int>(x => ConvertingProgress.Value = x), nf.progress);
          Thread.Sleep(100);
        }
        ConvertingProgress.BeginInvoke(new Action<int>(x => ConvertingProgress.Value = x), nf.fullProgressValue);     
      }
      else
      {
        int currFileNum = 1;

        while (currFileNum <= numOfFiles)
        {
          Thread.Sleep(100);
          while (nf.fullProgressValue == 0) { Thread.Sleep(100); }
          ConvertingProgress.BeginInvoke(new Action<int>(x => ConvertingProgress.Maximum = x), nf.fullProgressValue);

          string s = "Current file converting: " + nf.currentFile + "( " + currFileNum++ + "/" + numOfFiles + " )";
          currFileLabel.BeginInvoke(new Action<string>(x => currFileLabel.Text = x), s);

          while (nf.progress < nf.fullProgressValue - 1)
          {
            Thread.Sleep(100);
            ConvertingProgress.BeginInvoke(new Action<int>(x => ConvertingProgress.Value = x), nf.progress);
          }

          ConvertingProgress.BeginInvoke(new Action<int>(x => ConvertingProgress.Value = x), nf.fullProgressValue);
          nf.fullProgressValue = 0;
        }
      }
      MessageBox.Show("Success!");
    }
  }

}
