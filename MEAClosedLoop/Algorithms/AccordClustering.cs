﻿using Accord.MachineLearning;

using Accord.Math.Distances;
using Accord.Statistics.Distributions.DensityKernels;
using MEAClosedLoop.UIForms;
using System.Collections.Generic;

namespace MEAClosedLoop.Algorithms
{
  internal class AccordClustering
  {
    internal static KMeansClusterCollection kmeans(double[][] inputs, int k)
    {
      // Create a k-Means algorithm
      var kmeans = new KMeans(k)
      {
        //Distance = new CrossCorrelation(),
        //MaxIterations = 10000,
      };

      var model = kmeans.Learn(inputs);
      return model;
    }

    internal static KMeansClusterCollection KMeansByPearsonCorr(double[][] inputs, int k)
    {
      // Create a k-Means algorithm
      var kmeans = new KMeans(k)
      {
        Distance = new MyPearsonCorrelation(),
        MaxIterations = 10000
      };

      var model = kmeans.Learn(inputs);
      return model;
    }

    internal static KMeansClusterCollection binarySplit(double[][] inputs, int k)
    {
      // Create a binary-split algorithm
      var binarySplit = new BinarySplit(k)
      {
        //Distance = new CrossCorrelation(),
        //MaxIterations = 1000
      };

      var model = binarySplit.Learn(inputs);
      return model;
    }

    internal static MeanShiftClusterCollection meanShift(double[][] inputs)
    {
      // Create a mean-shfit algorithm
      var kmeans = new MeanShift()
      {
        Bandwidth = 0.1,
        Kernel = new EpanechnikovKernel(),
        //Distance = new CrossCorrelation(),
        //MaxIterations = 10000
      };

      var model = kmeans.Learn(inputs);
      return model;
    }

    private class CrossCorrelation : IDistance, IMetric<double[]>
    {
      public double Distance(double[] x, double[] y)
      {
        var corr = CorrelationCounter.crossCorrelation(x, y,false, true);
        return corr;
      }
    }

    private class MyPearsonCorrelation : IDistance, IMetric<double[]>
    {
      public double Distance(double[] x, double[] y)
      {
        var corr = new PearsonCorrelation().Similarity(x, y);
        return corr;
      }
    }
  }


}
