﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;

namespace MEAClosedLoop.Algorithms
{
  class CorrelationBitmap
  {

    static List<Color> interpolateColors(List<Color> stopColors, int count)
    {
      SortedDictionary<float, Color> gradient = new SortedDictionary<float, Color>();
      for (int i = 0; i < stopColors.Count; i++)
        gradient.Add(1f * i / (stopColors.Count - 1), stopColors[i]);
      List<Color> ColorList = new List<Color>();

      using (Bitmap bmp = new Bitmap(count, 1))
      using (Graphics G = Graphics.FromImage(bmp))
      {
        Rectangle bmpCRect = new Rectangle(Point.Empty, bmp.Size);
        LinearGradientBrush br = new LinearGradientBrush
                                (bmpCRect, Color.Empty, Color.Empty, 0, false);
        ColorBlend cb = new ColorBlend();
        cb.Positions = new float[gradient.Count];
        for (int i = 0; i < gradient.Count; i++)
          cb.Positions[i] = gradient.ElementAt(i).Key;
        cb.Colors = gradient.Values.ToArray();
        br.InterpolationColors = cb;
        G.FillRectangle(br, bmpCRect);
        for (int i = 0; i < count; i++) ColorList.Add(bmp.GetPixel(i, 0));
        br.Dispose();
      }
      return ColorList;
    }


    //Метод отрисовки теплокарт. Принимает данные для отрисовки и размер области отрисовки в пикселях 
    public static Bitmap DrawTask(double[,] DrawList, int width, int height, out float r_width, out float r_height)
    {
      int N = DrawList.GetLength(0), M = DrawList.GetLength(0);
      List<List<double>> DrawList2 = new List<List<double>>();
      for (int i = 0; i < N; i++)
      {
        DrawList2.Add(new List<double>(M));
        for(int j = 0; j < M; j++)
        {
          DrawList2[i].Add(DrawList[i, j]);
        }
      }
      return CorrelationBitmap.DrawTask(DrawList2, width, height, out r_width, out r_height);
    }

    public static Bitmap DrawTask(List<List<double>> DrawList, int width, int height, out float r_width, out float r_height)
    {
      r_width = 0;
      r_height = 0;
      if (DrawList.Count <= 0) return null;
      int maxRow = DrawList.Count;
      int maxCol = DrawList.Last().Count;
      float rectwidth = width / (float)maxCol, rectheight = height / (float)maxRow;
      r_width = rectwidth;
      r_height = rectheight;
      List<List<double>> newlist = new List<List<double>>();
      for (int i = 0; i < DrawList.Count; i++)
        if (DrawList[i].Count < maxCol && DrawList[i].Count > 1) maxCol = DrawList[i].Count;

      if (maxCol == 0) return null;
      double factor = 999;
      double minVal = Double.MaxValue;
      double maxVal = Double.MinValue;
      for (int i = 0; i < DrawList.Count; i++)
        for (int j = 0; j < DrawList[i].Count; j++)
        {
          if (DrawList[i][j] < minVal)
            minVal = DrawList[i][j];
          if (DrawList[i][j] > maxVal)
            maxVal = DrawList[i][j];
        }

      if (minVal < 0)
      {
        for (int i = 0; i < DrawList.Count; i++)
        {
          List<double> row = new List<double>();
          for (int j = 0; j < DrawList[i].Count; j++)
            row.Add(DrawList[i][j] + Math.Abs(minVal));
          newlist.Add(row);
        }
        maxVal += Math.Abs(minVal);

        minVal = 0;
        DrawList = newlist;

      }
      List<Color> baseColors = new List<Color>();  // create a color list
      baseColors.Add(Color.RoyalBlue);
      baseColors.Add(Color.LightSkyBlue);
      baseColors.Add(Color.LightGreen);
      baseColors.Add(Color.Yellow);
      baseColors.Add(Color.Orange);
      baseColors.Add(Color.Red);

      List<Color> colors = interpolateColors(baseColors, 10000);
      //Леша!!!!
      Bitmap bmp = new Bitmap(width, height);
      using (Graphics graph = Graphics.FromImage(bmp))
      {
        for (int i = 0; i < DrawList.Count; i++)
        {
          for (int j = 0; j < DrawList[i].Count && j < maxCol; j++)
          {
            int br_num = (int)(Math.Abs((DrawList[i][j] - minVal) * 10 / (maxVal - minVal)) * factor);
            if (br_num < 0 || br_num >= colors.Count())
              br_num = 0;
            Brush brush = new SolidBrush(colors[br_num]);
            Pen pen = new Pen(brush, 5);
            graph.FillRectangle(brush, j * rectwidth, i * rectheight, rectwidth, rectheight);
          }
        }
      }
      return bmp;
    }

  }
}
