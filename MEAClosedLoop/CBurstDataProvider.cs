﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Runtime.Serialization.Formatters.Binary;
using MEAClosedLoop.UIForms.SubForms;
namespace MEAClosedLoop
{
  static class CBurstDataProvider
  {
    public static string defaultPath = Application.StartupPath + "//" + Resources.BurstSavingFileName;
    public static Dictionary<ulong, CBurstContainer> BurstAdrCollection = new Dictionary<ulong, CBurstContainer>();
    private static List<ulong> EvokedBurstCollection;
    private static object FileWriteLock = new object();
    private static bool IsInited = false;
    private static object FileReadLock = new object();
    public static void RegBurstAsEvoked(ulong id)
    {
      BurstAdrCollection[id].IsEvoked = true;
    }

    public static bool CheckStore(string filePath = "")
    {
      if (filePath.Length == 0)
        filePath = defaultPath;
      if (!File.Exists(filePath)) return false;

      lock (FileWriteLock)
      {
        using (BinaryReader br = new BinaryReader(File.Open(filePath, FileMode.Open, FileAccess.Read)))
        {
          //if (view != null)
          //{
          //  //FBurstLoading form = new FBurstLoading();
          //  view.Init();
          //  //view.Show();
          //}
          br.BaseStream.Position = 2;
          int foundbursts = 0;
          int percentposition = 0;

          try
          {
            int FileDuration = br.ReadInt32();
            byte[] binData = br.ReadBytes(FileDuration);
            CBurst DataPacket = BinaryBurstToBurst(binData);
            if (DataPacket != null) return true;
            // DataPacket.start += startPosition;
            // AddBurst(DataPacket);
            foundbursts++;
            percentposition = (int)(br.BaseStream.Position * 100 / br.BaseStream.Length);
            //if (view != null)
            //  view.UpdateProgress(percentposition, foundbursts, FilePath);
          }
          catch (EndOfStreamException e)
          {
            return false;
          }
          catch (Exception e)
          {
            return false;
          }

          //if (view != null)
          //  view.SafeClose();
        }
      }

      return false;
    }

    public static bool InitializeFromStore(string filePath = "", FBurstLoading view = null)
    {
      if (filePath.Length == 0)
        filePath = defaultPath;
      if (!File.Exists(filePath)) return false;

      lock (FileWriteLock)
      {
        using (BinaryReader br = new BinaryReader(File.Open(filePath, FileMode.Open, FileAccess.Read)))
        {
          if (view != null)
          {
            //FBurstLoading form = new FBurstLoading();
            view.Init();
            //view.Show();
          }
          br.BaseStream.Position = 2;
          int foundbursts = 0;
          int percentposition = 0;
          while (true)
          {
            try
            {
              int FileDuration = br.ReadInt32();
              byte[] binData = br.ReadBytes(FileDuration);
              CBurst Burst = BinaryBurstToBurst(binData);

              ulong start = Burst.Start;
              if (!BurstAdrCollection.ContainsKey(start))
              {
                CBurstContainer cont = new CBurstContainer(br.BaseStream.Position- FileDuration, binData.Length, start, (ulong)Burst.Length);
                BurstAdrCollection.Add(cont.BeginTime, cont);
              }

              foundbursts++;
              percentposition = (int)(br.BaseStream.Position * 100 / br.BaseStream.Length);
              IsInited = true;
              if (view != null)
                view.UpdateProgress(percentposition, foundbursts, filePath);
            }
            catch (EndOfStreamException e)
            {
              return true;
              break;
            }
            catch (Exception e)
            {
            }
          }
          if (view != null)
            view.SafeClose();

        }
      }
      return false;
    }

    public static int AddBurst(CBurst burst)
    {
      if (!IsInited) Init();
      Write(burst);

      return 0;
    }

    public static CBurst GetBurst(ulong ID)
    {
      if (!IsInited) Init();
      if (!BurstAdrCollection.Keys.Contains(ID))
      {
        throw new KeyNotFoundException("this burst ID not exists in fileCollection");
      }
      CBurstContainer cont = BurstAdrCollection[ID];
      lock (FileWriteLock)
      {
        try
        {
          using (BinaryReader br = new BinaryReader(File.Open(defaultPath, FileMode.Open, FileAccess.Read)))
          {
            br.BaseStream.Position = cont.FilePosition;
            byte[] binData = br.ReadBytes((int)cont.FileDuration);
            CBurst Burst = BinaryBurstToBurst(binData);
            return Burst;
          }
        }
        catch (IOException ioe)
        {
          return null;
        }
        catch (Exception e)
        {
          return null;
        }
      }
    }

    private static bool Write(CBurst burst)
    {
      byte[] bindata = BurstToBinary(burst);
      try
      {
        lock (FileWriteLock)
        {
          using (BinaryWriter bw = new BinaryWriter(File.Open(defaultPath, FileMode.Append, FileAccess.Write)))
          {
            bw.Write(bindata.Length);
            ulong start = burst.Start;
            if (!BurstAdrCollection.ContainsKey(start))
            {
              //start++;
              //MessageBox.Show("learn 8=======> () it deep!", "Attenition");

              CBurstContainer cont = new CBurstContainer(bw.BaseStream.Position, bindata.Length, start, (ulong)burst.Length);
              BurstAdrCollection.Add(cont.BeginTime, cont);
              bw.Write(bindata);
            }
          }
        }
        return true;
      }
      catch (Exception e)
      {
        return false;
      }
    }

    private static void Init()
    {

      lock (FileWriteLock)
      {
        using (StreamWriter sw = new StreamWriter(Application.StartupPath + "//" + Resources.BurstSavingFileName))
        {
          sw.WriteLine("");
        }
      }
      IsInited = true;
    }

    public static void AddFrom(string FilePath, FBurstLoading view = null)
    {
      if (FilePath == defaultPath)
        throw new Exception("Cant load from this file");
      ulong startPosition;
      if (BurstAdrCollection.Count == 0)
        startPosition = 0;
      else
        startPosition = BurstAdrCollection.Select(b => b.Key).Max() + 25000 * 600; ///добавляем паузу после последнего

      lock (FileWriteLock)
      {
        using (BinaryReader br = new BinaryReader(File.Open(FilePath, FileMode.Open, FileAccess.Read)))
        {
          if (view != null)
          {
            //FBurstLoading form = new FBurstLoading();
            view.Init();
            //view.Show();
          }
          br.BaseStream.Position = 2;
          int foundbursts = 0;
          int percentposition = 0;
          while (true)
          {
            try
            {
              int FileDuration = br.ReadInt32();
              byte[] binData = br.ReadBytes(FileDuration);
              CBurst DataPacket = BinaryBurstToBurst(binData);
              DataPacket.start += startPosition;
              AddBurst(DataPacket);
              foundbursts++;
              percentposition = (int)(br.BaseStream.Position * 100 / br.BaseStream.Length);
              if (view != null)
                view.UpdateProgress(percentposition, foundbursts, FilePath);
            }
            catch (EndOfStreamException e)
            {
              break;
            }
            catch (Exception e)
            {
            }
          }
          if (view != null)
            view.SafeClose();
        }
      }
    }

    private static System.Byte[] BurstToBinary(CBurst DataPacket)
    {
      byte[] resultarray;

      MemoryStream ms = new MemoryStream();
      BinaryFormatter formatter = new BinaryFormatter();
      formatter.Serialize(ms, DataPacket);
      resultarray = ms.ToArray();
      return resultarray;
    }

    private static CBurst BinaryBurstToBurst(Byte[] data)
    {
      MemoryStream ms = new MemoryStream(data, false);
      BinaryFormatter formatter = new BinaryFormatter();
      CBurst result = (CBurst)formatter.Deserialize(ms);
      return result;
    }

    private static Type BinaryToType<Type>(Byte[] data)
    {
      MemoryStream ms = new MemoryStream(data, false);
      BinaryFormatter formatter = new BinaryFormatter();
      Type result = (Type)formatter.Deserialize(ms);
      return result;
    }

  }

  public class CBurstContainer
  {
    public long FilePosition;
    public long FileDuration;
    public ulong BeginTime;
    public ulong DurationTime;
    public bool IsEvoked;
    public CBurstContainer()
    {
    }
    public CBurstContainer(long filePosition, long fileDuration, ulong beginTime, ulong durationTime = 0, bool isEvoked = false)
    {
      this.FileDuration = fileDuration;
      this.FilePosition = filePosition;
      this.BeginTime = beginTime;
      this.DurationTime = durationTime;
      this.IsEvoked = isEvoked;
    }
    public override int GetHashCode()
    {
      //1 500 000 - 60 sec * 25000 Freq - order per minute
      return (int)(BeginTime % 1500000);
    }
    public override bool Equals(object obj)
    {
      CBurstContainer eql = obj as CBurstContainer;
      if (eql != null && this.FilePosition == eql.FilePosition && this.FileDuration == eql.FileDuration)
      {
        return true;
      }
      return base.Equals(obj);
    }
  }
}
