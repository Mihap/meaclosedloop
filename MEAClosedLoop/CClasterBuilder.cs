﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using MEAClosedLoop;
using MEAClosedLoop.Common;
using MEAClosedLoop.UIForms;
using MEAClosedLoop.Algorithms;

using System.Threading.Tasks;

namespace MEAClosedLoop
{
  [Serializable]
  public class CClasterBuilder
  {
    public List<ulong> BurstIds = new List<ulong>();
    private volatile int passed = 0;
    public double Progress
    {
      get
      {
        return passed / Math.Pow(BurstIds.Count, 2) * 100;
      }
      set
      {

      }
    }

    public CharactType FuncType = CharactType.IntegralFx;
    public int ch = 0;

    public int startIndex, endIndex;
    /// <summary>
    /// Матрица основных значений корреляций
    /// </summary>
    public double[,] MatrixD;
    /// <summary>
    /// Матрица корреляций от перемешанных характеристик пачек
    /// </summary>
    public double[,] MatrixSh;
    /// <summary>
    /// Список пачечных характеристик, по которым строилась матрица
    /// </summary>
    public List<double[]> Descriptions = new List<double[]>();
    /// <summary>
    /// Матрица основных значений характик в альтернативном формате
    /// </summary>
    public List<List<double>> Matrix = new List<List<double>>();

    /// <summary>
    /// Порог отсечения корреляции для автоматического создания кластеров - лежит в пределах от 0 до 1 
    /// </summary>
    public double threshold = 0;
    /// <summary>
    /// 
    /// </summary>
    private CBurstCluster Head = null;
    public List<CBurstCluster> Clasters = new List<CBurstCluster>();
    public List<Tuple<double, double>> histoDataNormal = new List<Tuple<double, double>>();
    public List<Tuple<double, double>> histoDataShuffl = new List<Tuple<double, double>>();

    private void ShuffleList<T>(List<T> list)
    {
      Random rand = new Random();
      for (int i = 0; i < list.Count; i++)
      {
        T tmp = list[i];
        list.RemoveAt(i);
        list.Insert(rand.Next(0, list.Count), tmp);
      }
    }

    public CClasterBuilder()
    {

    }

    public CClasterBuilder(List<ulong> BurstIds, CharactType FuncType, int ChNum, int startIndex = 0, int endIndex = 120)
    {
      this.BurstIds = BurstIds;
      this.FuncType = FuncType;
      this.ch = ChNum;
      this.startIndex = startIndex;
      this.endIndex = endIndex;

    }

    /// <summary>
    /// Метод синхронно производит расчет всех необходимых данных и кластеризацию
    /// </summary>
    public FMultiClusterView AnalyseSynс(bool UpdateGlobal = true, bool CrChCluster = false, bool clearMatrixes = false)
    {
      int N = BurstIds.Count;
      MatrixD = new double[N, N];
      MatrixSh = new double[N, N];
      List<double[]> allCharacts = new List<double[]>();
      List<double[]> allCharactsSfl = new List<double[]>();
      foreach (ulong ID in BurstIds)
      {
        CBurst burst = CBurstDataProvider.GetBurst(ID);
        if (burst == null) throw new InvalidOperationException("can't load Burst");

        burst.BuildDescription(startIndex * Param.MS, endIndex * Param.MS, ch, 40, 460, true, type: FuncType);
        if (FuncType == CharactType.SpikeRate)
          burst.BuildDescription(startIndex * Param.MS, endIndex * Param.MS, ch, 20 * Param.MS, 460, true, type: FuncType);

        //To DO: разобраться со временем так что бы время соовествовало отсчетам в массиве
        if (!CrChCluster)
        {
          double[] Charct = burst.description[(int)ch].ToArray();
          List<double> CharctShflList = burst.description[(int)ch].ToList();
          ShuffleList(CharctShflList);
          double[] CharctShfl = CharctShflList.ToArray();
          allCharacts.Add(Charct);
          allCharactsSfl.Add(CharctShfl);
          passed = allCharacts.Count * allCharacts.Count;
        }
      }

      var clustersNum = 6;
      var data = allCharacts.ToArray();
      int n = data.Max(x => x.Length);

      // Приводим хар функции к одной длине
      for (int i = 0; i < data.Length; i++)
      {
        if (data[i].Length < n)
        {
          var filled = new double[n];
          for (int j = 0; j < data[i].Length; j++)
            filled[j] = data[i][j];

          for (int j = data[i].Length; j < n; j++)
            filled[j] = 0;

          data[i] = filled;
        }
      }
      var model = AccordClustering.kmeans(data, clustersNum);
      //var model = AccordClustering.binarySplit(data, clustersNum);
      //var model = AccordClustering.meanShift(data);
      var labels = model.Decide(data);
      for (int i = 0; i < clustersNum; i++)
      {
        var idsInCluster = labels.Select((x, ind) => new { x, ind })
          .Where(x => x.x == i)
          .Select(x => x.ind);

        var burstIdsInCluster = BurstIds
          .Where((x, index) => idsInCluster.Contains(index)).ToList();
        var cluster = new CBurstCluster(burstIdsInCluster, i);
        cluster.Description = model.Clusters.Single(x => x.Index == i).Centroid;
        //cluster.Description = model.Modes[i];

        Clasters.Add(cluster);
      }


      //return;

      //AccordClustering.binarySplit(data, 5);
      //AccordClustering.meanShift(data);
      Descriptions = allCharacts;
      //посчитаем саму корреляцию
      passed = 0;
      // разобьем на M * M потоков - надо сделать
      int BurstPerTask = 8;
      //количество ячеек в одной партии
      int M = N / BurstPerTask;


      List<Task> Workers = new List<Task>();
      for (int start_N = 0; start_N < N; start_N += BurstPerTask)
      {
        int left_start_N = start_N;
        int left_end = start_N + BurstPerTask;
        Task left = new Task(new Action(() =>
        {
          for (int i = left_start_N; i < N && i < left_end; i++)
            for (int j = 0; j < N; j++)
            {
              MatrixD[i, j] = CorrelationCounter.crossCorrelation(allCharacts[i].ToArray(), allCharacts[j].ToArray(), ZeroShift: FuncType == CharactType.SpikeRate);
              if (MatrixD[i, j] == Double.NaN || (i == 0 && j == 1))
              {
                int x = 0;
              }
              passed++;
            }
        }));
        int right_start_N = start_N;
        int right_end = start_N + BurstPerTask;
        Task right = new Task(new Action(() =>
        {
          for (int i = right_start_N; i < N && i < right_end; i++)
            for (int j = 0; j < N; j++)
              MatrixSh[i, j] = CorrelationCounter.crossCorrelation(allCharactsSfl[i].ToArray(), allCharactsSfl[j].ToArray(), ZeroShift: FuncType == CharactType.SpikeRate);

        }));

        Workers.Add(left);
        Workers.Add(right);
      }
      //left.Start();
      //left.Wait();
      //right.Start();
      //right.Wait();

      //запустим все
      Workers.ForEach(x => x.Start());

      //подождем всех
      Workers.ForEach(x => x.Wait());

      //заполним старый формат для совмесимости
      //BuildListList();

      //выполним кластеризацию
      //BuildClasters();

      //Почему тут запись на диск?!
      //запишем в глобал
      if (UpdateGlobal)
        CClusterProvider.ClasterBuilder = this;
      if (clearMatrixes)
      {
        MatrixD = new double[1, 1];
        MatrixSh = new double[1, 1];
      }
      //BuildHistoData();
      return new FMultiClusterView(clustersNum, this, data);
    }
    /// <summary>
    /// метод выделяет кластеры
    /// </summary>
    public void BuildClasters()
    {

      //данные для гистограмм + трешхолд
      BuildHistoData();
      //дерево кластеров
      this.Head = BuildBinTree();
      //отсечка по трешхолду 
      this.Clasters = GetClusters(Head, threshold);
      //Поиск типичных представителей
      CalcNearest(Clasters);



    }

    /// <summary>
    /// Заполнение массивов с данными распределения количества различных уровней корреляции
    /// </summary>
    private void BuildHistoData()
    {
      double step = 0.025;
      int[] dataNorm = new int[(int)(1 / step)];
      int[] dataShfl = new int[(int)(1 / step)];
      List<double> AllValues = new List<double>();
      int n = 0;

      for (int celli = 0; celli < Descriptions.Count(); celli++)
        for (int cellj = 0; cellj < Descriptions.Count; cellj++)
        {
          //не учитываем корреляцию самих с собой
          if (celli == cellj) continue;
          #region поиск и добавление в нужный интервал
          double x = MatrixD[celli, cellj];
          for (int i = 0; i < dataNorm.Length; i++)
          {
            if (x > step * i && x <= step * (i + 1))
            {
              dataNorm[i]++;
              break;
            }
          }

          double y = MatrixSh[celli, cellj];
          for (int i = 0; i < dataNorm.Length; i++)
          {
            if (y > step * i && y <= step * (i + 1))
            {
              dataShfl[i]++;
              if (celli % 2 == 0 && cellj == 0)
                AllValues.Add(y);
              break;
            }
          }
          #endregion

        }
      histoDataNormal = new List<Tuple<double, double>>();
      histoDataShuffl = new List<Tuple<double, double>>();
      for (int i = 0; i < dataNorm.Length; i++)
      {
        histoDataNormal.Add(new Tuple<double, double>(i * step + step / 2, dataNorm[i]));
        histoDataShuffl.Add(new Tuple<double, double>(i * step + step / 2, dataShfl[i]));
      }
      AllValues.Sort();
      //подсчет трешхолда
      int endid = (int)(0.999 * AllValues.Count()) - 1;
      //if (AllValues.Count > 1)
      //  endid = AllValues.Count - 1;
      //threshold = (1.0  +  AllValues[endid])/2.0;
      threshold = AllValues[endid];
      //for (int i = 0; i < dataNorm.Length; i++)
      //{
      //  threshold = i * step;
      //  int N = dataShfl.Take(i + 1).Sum();
      //  if (N >= 0.95 * dataShfl.Sum()) break;
      //}
      threshold = 1.0 - threshold;
    }

    private void BuildListList()
    {
      int N = MatrixD.GetLength(0), M = MatrixD.GetLength(0);
      Matrix = new List<List<double>>();
      for (int i = 0; i < N; i++)
      {
        Matrix.Add(new List<double>(M));
        for (int j = 0; j < M; j++)
        {
          Matrix[i].Add(MatrixD[i, j]);
        }
      }
    }

    public CBurstCluster BuildBinTree(ClateringType type = ClateringType.WPGMA)
    {
      int N = BurstIds.Count;
      List<List<double>> corrCollection = new List<List<double>>();
      for (int i = 0; i < N; i++)
      {
        corrCollection.Add(new List<double>());
        for (int j = 0; j < N; j++)
        {
          corrCollection[i].Add(MatrixD[i, j]);
        }
      }
      //corrCollection = clonecorrCollection;
      if (corrCollection.Count < 3) return new CBurstCluster(null, null, 0);
      int FreeCount = corrCollection.Count;
      List<CBurstCluster> clasters = new List<CBurstCluster>();
      for (int i = 0; i < corrCollection.Count; i++)
      {
        clasters.Add(new CBurstCluster(null, null, 1, BurstIds[i], Descriptions[i]));
      }
      while (FreeCount > 1)
      {
        double max = double.MinValue;
        int iIndex = -1, jIndex = -1;
        //берем два элемента с максимальной корреляцией
        for (int i = 0; i < corrCollection.Count; i++)
        {
          for (int j = 0; j < i; j++)
          {
            if (corrCollection[i][j] > max)
            {
              max = corrCollection[i][j];
              iIndex = i;
              jIndex = j;
            }
          }
        }
        int leftIndex = (iIndex > jIndex) ? jIndex : iIndex;
        int rightIndex = (iIndex > jIndex) ? iIndex : jIndex;
        //производим слияние столбцов левого и првого элементов в столбец левого
        for (int i = 0; i < corrCollection.Count; i++)
        {
          if (type == ClateringType.WPGMA)
            corrCollection[leftIndex][i] = (corrCollection[iIndex][i] + corrCollection[jIndex][i]) * 0.5;
          else
          {
            double Ti = corrCollection[iIndex][i];
            double Tj = corrCollection[jIndex][i];
            List<CBurstCluster> col = new List<CBurstCluster>();
            CBurstCluster.RollToList(clasters[iIndex], col, false);
            int Di = col.Count;
            CBurstCluster.RollToList(clasters[jIndex], col, false);
            int Dj = col.Count;
            corrCollection[leftIndex][i] = (Ti * Di + Tj * Di) / (Ti + Tj);
          }
        }
        //производим слияние строк верхнего(левого) и нижнего(правого) элементов в строку верхнего(левого)
        for (int i = 0; i < corrCollection.Count; i++)
        {
          corrCollection[i][leftIndex] = (corrCollection[i][iIndex] + corrCollection[i][jIndex]) * 0.5;
          if (type == ClateringType.WPGMA)
            corrCollection[leftIndex][i] = (corrCollection[iIndex][i] + corrCollection[jIndex][i]) * 0.5;
          else
          {
            double Ti = corrCollection[i][iIndex];
            double Tj = corrCollection[i][jIndex];
            List<CBurstCluster> col = new List<CBurstCluster>();
            CBurstCluster.RollToList(clasters[iIndex], col, false);
            int Di = col.Count;
            CBurstCluster.RollToList(clasters[jIndex], col, false);
            int Dj = col.Count;
            corrCollection[i][leftIndex] = (Ti * Di + Tj * Di) / (double)(Di + Dj);
          }
        }
        //удаляем  строки и столбцы дальнего (правого/нижнего) элемента

        // строка
        corrCollection.RemoveAt(rightIndex);
        // столбцы
        for (int i = 0; i < corrCollection.Count; i++)
        {
          corrCollection[i].RemoveAt(rightIndex);
        }

        // сливаем две ветви кластеров в одну:
        // 1. На месте "левого" кластера в коллекции создаем новый из двух старых 
        // 2. "Правый" кластер удаляем из коллекции
        clasters[leftIndex] = new CBurstCluster(clasters[leftIndex], clasters[rightIndex], 1 - max);
        clasters.RemoveAt(rightIndex);

        // Таким образом изменения в таблице расстрояний/корреляций соответствуют изменениям в линейном массиве кластеров

        FreeCount--;
      }
      // Отметим старший кластер
      clasters[0].IsMainHead = true;

      return clasters[0];

    }

    public static List<CBurstCluster> GetClusters(CBurstCluster head, double maxHeight)
    {
      List<CBurstCluster> result = new List<CBurstCluster>();
      GetClustersFor(maxHeight, head, result);

      return result;
    }

    private static void GetClustersFor(double maxHeight, CBurstCluster head, List<CBurstCluster> result)
    {
      if (head.Height < maxHeight) result.Add(head);
      else if (head.innerLeafsCount > 0)
      {
        GetClustersFor(maxHeight, head.innerLeftCluster, result);
        GetClustersFor(maxHeight, head.innerRightCluster, result);
      }
    }

    public void CalcNearest(List<CBurstCluster> Clasters)
    {
      foreach (CBurstCluster claster in Clasters)
      {
        // 1 для каждого кластера найдем всех детей.
        List<CBurstCluster> childs = new List<CBurstCluster>();
        CBurstCluster.RollToList(claster, childs, true);
        // 2 для всех детей получим их индекс в общей таблице корреляций

        childs = childs.OrderBy(x => x.ID).ToList();
        List<ulong> Childs = childs.Select(x => x.ID).ToList();


        List<int> positions = GetPositions(childs.Select(x => x.ID).ToList());
        // 3 среди всех детей найдем того, сумма схожестей которого со всеми максимальна
        double sumMax = double.MinValue;
        double sumMin = double.MaxValue;
        double sum = 0;

        // индекс элемента с максимальной схожестью списке детей  
        int minId = -1;
        for (int i = 0; i < Childs.Count; i++)
        {
          sum = 0;
          for (int j = 0; j < Childs.Count; j++)
          {
            //if (i == j) continue;
            int ii = GetPosition(Childs[i]);
            int jj = GetPosition(Childs[j]);
            sum = sum + MatrixD[ii, jj];
          }
          if (sum > sumMax)
          {
            sumMax = sum;
            minId = i;
          }
        }
        for (int i = 0; i < Childs.Count; i++)
        {
          sum = 0;
          for (int j = 0; j < Childs.Count; j++)
          {
            int ii = GetPosition(Childs[i]);
            int jj = GetPosition(Childs[j]);
            if (MatrixD[ii, jj] < sumMin)
            {
              sumMin = MatrixD[ii, jj];
            }
          }
        }
        claster.MaxDistance = sumMin;
        claster.Description = this.Descriptions[positions[minId]];

      }
    }

    /// <summary>
    /// Возвращает список индексов пачек в глобальной матрице для некоторой выборки
    /// </summary>
    /// <param name="BurstLocalIds"></param>
    /// <returns></returns>
    public List<int> GetPositions(List<ulong> BurstLocalIds)
    {
      List<int> result = new List<int>(BurstLocalIds.Count);
      for (int i = 0; i < BurstLocalIds.Count; i++)
      {
        for (int j = 0; j < BurstIds.Count; j++)
        {
          if (BurstLocalIds[i] == BurstIds[j])
          {
            result.Add(j);
            break;
          }
        }
      }
      return result;
    }

    public int GetPosition(ulong BurstID)
    {
      for (int j = 0; j < BurstIds.Count; j++)
      {
        if (BurstID == BurstIds[j])
          return j;
      }
      return 0;
    }
  }

  public enum ClateringType
  {
    UPGMA,
    WPGMA
  }
}
