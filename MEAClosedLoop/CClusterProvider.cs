﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading.Tasks;

namespace MEAClosedLoop
{
  using TRepresentData = Dictionary<int, System.Double[]>;
  public static class CClusterProvider
  {
    private static object FileWriteLock = new object();
    public static string defaultPath = Application.StartupPath + "//" + Resources.ClusterDataFileName;


    private static CClasterBuilder _clasterBulder;

    /// <summary>
    ///  Содержит в себе текущий кластер, матрицу корреляции и набор пачек по которым строилась кросскорреляция
    /// </summary>
    public static CClasterBuilder ClasterBuilder
    {
      get
      {
          return _clasterBulder;
      }
      set
      {
          _clasterBulder = value;
          Write();
      }
    }

    public static void Export(string Path)
    {
      Write(Path);
    }
    private static void Write()
    {
      Write(defaultPath);
    }
    private static void Write(string Path)
    {
      byte[] bindata = DataToBinary();
      lock (FileWriteLock)
      {
        using (BinaryWriter bw = new BinaryWriter(File.Open(Path, FileMode.Create, FileAccess.Write)))
        {
          try
          {
            bw.Write(bindata);
          }
          catch (Exception e)
          {

          }
        }
      }
    }
    //Загрузка RepresentData из файла
    public static void Import(string Path)
    {
      Load(Path);
    }
    public static void Load()
    {
      Load(defaultPath);
    }
    public static void Load(string Path)
    {
      byte[] bindata;
      lock (FileWriteLock)
      {
        using (BinaryReader br = new BinaryReader(File.Open(Path, FileMode.Open, FileAccess.Read)))
        {
          bindata = br.ReadBytes((int)br.BaseStream.Length);
        }
      }
      BinaryToData(bindata);
    }

    private static System.Byte[] DataToBinary()
    {
      MemoryStream ms = new MemoryStream();
      BinaryFormatter formatter = new BinaryFormatter();
      try
      {
        formatter.Serialize(ms, _clasterBulder);
      }
      catch (Exception e)
      {

      }
      return ms.ToArray();
    }

    private static void BinaryToData(System.Byte[] bindata)
    {
      MemoryStream ms = new MemoryStream(bindata, false);
      BinaryFormatter formatter = new BinaryFormatter();
      _clasterBulder = (CClasterBuilder)formatter.Deserialize(ms);
    }

  }
}
