﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Threading;

namespace MEAClosedLoop
{
  #region Definitions
  using TData = System.Double;
  using TTime = System.UInt64;
  using TStimIndex = System.Int16;
  using TAbsStimIndex = System.UInt64;
  using TRawData = UInt16;
  using TRawDataPacket = Dictionary<int, ushort[]>;
  using TFltDataPacket = Dictionary<int, System.Double[]>;
  using TFltData = System.Double;
  #endregion

  class BurstReciever : IDisposable
  {
    private Task cycle;
    private bool takeNext = true;
    public IRecieveBurst reciever;
    private Queue<CBurst> dataQueue = new Queue<CBurst>();
    private object queueLock = new object();
    
    public BurstReciever()
    {
      cycle = new Task(new Action(() =>
      {
        while (takeNext)
        {
          lock (queueLock)
          {
            if (dataQueue.Count > 0 && reciever != null)
            {
              reciever.RecieveBurst(dataQueue.Dequeue());
            }
            else
            {
              Thread.Sleep(5);
            }
            Thread.Sleep(0);
          }
        }
      }));
      cycle.Start();
    }

    public void AddToPipe(CBurst data)
    {
      lock (queueLock)
      {
        if (dataQueue.Count() >= 10)
          dataQueue.Dequeue();
        dataQueue.Enqueue(data);
      }
    }

    public void Dispose()
    {
      takeNext = false;
      cycle.Wait();
      dataQueue.Clear();
    }
  }

  class FltDataReciever : IDisposable
  {
    private Task cycle;
    private bool takeNext = true;
    public IRecieveFltData reciever;
    private Queue<TFltDataPacket> dataQueue = new Queue<TFltDataPacket>();
    private object queueLock = new object();

    public FltDataReciever()
    {
      cycle = new Task(new Action(() =>
      {
        while (takeNext)
        {
          lock (queueLock)
          {
            if (dataQueue.Count > 0 && reciever != null)
            {
              reciever.RecieveFltData(dataQueue.Dequeue());
            }
            else
            {
              Thread.Sleep(5);
            }
            Thread.Sleep(0);
          }
        }
      }));
      cycle.Start();
    }

    public void AddToPipe(TFltDataPacket data)
    {
      lock (queueLock)
      {
        if (dataQueue.Count() >= 20)
          dataQueue.Dequeue();
        dataQueue.Enqueue(data);
      }
    }

    public void Dispose()
    {
      takeNext = false;
      cycle.Wait();
      dataQueue.Clear();
    }
  }

  class StimReciever : IDisposable
  {
    private Task cycle;
    private bool takeNext = true;
    public IRecieveStim reciever;
    private Queue<List<TAbsStimIndex>> dataQueue = new Queue<List<TAbsStimIndex>>();
    private object queueLock = new object();
    
    public StimReciever()
    {
      cycle = new Task(new Action(() => 
      {
        while (takeNext)
        {
          lock (queueLock)
          {
            if (dataQueue.Count > 0 && reciever != null)
            {
              reciever.RecieveStim(dataQueue.Dequeue());
            }
            else
            {
              Thread.Sleep(5);
            }
            Thread.Sleep(0);
          }
        }
      }));
    }
    public void AddToPipe(List<TAbsStimIndex> data)
    {
      lock (queueLock)
      {
        if (dataQueue.Count() >= 10)
          dataQueue.Dequeue();
        dataQueue.Enqueue(data);
      }
    }

    public void Dispose()
    {
      takeNext = false;
      cycle.Wait();
      dataQueue.Clear();
    }
  }

  class EvokedBurstReciever : IDisposable
  {
    private Task cycle;
    private bool takeNext = true;
    public IRecieveEvokedBurst reciever;
    private Queue<SEvokedPack> dataQueue = new Queue<SEvokedPack>();
    private object queueLock = new object();

    public EvokedBurstReciever()
    {
      cycle = new Task(new Action(() =>
      {
        while (takeNext)
        {
          lock (queueLock)
          {
            if (dataQueue.Count > 0 && reciever != null)
            {
              reciever.RecieveEvokedBurst(dataQueue.Dequeue());
            }
            else
            {
              Thread.Sleep(100);
            }
            Thread.Sleep(10);
          }
        }
      }));
    }
    public void AddToPipe(SEvokedPack data)
    {
      lock (queueLock)
      {
        if (dataQueue.Count() >= 10)
          dataQueue.Dequeue();
        dataQueue.Enqueue(data);
      }
    }

    public void Dispose()
    {
      takeNext = false;
      cycle.Wait();
      dataQueue.Clear();
    }
  }

  public class CDataFlowController
  {

    private List<BurstReciever>  burstRecieverList = new List<BurstReciever>();
    private List<FltDataReciever> fltDataRecieverList = new List<FltDataReciever>();
    private List<StimReciever> stimRecieverList = new List<StimReciever>();
    private List<EvokedBurstReciever> evokedBurstRecieverList = new List<EvokedBurstReciever>();

    private object LockBurstRecievers = new object();
    private object LockFltDataRecievers = new object();
    private object LockStimRecievers = new object();
    private object LockEvBurstRecievers = new object();

    //подписывание интерфейсов на раздачу потоков данных
    public void AddConsumer<ObjType>(ObjType Obj)
    {
      if (Obj is IRecieveBurst && (from r in burstRecieverList where r.reciever.Equals(Obj as IRecieveBurst) select r).Count() == 0)
      {
        lock (LockBurstRecievers)
        {
          BurstReciever reciever = new BurstReciever();
          reciever.reciever = Obj as IRecieveBurst;
          burstRecieverList.Add(reciever);
        }
      }

      if (Obj is IRecieveFltData && (from r in fltDataRecieverList where r.reciever.Equals(Obj as IRecieveFltData) select r).Count() == 0)
      {
        lock (LockFltDataRecievers)
        {
          FltDataReciever reciever = new FltDataReciever();
          reciever.reciever = Obj as IRecieveFltData;
          fltDataRecieverList.Add(reciever);
        }
        //(Obj as IRecieveFltData).OnDisconnect += new OnDisconnectDelegate(CDataFlowController_OnDisconnect);
      }
      
      if (Obj is IRecieveStim && (from r in stimRecieverList where r.reciever.Equals(Obj as IRecieveStim) select r).Count() == 0)
      {
        lock (LockStimRecievers)
        {
          StimReciever reciever = new StimReciever();
          reciever.reciever = Obj as IRecieveStim;
          stimRecieverList.Add(reciever);
        }
      }
      if (Obj is IRecieveEvokedBurst && (from r in evokedBurstRecieverList where r.reciever.Equals(Obj as IRecieveEvokedBurst) select r).Count() == 0)
      {
        lock (LockEvBurstRecievers)
        {
          EvokedBurstReciever reciever = new EvokedBurstReciever();
          reciever.reciever = Obj as IRecieveEvokedBurst;
          evokedBurstRecieverList.Add(reciever);
        }
      }
    }

    void CDataFlowController_OnDisconnect(object obj)
    {
      RemoveConsumer(obj);
    }

    //отписывание интерфейсов на раздачу потоков данных
    public void RemoveConsumer<ObjType>(ObjType Obj)
    {
      if (Obj is IRecieveBurst)
      {
        throw new NotImplementedException("in develop:");
      }
      //if (Obj is IRecieveFltData)                                  
      // TODO: Миша, разберись с удалением подписчиков
      //{
      //  lock (LockFltDataRecievers)
      //  {
      //    FltDataRecievers.Remove(Obj as IRecieveFltData);
      //  }
      //}
      if (Obj is IRecieveStim)
      {
        throw new NotImplementedException("in develop:");
      }
    }

    #region Recieve&Send Data Methods
    public void RecieveBurstData(CBurst Pack)
    {
      lock (LockBurstRecievers)
        foreach (BurstReciever reciever in burstRecieverList)
        {
          reciever.AddToPipe(Pack);
        }
    }
    public void RecieveFltData(TFltDataPacket DataPacket)
    {
      lock (LockFltDataRecievers)
        foreach (FltDataReciever reciever in fltDataRecieverList)
        {
          reciever.AddToPipe(DataPacket);
        }
    }
    
    public void RecieveStim(List<TAbsStimIndex> stims)
    {
      Global.stimsDataProvider.Stims.AddRange(stims);
      lock (LockStimRecievers)
        foreach (StimReciever reciever in stimRecieverList)
        {
          reciever.AddToPipe(stims);
        }
    }
    public void RecieveEvPack(SEvokedPack EvBurst)
    {
      lock(LockEvBurstRecievers)
        foreach (EvokedBurstReciever reciever in evokedBurstRecieverList)
        {
          reciever.AddToPipe(EvBurst);
        }
    }
    #endregion
    
  }
}
