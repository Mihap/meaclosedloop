﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MEAClosedLoop
{
  using TRawDataPacket = Dictionary<int, ushort[]>;
  using TFltDataPacket = Dictionary<int, System.Double[]>;

  public class CDataWriter: IRecieveFltData
  {
    public bool isFltDataWriting = false;
    public bool isRawDataWriting = false;

    void IRecieveFltData.RecieveFltData(TFltDataPacket packet)
    {
      if (isFltDataWriting)
      {
        CFltDataProvider.Add(packet);
      }
    }

    public event OnDisconnectDelegate OnDisconnect;
  }
}
