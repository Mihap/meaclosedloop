﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading.Tasks;

namespace MEAClosedLoop
{
  using TFltDataPacket = Dictionary<int, System.Double[]>;
  using TKvpID = KeyValuePair<int, System.Double[]>;
  using TKvpII = KeyValuePair<int, System.Int16[]>;

  static class CFltDataProvider
  {
    public static string defaultPath = Application.StartupPath + "//" + Resources.FltDataSavingFileName;
    private const int Freq = 25000; // частота
    private static bool IsInited = false;
    private static object FileWriteLock = new object();
    public static Dictionary<ulong, CFltDataContainer> FltDataCollection = new Dictionary<ulong, CFltDataContainer>();
    private static Queue<TFltDataPacket> FltDataPacketQueue = new Queue<TFltDataPacket>();

    public static int AddFltDataPacket(TFltDataPacket data)
    {
      if (!IsInited) Init();
      Write(data);
      return 0;
    }

    public static int Add(TFltDataPacket data)
    {
      if (!IsInited) Init();
      FltDataPacketQueue.Enqueue(data);
      return 0;
    }

    private static void WritingFromQ()
    {
      while (true)
      {
        if (FltDataPacketQueue.Count > 100)
        {
          throw new Exception("More than 100 FltDataPackets in Queue");
        }
        if (FltDataPacketQueue.Count > 0)
        {
          TFltDataPacket data = FltDataPacketQueue.Dequeue();
          Write(data);
        }
      }
    }


    public static TFltDataPacket GetFltData(ulong key)
    {
      if (!IsInited)
      {
        Init();
      }

      try
      {
        if (FltDataCollection.ContainsKey(key))
        {
          CFltDataContainer cont = FltDataCollection[key];
          using (BinaryReader br = new BinaryReader(File.Open(defaultPath, FileMode.Open, FileAccess.Read)))
          {
            br.BaseStream.Position = cont.FilePosition;
            byte[] binData = br.ReadBytes((int)cont.FileDuration);
            TFltDataPacket DataPacket = BinaryToFltData(binData);
            return DataPacket;
          }
        }
        else
        {

        }
        {
          throw new KeyNotFoundException();
        }
      }
      catch (Exception e)
      {
        //DebugForm form = new DebugForm(e.Message + " in GetFltData");
        return null;
      }
    }

    public static TFltDataPacket GetFltData(double time1, double time2)
    {
      if (!IsInited)
      {
        Init();
      }

      ulong key1 = GetKeyByTime(time1);
      ulong key2 = GetKeyByTime(time2);
      if (time1 >= time2) { return null; }

      Dictionary<int, List<double>> dataColl = new Dictionary<int, List<double>>();

      {
        TFltDataPacket dataPacket = GetFltData(key1);
        foreach (int key in dataPacket.Keys)
        {
          dataColl.Add(key, new List<double>());
          dataColl[key].AddRange(dataPacket[key].Skip((int)(FltDataCollection[key1].Begin + FltDataCollection[key1].Duration - time1 * Freq)));
        }
      }


      {
        ulong n = FltDataCollection[key1].Number;
        ulong key = GetKeyByNumber(++n);
        while (key != key2)
        {
          TFltDataPacket dataPacket = new TFltDataPacket();
          dataPacket = GetFltData(key);
          foreach (int k in dataPacket.Keys)
          {
            dataColl[k].AddRange(dataPacket[k]);
          }
          key = GetKeyByNumber(++n);
        }

        {
          TFltDataPacket dataPacket = new TFltDataPacket();
          dataPacket = GetFltData(key);
          foreach (int k in dataPacket.Keys)
          {
            dataColl[k].AddRange(dataPacket[k].Take((int)(time2 * Freq - FltDataCollection[key2].Begin)));
          }
        }
      }
      TFltDataPacket data = new TFltDataPacket();
      foreach (int key in dataColl.Keys)
      {
        data.Add(key, dataColl[key].ToArray());
      }
      return data;
    }


    static ulong GetKeyByTime(double time)
    {
      foreach (KeyValuePair<ulong, CFltDataContainer> kvp in FltDataCollection)
      {
        if ((time * Freq >= kvp.Value.Begin) && (time * Freq < (kvp.Value.Begin + kvp.Value.Duration)))
        {
          return kvp.Key;
        }
      }
      throw new ArgumentOutOfRangeException("Given time hasnt been registred yet.");
    }

    static ulong GetKeyByNumber(ulong number)
    {
      foreach (KeyValuePair<ulong, CFltDataContainer> kvp in FltDataCollection)
      {
        if (kvp.Value.Number == number)
        {
          return kvp.Key;
        }
      }
      throw new ArgumentOutOfRangeException();
    }

    private static bool Write(TFltDataPacket data)
    {
      byte[] bindata = FltDataToBinary(data);
      try
      {
        lock (FileWriteLock)
        {
          using (BinaryWriter bw = new BinaryWriter(File.Open(defaultPath, FileMode.Append, FileAccess.Write)))
          {
            bw.Write(bindata.Length);

            CFltDataContainer cont = new CFltDataContainer(bw.BaseStream.Position, bindata.Length, (ulong)data[data.Keys.First()].Length);
            FltDataCollection.Add(cont.Begin, cont);
            bw.Write(bindata);
          }
        }
        return true;
      }
      catch (Exception e)
      {
        return false;
      }
    }

    private static void Init()
    {
      Task t = new Task(WritingFromQ);
      t.Start();
      lock (FileWriteLock)
      {
        using (StreamWriter sw = new StreamWriter(defaultPath))
        {
          sw.WriteLine("");
        }
      }
      IsInited = true;
    }

    public static System.Byte[] FltDataToBinary(TFltDataPacket DataPacket)
    {
      byte[] resultarray;

      TKvpID[] DataPacketArray = DataPacket.ToArray();
      TKvpII[] ConvertedDataPacket = Array.ConvertAll<TKvpID, TKvpII>(DataPacketArray, new Converter<TKvpID, TKvpII>(KvpIDToKvpII));
      MemoryStream ms = new MemoryStream();
      BinaryFormatter formatter = new BinaryFormatter();
      formatter.Serialize(ms, ConvertedDataPacket);
      resultarray = ms.ToArray();
      return resultarray;
    }

    public static TKvpII KvpIDToKvpII(TKvpID kvpd)
    {

      return new TKvpII(kvpd.Key, Array.ConvertAll<System.Double, System.Int16>(kvpd.Value, new Converter<System.Double, System.Int16>(DoubleToInt)));
    }

    public static System.Int16 DoubleToInt(System.Double x)
    {
      return (System.Int16)x;
    }

    private static TFltDataPacket BinaryToFltData(Byte[] data)
    {
      MemoryStream ms = new MemoryStream(data, false);
      BinaryFormatter formatter = new BinaryFormatter();
      TKvpII[] ConvertedDataPacket = (TKvpII[])formatter.Deserialize(ms);
      TKvpID[] DataPacketArray = Array.ConvertAll<TKvpII, TKvpID>(ConvertedDataPacket, new Converter<TKvpII, TKvpID>(KvpIIToKvpID));
      TFltDataPacket result = DataPacketArray.ToDictionary((keyItem) => keyItem.Key, (valueItem) => valueItem.Value);
      return result;
    }

    public static TKvpID KvpIIToKvpID(TKvpII kvpi)
    {
      return new TKvpID(kvpi.Key, Array.ConvertAll<System.Int16, System.Double>(kvpi.Value, new Converter<System.Int16, System.Double>(IntToDouble)));
    }

    public static System.Double IntToDouble(System.Int16 x)
    {
      return (System.Double)x;
    }

  }

  public class CFltDataContainer
  {
    public ulong Number;
    public long FilePosition;
    public long FileDuration;
    public ulong Begin;
    public ulong Duration;
    private static ulong DurationSum = 0;
    private static ulong MaxNumber = 0;

    public CFltDataContainer()
    {
    }

    public CFltDataContainer(long filePosition, long fileDuration, ulong duration)
    {
      this.FileDuration = fileDuration;
      this.FilePosition = filePosition;
      this.Begin = DurationSum;
      this.Duration = duration;
      DurationSum += duration;
      Number = MaxNumber++;
    }

  }

}

