﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MEAClosedLoop
{
  using System.Threading.Tasks;
  using TFltDataPacket = Dictionary<int, System.Double[]>;
  class CManualPackDetector : IRecieveFltData
  {
    /// <summary>
    /// Список времен. На нечётных индексах начало пачки, на чётных - конец
    /// </summary>
    private List<ulong> timeList = new List<ulong>();
    /// <summary>
    /// Очередь обработки пакетов фильтрованных данных
    /// </summary>
    private Queue<TFltDataPacket> fltDataQueue = new Queue<TFltDataPacket>();
    /// <summary>
    /// Отступ влево от начала пачки, записывающися в данные CBurst. В отсчётах
    /// </summary>
    private const int SHIFT_VALUE = 200;

    public CManualPackDetector(List<ulong> _timeList)
    {
      timeList = _timeList;
    }

    public void Start()
    {
      Task task = new Task(BurstCreating);
      task.Start();
    }

    event OnDisconnectDelegate IDisconnectable.OnDisconnect
    {
      add
      {
        throw new NotImplementedException();
      }

      remove
      {
        throw new NotImplementedException();
      }
    }

    void IRecieveFltData.RecieveFltData(TFltDataPacket pack)
    {
      fltDataQueue.Enqueue(pack);
    }

    void BurstCreating()
    {
      int timeInd = 0;
      timeList.Sort();
      ulong time1 = timeList[timeInd++];
      ulong time2 = timeList[timeInd++];
      ulong currTime = 0;
      TFltDataPacket tmpData = new TFltDataPacket();
      ulong start = 0;
      double[] noise = new double[60];
      for (int i = 0; i < 60; i++)
      {
        double[] d = new double[] { };
        tmpData.Add(i, d);
      }
      while (true)
      {
        if (fltDataQueue.Count > 0)
        {
          TFltDataPacket data = fltDataQueue.Dequeue();
          if (currTime + (ulong)data[0].Length >= time1 - SHIFT_VALUE)
          {
            // Добавление данных пакета, в котором содержится начало пачки
            if (tmpData[0].Length == 0)
            {
              int end;
              if (time1 > currTime + (ulong)data[0].Length)
              {
                end = (int)(time1 - 200 - currTime);
                for (int i = 0; i < 60; i++)
                {
                  noise[i] = data[i].Take(end).Average();
                }
              }
              else
              {
                end = (int)(time1 - currTime);
                for (int i = 0; i < 60; i++)
                {
                  noise[i] = data[i].Take(end).Select(x => Math.Abs(x)).Average();
                }
                end -= 200;
              }
              start = currTime + (ulong)end;
              for (int i = 0; i < 60; i++)
              {
                tmpData[i] = data[i].Skip(end).ToArray();
              }

            }
            // Добавление данных между началом и концом
            else if (time2 >= currTime + (ulong)data[0].Length)
            {
              for (int i = 0; i < 60; i++)
              {
                tmpData[i] = tmpData[i].Concat(data[i]).ToArray();
              }
            }
            // Добавление пакета данных, в котором находится конец пачки
            else
            {
              for (int i = 0; i < 60; i++)
              {
                tmpData[i] = tmpData[i].Concat(data[i].Take((int)(time2 - currTime))).ToArray();
              }
              int length = (int)(time2 - start);

              // Добавим новую пачку в провайдер
              CBurst burst = new CBurst(start, length, tmpData, noise);
              CBurstDataProvider.AddBurst(burst);

              // Обновим все переменные
              if (timeList.Count > timeInd)
              {
                time1 = timeList[timeInd++];
                time2 = timeList[timeInd++];
                tmpData = new TFltDataPacket();
                for (int i = 0; i < 60; i++)
                {
                  double[] d = new double[] { };
                  tmpData.Add(i, d);
                }
              }
            }
          }
          currTime += (ulong)data[0].Length;
        }
      }
    }
  }
}
