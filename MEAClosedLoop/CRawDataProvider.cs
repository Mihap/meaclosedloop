﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading.Tasks;


namespace MEAClosedLoop
{
  using TRawDataPacket = Dictionary<int, ushort[]>;

  public static class CRawDataProvider
  {
    public static string defaultPath = Application.StartupPath + "//" + Resources.RawDataSavingFileName;
    private static object FileWriteLock = new object();
    private static bool IsInited = false;
    private static long StartPosition = 0;
    private static Queue<TRawDataPacket> RawDataPacketQueue = new Queue<TRawDataPacket>();
    private const int DEFAULT_BLOCKSIZE = Param.DAQ_FREQ / 10;



    //private static int zerolevel = 32768;
    //private static string Streams = "El_21;El_31;El_41;El_51;El_61;El_71;El_12;El_22;El_32;El_42;El_52;El_62;El_72;El_82;El_13;El_23;El_33;El_43";


    public static void Init()
    {
      lock (FileWriteLock)
      {
        Task t = new Task(WritingFromQ);
        t.Start();
        using (StreamWriter sw = new StreamWriter(defaultPath))
        {
          sw.WriteLine("");
          // Header
          /* string s = " MC_DataTool binary conversion\n Version 2.6.8\n MC_REC file = D:MC_Rack_Datadata.mcd\n Sample rate = " + Param.DAQ_FREQ.ToString()
             + "\n ADC zero = " + zerolevel.ToString() + "\n El = 0.1136µV/AD\n Streams = " + Streams + "\nEOH";
           sw.Write(s);
           StartPosition = s.Length + 5; */
        }
      }
      IsInited = true;
    }

    public static int Add(TRawDataPacket data)
    {
      if (!IsInited) Init();
      if (Global.dw.isRawDataWriting)
      {
        RawDataPacketQueue.Enqueue(data);
      }
      return 0;
    }

    public static void WritingFromQ()
    {
      while (true)
      {
        if (RawDataPacketQueue.Count > 100)
        {
          throw new Exception("More than 100 RawDataPacks in queue to write");
        }
        if (RawDataPacketQueue.Count > 0)
        {
          TRawDataPacket data = RawDataPacketQueue.Dequeue();
          Write(data);
        }
      }
    }

    private static bool Write(TRawDataPacket data)
    {
      try
      {
        lock (FileWriteLock)
        {
          using (BinaryWriter bw = new BinaryWriter(File.Open(defaultPath, FileMode.Append, FileAccess.Write)))
          {
            for (int j = 0; j < data.Values.First().Count(); j++)
            {
              for (int i = 0; i < data.Count; i++)
              {
                bw.Write(data[i][j]);
              }
            }
          }
        }
        return true;
      }
      catch (Exception e)
      {
        return false;
      }
    }

    public static System.Byte[] RawDataToBinary(TRawDataPacket DataPacket)
    {
      byte[] resultarray;
      MemoryStream ms = new MemoryStream();
      BinaryFormatter formatter = new BinaryFormatter();
      formatter.Serialize(ms, DataPacket);
      resultarray = ms.ToArray();
      return resultarray;
    }
    public static TRawDataPacket BinaryToFltData(Byte[] data)
    {
      MemoryStream ms = new MemoryStream(data, false);
      BinaryFormatter formatter = new BinaryFormatter();
      TRawDataPacket result = (TRawDataPacket)formatter.Deserialize(ms);
      return result;
    }

    // Читает из файла один пакет данных размером в blockSize начиная с StartPosition
    // и перемещает StartPosition к следующему пакету
    // При наличии заголовка в файле работать не будет
    public static TRawDataPacket GetRawDataPacket(int blockSize = DEFAULT_BLOCKSIZE)
    {
      try
      {
        TRawDataPacket data = new TRawDataPacket();
        for (int i = 0; i < 10; i++)
        {
          ushort[] arr = new ushort[blockSize];
          data.Add(i, arr);
        }
        using (BinaryReader br = new BinaryReader(File.Open(defaultPath, FileMode.Open, FileAccess.Read)))
        {
          br.BaseStream.Position = StartPosition;
          lock (br)
          {
            for (int j = 0; j < blockSize; ++j)
            {
              for (int k = 0; k < 10; ++k)
              {
                ushort tmpData = br.ReadUInt16();
                data[k][j] = tmpData;
              }
            }
          }
          StartPosition = br.BaseStream.Position;
        }
        return data;
      }
      catch (Exception e)
      {
        return null;
      }
    }

  }
}
