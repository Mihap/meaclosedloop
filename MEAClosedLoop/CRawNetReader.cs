﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Text;
using System.IO;
using System.Linq;
using System.Threading;
using UsbNetDll;

namespace MEAClosedLoop
{
  using System.Net;
  using System.Net.Sockets;
  using System.Threading.Tasks;
  using System.Windows.Forms;
  using TRawDataPacket = Dictionary<int, ushort[]>;
  class CRawNetReader : IRawDataProvider
  {
    private const int CRLF = 2;                     // Length of CRLF in Windows = 2
    private const int CYCLE_QUEUE_SIZE = 10;
    private const int START_CHANNEL_LIST = 13;
    private const int DEFAULT_SAMPLE_RATE = Param.DAQ_FREQ;
    private const int DEFAULT_ZERO_LEVEL = 32768;
    private const int DEFAULT_BLOCK_SIZE = Param.DAQ_FREQ / 10;
    

    private string m_Addr;
    private int Port;
    private bool m_paused = false;
    private AutoResetEvent waitEOF;

    // Callback functions
    private OnChannelData m_onChannelData;
    private OnError m_onError;

    private int[] m_channelsToRead;
    private int m_blockSize;
    private int m_sampleRate;
    public int SampleRate { get { return m_sampleRate; } }
    private int m_zeroLevel;
    public int ZeroLevel { get { return m_zeroLevel; } }
    private System.Timers.Timer m_readoutTimer = new System.Timers.Timer();

    private TRawDataPacket last = null;

    Task PortListenner;

    // [DEBUG]
    // private long[] log;

    // Public methods ===================================================================================================
    // Constructor ======================================================================================================
    public CRawNetReader(string addr, int Port, OnChannelData onChannelData, OnError onError)
    {
      this.m_Addr = addr;
      this.Port = Port;
      m_onChannelData = onChannelData;
      m_onError = onError;
      m_blockSize = DEFAULT_BLOCK_SIZE;


    }

    // Set Sample Rate ==================================================================================================
    public uint SetSampleRate(int rate)
    {
      return 0;
    }

    // Start Acquisition ================================================================================================
    public void StartDacq()
    {
      PortListenner = new Task(ListenFunction);
      PortListenner.Start();
    }

   public Dictionary<int, ushort[]> ChannelBlock_ReadFramesDictUI16(int cbHandle, int numSamples, out int retSize)
    {
      TRawDataPacket data = last;
        retSize = m_blockSize * 60 * sizeof(ushort);
      return data;
    }

    private void ListenFunction()
    {
      int CbHandle = 0;
      // Буфер для входящих данных
      byte[] bytes = new byte[1021024];
      connectionStart:
      // Соединяемся с удаленным устройством

      // Устанавливаем удаленную точку для сокета
      IPHostEntry ipHost = Dns.GetHostEntry("localhost");
      IPAddress ipAddr = ipHost.AddressList[0];
      IPEndPoint ipEndPoint = new IPEndPoint(ipAddr, Port);

      Socket server = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
       
      // Соединяем сокет с удаленной точкой
      while (true)
      {
        try
        {
          server.Connect(ipEndPoint);
          break;
        }
        catch { continue; }
      }
      while (true)
      {
        try
        {
          // Получаем ответ от сервера
          int bytesRec = server.Receive(bytes);
          TRawDataPacket t = CRawDataProvider.BinaryToFltData(bytes);
          last = t;
          m_onChannelData(CbHandle, t[0].Length);
          server.Send(new byte[1] { 255 });
          Thread.Sleep(1);
        }
        catch(Exception e)
        {
          MessageBox.Show("Ошибка при получении информации:" + e.Message);
          goto connectionStart;
        }
      }
      // Освобождаем сокет
      server.Shutdown(SocketShutdown.Both);
      server.Close();

    }

    public void StopDacq()
    {
    
    }

    public void PauseDacq()
    {
    
    }
    public void ResumeDacq()
    {
    
    }
   
    public void SetSelectedChannelsQueue(bool[] selectedChannels, int queuesize, int threshold, CMcsUsbDacqNet.SampleSize samplesize)
    {
     
    }
    private void UpdateTimer(int blockSize, int sampleRate)
    {
      m_readoutTimer.Interval = 1000 * blockSize / sampleRate;
    }
  }
}
