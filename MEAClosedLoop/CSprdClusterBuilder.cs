﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using MEAClosedLoop.Common;
using MEAClosedLoop.UIForms;
using MEAClosedLoop.Algorithms;

namespace MEAClosedLoop
{
  public class CSprdClusterBuilder
  {
    /// <summary>
    /// Список векторов времени начала пачек по каждому каналу
    /// </summary>
    private List<Vector60> vectorList;

    /// <summary>
    /// Матрица расстояний между векторами по заданной метрике
    /// </summary>
    public double[,] corrMatrix;

    /// <summary>
    /// Делегат, задавающий метод вычисления расстояния между векторами
    /// </summary>
    public delegate double DistanceDelegate(Vector60 v1, Vector60 v2);

    /// <summary>
    /// Порог отсечения корреляции для автоматического создания кластеров - лежит в пределах от 0 до 1 
    /// </summary>
    public double threshold = 0;

    public double[,] shCorrMatrix;

    public CSprdBurstCluster Head = null;
    public List<CSprdBurstCluster> Clusters = new List<CSprdBurstCluster>();
    public List<Tuple<double, double>> histoDataNormal = new List<Tuple<double, double>>();
    public List<Tuple<double, double>> histoDataShuffl = new List<Tuple<double, double>>();
    public List<ulong> BurstIds;
    public int clustNum;
    public Dictionary<int, double> Diff2Dict;
    public Dictionary<int, double> ClustDict;
    public List<int> MaxList;

    public Dictionary<ulong, Vector60> VectorDict = new Dictionary<ulong, Vector60>();

    public CSprdClusterBuilder(List<ulong> bursts)
    {
      BurstIds = bursts;
    }

    public FMultiClusterView Analyse()
    {
      LoadVectorList();
      List<int> emptyChList = GetEmptyChannelsList(0.25);
      RemoveElements(emptyChList);

      vectorList.ForEach(x => x.FillZeroValues());

      BuildCorrMatrix(Vector60.PearsonCorr);
      BuildShuffleCorrMatrix(Vector60.PearsonCorr);

      BuildHistoData();


      //через Accord K-Means
      int clustersNum = 6;
      var data = vectorList.Select(x => x.Elements.Select(y => (double)y.Value)
        .ToArray()).ToArray();
      var model = AccordClustering.kmeans(data, clustersNum);
      var labels = model.Decide(data);
      for (int i = 0; i < clustersNum; i++)
      //for (int i = 0; i < model.Count; i++)
      {
        var idsInCluster = labels.Select((x, ind) => new { x, ind })
          .Where(x => x.x == i)
          .Select(x => x.ind);

        var burstIdsInCluster = BurstIds
          .Where((x, index) => idsInCluster.Contains(index)).ToList();
        var cluster = new CSprdBurstCluster(burstIdsInCluster, i);
        var vector = new Vector60();
        var centroid = model.Clusters.Single(x => x.Index == i).Centroid;
        for (int j = 0; j < vectorList[0].Elements.Count; j++)
          vector.Elements.Add(vectorList[0].Elements.ElementAt(j).Key, (int)centroid[j]);

        cluster.Vector = vector;

        //cluster.Description = model.Modes[i];

        Clusters.Add(cluster);
      }

      // Классический алгоритм
      //this.Head = BuildBinTree();
      //threshold = GetThreshold();
      //this.Clusters = GetClusters(Head, threshold);
      //CalcNearest(Clusters);


      // Запишем в глобал
      CSprdClusterProvider.ClusterBuilder = this;

      return new FMultiClusterView(clustersNum, this, data, labels);
    }

    public void ChangeThreshold(double th, int n)
    {
      this.threshold = th;
      this.clustNum = n;
      this.Clusters = GetClusters(Head, threshold);
      CalcNearest(Clusters);
    }

    public double GetThreshold()
    {
      double result = 0;
      double step = 0.00001;
      CSprdBurstCluster head0 = Head;
      //List<int> clustNum = new List<int>();
      Dictionary<int, double> clustDict = new Dictionary<int, double>();
      for (double th = 0; th < 1; th += step)
      {
        try
        {
          int n = GetClusters(Head, th).Count;
          Head = head0;

          if (!clustDict.ContainsKey(n))
          {
            clustDict.Add(n, th);
          }
        }
        catch (Exception) { }
      }
      //List<double> diff1List = new List<double>();
      Dictionary<int, double> diff1Dict = new Dictionary<int, double>();
      foreach (KeyValuePair<int, double> kvp in clustDict)
      {
        if (clustDict.ContainsKey(kvp.Key + 1))
        {
          double d = (kvp.Value - clustDict[kvp.Key + 1]);
          diff1Dict.Add(kvp.Key, d);
        }
      }
      Dictionary<int, double> diff2Dict = new Dictionary<int, double>();
      foreach (KeyValuePair<int, double> kvp in diff1Dict)
      {
        if (diff1Dict.ContainsKey(kvp.Key + 1))
        {
          double d = (kvp.Value - diff1Dict[kvp.Key + 1]);
          diff2Dict.Add(kvp.Key, d);
        }
      }

      int k = 0;
      double max = 0;
      List<int> locMaxList = new List<int>();
      foreach (KeyValuePair<int, double> kvp in diff2Dict)
      {
        if (diff2Dict.ContainsKey(kvp.Key + 1) && diff2Dict.ContainsKey(kvp.Key - 1))
        {
          if ((diff2Dict[kvp.Key - 1] < kvp.Value) && (kvp.Value > diff2Dict[kvp.Key + 1]))
          {
            locMaxList.Add(kvp.Key);
          }
        }
        else if (diff2Dict.ContainsKey(kvp.Key + 1))
        {
          if ((kvp.Value > diff2Dict[kvp.Key + 1]))
          {
            locMaxList.Add(kvp.Key);
          }
        }
        else if (diff2Dict.ContainsKey(kvp.Key - 1))
        {
          if ((diff2Dict[kvp.Key - 1] < kvp.Value))
          {
            locMaxList.Add(kvp.Key);
          }
        }

        if ((kvp.Value) > max && (kvp.Key < (double)diff2Dict.Count / 2))
        {
          max = (kvp.Value);
          k = kvp.Key;
        }
      }

      result = clustDict[k + 2];

      ClustDict = clustDict;
      Diff2Dict = diff2Dict;
      clustNum = k;
      MaxList = locMaxList;

      return result;
    }

    // Создание векторов по имеющимся пачкам
    private void LoadVectorList()
    {
      VectorDict = new Dictionary<ulong, Vector60>();
      vectorList = new List<Vector60>();
      List<ulong> bursts = new List<ulong>(BurstIds);
      var locker = new object();
      bursts.AsParallel().ForAll(id =>
      {
        CBurst burst = CBurstDataProvider.GetBurst(id);
        Vector60 vector = new Vector60(burst);
        int zeroElCount = vector.Elements.Where(x => x.Value < 50 || x.Value > Vector60.MAX_START * 25).Count();

        // векторы, пачка в которых начинается раньше 2 мс или позже maxstart*25 на 50% каналов выкидываем
        lock (locker)
        {
          if (zeroElCount <= 0.65 * vector.Elements.Count)
          {
            vectorList.Add(vector);
            VectorDict.Add(vector.BurstID, vector);
          }
          else
          {
            BurstIds.Remove(id);
          }
        }

      });
      vectorList = vectorList.OrderBy(x => x.BurstID).ToList();
      VectorDict = VectorDict.OrderBy(x => x.Key).ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
    }

    // Удаление элементов по заданным индексам из всех векторов 
    private void RemoveElements(List<int> elsToRemoveList)
    {
      foreach (Vector60 vect in vectorList)
      {
        vect.RemoveElementsAt(elsToRemoveList);
      }

    }

    /// <summary>
    /// Поиск неподходящих электродов
    /// </summary>
    /// <param name="minBurstCount">Допустимый процент пачек на канале от всех пачек [0;1] </param>
    /// <returns>Cписок каналов с допустимым кол-вом пачек</returns>
    private List<int> GetEmptyChannelsList(double minBurstCount)
    {
      List<int> result = new List<int>();
      List<int> chNoBurstCount = new List<int>();
      for (int i = 0; i < 60; i++)
      {
        chNoBurstCount.Add(0);
      }

      foreach (Vector60 vect in vectorList)
      {
        foreach (int ch in vect.ChWithZeroElList)
        {
          chNoBurstCount[ch]++;
        }
      }
      double burstCount = (1 - minBurstCount) * vectorList.Count;
      for (int i = 0; i < 60; i++)
      {
        if (chNoBurstCount[i] >= burstCount)
        {
          result.Add(i);
        }
      }
      return result;
    }

    /// <summary>
    /// Метод заполняет матрицу расстояний
    /// </summary>
    /// <param name="dist">метод метрики</param>
    private void BuildCorrMatrix(DistanceDelegate dist)
    {
      corrMatrix = new double[vectorList.Count, vectorList.Count];
      for (int i = 0; i < vectorList.Count; i++)
      {
        for (int j = i; j < vectorList.Count; j++)
        {
          corrMatrix[i, j] = (dist(vectorList[i], vectorList[j]) + 1) / 2.0;
          corrMatrix[j, i] = corrMatrix[i, j];
        }
      }


      // Код ниже нужно использовать в случае вычисления расстояний в эвклидовой метрике
      //double max = corrMatrix.Cast<double>().Max();
      //for (int i = 0; i < vectorList.Count; i++)
      //{
      //  for (int j = 0; j < vectorList.Count; j++)
      //  {
      //    corrMatrix[i, j] = corrMatrix[i, j] / max;
      //    corrMatrix[i, j] = 1 - corrMatrix[i, j];
      //  }
      //}
    }

    // Матрица расстояний для векторов с перемешанными данными
    private void BuildShuffleCorrMatrix(DistanceDelegate dist)
    {
      Vector60.MainRand = new Random(Environment.TickCount);
      List<Vector60> shVectList = new List<Vector60>();
      foreach (Vector60 v in vectorList)
      {
        Vector60 shVect = new Vector60(v);
        shVect.Shuffle();
        shVectList.Add(shVect);
      }

      shCorrMatrix = new double[vectorList.Count, vectorList.Count];
      for (int i = 0; i < vectorList.Count; i++)
      {
        for (int j = i; j < vectorList.Count; j++)
        {
          Vector60 shVect1 = shVectList[i];
          Vector60 shVect2 = shVectList[j];
          shCorrMatrix[i, j] = (dist(shVect1, shVect2) + 1.0) / 2.0;
          shCorrMatrix[j, i] = shCorrMatrix[i, j];
        }
      }

      // Код ниже нужно использовать в случае вычисления расстояний в эвклидовой метрике
      //double max = shCorrMatrix.Cast<double>().Max();
      //for (int i = 0; i < vectorList.Count; i++)
      //{
      //  for (int j = 0; j < vectorList.Count; j++)
      //  {
      //    shCorrMatrix[i, j] = shCorrMatrix[i, j] / max;
      //    shCorrMatrix[i, j] = 1 - shCorrMatrix[i, j];
      //  }
      //}
    }

    private void BuildHistoData()
    {
      double step = 0.025;
      int[] dataNorm = new int[(int)(1 / step)];
      int[] dataShfl = new int[(int)(1 / step)];
      List<double> AllValues = new List<double>();
      int n = 0;

      for (int celli = 0; celli < corrMatrix.GetLength(0); celli++)
        for (int cellj = 0; cellj < corrMatrix.GetLength(0); cellj++)
        {
          //не учитываем корреляцию самих с собой
          if (celli == cellj) continue;
          #region поиск и добавление в нужный интервал
          double x = corrMatrix[celli, cellj];
          for (int i = 0; i < dataNorm.Length; i++)
          {
            if (x > step * i && x <= step * (i + 1))
            {
              dataNorm[i]++;
              break;
            }
          }

          double y = shCorrMatrix[celli, cellj];
          for (int i = 0; i < dataNorm.Length; i++)
          {
            if (y > step * i && y <= step * (i + 1))
            {
              dataShfl[i]++;
              if (celli % 2 == 0 && cellj == 0)
                AllValues.Add(y);
              break;
            }
          }
          #endregion

        }
      histoDataNormal = new List<Tuple<double, double>>();
      histoDataShuffl = new List<Tuple<double, double>>();
      for (int i = 0; i < dataNorm.Length; i++)
      {
        histoDataNormal.Add(new Tuple<double, double>(i * step + step / 2, dataNorm[i]));
        histoDataShuffl.Add(new Tuple<double, double>(i * step + step / 2, dataShfl[i]));
      }
      AllValues.Sort();
      //подсчет трешхолда
      int endid = (int)(0.95 * AllValues.Count()) - 1;
      threshold = AllValues[endid];
      //for (int i = 0; i < dataNorm.Length; i++)
      //{
      //  threshold = i * step;
      //  int N = dataShfl.Take(i + 1).Sum();
      //  if (N >= 0.95 * dataShfl.Sum()) break;
      //}
      threshold = 1.0 - threshold;
    }

    public CSprdBurstCluster BuildBinTree(ClateringType type = ClateringType.WPGMA)
    {
      int N = vectorList.Count;
      List<List<double>> corrCollection = new List<List<double>>();
      for (int i = 0; i < N; i++)
      {
        corrCollection.Add(new List<double>());
        for (int j = 0; j < N; j++)
        {
          corrCollection[i].Add(corrMatrix[i, j]);
        }
      }
      //corrCollection = clonecorrCollection;
      if (corrCollection.Count < 3) return new CSprdBurstCluster(null, null, 0);
      int FreeCount = corrCollection.Count;
      List<CSprdBurstCluster> clusterList = new List<CSprdBurstCluster>();
      for (int i = 0; i < corrCollection.Count; i++)
      {
        clusterList.Add(new CSprdBurstCluster(null, null, 1, BurstIds[i], vectorList[i]));
      }
      while (FreeCount > 1)
      {
        double max = double.MinValue;
        int iIndex = -1, jIndex = -1;
        //берем два элемента с максимальной корреляцией
        for (int i = 0; i < corrCollection.Count; i++)
        {
          for (int j = 0; j < i; j++)
          {
            if (corrCollection[i][j] > max)
            {
              max = corrCollection[i][j];
              iIndex = i;
              jIndex = j;
            }
          }
        }
        int leftIndex = (iIndex > jIndex) ? jIndex : iIndex;
        int rightIndex = (iIndex > jIndex) ? iIndex : jIndex;
        //производим слияние столбцов левого и првого элементов в столбец левого
        for (int i = 0; i < corrCollection.Count; i++)
        {
          if (type == ClateringType.WPGMA)
            corrCollection[leftIndex][i] = (corrCollection[iIndex][i] + corrCollection[jIndex][i]) * 0.5;
          else
          {
            double Ti = corrCollection[iIndex][i];
            double Tj = corrCollection[jIndex][i];
            List<CSprdBurstCluster> col = new List<CSprdBurstCluster>();
            CSprdBurstCluster.RollToList(clusterList[iIndex], col, false);
            int Di = col.Count;
            CSprdBurstCluster.RollToList(clusterList[jIndex], col, false);
            int Dj = col.Count;
            corrCollection[leftIndex][i] = (Ti * Di + Tj * Di) / (Ti + Tj);
          }
        }
        //производим слияние строк верхнего(левого) и нижнего(правого) элементов в строку верхнего(левого)
        for (int i = 0; i < corrCollection.Count; i++)
        {
          corrCollection[i][leftIndex] = (corrCollection[i][iIndex] + corrCollection[i][jIndex]) * 0.5;
          if (type == ClateringType.WPGMA)
            corrCollection[leftIndex][i] = (corrCollection[iIndex][i] + corrCollection[jIndex][i]) * 0.5;
          else
          {
            double Ti = corrCollection[i][iIndex];
            double Tj = corrCollection[i][jIndex];
            List<CSprdBurstCluster> col = new List<CSprdBurstCluster>();
            CSprdBurstCluster.RollToList(clusterList[iIndex], col, false);
            int Di = col.Count;
            CSprdBurstCluster.RollToList(clusterList[jIndex], col, false);
            int Dj = col.Count;
            corrCollection[i][leftIndex] = (Ti * Di + Tj * Di) / (double)(Di + Dj);
          }
        }
        //удаляем  строки и столбцы дальнего (правого/нижнего) элемента

        // строка
        corrCollection.RemoveAt(rightIndex);
        // столбцы
        for (int i = 0; i < corrCollection.Count; i++)
        {
          corrCollection[i].RemoveAt(rightIndex);
        }

        // сливаем две ветви кластеров в одну:
        // 1. На месте "левого" кластера в коллекции создаем новый из двух старых 
        // 2. "Правый" кластер удаляем из коллекции
        clusterList[leftIndex] = new CSprdBurstCluster(clusterList[leftIndex], clusterList[rightIndex], 1 - max);
        clusterList.RemoveAt(rightIndex);

        // Таким образом изменения в таблице расстрояний/корреляций соответствуют изменениям в линейном массиве кластеров

        FreeCount--;
      }
      // Отметим старший кластер
      clusterList[0].IsMainHead = true;

      return clusterList[0];

    }

    private static void GetClustersFor(double maxHeight, CSprdBurstCluster head, List<CSprdBurstCluster> result)
    {
      if (head.Height < maxHeight) result.Add(head);
      else if (head.innerLeafsCount > 0)
      {
        GetClustersFor(maxHeight, head.innerLeftCluster, result);
        GetClustersFor(maxHeight, head.innerRightCluster, result);
      }
    }

    public static List<CSprdBurstCluster> GetClusters(CSprdBurstCluster head, double maxHeight)
    {
      List<CSprdBurstCluster> result = new List<CSprdBurstCluster>();
      GetClustersFor(maxHeight, head, result);

      return result;
    }

    public void CalcNearest(List<CSprdBurstCluster> ClusterList)
    {
      foreach (CSprdBurstCluster cluster in ClusterList)
      {
        // 1 для каждого кластера найдем всех детей.
        List<CSprdBurstCluster> childs = new List<CSprdBurstCluster>();
        CSprdBurstCluster.RollToList(cluster, childs, true);
        // 2 для всех детей получим их индекс в общей таблице корреляций
        List<int> positions = GetPositions(childs.Select(x => x.ID).ToList());
        // 3 среди всех детей найдем того, сумма расстояний от которого до всех остальных минимальна
        double sumMin = double.MinValue;
        double sum;
        /// индекс элемента с минимальным расстоянием в списке детей  
        int minId = -1;
        for (int i = 0; i < childs.Count; i++)
        {
          sum = 0;
          for (int j = 0; j < childs.Count; j++)
          {
            sum += corrMatrix[positions[i], positions[j]];
          }
          if (sum > sumMin)
          {
            sumMin = sum;
            minId = i;

          }
        }
        cluster.Vector = this.vectorList[positions[minId]];

      }
    }

    public List<int> GetPositions(List<ulong> BurstLocalIds)
    {
      List<int> result = new List<int>(BurstLocalIds.Count);
      for (int i = 0; i < BurstLocalIds.Count; i++)
      {
        for (int j = 0; j < BurstIds.Count; j++)
        {
          if (BurstLocalIds[i] == BurstIds[j])
          {
            result.Add(j);
            break;
          }
        }
      }
      return result;
    }

    private void ShuffleList<T>(List<T> list)
    {
      Random rand = new Random(Environment.TickCount);
      for (int i = 0; i < list.Count; i++)
      {
        T tmp = list[i];
        list.RemoveAt(i);
        list.Insert(rand.Next(0, list.Count), tmp);
      }
    }
  }
}
