﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MEAClosedLoop
{
  static class CSprdClusterProvider
  {
    private static CSprdClusterBuilder _clasterBuilder;

    public static CSprdClusterBuilder ClusterBuilder
    {
      get
      {
        return _clasterBuilder;
      }

      set
      {
        _clasterBuilder = value;
      }
    }

  }
}
