﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Text;

using MEAClosedLoop.Common;
using MEAClosedLoop.Algorithms;

namespace MEAClosedLoop
{
  public delegate void OnNewClusteredBurstDelegate(ulong BurstID, int Cluster);
  public static class CTimeLineClustering
  {
    private static Task Sorting;
    private static bool DoSorting = true;

    private static int Added = 0;
    private static int Sorted = 0;

    private static CharactType FuncType = CharactType.IntegralFx;
    private static int ch = 0;

    private static int startIndex, endIndex;

    public static event OnNewClusteredBurstDelegate OnNewClusteredBurst;

    public static double Passed
    {
      get
      {
        if (Added == 0) return 100;
        else
          return (Sorted / (double)Added * 100);
      }
      set { }
    }

    private static Queue<ulong> PushedKeys = new Queue<ulong>();
    private static object queuelock = new object();


    /// <summary>
    /// Словарь отсортированных пачек
    /// </summary>
    public static Dictionary<int, List<ulong>> BurstDistribution = new Dictionary<int, List<ulong>>();
    public static List<ulong> UnsortedBursts = new List<ulong>();
   /// <summary>
   /// Словарь дескрипторов
   /// </summary>
    public static Dictionary<ulong, double[]> Descriptors = new Dictionary<ulong, double[]>();
    /// <summary>
    /// Список кластеров
    /// </summary>
    private static List<CBurstCluster> clusters = new List<CBurstCluster>();
    public static void Init()
    {
      Added = 0;
      Sorted = 0;
      BurstDistribution.Clear();
      UnsortedBursts.Clear();
      clusters = CClusterProvider.ClasterBuilder.Clasters;
      for (int i = 0; i < clusters.Count; i++)
      {
        BurstDistribution.Add(i, new List<ulong>());
      }
      FuncType = CClusterProvider.ClasterBuilder.FuncType;
      startIndex = CClusterProvider.ClasterBuilder.startIndex;
      endIndex = CClusterProvider.ClasterBuilder.endIndex;

      Descriptors.Clear();
      Sorting = new Task(SortingFunction);
    }

    public static void AddBurst(ulong id)
    {
      Added++;
      PushedKeys.Enqueue(id);
    }

    public static void AddBurst(List<ulong> ids)
    {
      foreach(ulong id in ids)
      {
        AddBurst(id);
      }
    }

    public static void StartSorting()
    {
      if (Sorting != null && Sorting.Status != TaskStatus.Running)
      {
        Sorting = new Task(SortingFunction);
        DoSorting = true;
        Sorting.Start();
      }
    }

    public static void StopSorting()
    {
      if (Sorting != null && Sorting.Status == TaskStatus.Running)
      {
        DoSorting = false;
        Sorting.Wait();
      }
    }

    private static void SortingFunction()
    {
      while (DoSorting)
      {
        ulong id;

        //если очередь пуста - ждем, не грузим процессор
        if (PushedKeys.Count == 0)
        {
          Thread.Sleep(300);
          continue;
        }
        // берем пачку и смотрим к какому кластеру она ближе всего
        id = PushedKeys.Dequeue();

        int trycount = 1;
        //1. Посчитаем характеристики.
        CBurst Burst = null;
        // костыль на тот случай, если сюда пачка пришла раньше чем в провайдер, поэтому просто тупо ждем
        while (trycount < 10)
        {
          if (CBurstDataProvider.BurstAdrCollection.Keys.Contains(id))
          {
            Burst = CBurstDataProvider.GetBurst(id);
            break;
          }
          else
          {
            Thread.Sleep(300 * trycount);
            trycount++;
          }
        
        }
        if (trycount == 10 || Burst == null)
          continue;
        //  throw new Exception("burst key nit found in provider");
        Burst.BuildDescription(startIndex * Param.MS, endIndex * Param.MS, ch, 40, 460, true, type: FuncType);
        double[] data = Burst.description[ch];
        //1.1 Сохраним дескриптор в словарб
        if(!Descriptors.Keys.Contains(id))
        Descriptors.Add(id, data);
        //2. Посчитаем корреляцию со всеми в поисках максимальной
        double max = double.MinValue;
        int maxid = -1;
        if(Burst.Start == 200226)
        {

        }
        for (int i = 0; i < clusters.Count; i++)
        {
          double corr = CorrelationCounter.crossCorrelation(clusters[i].Description, data);
          if(corr > max)
          {
            maxid = i;
            max = corr;
          }
        }
        //3. Добавим к самому близкому кластеру если расстояние до него меньше порога
        // в противном случае в неотсортированные
       if (max >= clusters[maxid].MaxDistance)
        {
          BurstDistribution[maxid].Add(id);
        }
        else
          UnsortedBursts.Add(id);
        Sorted++;
        if (OnNewClusteredBurst != null) OnNewClusteredBurst(id, maxid);
        //OnNewClusteredBurst?.Invoke(id);
        //Осввободим процессор потока
        Thread.Sleep(1);
      }
    }
  }
}
