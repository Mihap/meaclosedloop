﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Runtime.Serialization.Formatters.Binary;

namespace MEAClosedLoop
{
  using TTime = System.UInt64;
  using TData = System.Double;
  using TFltDataPacket = Dictionary<int, System.Double[]>;
  using TSpikesBinary = Dictionary<int, bool[]>;
  using TAbsStimIndex = System.UInt64;

  // Pack events can be of two types:
  // S: Start, EOP = false; T: Stop, EOP = true
  // Length > 0 indicates T type of a pack event
  // In this case Start time means time of the pack end.
  // Data contains Param.PRE_SPIKE samples before the first spike and Param.POST_SPIKE samples after the EOP
  // But Start points the real start of the pack, i.e. the absolute time of the first spike in the pack
  [Serializable]
  public class CBurst
  {

    public TTime start;
    private Int32 length;
    private TData[] noiseLevel;
    private Dictionary<int, int[]> intData;

    [NonSerialized]
    private TFltDataPacket data;

    public TTime Start { get { return start; } }
    public TData[] NoiseLevel { get { return noiseLevel; } }
    public Int32 Length { get { return length; } set { length = value; } }
    public TFltDataPacket Data
    {
      get
      {
        if(data == null)
        {
          data = new TFltDataPacket();
          foreach (var kvp in intData)
          {
            double[] newArr = kvp.Value.Select(d => (double)d).ToArray();
            data.Add(kvp.Key, newArr);
          }
          intData = null;
          return data;
        }
        else
        {
          return data;
        }
      }
    }
    public bool EOP { get { return length != 0; } }
    public TFltDataPacket description;

    public CBurst(TTime _start, Int32 _length, TFltDataPacket _data = null, TData[] _noiseLevel = null)
    {
      start = _start;
      length = _length;
      if (_data != null)
      {
        intData = new Dictionary<int, int[]>();
        foreach (var kvp in _data)
        {
          intData.Add(kvp.Key, kvp.Value.Select(d => (int)d).ToArray());
        }
      }

      noiseLevel = _noiseLevel;
      description = null;
    }
    /// <summary>
    /// Заполняет внутренний масив данными описательной функции
    /// по формуле f[x] = f[x-1] + U[x] / 30 - f[x-1]/Decrease  
    /// </summary>
    /// <param name="windowWidth"> Длина окна сглаживания</param>
    /// <param name="Decrease"> Обратная скорость убывания</param>
    public void BuildDescription(int windowWidth = 140, int Decrease = 460, bool normalise = true, CharactType type = CharactType.IntegralFx, int chNum = -1)
    {
      BuildDescription(0, Data[0].Length, chNum, windowWidth, Decrease, normalise, type);
    }
    public void BuildDescription(int startIndex, int endIndex, int chNum = -1, int windowWidth = 140, int Decrease = 460, bool normalise = true, CharactType type = CharactType.IntegralFx)
    {
      /// DEBUG!!!
      /// 
      normalise = false;


      Stopwatch w = new Stopwatch();
      w.Start();
      int window = windowWidth;
      description = new TFltDataPacket();
      if (Data.Keys.Count == 0) return;

      if (type == CharactType.SpikeRate)
      {
        //startIndex += Param.PRE_SPIKE;
        //endIndex += Param.PRE_SPIKE;

        foreach (int key in Data.Keys)
        {
          #region Для одного канала
          if (chNum != -1 && key != chNum) continue;

          Average stat = new Average();
          // считаем среднее 
          for (int i = 0; i < 100; i++)
          {
            //if (Data[key][i] > 0)
              stat.AddValueElem(Data[key][i]);
          }
          stat.Calc();


          double last;
          if (endIndex >= Data[key].Length)
          {
            endIndex = Data[key].Length;
          }
          if (startIndex + 1 > endIndex) throw new ArgumentException("startIndex > endIndex");


          // длина бита 
          int bit_length = windowWidth;
          // число битов
          int n = (endIndex - startIndex) / bit_length;


          double[] fxs = new double[n];

          for (int id = 0; id < n; id++)
          {
            int SpikeRate = 0;
            for (int i = startIndex + id * bit_length; i < startIndex + (id + 1) * bit_length && i < endIndex; i++)
            {
              //if (Data[key][i] > stat.Sigma * 3)
              //  SpikeRate++;
              if (!stat.IsInArea(Data[key][i]))
                SpikeRate++;
            }
            fxs[id] = 100 * SpikeRate / (double)bit_length;
          }
          description.Add(key, fxs);

          #endregion
        }
        return;

        w.Stop();
      }
      foreach (int key in Data.Keys)
      {
        if (chNum != -1 && key != chNum) continue;
        Average stat = new Average();
        for (int i = 0; i < 200; i++)
        {
          if (Data[key][i] > 0) stat.AddValueElem(Data[key][i]);
        }
        stat.Calc();


        double last;

        if (endIndex >= Data[key].Length)
        {
          endIndex = Data[key].Length;
        }

        if (startIndex + 1 > endIndex) throw new ArgumentException("startIndex > endIndex");
        double[] fxs = new double[endIndex - startIndex];
        double[] averages = new double[fxs.Length];
        fxs[0] = Math.Abs(Data[key][startIndex]);
        double NoiseThreshold = this.NoiseLevel.Select(x => Math.Abs(x)).Max() * 0.6;
        for (int i = 1; i < fxs.Length; i++)
        {
          last = fxs[i - 1];
          fxs[i] = (Data[key][i] > stat.Sigma * 3) ? last + Math.Abs(Data[key][i + startIndex]) / 30 - last / 460 : last - last / 460;
          //fxs[i] = (Data[key][i] > stat.Value + stat.Sigma) ? last + Math.Abs(Data[key][i + startIndex]) / 30 - last / 400 : last - last / 200;
          //fxs[i] = (Data[key][i] > NoiseThreshold * 3) ? last + Math.Abs(Data[key][i + startIndex]) / 30 - last / 400 : last - last / 200;
        }

        double average = 0;//stat.Sigma * 4;
        for (int i = 0; i < window; i++)
        {
          averages[i] = average;
        }
        for (int i = window; i < fxs.Length; i++)
        {
          average += (fxs[i] - fxs[i - window]) / (double)window;
          averages[i] = average > 0 ? average : 0;
        }
        if (normalise)
        {
          double min = averages.Min();
          for (int i = 0; i < averages.Length; i++)
          {
            averages[i] -= min;
          }
          double max = averages.Max();
          double k = 1 / max;
          for (int i = 0; i < averages.Length; i++)
          {
            averages[i] *= k;
          }
        }
        // сожмем поток данных
        int CompressRate = 25;

        double[] CompressedAverages = new double[averages.Length / CompressRate];

        for (int i = 0; i < CompressedAverages.Count(); i++)
        {
          CompressedAverages[i] = averages[i * CompressRate];
        }
        switch (type)
        {
          case CharactType.IntegralFx:
            description.Add(key, CompressedAverages);
            break;
          case CharactType.DIntegralFx:
            double[] dfx = new double[CompressedAverages.Length - 1];
            for (int i = 0; i < dfx.Length; i++)
            {
              dfx[i] = CompressedAverages[i] - CompressedAverages[i + 1];
            }
            description.Add(key, dfx);
            break;
          case CharactType.Average:

            break;
        }
      }
      w.Stop();
    }
  }
  public enum CharactType
  {
    IntegralFx,
    DIntegralFx,
    Average,
    SpikeRate
  }
  public struct SEvokedPack
  {
    public CBurst Burst;
    public TAbsStimIndex stim;
  }
}
