﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
namespace MEAClosedLoop.Common
{
  [Serializable]
  public class CBurstCluster
  {
    public List<ulong> innerIDS = new List<ulong>();
    public ulong ID = 0;
    public CBurstCluster innerLeftCluster;
    public CBurstCluster innerRightCluster;
    public double Height;
    public double MaxDistance = Double.MaxValue;
    public PointF Coord;
    public double[] Description;
    public int ClusterNum { get; set; }
    // this is id Of wrapper claster which it exists
    // may be used for coloring, special drawing, any selecting, etc
    private int _BossClusterID = -1;
    public int BossClusterID
    {
      get
      {
        return _BossClusterID;
      }
      set
      {
        _BossClusterID = value;
        // устанавливаем для всех нижних ветвей
        if (innerLeftCluster != null)
          innerLeftCluster.BossClusterID = value;
        if (innerRightCluster != null)
          innerRightCluster.BossClusterID = value;
      }
    }
    public bool IsMainHead = false;

    public CBurstCluster(List<ulong> ids, int clusterNum)
    {
      innerIDS = ids;
      ClusterNum = clusterNum;
    }

    public CBurstCluster()
    {
      innerIDS = new List<ulong>();
      innerLeftCluster = null;
      innerRightCluster = null;
    }

    public CBurstCluster(CBurstCluster left, CBurstCluster right)
    {
      innerIDS.Clear();
      if (left != null)
        innerIDS.AddRange(left.innerIDS.Where(x => x > 0));
      if (right != null)
        innerIDS.AddRange(right.innerIDS.Where(x => x > 0));

      if (left != null && left.ID > 0) innerIDS.Add(left.ID);
      if (right != null && right.ID > 0) innerIDS.Add(right.ID);

      innerLeftCluster = left;
      innerRightCluster = right;
    }

    public CBurstCluster(CBurstCluster left, CBurstCluster right, double height)
      : this(left, right)
    {
      Height = height;
    }

    public CBurstCluster(CBurstCluster left, CBurstCluster right, double height, ulong ID)
      : this(left, right)
    {
      this.ID = ID;
    }
    public CBurstCluster(CBurstCluster left, CBurstCluster right, double height, ulong ID, double[] description)
      : this(left, right, height, ID)
    {
      this.Description = description;
    }

    public static List<CBurstCluster>[] BuildRoutesToTop(CBurstCluster head)
    {
      List<CBurstCluster> allClusters = new List<CBurstCluster>();
      List<CBurstCluster> leafClusters = new List<CBurstCluster>();
      CBurstCluster.RollToList(head, allClusters);
      //выделим листья из общей коллекции
      CBurstCluster.RollToList(head, leafClusters, true);
      for (int i = 0; i < leafClusters.Count; i++)
      {
        //поставим каждый лист на ось X
        leafClusters[i].Coord = new PointF(i, (float)leafClusters[i].Height);
      }
      List<CBurstCluster>[] result = new List<CBurstCluster>[leafClusters.Count()];

      // из нижних листов по ветвям будем собирать маршруты
      for (int i = 0; i < result.Length; i++)
      {
        result[i] = new List<CBurstCluster>();
        CBurstCluster current = leafClusters[i];
        result[i].Add(current);
        while (current != head)
        {
          if ((from c in allClusters where c.innerLeftCluster == current || c.innerRightCluster == current select c).Count() > 0)
          {
            CBurstCluster next = (from c in allClusters where c.innerLeftCluster == current || c.innerRightCluster == current select c).First();

            current = next;
            current.Coord = new PointF(current.innerLeftCluster.Coord.X + 0.5f, (float)current.Height);
            result[i].Add(current);
          }
        }
      }
      allClusters = allClusters.Where(x => x.ID == 0).OrderBy(x => x.Height).ToList();
      for (int i = 0; i < allClusters.Count; i++)
      {
        double x = (allClusters[i].innerLeftCluster.Coord.X + allClusters[i].innerRightCluster.Coord.X) * 0.5;
        allClusters[i].Coord = new PointF((float)x, (float)allClusters[i].Height);
      }
      return result;
    }

    public int innerLeafsCount
    {
      get
      {
        return CBurstCluster.innerleafs(this);
      }
      set { }
    }

    private static int innerleafs(CBurstCluster head)
    {
      int result = 0;
      if (head == null) return 0;
      if (head.ID > 0) return 1;
      if (head.innerLeftCluster != null) result += innerleafs(head.innerLeftCluster);
      if (head.innerRightCluster != null) result += innerleafs(head.innerRightCluster);
      return result;
    }
    public static void RollToList(CBurstCluster head, List<CBurstCluster> collection, bool JustLeaf = false)
    {
      if (JustLeaf == false)
        collection.Add(head);
      else if (head.ID > 0)
        collection.Add(head);

      if (head.innerLeftCluster != null)
        RollToList(head.innerLeftCluster, collection, JustLeaf);
      if (head.innerRightCluster != null)
        RollToList(head.innerRightCluster, collection, JustLeaf);
    }

    public override string ToString()
    {
      return $"№{ClusterNum} ({innerIDS.Count()})";
    }
  }
}
