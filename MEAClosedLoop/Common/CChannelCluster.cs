﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace MEAClosedLoop.Common
{
  class CChannelCluster
  {
    public List<int> innerIDS = new List<int>();
    public int ChID = 0;
    public CChannelCluster innerLeftCluster;
    public CChannelCluster innerRightCluster;
    public double Height;
    public PointF Coord;
    // this is id Of wrapper claster which it exists
    // may be used for coloring, special drawing, any selecting, etc
    private int _BossClusterID = -1;
    public int BossClusterID
    {
      get
      {
        return _BossClusterID;
      }
      set
      {
        _BossClusterID = value;
        // устанавливаем для всех нижних ветвей
        if (innerLeftCluster != null)
          innerLeftCluster.BossClusterID = value;
        if (innerRightCluster != null)
          innerRightCluster.BossClusterID = value;
      }
    }
    public bool IsMainHead = false;

    public CChannelCluster()
    {
      innerIDS = new List<int>();
      innerLeftCluster = null;
      innerRightCluster = null;
    }

    public CChannelCluster(CChannelCluster left, CChannelCluster right)
    {
      innerIDS.Clear();
      if (left != null)
        innerIDS.AddRange(left.innerIDS.Where(x => x > 0));
      if (right != null)
        innerIDS.AddRange(right.innerIDS.Where(x => x > 0));

      if (left != null && left.ChID > 0) innerIDS.Add(left.ChID);
      if (right != null && right.ChID > 0) innerIDS.Add(right.ChID);

      innerLeftCluster = left;
      innerRightCluster = right;
    }

    public CChannelCluster(CChannelCluster left, CChannelCluster right, double height)
      : this(left, right)
    {
      Height = height;
    }

    public CChannelCluster(CChannelCluster left, CChannelCluster right, double height, int ChID)
      : this(left, right)
    {
      this.ChID = ChID;
    }

    public static List<CChannelCluster>[] BuildRoutesToTop(CChannelCluster head)
    {
      List<CChannelCluster> allClusters = new List<CChannelCluster>();
      List<CChannelCluster> leafClusters = new List<CChannelCluster>();
      CChannelCluster.RollToList(head, allClusters);
      //выделим листья из общей коллекции
      CChannelCluster.RollToList(head, leafClusters, true);
      for (int i = 0; i < leafClusters.Count; i++)
      {
        //поставим каждый лист на ось X
        leafClusters[i].Coord = new PointF(i, (float)leafClusters[i].Height);
      }
      List<CChannelCluster>[] result = new List<CChannelCluster>[leafClusters.Count()];

      // из нижних листов по ветвям будем собирать маршруты
      for (int i = 0; i < result.Length; i++)
      {
        result[i] = new List<CChannelCluster>();
        CChannelCluster current = leafClusters[i];
        result[i].Add(current);
        while (current != head)
        {
          if ((from c in allClusters where c.innerLeftCluster == current || c.innerRightCluster == current select c).Count() > 0)
          {
            CChannelCluster next = (from c in allClusters where c.innerLeftCluster == current || c.innerRightCluster == current select c).First();

            current = next;
            current.Coord = new PointF(current.innerLeftCluster.Coord.X + 0.5f, (float)current.Height);
            result[i].Add(current);
          }
        }
      }
      //Уорядочивание кластеров по высоте, у которых ID==0
      allClusters = allClusters.Where(x => x.ChID == 0).OrderBy(x => x.Height).ToList();
      for (int i = 0; i < allClusters.Count; i++)
      {
        double x = (allClusters[i].innerLeftCluster.Coord.X + allClusters[i].innerRightCluster.Coord.X) * 0.5;//IT BROKE HERE
        allClusters[i].Coord = new PointF((float)x, (float)allClusters[i].Height);
      }
      return result;
    }

    public int innerLeafsCount
    {
      get
      {
        return CChannelCluster.innerleafs(this);
      }
      set { }
    }

    private static int innerleafs(CChannelCluster head)
    {
      int result = 0;
      if (head == null) return 0;
      if (head.ChID > 0) return 1;
      if (head.innerLeftCluster != null) result += innerleafs(head.innerLeftCluster);
      if (head.innerRightCluster != null) result += innerleafs(head.innerRightCluster);
      return result;
    }
    public static void RollToList(CChannelCluster head, List<CChannelCluster> collection, bool JustLeaf = false)
    {
      if (JustLeaf == false)
        collection.Add(head);
      else if (head.ChID > 0)
        collection.Add(head);

      if (head.innerLeftCluster != null)
        RollToList(head.innerLeftCluster, collection, JustLeaf);
      if (head.innerRightCluster != null)
        RollToList(head.innerRightCluster, collection, JustLeaf);
    }
  }
}
