﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MEAClosedLoop.Common
{
  class CChannelClusterisation
  {
    /// <summary>
    /// Построение бинарного дерева для поканальной кластеризации
    /// </summary>
    /// <param name="corrCollection"></param> Основная матрица с данными взаимной кластеризации
    /// <param name="type"></param>
    /// <returns></returns>
    public static CChannelCluster BuildBinTree(List<List<double>> corrCollection, List<int> ChIds, ClateringType type = ClateringType.WPGMA)
    {
      // Копируем коллекцию в новую коллекцию
      List<List<double>> clonecorrCollection = new List<List<double>>();
      for (int i = 0; i < corrCollection.Count; i++)
      {
        clonecorrCollection.Add(new List<double>());
        for (int j = 0; j < corrCollection[i].Count; j++)
        {
          clonecorrCollection[i].Add(corrCollection[i][j]);
        }
      }
      corrCollection = clonecorrCollection;

      if (corrCollection.Count < 3) return new CChannelCluster(null, null, 0);
      int FreeCount = corrCollection.Count;
      //Заполняем Clusters пустыми не обработанными значениями
      List<CChannelCluster> clusters = new List<CChannelCluster>();
      for (int i = 0; i < corrCollection.Count; i++)
      {
        clusters.Add(new CChannelCluster(null, null, 1, ChIds[i]));
      }
      while (FreeCount > 1)
      {
        double max = double.MinValue;
        int iIndex = -1, jIndex = -1;
        //ищем два элемента с максимальной корреляцией
        for (int i = 0; i < corrCollection.Count; i++)
        {
          for (int j = 0; j < i; j++)
          {
            if (corrCollection[i][j] > max)
            {
              max = corrCollection[i][j];
              iIndex = i;
              jIndex = j;
            }
          }
        }
        int leftIndex = (iIndex > jIndex) ? jIndex : iIndex;
        int rightIndex = (iIndex > jIndex) ? iIndex : jIndex;
        //производим слияние столбцов левого и првого элементов в столбец левого
        for (int i = 0; i < corrCollection.Count; i++)
        {
          if (type == ClateringType.WPGMA)
            corrCollection[leftIndex][i] = (corrCollection[iIndex][i] + corrCollection[jIndex][i]) * 0.5;
          else
          {
            double Ti = corrCollection[iIndex][i];
            double Tj = corrCollection[jIndex][i];
            List<CChannelCluster> col = new List<CChannelCluster>();
            CChannelCluster.RollToList(clusters[iIndex], col, false);
            int Di = col.Count;
            CChannelCluster.RollToList(clusters[jIndex], col, false);
            int Dj = col.Count;
            corrCollection[leftIndex][i] = (Ti * Di + Tj * Di) / (Ti + Tj);
          }
        }
        //производим слияние строк верхнего(левого) и нижнего(правого) элементов в строку верхнего(левого)
        for (int i = 0; i < corrCollection.Count; i++)
        {
          corrCollection[i][leftIndex] = (corrCollection[i][iIndex] + corrCollection[i][jIndex]) * 0.5;
          if (type == ClateringType.WPGMA)
            corrCollection[leftIndex][i] = (corrCollection[iIndex][i] + corrCollection[jIndex][i]) * 0.5;
          else
          {
            double Ti = corrCollection[i][iIndex];
            double Tj = corrCollection[i][jIndex];
            List<CChannelCluster> col = new List<CChannelCluster>();
            CChannelCluster.RollToList(clusters[iIndex], col, false);
            int Di = col.Count;
            CChannelCluster.RollToList(clusters[jIndex], col, false);
            int Dj = col.Count;
            corrCollection[i][leftIndex] = (Ti * Di + Tj * Di) / (double)(Di + Dj);
          }
        }
        //удаляем  строки и столбцы дальнего (правого/нижнего) элемента

        // строка
        corrCollection.RemoveAt(rightIndex);
        // столбцы
        for (int i = 0; i < corrCollection.Count; i++)
        {
          corrCollection[i].RemoveAt(rightIndex);
        }

        // сливаем две ветви кластеров в одну:
        // 1. На месте "левого" кластера в коллекции создаем новый из двух старых 
        // 2. "Правый" кластер удаляем из коллекции
        clusters[leftIndex] = new CChannelCluster(clusters[leftIndex], clusters[rightIndex], 1 - max);
        clusters.RemoveAt(rightIndex);

        // Таким образом изменения в таблице расстрояний/корреляций соответствуют изменениям в линейном массиве кластеров

        FreeCount--;
      }
      // Отметим старший кластер
      clusters[0].IsMainHead = true;

      return clusters[0];

    }
    public static List<CChannelCluster> GetClusters(CChannelCluster head, double maxHeight)
    {
      List<CChannelCluster> result = new List<CChannelCluster>();
      GetClustersFor(maxHeight, head, result);

      return result;
    }
    private static void GetClustersFor(double maxHeight, CChannelCluster head, List<CChannelCluster> result)
    {
      if (head.Height < maxHeight) result.Add(head);
      else if (head.innerLeafsCount > 0)
      {
        GetClustersFor(maxHeight, head.innerLeftCluster, result);
        GetClustersFor(maxHeight, head.innerRightCluster, result);
      }
    }

    public enum ClateringType
    {
      UPGMA,
      WPGMA
    }
  }
}
