﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace MEAClosedLoop.Common
{
  public class CSprdBurstCluster
  {
    public List<ulong> innerIDS = new List<ulong>();
    public ulong ID { get; set; } = 0;
    public CSprdBurstCluster innerLeftCluster;
    public CSprdBurstCluster innerRightCluster;
    public double Height;
    public PointF Coord;
    public Vector60 Vector;
    // this is id Of wrapper claster which it exists
    // may be used for coloring, special drawing, any selecting, etc
    private int _BossClusterID = -1;
    public int BossClusterID
    {
      get
      {
        return _BossClusterID;
      }
      set
      {
        _BossClusterID = value;
        // устанавливаем для всех нижних ветвей
        if (innerLeftCluster != null)
          innerLeftCluster.BossClusterID = value;
        if (innerRightCluster != null)
          innerRightCluster.BossClusterID = value;
      }
    }
    public bool IsMainHead = false;
    public int ClusterNum { get; set; }

    public CSprdBurstCluster()
    {
      innerIDS = new List<ulong>();
      innerLeftCluster = null;
      innerRightCluster = null;
    }


    public CSprdBurstCluster(List<ulong> burstIdsInCluster, int i)
    {
      innerIDS = burstIdsInCluster;
      ClusterNum = i;
    }

    public CSprdBurstCluster(CSprdBurstCluster left, CSprdBurstCluster right)
    {
      innerIDS.Clear();
      if (left != null)
        innerIDS.AddRange(left.innerIDS.Where(x => x > 0));
      if (right != null)
        innerIDS.AddRange(right.innerIDS.Where(x => x > 0));

      if (left != null && left.ID > 0) innerIDS.Add(left.ID);
      if (right != null && right.ID > 0) innerIDS.Add(right.ID);

      innerLeftCluster = left;
      innerRightCluster = right;
    }

    public CSprdBurstCluster(CSprdBurstCluster left, CSprdBurstCluster right, double height)
      : this(left, right)
    {
      Height = height;
    }

    public CSprdBurstCluster(CSprdBurstCluster left, CSprdBurstCluster right, double height, ulong ID)
      : this(left, right)
    {
      this.ID = ID;
    }
    public CSprdBurstCluster(CSprdBurstCluster left, CSprdBurstCluster right, double height, ulong ID, Vector60 vector)
      : this(left, right, height, ID)
    {
      this.Vector = vector;
    }


    public static List<CSprdBurstCluster>[] BuildRoutesToTop(CSprdBurstCluster head)
    {
      List<CSprdBurstCluster> allClusters = new List<CSprdBurstCluster>();
      List<CSprdBurstCluster> leafClusters = new List<CSprdBurstCluster>();
      CSprdBurstCluster.RollToList(head, allClusters);
      //выделим листья из общей коллекции
      CSprdBurstCluster.RollToList(head, leafClusters, true);
      for (int i = 0; i < leafClusters.Count; i++)
      {
        //поставим каждый лист на ось X
        leafClusters[i].Coord = new PointF(i, (float)leafClusters[i].Height);
      }
      List<CSprdBurstCluster>[] result = new List<CSprdBurstCluster>[leafClusters.Count()];

      // из нижних листов по ветвям будем собирать маршруты
      for (int i = 0; i < result.Length; i++)
      {
        result[i] = new List<CSprdBurstCluster>();
        CSprdBurstCluster current = leafClusters[i];
        result[i].Add(current);
        while (current != head)
        {
          if ((from c in allClusters where c.innerLeftCluster == current || c.innerRightCluster == current select c).Count() > 0)
          {
            CSprdBurstCluster next = (from c in allClusters where c.innerLeftCluster == current || c.innerRightCluster == current select c).First();

            current = next;
            current.Coord = new PointF(current.innerLeftCluster.Coord.X + 0.5f, (float)current.Height);
            result[i].Add(current);
          }
        }
      }
      allClusters = allClusters.Where(x => x.ID == 0).OrderBy(x => x.Height).ToList();
      for (int i = 0; i < allClusters.Count; i++)
      {
        double x = (allClusters[i].innerLeftCluster.Coord.X + allClusters[i].innerRightCluster.Coord.X) * 0.5;
        allClusters[i].Coord = new PointF((float)x, (float)allClusters[i].Height);
      }
      return result;
    }

    public int innerLeafsCount
    {
      get
      {
        return CSprdBurstCluster.innerleafs(this);
      }
      set { }
    }

    private static int innerleafs(CSprdBurstCluster head)
    {
      int result = 0;
      if (head == null) return 0;
      if (head.ID > 0) return 1;
      if (head.innerLeftCluster != null) result += innerleafs(head.innerLeftCluster);
      if (head.innerRightCluster != null) result += innerleafs(head.innerRightCluster);
      return result;
    }
    public static void RollToList(CSprdBurstCluster head, List<CSprdBurstCluster> collection, bool JustLeaf = false)
    {
      if (JustLeaf == false)
        collection.Add(head);
      else if (head.ID > 0)
        collection.Add(head);

      if (head.innerLeftCluster != null)
        RollToList(head.innerLeftCluster, collection, JustLeaf);
      if (head.innerRightCluster != null)
        RollToList(head.innerRightCluster, collection, JustLeaf);
    }
  }
}
