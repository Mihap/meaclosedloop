﻿using System;
using System.Drawing;

namespace MEAClosedLoop.Common.Extensions
{
  public static class PointExtensions
  {
    public static PointF Scale(this PointF p, int val)
    {
      return new PointF(p.X * val, p.Y * val);
    }

    public static PointF Add(this PointF p1, Point p2)
    {
      return new PointF(p1.X + p2.X, p1.Y + p2.Y);
    }

    public static PointF HalfShift(this PointF p, Size s)
    {
      return new PointF(
          p.X - (float)s.Width / 2,
          p.Y - (float)s.Height / 2);
    }

    public static PointF GetShiftOnCircle(this Point point)
    {
      var center = new Point(4, 4);
      var delta = Point.Subtract(center, (Size)point);
      var direction = new Point(1, 1);
      // скорректируем координату, найдем четверть
      if (delta.X >= 0 && delta.Y >= 0)
      {
        // 2
        direction.Y = -1;
        direction.X = -1;

        delta.X++;
        delta.Y++;
      }
      else if (delta.X >= 0)
      {
        // 3
        direction.X = -1;

        delta.X++;
        delta.Y = Math.Abs(delta.Y);
      }
      else if (delta.Y >= 0)
      {
        // 1
        direction.Y = -1;

        delta.Y++;
        delta.X = Math.Abs(delta.X);
      }
      else
      {
        // 4

        delta.Y = Math.Abs(delta.Y);
        delta.X = Math.Abs(delta.X);
      }

      // угол
      var angle = Math.Atan((float)delta.X / delta.Y);

      var result = new PointF(
              (float)(Math.Sin(angle) * direction.X),
              (float)(Math.Cos(angle) * direction.Y));

      return result;
    }
  }
}
