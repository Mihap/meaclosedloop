﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace MEAClosedLoop.Common
{
  public static class MyExtensions
  {
    public static Point Summ(this Point p, Point p1)
    {
      return new Point(p.X + p1.X, p.Y + p1.Y);
    }
  }
}
