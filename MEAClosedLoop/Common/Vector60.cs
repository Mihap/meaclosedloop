﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MEAClosedLoop.Common
{
  public class Vector60
  {
    /// <summary>
    /// Словарь - номер канала, начало пачки на этом канале
    /// </summary>
    public Dictionary<int, int> Elements { get; private set; }
    /// <summary>
    /// Список номеров элементов вектора, при добавлении равных нулю
    /// </summary>
    public List<int> ChWithZeroElList { get; private set; }
    /// <summary>
    /// Номер пачки
    /// </summary>
    public ulong BurstID { get; private set; }

    /// <summary>
    /// Максимальное время начало пачки на электроде (в мс).
    /// Если начало задетектировано после этого времени, то считаем,
    /// что на этом электроде пачки не было.
    /// </summary>
    public const int MAX_START = 100;

    public static Random MainRand { get; set; }

    public Vector60()
    {
      Elements = new Dictionary<int, int>();
    }

    public Vector60(List<int> bursts, ulong id)
    {
      Init(bursts, id);
    }

    public Vector60(CBurst burst)
    {
      List<int> burstStartList = new List<int>();
      int n = 60;
      for (int chNum = 0; chNum < n; chNum++)
      {
        Average stat = new Average();
        int burstStart = 0;
        for (int i = 0; i < burst.Data[chNum].Length && i < 200; i++)
        {
          stat.AddValueElem(burst.Data[chNum][i]);
        }
        stat.Calc();
        for (int i = 200; i < burst.Data[chNum].Length - 2; i++)
        {
          int delta = 200; // 1 ms
          int eps = 80;
          if (Math.Abs(burst.Data[chNum][i]) > stat.Value + stat.Sigma * 3)
          {
            int count = 0;
            for (int j = i; j < j + delta && j < burst.Data[chNum].Length - 2; j++)
            {
              if (Math.Abs(burst.Data[chNum][j]) > stat.Value + stat.Sigma * 3)
              {
                count++;
                if (count == eps)
                  break;
              }
            }

            if (count >= eps)
            {
              burstStart = i;
              break;
            }
          }
        }
        burstStartList.Add(burstStart);
      }
      Init(burstStartList, burst.Start);
    }

    public Vector60(Vector60 vect)
    {
      BurstID = vect.BurstID;
      Elements = new Dictionary<int, int>();
      ChWithZeroElList = new List<int>();
      foreach (int key in vect.Elements.Keys)
      {
        Elements.Add(key, vect.Elements[key]);
      }
    }

    private void Init(List<int> burstValues, ulong id)
    {
      BurstID = id;
      Elements = new Dictionary<int, int>();
      ChWithZeroElList = new List<int>();
      int firstBurst = burstValues.Any(x => x > 0)
        ? burstValues.Where(x => x > 0).Min()
        : 0;
      int n = 60;
      for (int i = 0; i < n; i++)
      {
        Elements.Add(i, burstValues[i] - firstBurst);
        if (burstValues[i] <= 0 || burstValues[i] >= (MAX_START * 25))
        {
          ChWithZeroElList.Add(i);
        }
      }
    }

    /// <summary>
    /// Удаляет из вектора элементы по индексам
    /// </summary>
    /// <param name="elsToRemoveList">Список индексов элементов</param>
    public void RemoveElementsAt(List<int> elsToRemoveList)
    {
      foreach (int ch in elsToRemoveList)
      {
        Elements.Remove(ch);
        ChWithZeroElList.Remove(ch);
      }
    }

    /// <summary>
    /// Вычисление расстояния между векторами в Эвклидовом пространстве
    /// </summary>
    public static double EvklidDist(Vector60 v1, Vector60 v2)
    {
      return (v1 - v2).Length();
    }

    public static double PearsonCorr(Vector60 v1, Vector60 v2)
    {
      double result = 0;
      double s = 0;
      double xv = 0;
      double yv = 0;
      double t1 = 0;
      double t2 = 0;

      //
      // Calculate mean.
      //
      //
      // Additonally we calculate SameX and SameY -
      // flag variables which are set to True when
      // all X[] (or Y[]) contain exactly same value.
      //
      // If at least one of them is True, we return zero
      // (othwerwise we risk to get nonzero correlation
      // because of roundoff).
      //

      int n = v1.Elements.Count;
      double xmean = 0;
      double ymean = 0;
      bool samex = true;
      bool samey = true;
      var keys = v1.Elements.Keys;
      double x0 = v1.Elements[keys.First()];
      double y0 = v2.Elements[keys.First()];
      double v = (double)1 / (double)n;
      foreach (int i in keys)
      {
        if (!(v1.ChWithZeroElList.Contains(i) || v2.ChWithZeroElList.Contains(i)))
        {
          s = v1.Elements[i];
          samex = samex & (double)(s) == (double)(x0);
          xmean = xmean + s * v;
          s = v2.Elements[i];
          samey = samey & (double)(s) == (double)(y0);
          ymean = ymean + s * v;
        }
      }
      if (samex | samey)
      {
        result = 0;
        return result;
      }

      //
      // numerator and denominator
      //
      s = 0;
      xv = 0;
      yv = 0;
      foreach (int i in keys)
      {
        if (!(v1.ChWithZeroElList.Contains(i) || v2.ChWithZeroElList.Contains(i)))
        {
          t1 = v1.Elements[i] - xmean;
          t2 = v2.Elements[i] - ymean;
          xv = xv + Math.Pow(t1, 2);
          yv = yv + Math.Pow(t2, 2);
          s = s + t1 * t2;
        }
      }
      if ((double)(xv) == (double)(0) | (double)(yv) == (double)(0))
      {
        result = 0;
      }
      else
      {
        result = s / (Math.Sqrt(xv) * Math.Sqrt(yv));
      }
      return result;
    }

    /// <summary>
    /// Разность векторов
    /// </summary>
    public static Vector60 operator -(Vector60 v1, Vector60 v2)
    {
      if (v1.Elements.Count != v2.Elements.Count)
      {
        throw new Exception("Error: vectors with different size");
      }
      int n = v1.Elements.Count;
      Vector60 result = new Vector60();
      foreach (int key in v1.Elements.Keys)
      {
        result.Elements.Add(key, v1.Elements[key] - v2.Elements[key]);
      }
      return result;
    }

    public double Length()
    {
      double result = 0;
      foreach (int val in Elements.Values)
      {
        result += Math.Pow(val, 2);
      }
      return Math.Sqrt(result);
    }

    /// <summary>
    /// Если канал обычно участвует в пачечной активности, но в данной пачке
    /// не участвовал, то его значение является средним по данной пачке
    /// </summary>
    public void FillZeroValues()
    {
      //double av = elements.Values.Where(x => x > 0 && x <= maxStart * 25).Average();
      //chWithZeroElList.ForEach(x => elements[x] = (int)av);

      double av = Elements.Values.Any(x => x > 0 && x <= MAX_START * 25)
        ? Elements.Values.Where(x => x > 0 && x <= MAX_START * 25).Average()
        : 0;
      foreach (var ch in ChWithZeroElList)
      {
        var channelsAround = GetNeighbors(ch)
         .Where(x => !ChWithZeroElList.Contains(x) && Elements.ContainsKey(x))
         .Select(x => Elements[x]);

        if (channelsAround.Count() > 0)
        {
          Elements[ch] = (int)channelsAround.Average();
        }
        else
        {
          Elements[ch] = (int)av;
        }
      }
    }


    /// <summary>
    /// Перемешивает данные в векторе
    /// </summary>
    public void Shuffle()
    {
      // Dictionary<int, int>
      Random rand = new Random(MainRand.Next());
      List<int> valuestoshuffle = new List<int>();
      valuestoshuffle.AddRange(this.Elements.Values);

      for (int i = 0; i < valuestoshuffle.Count; i++)
      {
        int tmp = valuestoshuffle[i];
        valuestoshuffle.RemoveAt(i);
        valuestoshuffle.Insert(rand.Next(0, valuestoshuffle.Count), tmp);
      }
      for (int i = 0; i < this.Elements.Keys.Count; i++)
      {
        int ch = this.Elements.Keys.ToList()[i];
        this.Elements[ch] = valuestoshuffle[i];
      }
    }

    /// <summary>
    /// Возвращает точку начала и конца распростронения возбуждения.
    /// </summary>
    /// <returns>Формат индексов 11, 12,...</returns>
    public Tuple<int, int> GetGradientPoints()
    {
      var poolingDict = new Dictionary<int, double>();
      foreach (var el in Elements)
      {
        var aver = GetNeighbors(el.Key).Where(x => Elements.ContainsKey(x))
          .Select(x => Elements[x]).Average();
        poolingDict.Add(MEA.IDX2NAME[el.Key], aver);
      }
      var min = poolingDict.Min(x => x.Value);
      var max = poolingDict.Max(x => x.Value);
      return new Tuple<int, int>
        (poolingDict.First(x => x.Value == min).Key,
        poolingDict.Last(x => x.Value == max).Key);
    }

    private List<int> GetNeighbors(int chNum)
    {
      var result = new List<int>();
      var shifts = new[] { 0, 1, -1 };
      var index = MEA.NAME2IDX.ToList().IndexOf(chNum);
      foreach (var s1 in shifts)
      {
        foreach (var s2 in shifts)
        {
          var neighbor = MEA.NAME2IDX[index + s1 + s2 * 10];
          if (neighbor != -1)
            result.Add(neighbor);
        }
      }
      return result;
    }
  }
}
