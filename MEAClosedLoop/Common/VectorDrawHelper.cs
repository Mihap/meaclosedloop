﻿using MEAClosedLoop.Algorithms;
using MEAClosedLoop.Common.Extensions;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MEAClosedLoop.Common
{
  internal static class VectorDrawHelper
  {
    internal static void DrawVectorHeatMap(Vector60 vector, PictureBox pictureBox, List<Color> colors, bool withArrow = false)
    {
      var height = pictureBox.Height;
      var width = pictureBox.Width;
      var bmp = new Bitmap(width, height);

      using (var g = Graphics.FromImage(bmp))
      using (var myPen = new Pen(Color.Black))
      {
        // Отрисовка линий
        float h = height / 9;
        float w = width / 9;

        // Отрисовка квадратов
        FontFamily fontFamily = new FontFamily("Arial");
        Font font = new Font(fontFamily, 13, FontStyle.Bold, GraphicsUnit.Pixel);
        for (int i = 1; i < 9; i++)
        {
          for (int j = 1; j < 9; j++)
          {
            if ((i == 1 || i == 8) && (j == 1 || j == 8))
              continue;

            int val = -1;
            int chId = MEA.NAME2IDX[10 * i + j];
            if (vector.Elements.ContainsKey(chId))
            {
              val = vector.Elements[chId] / 25;
            }

            Color brushColor;
            if (val != -1)
            //if (val >= 0)
            {
              if (val > colors.Count - 1)
              {
                brushColor = colors[colors.Count - 1];
              }
              else
              {
                brushColor = colors[val];
              }
              SolidBrush ellBrush = new SolidBrush(brushColor);
              g.FillRectangle(ellBrush, w * j - w / 2, h * i - h / 2, w, h);
            }
            else
            {
              //g.FillRectangle(new SolidBrush(Color.Black), w * j, h * i, w, h);
            }
          }
        }

        if (withArrow)
        {
          var arrowPoints = vector.GetGradientPoints();
          using (Pen p = new Pen(Color.FromArgb(120, Color.Black), 24))
          {
            p.EndCap = System.Drawing.Drawing2D.LineCap.ArrowAnchor;

            g.DrawLine(p,
              arrowPoints.Item1 % 10 * w - 0.75f * w, arrowPoints.Item1 / 10 * h - 0.75f * h,
              arrowPoints.Item2 % 10 * w + 0.75f * w, arrowPoints.Item2 / 10 * h + 0.75f * h);
          }
        }

      }

      var tmp = pictureBox.Image;
      pictureBox.Image = bmp;

      if (tmp != null)
        tmp.Dispose();
    }

    internal static void DrawVectorAsElectrodes(Vector60 vector, PictureBox pictureBox, List<Color> colors, bool withArrow = false)
    {
      var height = pictureBox.Height;
      var width = pictureBox.Width;
      var bmp = new Bitmap(width, height);

      using (var g = Graphics.FromImage(bmp))
      using (var fontFamily = new FontFamily("Arial"))
      using (var font = new Font(fontFamily, 11, FontStyle.Italic, GraphicsUnit.Pixel))
      using (var redPen = new Pen(Color.Red, 2))
      {
        // Отрисовка линий
        Pen myPen = new Pen(Color.Black);
        float h = height / 9;
        float w = width / 9;
        for (int i = 1; i < 9; i++)
        {
          g.DrawLine(myPen, 0, h * i, width - 1, h * i);
          g.DrawLine(myPen, w * i, 0, w * i, height - 1);
        }

        // Отрисовка кругов
        float diam = Math.Min(h, w) / 2;
        //Pen ellPen = new Pen(Color.Black);
        for (int i = 1; i < 9; i++)
        {
          for (int j = 1; j < 9; j++)
          {
            if ((i == 1 || i == 8) && (j == 1 || j == 8))
              continue;

            int val = -1;
            int chId = MEA.NAME2IDX[10 * i + j];
            if (vector.Elements.ContainsKey(chId))
            {
              val = vector.Elements[chId] / 25;
            }

            Color brushColor;
            if (val != -1)
            {
              if (val > colors.Count - 1)
              {
                brushColor = colors[colors.Count - 1];
              }
              else
              {
                brushColor = colors[val];
              }
              SolidBrush ellBrush = new SolidBrush(brushColor);
              g.FillEllipse(ellBrush, w * j - diam / 2, h * i - diam / 2, diam, diam);
              g.DrawString(val.ToString(), font, new SolidBrush(Color.Black), new PointF(w * j + diam / 4, h * i + diam / 4));
            }
            else
            {
              g.FillEllipse(new SolidBrush(Color.Black), w * j - diam / 2, h * i - diam / 2, diam, diam);
              g.DrawLine(redPen, w * j - diam / 2, h * i - diam / 2, w * j + diam / 2, h * i + diam / 2);
              g.DrawLine(redPen, w * j - diam / 2, h * i + diam / 2, w * j + diam / 2, h * i - diam / 2);
            }
          }
        }

        if (withArrow)
        {
          var arrowPoints = vector.GetGradientPoints();
          using (Pen p = new Pen(Color.FromArgb(100, Color.Black), 20))
          {
            p.EndCap = System.Drawing.Drawing2D.LineCap.ArrowAnchor;

            g.DrawLine(p,
              arrowPoints.Item1 % 10 * w - w / 2, arrowPoints.Item1 / 10 * h - h / 2,
              arrowPoints.Item2 % 10 * w + w / 2, arrowPoints.Item2 / 10 * h + h / 2);
          }
        }

      }

      var tmp = pictureBox.Image;
      pictureBox.Image = bmp;

      if (tmp != null)
        tmp.Dispose();
    }

    internal static void DrawComplexMap(Vector60 vector, PictureBox pictureBox, List<Color> colors, bool withArrow = false)
    {
      var height = pictureBox.Height;
      var width = pictureBox.Width;
      var bmp = new Bitmap(width, height);

      using (var g = Graphics.FromImage(bmp))
      using (var myPen = new Pen(Color.Black))
      using (var redPen = new Pen(Color.Red, 2))
      {
        float h = height / 9;
        float w = width / 9;

        #region Отрисовка квадратов
        FontFamily fontFamily = new FontFamily("Arial");
        Font font = new Font(fontFamily, 9, FontStyle.Bold, GraphicsUnit.Pixel);
        for (int i = 1; i < 9; i++)
        {
          for (int j = 1; j < 9; j++)
          {
            if ((i == 1 || i == 8) && (j == 1 || j == 8))
              continue;

            int val = -1;
            int chId = MEA.NAME2IDX[10 * i + j];
            if (vector.Elements.ContainsKey(chId))
            {
              val = vector.Elements[chId] / 25;
            }

            Color brushColor;
            if (val != -1)
            //if (val >= 0)
            {
              if (val > colors.Count - 1)
              {
                brushColor = colors[colors.Count - 1];
              }
              else
              {
                brushColor = colors[val];
              }
              SolidBrush ellBrush = new SolidBrush(brushColor);
              g.FillRectangle(ellBrush, w * j - w / 2, h * i - h / 2, w, h);
            }
            else
            {
              //g.FillRectangle(new SolidBrush(Color.Black), w * j, h * i, w, h);
            }
          }
        }

        #endregion

        #region Отрисовка линий
        for (int i = 1; i < 9; i++)
        {
          g.DrawLine(myPen, 0, h * i, width - 1, h * i);
          g.DrawLine(myPen, w * i, 0, w * i, height - 1);
        }
        #endregion

        #region Отрисовка кругов
        float diam = Math.Min(h, w) / 2;
        //Pen ellPen = new Pen(Color.Black);
        for (int i = 1; i < 9; i++)
        {
          for (int j = 1; j < 9; j++)
          {
            if ((i == 1 || i == 8) && (j == 1 || j == 8))
              continue;

            int val = -1;
            int chId = MEA.NAME2IDX[10 * i + j];
            if (vector.Elements.ContainsKey(chId))
            {
              val = vector.Elements[chId] / 25;
            }

            Color brushColor;
            if (val != -1)
            {
              if (val > colors.Count - 1)
              {
                brushColor = colors[colors.Count - 1];
              }
              else
              {
                brushColor = colors[val];
              }
              SolidBrush ellBrush = new SolidBrush(brushColor);
              g.FillEllipse(ellBrush, w * j - diam / 2, h * i - diam / 2, diam, diam);
              g.DrawString(val.ToString(), font, new SolidBrush(Color.Black), new PointF(w * j + diam / 4, h * i + diam / 4));
            }
            else
            {
              g.FillEllipse(new SolidBrush(Color.Black), w * j - diam / 2, h * i - diam / 2, diam, diam);
              g.DrawLine(redPen, w * j - diam / 2, h * i - diam / 2, w * j + diam / 2, h * i + diam / 2);
              g.DrawLine(redPen, w * j - diam / 2, h * i + diam / 2, w * j + diam / 2, h * i - diam / 2);
            }
          }
        }
        #endregion

        if (withArrow)
        {
          var arrowPoints = vector.GetGradientPoints();
          using (Pen p = new Pen(Color.FromArgb(120, Color.Black), 24))
          {
            p.EndCap = System.Drawing.Drawing2D.LineCap.ArrowAnchor;

            g.DrawLine(p,
              arrowPoints.Item1 % 10 * w - 0.75f * w, arrowPoints.Item1 / 10 * h - 0.75f * h,
              arrowPoints.Item2 % 10 * w + 0.75f * w, arrowPoints.Item2 / 10 * h + 0.75f * h);
          }
        }

      }

      var tmp = pictureBox.Image;
      pictureBox.Image = bmp;

      if (tmp != null)
        tmp.Dispose();
    }

    /// <summary>
    /// Отрисовка градиента с подписью
    /// </summary>
    internal static void DrawGradient(PictureBox pictureBox, List<Color> colors)
    {
      var height = pictureBox.Height * 4;
      var width = pictureBox.Width * 4;
      var bmp = new Bitmap(width, height);

      using (var fontFamily = new FontFamily("Arial"))
      using (var font = new Font(fontFamily, 60, FontStyle.Regular, GraphicsUnit.Pixel))
      using (var gradGraphics = Graphics.FromImage(bmp))
      {
        var h = height / 2;
        var w = width / colors.Count;

        //gradGraphics.Clear(Color.White);
        for (int i = 0; i < colors.Count; i++)
        {
          gradGraphics.FillRectangle(new SolidBrush(colors[i]), w * i, 0, w, h);
          if (i % 5 == 0)
            gradGraphics.DrawString(i.ToString(), font, new SolidBrush(Color.Black), new PointF(w * i, h + h / 4));
        }

        var tmp = pictureBox.Image;
        pictureBox.SizeMode = PictureBoxSizeMode.StretchImage;
        pictureBox.Image = bmp;

        if (tmp != null)
          tmp.Dispose();
      }
    }

    internal static List<Color> CalculateColors(int colorsCount)
    {
      var heatMap = new ColorHeatMap();
      var result = new List<Color>();
      for (int i = 0; i < colorsCount; i++)
      {
        //Color col = Color.FromArgb(0, 255 - i * 255 / 15, i * 255 / 10);
        Color col = heatMap.GetColorForValue(i, colorsCount);
        result.Add(col);
      }
      return result;
    }

    internal static void DrawVectorsCircle(IEnumerable<Vector60> vectors, PictureBox pictureBox)
    {
      var startPointList = new List<PointF>();
      var endPointList = new List<PointF>();

      var radius = Math.Min(pictureBox.Height, pictureBox.Width) / 2 - 10;
      var center = new Point(pictureBox.Width / 2, pictureBox.Height / 2);
      var pointSize = new Size(15, 15);

      foreach (var vector in vectors)
      {
        var points = vector.GetGradientPoints();

        var startPoint = new Point(points.Item1 % 10, points.Item1 / 10)
          .GetShiftOnCircle().Scale(radius).HalfShift(pointSize).Add(center);

        var endPoint = new Point(points.Item2 % 10, points.Item2 / 10)
          .GetShiftOnCircle().Scale(radius).HalfShift(pointSize).Add(center);

        startPointList.Add(startPoint);
        endPointList.Add(endPoint);
      }

      List<PointF> startPointListFiltered;
      List<PointF> endPointListFiltered;

      // отсеим самый ад
      if (vectors.Count() > 5)
      {
        var stayPoints = 0.7f;

        var startData = startPointList.Select(point => new double[] { point.X, point.Y }).ToArray();
        var startModel = AccordClustering.kmeans(startData, 3);
        var startMax = startModel.Clusters
          .First(x => x.Proportion == startModel.Clusters.Max(y => y.Proportion));

        startPointList = startPointList.OrderBy(p =>
          Accord.Math.Distance.Euclidean(new double[] { p.X, p.Y }, startMax.Centroid))
          .ToList();

        startPointListFiltered = startPointList
          .Where((x, ind) => ind < startPointList.Count * stayPoints)
          .ToList();

        var endData = endPointList.Select(point => new double[] { point.X, point.Y }).ToArray();
        var endModel = AccordClustering.kmeans(endData, 3);
        var endMax = endModel.Clusters
          .First(x => x.Proportion == endModel.Clusters.Max(y => y.Proportion));

        endPointList = endPointList.OrderBy(p =>
          Accord.Math.Distance.Euclidean(new double[] { p.X, p.Y }, endMax.Centroid))
          .ToList();

        endPointListFiltered = endPointList
          .Where((x, ind) => ind < endPointList.Count * stayPoints)
          .ToList();
      }
      else
      {
        startPointListFiltered = startPointList;
        endPointListFiltered = endPointList;
      }

      var bitmap = new Bitmap(pictureBox.Width, pictureBox.Height);
      using (var g = Graphics.FromImage(bitmap))
      using (var circlePen = new Pen(Color.FromArgb(100, Color.Black)))
      using (var startBrush = new SolidBrush(Color.FromArgb(50, Color.Blue)))
      using (var endBrush = new SolidBrush(Color.FromArgb(50, Color.OrangeRed)))
      {
        g.DrawEllipse(circlePen, center.X - radius, center.Y - radius, radius * 2, radius * 2);

        foreach (var point in startPointListFiltered)
          g.FillEllipse(startBrush, new RectangleF(point, pointSize));

        foreach (var point in endPointListFiltered)
          g.FillEllipse(endBrush, new RectangleF(point, pointSize));
      }

      pictureBox.Image = bitmap;
    }
  }
}
