﻿namespace MEAClosedLoop
{
  partial class FMainWindow
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.MainTopMenu = new System.Windows.Forms.MenuStrip();
      this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.newProjectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.saveProjectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.projectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.dataBaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.windowsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.managerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.mchannelDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.singleChannelViewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.multiChannelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.визуализацияПачекToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.experimentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.findGoodChannelsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.mainLearningExpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.roboLearningToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.burstSortingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.burstDescMethodsDevelopToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.buildMatixToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.buildMatrixToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.clasterAnalysisToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.crossChannelClusterizationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.durationIntervalGraphsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.кластеризацияПоПространственнойХаркеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.анализКластеровПоПространствХаккеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.recordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.recorderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.recordOptionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.loadBurstsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.clustersProviderOptionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.fileRecorderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.opitonsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.beginBurstIOTestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.StimulationControl = new System.Windows.Forms.ToolStripMenuItem();
      this.dataFlowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.findEvokedPacksToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.dataControlToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.aboutProgrammToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.versionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.reportABugToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.пачкаОтносительноКластеровToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.MainTopMenu.SuspendLayout();
      this.SuspendLayout();
      // 
      // MainTopMenu
      // 
      this.MainTopMenu.ImageScalingSize = new System.Drawing.Size(36, 36);
      this.MainTopMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.windowsToolStripMenuItem,
            this.experimentsToolStripMenuItem,
            this.recordToolStripMenuItem,
            this.opitonsToolStripMenuItem,
            this.dataFlowToolStripMenuItem,
            this.aboutToolStripMenuItem});
      this.MainTopMenu.Location = new System.Drawing.Point(0, 0);
      this.MainTopMenu.Name = "MainTopMenu";
      this.MainTopMenu.Size = new System.Drawing.Size(1551, 24);
      this.MainTopMenu.TabIndex = 0;
      this.MainTopMenu.Text = "MainTopMenu";
      // 
      // fileToolStripMenuItem
      // 
      this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newProjectToolStripMenuItem,
            this.saveProjectToolStripMenuItem,
            this.openToolStripMenuItem,
            this.exitToolStripMenuItem});
      this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
      this.fileToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
      this.fileToolStripMenuItem.Text = "Файл";
      // 
      // newProjectToolStripMenuItem
      // 
      this.newProjectToolStripMenuItem.Name = "newProjectToolStripMenuItem";
      this.newProjectToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
      this.newProjectToolStripMenuItem.Text = "Новый проект";
      // 
      // saveProjectToolStripMenuItem
      // 
      this.saveProjectToolStripMenuItem.Name = "saveProjectToolStripMenuItem";
      this.saveProjectToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
      this.saveProjectToolStripMenuItem.Text = "Сохранить проект";
      // 
      // openToolStripMenuItem
      // 
      this.openToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.projectToolStripMenuItem,
            this.dataBaseToolStripMenuItem});
      this.openToolStripMenuItem.Name = "openToolStripMenuItem";
      this.openToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
      this.openToolStripMenuItem.Text = "Открыть";
      // 
      // projectToolStripMenuItem
      // 
      this.projectToolStripMenuItem.Name = "projectToolStripMenuItem";
      this.projectToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
      this.projectToolStripMenuItem.Text = "Проект";
      // 
      // dataBaseToolStripMenuItem
      // 
      this.dataBaseToolStripMenuItem.Name = "dataBaseToolStripMenuItem";
      this.dataBaseToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
      this.dataBaseToolStripMenuItem.Text = "Базу данных";
      // 
      // exitToolStripMenuItem
      // 
      this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
      this.exitToolStripMenuItem.Size = new System.Drawing.Size(173, 22);
      this.exitToolStripMenuItem.Text = "Выход";
      this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
      // 
      // windowsToolStripMenuItem
      // 
      this.windowsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.managerToolStripMenuItem,
            this.mchannelDataToolStripMenuItem,
            this.singleChannelViewToolStripMenuItem,
            this.multiChannelToolStripMenuItem,
            this.визуализацияПачекToolStripMenuItem,
            this.пачкаОтносительноКластеровToolStripMenuItem});
      this.windowsToolStripMenuItem.Name = "windowsToolStripMenuItem";
      this.windowsToolStripMenuItem.Size = new System.Drawing.Size(47, 20);
      this.windowsToolStripMenuItem.Text = "Окна";
      // 
      // managerToolStripMenuItem
      // 
      this.managerToolStripMenuItem.Name = "managerToolStripMenuItem";
      this.managerToolStripMenuItem.Size = new System.Drawing.Size(276, 22);
      this.managerToolStripMenuItem.Text = "Контроллер потоков данных";
      this.managerToolStripMenuItem.Click += new System.EventHandler(this.newManagerToolStripMenuItem_Click);
      // 
      // mchannelDataToolStripMenuItem
      // 
      this.mchannelDataToolStripMenuItem.Name = "mchannelDataToolStripMenuItem";
      this.mchannelDataToolStripMenuItem.Size = new System.Drawing.Size(276, 22);
      this.mchannelDataToolStripMenuItem.Text = "Низкоуровневая отрисовка";
      // 
      // singleChannelViewToolStripMenuItem
      // 
      this.singleChannelViewToolStripMenuItem.Name = "singleChannelViewToolStripMenuItem";
      this.singleChannelViewToolStripMenuItem.Size = new System.Drawing.Size(276, 22);
      this.singleChannelViewToolStripMenuItem.Text = "Визуализация одного канала данных";
      this.singleChannelViewToolStripMenuItem.Click += new System.EventHandler(this.singleChannelViewToolStripMenuItem_Click);
      // 
      // multiChannelToolStripMenuItem
      // 
      this.multiChannelToolStripMenuItem.Name = "multiChannelToolStripMenuItem";
      this.multiChannelToolStripMenuItem.Size = new System.Drawing.Size(276, 22);
      this.multiChannelToolStripMenuItem.Text = "Визуализация все каналов данных";
      this.multiChannelToolStripMenuItem.Click += new System.EventHandler(this.multiChannelToolStripMenuItem_Click);
      // 
      // визуализацияПачекToolStripMenuItem
      // 
      this.визуализацияПачекToolStripMenuItem.Name = "визуализацияПачекToolStripMenuItem";
      this.визуализацияПачекToolStripMenuItem.Size = new System.Drawing.Size(276, 22);
      this.визуализацияПачекToolStripMenuItem.Text = "Визуализация пачек";
      this.визуализацияПачекToolStripMenuItem.Click += new System.EventHandler(this.визуализацияПачекToolStripMenuItem_Click);
      // 
      // experimentsToolStripMenuItem
      // 
      this.experimentsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.findGoodChannelsToolStripMenuItem,
            this.mainLearningExpToolStripMenuItem,
            this.roboLearningToolStripMenuItem,
            this.burstSortingToolStripMenuItem,
            this.buildMatrixToolStripMenuItem,
            this.clasterAnalysisToolStripMenuItem,
            this.crossChannelClusterizationToolStripMenuItem,
            this.durationIntervalGraphsToolStripMenuItem,
            this.кластеризацияПоПространственнойХаркеToolStripMenuItem,
            this.анализКластеровПоПространствХаккеToolStripMenuItem});
      this.experimentsToolStripMenuItem.Name = "experimentsToolStripMenuItem";
      this.experimentsToolStripMenuItem.Size = new System.Drawing.Size(101, 20);
      this.experimentsToolStripMenuItem.Text = "Эксперименты";
      // 
      // findGoodChannelsToolStripMenuItem
      // 
      this.findGoodChannelsToolStripMenuItem.Name = "findGoodChannelsToolStripMenuItem";
      this.findGoodChannelsToolStripMenuItem.Size = new System.Drawing.Size(317, 22);
      this.findGoodChannelsToolStripMenuItem.Text = "Поиск правильных каналов";
      this.findGoodChannelsToolStripMenuItem.Click += new System.EventHandler(this.findGoodChannels_Click);
      // 
      // mainLearningExpToolStripMenuItem
      // 
      this.mainLearningExpToolStripMenuItem.Name = "mainLearningExpToolStripMenuItem";
      this.mainLearningExpToolStripMenuItem.Size = new System.Drawing.Size(317, 22);
      this.mainLearningExpToolStripMenuItem.Text = "Классическое обучение";
      // 
      // roboLearningToolStripMenuItem
      // 
      this.roboLearningToolStripMenuItem.Name = "roboLearningToolStripMenuItem";
      this.roboLearningToolStripMenuItem.Size = new System.Drawing.Size(317, 22);
      this.roboLearningToolStripMenuItem.Text = "Обучение робота";
      this.roboLearningToolStripMenuItem.Click += new System.EventHandler(this.roboLearningToolStripMenuItem_Click);
      // 
      // burstSortingToolStripMenuItem
      // 
      this.burstSortingToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.burstDescMethodsDevelopToolStripMenuItem,
            this.buildMatixToolStripMenuItem});
      this.burstSortingToolStripMenuItem.Name = "burstSortingToolStripMenuItem";
      this.burstSortingToolStripMenuItem.Size = new System.Drawing.Size(317, 22);
      this.burstSortingToolStripMenuItem.Text = "Сортировка пачек";
      // 
      // burstDescMethodsDevelopToolStripMenuItem
      // 
      this.burstDescMethodsDevelopToolStripMenuItem.Name = "burstDescMethodsDevelopToolStripMenuItem";
      this.burstDescMethodsDevelopToolStripMenuItem.Size = new System.Drawing.Size(317, 22);
      this.burstDescMethodsDevelopToolStripMenuItem.Text = "Визуализация характеристической функции";
      this.burstDescMethodsDevelopToolStripMenuItem.Click += new System.EventHandler(this.burstDescMethodsDevelopToolStripMenuItem_Click);
      // 
      // buildMatixToolStripMenuItem
      // 
      this.buildMatixToolStripMenuItem.Name = "buildMatixToolStripMenuItem";
      this.buildMatixToolStripMenuItem.Size = new System.Drawing.Size(317, 22);
      this.buildMatixToolStripMenuItem.Text = "Построить матрицу";
      this.buildMatixToolStripMenuItem.Click += new System.EventHandler(this.buildMatixToolStripMenuItem_Click);
      // 
      // buildMatrixToolStripMenuItem
      // 
      this.buildMatrixToolStripMenuItem.Name = "buildMatrixToolStripMenuItem";
      this.buildMatrixToolStripMenuItem.Size = new System.Drawing.Size(317, 22);
      this.buildMatrixToolStripMenuItem.Text = "Кластеризация на одном канале";
      this.buildMatrixToolStripMenuItem.Click += new System.EventHandler(this.buildMatrixToolStripMenuItem_Click);
      // 
      // clasterAnalysisToolStripMenuItem
      // 
      this.clasterAnalysisToolStripMenuItem.Name = "clasterAnalysisToolStripMenuItem";
      this.clasterAnalysisToolStripMenuItem.Size = new System.Drawing.Size(317, 22);
      this.clasterAnalysisToolStripMenuItem.Text = "Анализ кластеров";
      this.clasterAnalysisToolStripMenuItem.Click += new System.EventHandler(this.clasterAnalysisToolStripMenuItem_Click);
      // 
      // crossChannelClusterizationToolStripMenuItem
      // 
      this.crossChannelClusterizationToolStripMenuItem.Name = "crossChannelClusterizationToolStripMenuItem";
      this.crossChannelClusterizationToolStripMenuItem.Size = new System.Drawing.Size(317, 22);
      this.crossChannelClusterizationToolStripMenuItem.Text = "Кросс канальная кластерищация";
      this.crossChannelClusterizationToolStripMenuItem.Click += new System.EventHandler(this.crossChannelClusterizationToolStripMenuItem_Click);
      // 
      // durationIntervalGraphsToolStripMenuItem
      // 
      this.durationIntervalGraphsToolStripMenuItem.Name = "durationIntervalGraphsToolStripMenuItem";
      this.durationIntervalGraphsToolStripMenuItem.Size = new System.Drawing.Size(317, 22);
      this.durationIntervalGraphsToolStripMenuItem.Text = "Графики продолжительности и интервалов";
      this.durationIntervalGraphsToolStripMenuItem.Click += new System.EventHandler(this.durationIntervalGraphsToolStripMenuItem_Click);
      // 
      // кластеризацияПоПространственнойХаркеToolStripMenuItem
      // 
      this.кластеризацияПоПространственнойХаркеToolStripMenuItem.Name = "кластеризацияПоПространственнойХаркеToolStripMenuItem";
      this.кластеризацияПоПространственнойХаркеToolStripMenuItem.Size = new System.Drawing.Size(317, 22);
      this.кластеризацияПоПространственнойХаркеToolStripMenuItem.Text = "Кластеризация по пространственной хар-ке";
      this.кластеризацияПоПространственнойХаркеToolStripMenuItem.Click += new System.EventHandler(this.кластеризацияПоПространственнойХаркеToolStripMenuItem_Click);
      // 
      // анализКластеровПоПространствХаккеToolStripMenuItem
      // 
      this.анализКластеровПоПространствХаккеToolStripMenuItem.Name = "анализКластеровПоПространствХаккеToolStripMenuItem";
      this.анализКластеровПоПространствХаккеToolStripMenuItem.Size = new System.Drawing.Size(317, 22);
      this.анализКластеровПоПространствХаккеToolStripMenuItem.Text = "Анализ кластеров по пространств. хак-ке";
      this.анализКластеровПоПространствХаккеToolStripMenuItem.Click += new System.EventHandler(this.анализКластеровПоПространствХаккеToolStripMenuItem_Click);
      // 
      // recordToolStripMenuItem
      // 
      this.recordToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.recorderToolStripMenuItem,
            this.recordOptionsToolStripMenuItem,
            this.loadBurstsToolStripMenuItem,
            this.clustersProviderOptionsToolStripMenuItem,
            this.fileRecorderToolStripMenuItem});
      this.recordToolStripMenuItem.Name = "recordToolStripMenuItem";
      this.recordToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
      this.recordToolStripMenuItem.Text = "Запись";
      // 
      // recorderToolStripMenuItem
      // 
      this.recorderToolStripMenuItem.Name = "recorderToolStripMenuItem";
      this.recorderToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
      this.recorderToolStripMenuItem.Text = "Запись в БД";
      this.recorderToolStripMenuItem.Click += new System.EventHandler(this.recorderToolStripMenuItem_Click);
      // 
      // recordOptionsToolStripMenuItem
      // 
      this.recordOptionsToolStripMenuItem.Name = "recordOptionsToolStripMenuItem";
      this.recordOptionsToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
      this.recordOptionsToolStripMenuItem.Text = "Опции записи";
      // 
      // loadBurstsToolStripMenuItem
      // 
      this.loadBurstsToolStripMenuItem.Name = "loadBurstsToolStripMenuItem";
      this.loadBurstsToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
      this.loadBurstsToolStripMenuItem.Text = "Загрузка пачек из файла";
      this.loadBurstsToolStripMenuItem.Click += new System.EventHandler(this.loadBurstsToolStripMenuItem_Click);
      // 
      // clustersProviderOptionsToolStripMenuItem
      // 
      this.clustersProviderOptionsToolStripMenuItem.Name = "clustersProviderOptionsToolStripMenuItem";
      this.clustersProviderOptionsToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
      this.clustersProviderOptionsToolStripMenuItem.Text = "Статус провадера кластеров";
      this.clustersProviderOptionsToolStripMenuItem.Click += new System.EventHandler(this.clustersProviderOptionsToolStripMenuItem_Click);
      // 
      // fileRecorderToolStripMenuItem
      // 
      this.fileRecorderToolStripMenuItem.Name = "fileRecorderToolStripMenuItem";
      this.fileRecorderToolStripMenuItem.Size = new System.Drawing.Size(230, 22);
      this.fileRecorderToolStripMenuItem.Text = "Запись в файл";
      this.fileRecorderToolStripMenuItem.Click += new System.EventHandler(this.fileRecorderToolStripMenuItem_Click);
      // 
      // opitonsToolStripMenuItem
      // 
      this.opitonsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.beginBurstIOTestToolStripMenuItem,
            this.StimulationControl});
      this.opitonsToolStripMenuItem.Name = "opitonsToolStripMenuItem";
      this.opitonsToolStripMenuItem.Size = new System.Drawing.Size(56, 20);
      this.opitonsToolStripMenuItem.Text = "Опции";
      // 
      // beginBurstIOTestToolStripMenuItem
      // 
      this.beginBurstIOTestToolStripMenuItem.Name = "beginBurstIOTestToolStripMenuItem";
      this.beginBurstIOTestToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
      this.beginBurstIOTestToolStripMenuItem.Text = "Тест ввода вывода";
      this.beginBurstIOTestToolStripMenuItem.Click += new System.EventHandler(this.beginBurstIOTestToolStripMenuItem_Click);
      // 
      // StimulationControl
      // 
      this.StimulationControl.Name = "StimulationControl";
      this.StimulationControl.Size = new System.Drawing.Size(216, 22);
      this.StimulationControl.Text = "Управление стимуляцией";
      this.StimulationControl.Click += new System.EventHandler(this.StimulationControl_Click);
      // 
      // dataFlowToolStripMenuItem
      // 
      this.dataFlowToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.findEvokedPacksToolStripMenuItem,
            this.dataControlToolStripMenuItem});
      this.dataFlowToolStripMenuItem.Name = "dataFlowToolStripMenuItem";
      this.dataFlowToolStripMenuItem.Size = new System.Drawing.Size(103, 20);
      this.dataFlowToolStripMenuItem.Text = "Потоки данных";
      // 
      // findEvokedPacksToolStripMenuItem
      // 
      this.findEvokedPacksToolStripMenuItem.Name = "findEvokedPacksToolStripMenuItem";
      this.findEvokedPacksToolStripMenuItem.Size = new System.Drawing.Size(270, 22);
      this.findEvokedPacksToolStripMenuItem.Text = "Автоопределение вызванных пачек";
      // 
      // dataControlToolStripMenuItem
      // 
      this.dataControlToolStripMenuItem.Name = "dataControlToolStripMenuItem";
      this.dataControlToolStripMenuItem.Size = new System.Drawing.Size(270, 22);
      this.dataControlToolStripMenuItem.Text = "Управление данными";
      this.dataControlToolStripMenuItem.Click += new System.EventHandler(this.dataControlToolStripMenuItem_Click);
      // 
      // aboutToolStripMenuItem
      // 
      this.aboutToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutProgrammToolStripMenuItem,
            this.versionToolStripMenuItem,
            this.reportABugToolStripMenuItem});
      this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
      this.aboutToolStripMenuItem.Size = new System.Drawing.Size(94, 20);
      this.aboutToolStripMenuItem.Text = "О программе";
      // 
      // aboutProgrammToolStripMenuItem
      // 
      this.aboutProgrammToolStripMenuItem.Name = "aboutProgrammToolStripMenuItem";
      this.aboutProgrammToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
      this.aboutProgrammToolStripMenuItem.Text = "О программе";
      // 
      // versionToolStripMenuItem
      // 
      this.versionToolStripMenuItem.Name = "versionToolStripMenuItem";
      this.versionToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
      this.versionToolStripMenuItem.Text = "Версия";
      // 
      // reportABugToolStripMenuItem
      // 
      this.reportABugToolStripMenuItem.Name = "reportABugToolStripMenuItem";
      this.reportABugToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
      this.reportABugToolStripMenuItem.Text = "Сообщить об ошибке";
      // 
      // пачкаОтносительноКластеровToolStripMenuItem
      // 
      this.пачкаОтносительноКластеровToolStripMenuItem.Name = "пачкаОтносительноКластеровToolStripMenuItem";
      this.пачкаОтносительноКластеровToolStripMenuItem.Size = new System.Drawing.Size(276, 22);
      this.пачкаОтносительноКластеровToolStripMenuItem.Text = "Пачка относительно кластеров";
      this.пачкаОтносительноКластеровToolStripMenuItem.Click += new System.EventHandler(this.пачкаОтносительноКластеровToolStripMenuItem_Click);
      // 
      // FMainWindow
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.SystemColors.ButtonFace;
      this.ClientSize = new System.Drawing.Size(1551, 552);
      this.Controls.Add(this.MainTopMenu);
      this.IsMdiContainer = true;
      this.MainMenuStrip = this.MainTopMenu;
      this.Name = "FMainWindow";
      this.Text = "Проект MEA - станция обчуения и изучения ";
      this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
      this.Load += new System.EventHandler(this.FMainWindow_Load);
      this.MainTopMenu.ResumeLayout(false);
      this.MainTopMenu.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.MenuStrip MainTopMenu;
    private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem experimentsToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem opitonsToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem aboutProgrammToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem versionToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem reportABugToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem recordToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem recorderToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem recordOptionsToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem newProjectToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem saveProjectToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem projectToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem dataBaseToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem windowsToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem managerToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem mchannelDataToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem findGoodChannelsToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem mainLearningExpToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem dataFlowToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem findEvokedPacksToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem dataControlToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem burstSortingToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem burstDescMethodsDevelopToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem singleChannelViewToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem multiChannelToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem beginBurstIOTestToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem buildMatixToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem loadBurstsToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem buildMatrixToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem clasterAnalysisToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem clustersProviderOptionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fileRecorderToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem roboLearningToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem crossChannelClusterizationToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem durationIntervalGraphsToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem StimulationControl;
    private System.Windows.Forms.ToolStripMenuItem кластеризацияПоПространственнойХаркеToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem анализКластеровПоПространствХаккеToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem визуализацияПачекToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem пачкаОтносительноКластеровToolStripMenuItem;
  }
}