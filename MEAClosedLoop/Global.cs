﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MEAClosedLoop
{

  public static class Global
  {
    public static CDataWriter dw = new CDataWriter();
    public static CLoopController lp;
    public static CFiltering filter;
    public static CDataFlowController dataFlowController;
    public static CStimsDataProvider stimsDataProvider = new CStimsDataProvider();
  }
}
