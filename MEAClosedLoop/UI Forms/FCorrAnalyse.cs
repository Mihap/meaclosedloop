﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MEAClosedLoop.Common;
using ZedGraph;
namespace MEAClosedLoop.UIForms
{
  public partial class FCorrAnalyse : Form
  {
    private List<List<double>> corrCollection = new List<List<double>>();
    private List<List<double>> routes = new List<List<double>>();
    private List<ulong> BurstIDs = new List<ulong>();
    public FCorrAnalyse()
    {
      InitializeComponent();
    }
    public FCorrAnalyse(List<List<double>> initCollection, List<ulong> iDs)
      : this()
    {
      BurstIDs = iDs;
      corrCollection = initCollection;

    }

    private void FCorrAnalyse_Load(object sender, EventArgs e)
    {
      CBurstCluster MainCluster = CClusterisation.BuildBinTree(corrCollection, BurstIDs);
      fillRoutes(MainCluster, new List<double>());
      routes.Count();
      drawTree(MainCluster);

    }

    public void drawTree(CBurstCluster cluster)
    {

      int count = countLeaves(cluster);
      for (int i = 0; i < routes.Count; i++)
      {

        PointPairList linelist = new PointPairList();
        linelist.Add(count / 2, cluster.Height);
        for (int j = 1; j < routes[i].Count; j++)
        {
          linelist.Add(i + j, routes[i][j]);
        }
        matrixDiagramControl.GraphPane.AddCurve("", linelist, Color.Black, SymbolType.None);
      }
      matrixDiagramControl.AxisChange();
      matrixDiagramControl.Refresh();

    }

    private void fillRoutes(CBurstCluster cluster, List<double> history)
    {
      history.Add(cluster.Height);
      if (cluster.innerLeftCluster != null)
      {
        List<double> leftRoute = new List<double>();
        leftRoute.AddRange(history);
        fillRoutes(cluster.innerLeftCluster, leftRoute);
      }
      if (cluster.innerRightCluster != null)
      {
        List<double> rightRoute = new List<double>();
        rightRoute.AddRange(history);
        fillRoutes(cluster.innerRightCluster, rightRoute);
      }
      if (cluster.innerLeftCluster == null && cluster.innerRightCluster == null)
        routes.Add(history);

    }
    private int countLeaves(CBurstCluster cluster)
    {
      if (cluster == null) return 0;
      if (cluster.innerLeftCluster == null && cluster.innerRightCluster == null)
      {
        return 1;
      }
      return countLeaves(cluster.innerLeftCluster) + countLeaves(cluster.innerRightCluster);
    }

    private int countLevels(CBurstCluster cluster)
    {

      if (cluster == null) return 0;
      if (cluster.innerLeftCluster == null && cluster.innerRightCluster == null)
      {
        return 1;
      }
      return 1 + Math.Max(countLevels(cluster.innerLeftCluster), countLevels(cluster.innerRightCluster));
    }


  }
}