﻿namespace MEAClosedLoop.UIForms
{
  partial class CRoboLearning
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.groupBox1 = new System.Windows.Forms.GroupBox();
      this.MaxStimNUD = new System.Windows.Forms.NumericUpDown();
      this.label5 = new System.Windows.Forms.Label();
      this.RelaxeNUD = new System.Windows.Forms.NumericUpDown();
      this.label7 = new System.Windows.Forms.Label();
      this.label8 = new System.Windows.Forms.Label();
      this.label6 = new System.Windows.Forms.Label();
      this.label4 = new System.Windows.Forms.Label();
      this.GridNUD = new System.Windows.Forms.NumericUpDown();
      this.label3 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.label1 = new System.Windows.Forms.Label();
      this.RotatePatternSelector = new System.Windows.Forms.ComboBox();
      this.MovePatterSelector = new System.Windows.Forms.ComboBox();
      this.LoadPatterns = new System.Windows.Forms.Button();
      this.StartBtn = new System.Windows.Forms.Button();
      this.LearningCurve = new ZedGraph.ZedGraphControl();
      this.RoboMap = new ZedGraph.ZedGraphControl();
      this.groupBox2 = new System.Windows.Forms.GroupBox();
      this.WallLbl = new System.Windows.Forms.Label();
      this.label12 = new System.Windows.Forms.Label();
      this.label11 = new System.Windows.Forms.Label();
      this.label10 = new System.Windows.Forms.Label();
      this.label9 = new System.Windows.Forms.Label();
      this.groupBox1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.MaxStimNUD)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.RelaxeNUD)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.GridNUD)).BeginInit();
      this.groupBox2.SuspendLayout();
      this.SuspendLayout();
      // 
      // groupBox1
      // 
      this.groupBox1.Controls.Add(this.MaxStimNUD);
      this.groupBox1.Controls.Add(this.label5);
      this.groupBox1.Controls.Add(this.RelaxeNUD);
      this.groupBox1.Controls.Add(this.label7);
      this.groupBox1.Controls.Add(this.label8);
      this.groupBox1.Controls.Add(this.label6);
      this.groupBox1.Controls.Add(this.label4);
      this.groupBox1.Controls.Add(this.GridNUD);
      this.groupBox1.Controls.Add(this.label3);
      this.groupBox1.Controls.Add(this.label2);
      this.groupBox1.Controls.Add(this.label1);
      this.groupBox1.Controls.Add(this.RotatePatternSelector);
      this.groupBox1.Controls.Add(this.MovePatterSelector);
      this.groupBox1.Location = new System.Drawing.Point(13, 13);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new System.Drawing.Size(275, 158);
      this.groupBox1.TabIndex = 0;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "Параметры";
      // 
      // MaxStimNUD
      // 
      this.MaxStimNUD.Location = new System.Drawing.Point(146, 126);
      this.MaxStimNUD.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
      this.MaxStimNUD.Name = "MaxStimNUD";
      this.MaxStimNUD.Size = new System.Drawing.Size(51, 20);
      this.MaxStimNUD.TabIndex = 3;
      this.MaxStimNUD.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
      this.MaxStimNUD.ValueChanged += new System.EventHandler(this.MaxStimNUD_ValueChanged);
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Location = new System.Drawing.Point(6, 128);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(85, 13);
      this.label5.TabIndex = 2;
      this.label5.Text = "Макс стимулов";
      // 
      // RelaxeNUD
      // 
      this.RelaxeNUD.Location = new System.Drawing.Point(146, 100);
      this.RelaxeNUD.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      this.RelaxeNUD.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
      this.RelaxeNUD.Name = "RelaxeNUD";
      this.RelaxeNUD.Size = new System.Drawing.Size(51, 20);
      this.RelaxeNUD.TabIndex = 3;
      this.RelaxeNUD.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
      this.RelaxeNUD.ValueChanged += new System.EventHandler(this.RelaxeNUD_ValueChanged);
      // 
      // label7
      // 
      this.label7.AutoSize = true;
      this.label7.Location = new System.Drawing.Point(202, 76);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(67, 13);
      this.label7.TabIndex = 2;
      this.label7.Text = "(разбиение)";
      // 
      // label8
      // 
      this.label8.AutoSize = true;
      this.label8.Location = new System.Drawing.Point(203, 128);
      this.label8.Name = "label8";
      this.label8.Size = new System.Drawing.Size(61, 13);
      this.label8.TabIndex = 2;
      this.label8.Text = "(стимулов)";
      // 
      // label6
      // 
      this.label6.AutoSize = true;
      this.label6.Location = new System.Drawing.Point(202, 102);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(31, 13);
      this.label6.TabIndex = 2;
      this.label6.Text = "(сек)";
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(6, 102);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(73, 13);
      this.label4.TabIndex = 2;
      this.label4.Text = "Время отыха";
      // 
      // GridNUD
      // 
      this.GridNUD.Location = new System.Drawing.Point(146, 74);
      this.GridNUD.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      this.GridNUD.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
      this.GridNUD.Name = "GridNUD";
      this.GridNUD.Size = new System.Drawing.Size(51, 20);
      this.GridNUD.TabIndex = 3;
      this.GridNUD.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
      this.GridNUD.ValueChanged += new System.EventHandler(this.GridNUD_ValueChanged);
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(6, 76);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(37, 13);
      this.label3.TabIndex = 2;
      this.label3.Text = "Сетка";
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(6, 50);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(99, 13);
      this.label2.TabIndex = 2;
      this.label2.Text = "Паттерн - поворот";
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(6, 23);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(108, 13);
      this.label1.TabIndex = 2;
      this.label1.Text = "Паттерн - движение";
      // 
      // RotatePatternSelector
      // 
      this.RotatePatternSelector.FormattingEnabled = true;
      this.RotatePatternSelector.Location = new System.Drawing.Point(146, 47);
      this.RotatePatternSelector.Name = "RotatePatternSelector";
      this.RotatePatternSelector.Size = new System.Drawing.Size(121, 21);
      this.RotatePatternSelector.TabIndex = 1;
      this.RotatePatternSelector.SelectedIndexChanged += new System.EventHandler(this.RotatePatternSelector_SelectedIndexChanged);
      // 
      // MovePatterSelector
      // 
      this.MovePatterSelector.FormattingEnabled = true;
      this.MovePatterSelector.Location = new System.Drawing.Point(146, 20);
      this.MovePatterSelector.Name = "MovePatterSelector";
      this.MovePatterSelector.Size = new System.Drawing.Size(121, 21);
      this.MovePatterSelector.TabIndex = 1;
      this.MovePatterSelector.SelectedIndexChanged += new System.EventHandler(this.MovePatterSelector_SelectedIndexChanged);
      // 
      // LoadPatterns
      // 
      this.LoadPatterns.Location = new System.Drawing.Point(132, 324);
      this.LoadPatterns.Name = "LoadPatterns";
      this.LoadPatterns.Size = new System.Drawing.Size(75, 23);
      this.LoadPatterns.TabIndex = 0;
      this.LoadPatterns.Text = "Загрузить";
      this.LoadPatterns.UseVisualStyleBackColor = true;
      this.LoadPatterns.Click += new System.EventHandler(this.LoadPatterns_Click);
      // 
      // StartBtn
      // 
      this.StartBtn.Location = new System.Drawing.Point(213, 324);
      this.StartBtn.Name = "StartBtn";
      this.StartBtn.Size = new System.Drawing.Size(75, 23);
      this.StartBtn.TabIndex = 0;
      this.StartBtn.Text = "Начать";
      this.StartBtn.UseVisualStyleBackColor = true;
      this.StartBtn.Click += new System.EventHandler(this.StartBtn_Click);
      // 
      // LearningCurve
      // 
      this.LearningCurve.Location = new System.Drawing.Point(10, 358);
      this.LearningCurve.Margin = new System.Windows.Forms.Padding(7);
      this.LearningCurve.Name = "LearningCurve";
      this.LearningCurve.ScrollGrace = 0D;
      this.LearningCurve.ScrollMaxX = 0D;
      this.LearningCurve.ScrollMaxY = 0D;
      this.LearningCurve.ScrollMaxY2 = 0D;
      this.LearningCurve.ScrollMinX = 0D;
      this.LearningCurve.ScrollMinY = 0D;
      this.LearningCurve.ScrollMinY2 = 0D;
      this.LearningCurve.Size = new System.Drawing.Size(281, 194);
      this.LearningCurve.TabIndex = 1;
      // 
      // RoboMap
      // 
      this.RoboMap.Location = new System.Drawing.Point(295, 23);
      this.RoboMap.Margin = new System.Windows.Forms.Padding(7);
      this.RoboMap.Name = "RoboMap";
      this.RoboMap.ScrollGrace = 0D;
      this.RoboMap.ScrollMaxX = 0D;
      this.RoboMap.ScrollMaxY = 0D;
      this.RoboMap.ScrollMaxY2 = 0D;
      this.RoboMap.ScrollMinX = 0D;
      this.RoboMap.ScrollMinY = 0D;
      this.RoboMap.ScrollMinY2 = 0D;
      this.RoboMap.Size = new System.Drawing.Size(658, 523);
      this.RoboMap.TabIndex = 2;
      // 
      // groupBox2
      // 
      this.groupBox2.Controls.Add(this.WallLbl);
      this.groupBox2.Controls.Add(this.label12);
      this.groupBox2.Controls.Add(this.label11);
      this.groupBox2.Controls.Add(this.label10);
      this.groupBox2.Controls.Add(this.label9);
      this.groupBox2.Location = new System.Drawing.Point(16, 190);
      this.groupBox2.Name = "groupBox2";
      this.groupBox2.Size = new System.Drawing.Size(275, 116);
      this.groupBox2.TabIndex = 0;
      this.groupBox2.TabStop = false;
      this.groupBox2.Text = "Параметры";
      // 
      // WallLbl
      // 
      this.WallLbl.AutoSize = true;
      this.WallLbl.Location = new System.Drawing.Point(88, 21);
      this.WallLbl.Name = "WallLbl";
      this.WallLbl.Size = new System.Drawing.Size(23, 13);
      this.WallLbl.TabIndex = 2;
      this.WallLbl.Text = "NO";
      // 
      // label12
      // 
      this.label12.AutoSize = true;
      this.label12.Location = new System.Drawing.Point(18, 99);
      this.label12.Name = "label12";
      this.label12.Size = new System.Drawing.Size(43, 13);
      this.label12.TabIndex = 2;
      this.label12.Text = "Стенка";
      this.label12.Click += new System.EventHandler(this.label9_Click);
      // 
      // label11
      // 
      this.label11.AutoSize = true;
      this.label11.Location = new System.Drawing.Point(18, 73);
      this.label11.Name = "label11";
      this.label11.Size = new System.Drawing.Size(43, 13);
      this.label11.TabIndex = 2;
      this.label11.Text = "Стенка";
      this.label11.Click += new System.EventHandler(this.label9_Click);
      // 
      // label10
      // 
      this.label10.AutoSize = true;
      this.label10.Location = new System.Drawing.Point(18, 47);
      this.label10.Name = "label10";
      this.label10.Size = new System.Drawing.Size(43, 13);
      this.label10.TabIndex = 2;
      this.label10.Text = "Стенка";
      this.label10.Click += new System.EventHandler(this.label9_Click);
      // 
      // label9
      // 
      this.label9.AutoSize = true;
      this.label9.Location = new System.Drawing.Point(18, 21);
      this.label9.Name = "label9";
      this.label9.Size = new System.Drawing.Size(43, 13);
      this.label9.TabIndex = 2;
      this.label9.Text = "Стенка";
      this.label9.Click += new System.EventHandler(this.label9_Click);
      // 
      // CRoboLearning
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(970, 570);
      this.Controls.Add(this.RoboMap);
      this.Controls.Add(this.LearningCurve);
      this.Controls.Add(this.groupBox2);
      this.Controls.Add(this.groupBox1);
      this.Controls.Add(this.LoadPatterns);
      this.Controls.Add(this.StartBtn);
      this.Name = "CRoboLearning";
      this.Text = "CRoboLearning";
      this.Load += new System.EventHandler(this.CRoboLearning_Load);
      this.groupBox1.ResumeLayout(false);
      this.groupBox1.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.MaxStimNUD)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.RelaxeNUD)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.GridNUD)).EndInit();
      this.groupBox2.ResumeLayout(false);
      this.groupBox2.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.GroupBox groupBox1;
    private System.Windows.Forms.Button StartBtn;
    private System.Windows.Forms.ComboBox MovePatterSelector;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.ComboBox RotatePatternSelector;
    private System.Windows.Forms.Label label2;
    private ZedGraph.ZedGraphControl LearningCurve;
    private ZedGraph.ZedGraphControl RoboMap;
    private System.Windows.Forms.NumericUpDown GridNUD;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Button LoadPatterns;
    private System.Windows.Forms.NumericUpDown MaxStimNUD;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.NumericUpDown RelaxeNUD;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.Label label8;
    private System.Windows.Forms.GroupBox groupBox2;
    private System.Windows.Forms.Label WallLbl;
    private System.Windows.Forms.Label label9;
    private System.Windows.Forms.Label label12;
    private System.Windows.Forms.Label label11;
    private System.Windows.Forms.Label label10;
  }
}