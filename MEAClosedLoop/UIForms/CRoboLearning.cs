﻿using MEAClosedLoop.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using ZedGraph;
namespace MEAClosedLoop.UIForms
{
  public partial class CRoboLearning : Form
  {
    private int _MaxStimCount;
    private int _RelaxTime;
    private int _GridSplit;

    private int MaxStimCount
    {
      get { return _MaxStimCount; }
      set
      {
        _MaxStimCount = value;
        if (value != MaxStimNUD.Value)
          MaxStimNUD.Value = value;
      }
    }

    private int RelaxTime
    {
      get { return _RelaxTime; }
      set
      {
        _RelaxTime = value;
        if (value != RelaxeNUD.Value)
          RelaxeNUD.Value = value;
      }
    }

    private int GridSplit
    {
      get { return _GridSplit; }
      set
      {
        _GridSplit = value;
        if (value != GridNUD.Value)
          GridNUD.Value = value;
      }
    }

    private Robo robo = null;
    private List<int> RotationCodes = new List<int>();
    private List<int> MoveCodes = new List<int>();

    public CRoboLearning()
    {
      InitializeComponent();
      GridSplit = (int)GridNUD.Value;
      RelaxTime = (int)RelaxeNUD.Value;
      MaxStimCount = (int)MaxStimNUD.Value;

    }

    private void CRoboLearning_Load(object sender, EventArgs e)
    {
      CTimeLineClustering.OnNewClusteredBurst += CTimeLineClustering_OnNewClusteredBurst;
    }

    private void CTimeLineClustering_OnNewClusteredBurst(ulong BurstID, int Cluster)
    {
      if (robo == null || this.IsDisposed) return;
      robo.MoveRobo(Cluster);
      this.BeginInvoke(new Action(() => { DrawRobo(); }));
    }

    private void MovePatterSelector_SelectedIndexChanged(object sender, EventArgs e)
    {
      MoveCodes.Clear();
      int target = -1;
      if (MovePatterSelector.SelectedItem is int)
      {
        target = (int)MovePatterSelector.SelectedItem;
        if (RotationCodes.Contains(target))
        {
          MovePatterSelector.SelectedIndex = 0;
          return;
        }
        MoveCodes.Add(target);
      }
      else
      {
        foreach (int code in MovePatterSelector.Items.OfType<int>())
        {
          if (!RotationCodes.Contains(code))
            MoveCodes.Add(code);
        }
      }
    }

    private void RotatePatternSelector_SelectedIndexChanged(object sender, EventArgs e)
    {
      RotationCodes.Clear();
      if (RotatePatternSelector.SelectedItem is int)
      {
        int target = (int)RotatePatternSelector.SelectedItem;
        RotationCodes.Add(target);
        if (MoveCodes.Contains(target))
        {
          MovePatterSelector.SelectedIndex = 1;
          MovePatterSelector.SelectedIndex = 0;
        }
        if (MovePatterSelector.SelectedIndex < 0)
        {
          MovePatterSelector.SelectedIndex = 0;
        }
      }
    }

    private void GridNUD_ValueChanged(object sender, EventArgs e)
    {
      GridSplit = (int)GridNUD.Value;
    }

    private void RelaxeNUD_ValueChanged(object sender, EventArgs e)
    {
      RelaxTime = (int)RelaxeNUD.Value;
    }

    private void MaxStimNUD_ValueChanged(object sender, EventArgs e)
    {
      MaxStimCount = (int)RelaxeNUD.Value;
    }

    private void DrawRobo()
    {
      GraphPane pane = RoboMap.GraphPane;
      pane.IsFontsScaled = false;
      pane.Title.Text = "Положение робота";
      pane.XAxis.Title.Text = "х";
      pane.YAxis.Title.Text = "y";
      pane.Legend.IsVisible = false;
      pane.YAxis.Scale.Min = 0;
      pane.YAxis.Scale.Max = GridSplit + 1;
      pane.XAxis.Scale.Min = 0;
      pane.XAxis.Scale.Max = GridSplit + 1;
      pane.XAxis.Scale.MajorStep = GridSplit / 5;
      pane.XAxis.Scale.MinorStep = GridSplit / 10;
      pane.YAxis.Scale.MajorStep = GridSplit / 5;
      pane.YAxis.Scale.MinorStep = GridSplit / 10;



      // Очистим список кривых на тот случай, если до этого сигналы уже были нарисованы
      pane.CurveList.Clear();
      // Создадим окружность для текущего положения
      PointPairList ringList = new PointPairList();
      PointPairList directionList = new PointPairList();
      PointPairList traectory = new PointPairList();
      double r = 1;
      for (int i = 0; i < 60; i++)
      {
        ringList.Add(this.robo.Position.X + r * Math.Cos(Math.PI * 2 * i / 60), this.robo.Position.Y + r * Math.Sin(Math.PI * 2 * i / 60));
      }
      if (robo.Traectory.Count > 1)
        for (int i = 0; i < robo.Traectory.Count; i++)
          traectory.Add(new PointPair(robo.Traectory[i]));

      directionList.Add(new PointPair(robo.Position));
      directionList.Add(new PointPair(robo.NextPosition));
      pane.AddCurve("", ringList, Color.Red, SymbolType.None);
      pane.AddCurve("", directionList, Color.Red, SymbolType.None);
      if (traectory.Count > 1)
        pane.AddCurve("", traectory, Color.FromArgb(80, 100, 100, 100), SymbolType.None).Line.Width=4;
      // Обновляем оси
      //RoboMap.AxisChange();

      // Обновляем график
      RoboMap.Invalidate();
    }

    private void LoadPatterns_Click(object sender, EventArgs e)
    {
      if (CClusterProvider.ClasterBuilder.Clasters.Count == 0)
      {
        MessageBox.Show("паттерны не выделены или не загружены");
        return;
      }
      MovePatterSelector.Items.Add("любой");
      for (int i = CClusterProvider.ClasterBuilder.Clasters.Count - 1; i >= 0; i--)
      {
        MovePatterSelector.Items.Add(i);
        RotatePatternSelector.Items.Add(i);
      }
    }

    private void StartBtn_Click(object sender, EventArgs e)
    {
      robo = new Robo(RotationCodes, MoveCodes, GridSplit);
      robo.OnWallFound += Robo_OnWallFound;
      robo.OnWallLeft += Robo_OnWallLeft;
      DrawRobo();
    }

    private void Robo_OnWallLeft()
    {
      this.BeginInvoke(new Action(() => 
      {
        WallLbl.Text = "NO";
      }
      ));
      if (Global.lp != null)
        Global.lp.DoStim = false;
    }

    private void Robo_OnWallFound()
    {
      this.BeginInvoke(new Action(() =>
      {
        WallLbl.Text = "YES";
      }));
      if (Global.lp != null)
        Global.lp.DoStim = true;
    }

    private void label9_Click(object sender, EventArgs e)
    {

    }
  }
  public enum Orientation
  {
    Top,
    Right,
    Bottom,
    Left
  }


  class Robo
  {
    public delegate void OnWallFoundDelegate();
    public delegate void OnWallLeftDelegate();
    public event OnWallFoundDelegate OnWallFound;
    public event OnWallLeftDelegate OnWallLeft;

    public Orientation direction = Orientation.Top;
    public Point Move
    {
      set { }
      get
      {
        if (direction == Orientation.Left) return new Point(-1, 0);
        else if (direction == Orientation.Top) return new Point(0, 1);
        else if (direction == Orientation.Right) return new Point(1, 0);
        else if (direction == Orientation.Bottom) return new Point(0, -1);
        return new Point(0, 0);
      }
    }
    public Point NextPosition
    {
      set { }
      get
      {
        return new Point(Position.X + Move.X, Position.Y + Move.Y);
      }
    }
    public Point Position;
    int Size;
    public bool AtWall = false;

    public List<Point> Traectory = new List<Point>();

    List<int> RotationCodes, MovingCodes;
    public void MoveRobo(int Code)
    {
      if (RotationCodes.Contains(Code))
      {
        if (direction == Orientation.Left) direction = Orientation.Top;
        else if (direction == Orientation.Top) direction = Orientation.Right;
        else if (direction == Orientation.Right) direction = Orientation.Bottom;
        else if (direction == Orientation.Bottom) direction = Orientation.Left;
        if (CheckWall(NextPosition))
        {
          AtWall = false;
          OnWallLeft?.Invoke();
        }
        return;
      }
      if (MovingCodes.Contains(Code))
      {
        // если не касаемся стенки - двигаемся дальше
        if (!CheckWall(NextPosition))
        {
          Position = NextPosition;
          Traectory.Add(Position);
        }
        else
        {
          if (!AtWall)
          {
            AtWall = true;
            OnWallFound?.Invoke();
          }
        }
        return;
      }
    }

    /// <summary>
    /// Возвращает true, если точка касается сенсором стенки
    /// </summary>
    /// <param name="p"></param>
    /// <returns></returns>
    private bool CheckWall(Point p)
    {
      if (p.X <= 0 || p.Y <= 0)
        return true;
      if (p.X > Size || p.Y > Size)
        return true;
      return false;
    }
    public Robo(List<int> RotationCodes, List<int> MovingCodes, int Size)
    {
      this.MovingCodes = new List<int>();
      this.RotationCodes = new List<int>();
      this.MovingCodes.AddRange(MovingCodes);
      this.RotationCodes.AddRange(RotationCodes);
      this.Size = Size;
      this.Position = new Point(Size / 2, Size / 2);
    }

  }
}
