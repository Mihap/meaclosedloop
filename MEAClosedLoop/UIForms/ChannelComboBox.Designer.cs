﻿namespace MEAClosedLoop.UIForms
{
  partial class ChannelComboBox
  {
    /// <summary> 
    /// Требуется переменная конструктора.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Освободить все используемые ресурсы.
    /// </summary>
    /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Код, автоматически созданный конструктором компонентов

    /// <summary> 
    /// Обязательный метод для поддержки конструктора - не изменяйте 
    /// содержимое данного метода при помощи редактора кода.
    /// </summary>
    private void InitializeComponent()
    {
            this.selectedChTextBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // selectedChTextBox
            // 
            this.selectedChTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.selectedChTextBox.Location = new System.Drawing.Point(6, 6);
            this.selectedChTextBox.Margin = new System.Windows.Forms.Padding(3, 3, 6, 3);
            this.selectedChTextBox.Name = "selectedChTextBox";
            this.selectedChTextBox.ReadOnly = true;
            this.selectedChTextBox.Size = new System.Drawing.Size(67, 20);
            this.selectedChTextBox.TabIndex = 0;
            this.selectedChTextBox.Click += new System.EventHandler(this.openSelectorButton_Click);
            // 
            // ChannelComboBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.selectedChTextBox);
            this.Name = "ChannelComboBox";
            this.Padding = new System.Windows.Forms.Padding(3);
            this.Size = new System.Drawing.Size(77, 29);
            this.ResumeLayout(false);
            this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.TextBox selectedChTextBox;
  }
}
