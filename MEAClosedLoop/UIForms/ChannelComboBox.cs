﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MEAClosedLoop.UIForms
{
  public delegate void OnSelectedChannelsChangedDelegate();
  
  public partial class ChannelComboBox : UserControl
  {
    public List<int> chIDList = new List<int>();
    public event OnSelectedChannelsChangedDelegate OnSelectedChannelsChanged;
    public channelSelectMode selectMode = channelSelectMode.Single;
    public ChannelComboBox()
    {
      InitializeComponent();
      FChSelector select = new FChSelector(selectedChTextBox, selectMode) {ChNameList = new List<int> {74}};
      chIDList = select.ChIDList;
    }

    public ChannelComboBox(channelSelectMode csm) : base()
    {
      selectMode = csm;
    }

    private void openSelectorButton_Click(object sender, EventArgs e)
    {
      FChSelector select = new FChSelector(selectedChTextBox, selectMode);
      select.StartPosition = FormStartPosition.Manual;
      if (Parent.Parent == null)
      {
        select.Location = new Point(Location.X + Parent.Location.X + 10, Location.Y + Size.Height * 2 + Parent.Location.Y);
      }
      else
      {
        select.Location = new Point(Location.X + Parent.Location.X + Parent.Parent.Location.X + 10, Location.Y + Size.Height * 2 + Parent.Location.Y + Parent.Parent.Location.Y);
      }

      select.ShowDialog();
      chIDList = select.ChIDList;

      if (OnSelectedChannelsChanged != null) OnSelectedChannelsChanged();
    }
  }
}
