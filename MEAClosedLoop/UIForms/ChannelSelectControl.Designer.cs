﻿namespace MEAClosedLoop.UIForms
{
  partial class ChannelSelectControl
  {
    /// <summary> 
    /// Требуется переменная конструктора.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Освободить все используемые ресурсы.
    /// </summary>
    /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Код, автоматически созданный конструктором компонентов

    /// <summary> 
    /// Обязательный метод для поддержки конструктора - не изменяйте 
    /// содержимое данного метода при помощи редактора кода.
    /// </summary>
    private void InitializeComponent()
    {
      this.panel = new System.Windows.Forms.FlowLayoutPanel();
      this.closeButton = new System.Windows.Forms.Button();
      this.SuspendLayout();
      // 
      // panel
      // 
      this.panel.BackColor = System.Drawing.SystemColors.Control;
      this.panel.Location = new System.Drawing.Point(3, 3);
      this.panel.Name = "panel";
      this.panel.Size = new System.Drawing.Size(216, 216);
      this.panel.TabIndex = 0;
      // 
      // closeButton
      // 
      this.closeButton.Location = new System.Drawing.Point(71, 225);
      this.closeButton.Name = "closeButton";
      this.closeButton.Size = new System.Drawing.Size(75, 23);
      this.closeButton.TabIndex = 1;
      this.closeButton.Text = "OK";
      this.closeButton.UseVisualStyleBackColor = true;
      // 
      // ChannelSelectControl
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.SystemColors.ControlDark;
      this.Controls.Add(this.closeButton);
      this.Controls.Add(this.panel);
      this.Name = "ChannelSelectControl";
      this.Size = new System.Drawing.Size(223, 252);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.FlowLayoutPanel panel;
    private System.Windows.Forms.Button closeButton;
  }
}
