﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MEAClosedLoop.UIForms
{
  public partial class ChannelSelectControl : UserControl
  {
    public ChannelSelectControl()
    {
      InitializeComponent();
      buttonsConstructor();
    }

    private void buttonsConstructor()
    {
      for (int i = 1; i <= 8; i++)
      {
        for (int j = 1; j <= 8; j++)
        {
          Button b = new Button();
          int ID = j * 10 + i;
          b.Text = ID.ToString();
          b.Font = new System.Drawing.Font("Microsoft Sans Serif", 6);
          b.Size = new Size(24, 24);
          b.Name = "button" + ID;
          panel.Controls.Add(b);
          b.Click +=b_Click;

          b.Click += new EventHandler( new Action<object, EventArgs>((sender, e) => 
          {
            int id = ID;
            // some actions with id, NOT ID!

          }));
        }
      }
    }

    void b_Click(object sender, EventArgs e)
    {
      throw new NotImplementedException();
    }
  }
}
