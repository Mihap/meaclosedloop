﻿namespace MEAClosedLoop.UIForms
{
  partial class FBurstAnalyse
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.GetCollectionBtn = new System.Windows.Forms.Button();
      this.MainMapPicture = new System.Windows.Forms.PictureBox();
      this.label1 = new System.Windows.Forms.Label();
      this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
      this.DrawButton = new System.Windows.Forms.Button();
      this.label2 = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.StartTime = new System.Windows.Forms.NumericUpDown();
      this.StopTime = new System.Windows.Forms.NumericUpDown();
      this.label4 = new System.Windows.Forms.Label();
      this.FunctionSelector = new System.Windows.Forms.ComboBox();
      this.ParamGroupBox = new System.Windows.Forms.GroupBox();
      this.ShowClustersCheckBox = new System.Windows.Forms.CheckBox();
      this.ShowAllPairsInRangeBtn = new System.Windows.Forms.Button();
      this.ToValueTB = new System.Windows.Forms.TextBox();
      this.FromValueTB = new System.Windows.Forms.TextBox();
      this.channelComboBox1 = new MEAClosedLoop.UIForms.ChannelComboBox();
      this.durationLabel = new System.Windows.Forms.Label();
      this.progressLabel = new System.Windows.Forms.Label();
      this.FindClustersBtn = new System.Windows.Forms.Button();
      this.EvokedCountLbl = new System.Windows.Forms.Label();
      this.SpontanousCountLbl = new System.Windows.Forms.Label();
      this.AnalyseAllDirectBtn = new System.Windows.Forms.Button();
      this.HistoContainer = new System.Windows.Forms.GroupBox();
      this.label7 = new System.Windows.Forms.Label();
      this.label6 = new System.Windows.Forms.Label();
      this.ThresholdTB = new System.Windows.Forms.TextBox();
      this.label5 = new System.Windows.Forms.Label();
      this.zeroHypDisribGraph = new ZedGraph.ZedGraphControl();
      this.NormalDistribGraph = new ZedGraph.ZedGraphControl();
      this.panel1 = new System.Windows.Forms.Panel();
      ((System.ComponentModel.ISupportInitialize)(this.MainMapPicture)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.StartTime)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.StopTime)).BeginInit();
      this.ParamGroupBox.SuspendLayout();
      this.HistoContainer.SuspendLayout();
      this.panel1.SuspendLayout();
      this.SuspendLayout();
      // 
      // GetCollectionBtn
      // 
      this.GetCollectionBtn.Location = new System.Drawing.Point(6, 19);
      this.GetCollectionBtn.Name = "GetCollectionBtn";
      this.GetCollectionBtn.Size = new System.Drawing.Size(65, 23);
      this.GetCollectionBtn.TabIndex = 0;
      this.GetCollectionBtn.Text = "выборка";
      this.GetCollectionBtn.UseVisualStyleBackColor = true;
      this.GetCollectionBtn.Click += new System.EventHandler(this.AnalyseAllCollected_Click);
      // 
      // MainMapPicture
      // 
      this.MainMapPicture.BackColor = System.Drawing.SystemColors.AppWorkspace;
      this.MainMapPicture.Dock = System.Windows.Forms.DockStyle.Fill;
      this.MainMapPicture.Location = new System.Drawing.Point(0, 0);
      this.MainMapPicture.Name = "MainMapPicture";
      this.MainMapPicture.Size = new System.Drawing.Size(881, 667);
      this.MainMapPicture.TabIndex = 1;
      this.MainMapPicture.TabStop = false;
      this.MainMapPicture.Click += new System.EventHandler(this.MainMapPicture_Click);
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(350, 24);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(37, 13);
      this.label1.TabIndex = 3;
      this.label1.Text = "канал";
      // 
      // DrawButton
      // 
      this.DrawButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
      this.DrawButton.Location = new System.Drawing.Point(269, 19);
      this.DrawButton.Name = "DrawButton";
      this.DrawButton.Size = new System.Drawing.Size(75, 23);
      this.DrawButton.TabIndex = 4;
      this.DrawButton.Text = "Отрисовать";
      this.DrawButton.UseVisualStyleBackColor = true;
      this.DrawButton.Click += new System.EventHandler(this.DrawButton_Click);
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(473, 24);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(49, 13);
      this.label2.TabIndex = 5;
      this.label2.Text = "start, ms ";
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(586, 24);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(46, 13);
      this.label3.TabIndex = 5;
      this.label3.Text = "stop, ms";
      // 
      // StartTime
      // 
      this.StartTime.Location = new System.Drawing.Point(528, 24);
      this.StartTime.Maximum = new decimal(new int[] {
            980,
            0,
            0,
            0});
      this.StartTime.Name = "StartTime";
      this.StartTime.Size = new System.Drawing.Size(52, 20);
      this.StartTime.TabIndex = 6;
      // 
      // StopTime
      // 
      this.StopTime.Location = new System.Drawing.Point(638, 24);
      this.StopTime.Maximum = new decimal(new int[] {
            3000,
            0,
            0,
            0});
      this.StopTime.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
      this.StopTime.Name = "StopTime";
      this.StopTime.Size = new System.Drawing.Size(52, 20);
      this.StopTime.TabIndex = 6;
      this.StopTime.Value = new decimal(new int[] {
            80,
            0,
            0,
            0});
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(696, 26);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(27, 13);
      this.label4.TabIndex = 5;
      this.label4.Text = "type";
      // 
      // FunctionSelector
      // 
      this.FunctionSelector.FormattingEnabled = true;
      this.FunctionSelector.Items.AddRange(new object[] {
            "Fx",
            "dFx"});
      this.FunctionSelector.Location = new System.Drawing.Point(729, 23);
      this.FunctionSelector.Name = "FunctionSelector";
      this.FunctionSelector.Size = new System.Drawing.Size(135, 21);
      this.FunctionSelector.TabIndex = 7;
      this.FunctionSelector.SelectedIndexChanged += new System.EventHandler(this.FunctionSelector_SelectedIndexChanged);
      // 
      // ParamGroupBox
      // 
      this.ParamGroupBox.Controls.Add(this.ShowClustersCheckBox);
      this.ParamGroupBox.Controls.Add(this.ShowAllPairsInRangeBtn);
      this.ParamGroupBox.Controls.Add(this.ToValueTB);
      this.ParamGroupBox.Controls.Add(this.FromValueTB);
      this.ParamGroupBox.Controls.Add(this.channelComboBox1);
      this.ParamGroupBox.Controls.Add(this.durationLabel);
      this.ParamGroupBox.Controls.Add(this.progressLabel);
      this.ParamGroupBox.Controls.Add(this.FindClustersBtn);
      this.ParamGroupBox.Controls.Add(this.EvokedCountLbl);
      this.ParamGroupBox.Controls.Add(this.SpontanousCountLbl);
      this.ParamGroupBox.Controls.Add(this.AnalyseAllDirectBtn);
      this.ParamGroupBox.Controls.Add(this.GetCollectionBtn);
      this.ParamGroupBox.Controls.Add(this.FunctionSelector);
      this.ParamGroupBox.Controls.Add(this.StopTime);
      this.ParamGroupBox.Controls.Add(this.label1);
      this.ParamGroupBox.Controls.Add(this.StartTime);
      this.ParamGroupBox.Controls.Add(this.DrawButton);
      this.ParamGroupBox.Controls.Add(this.label4);
      this.ParamGroupBox.Controls.Add(this.label2);
      this.ParamGroupBox.Controls.Add(this.label3);
      this.ParamGroupBox.Location = new System.Drawing.Point(4, 1);
      this.ParamGroupBox.Name = "ParamGroupBox";
      this.ParamGroupBox.Size = new System.Drawing.Size(1366, 52);
      this.ParamGroupBox.TabIndex = 8;
      this.ParamGroupBox.TabStop = false;
      this.ParamGroupBox.Text = "Параметры";
      // 
      // ShowClustersCheckBox
      // 
      this.ShowClustersCheckBox.AutoSize = true;
      this.ShowClustersCheckBox.Location = new System.Drawing.Point(182, 24);
      this.ShowClustersCheckBox.Name = "ShowClustersCheckBox";
      this.ShowClustersCheckBox.Size = new System.Drawing.Size(15, 14);
      this.ShowClustersCheckBox.TabIndex = 18;
      this.ShowClustersCheckBox.UseVisualStyleBackColor = true;
      // 
      // ShowAllPairsInRangeBtn
      // 
      this.ShowAllPairsInRangeBtn.Location = new System.Drawing.Point(1248, 11);
      this.ShowAllPairsInRangeBtn.Name = "ShowAllPairsInRangeBtn";
      this.ShowAllPairsInRangeBtn.Size = new System.Drawing.Size(109, 36);
      this.ShowAllPairsInRangeBtn.TabIndex = 17;
      this.ShowAllPairsInRangeBtn.Text = "Отобразить пары в диапазоне";
      this.ShowAllPairsInRangeBtn.UseVisualStyleBackColor = true;
      this.ShowAllPairsInRangeBtn.Click += new System.EventHandler(this.ShowAllPairsInRangeBtn_Click);
      // 
      // ToValueTB
      // 
      this.ToValueTB.Location = new System.Drawing.Point(1202, 30);
      this.ToValueTB.Name = "ToValueTB";
      this.ToValueTB.Size = new System.Drawing.Size(40, 20);
      this.ToValueTB.TabIndex = 16;
      this.ToValueTB.Text = "1";
      // 
      // FromValueTB
      // 
      this.FromValueTB.Location = new System.Drawing.Point(1202, 9);
      this.FromValueTB.Name = "FromValueTB";
      this.FromValueTB.Size = new System.Drawing.Size(40, 20);
      this.FromValueTB.TabIndex = 15;
      this.FromValueTB.Text = "0,9";
      // 
      // channelComboBox1
      // 
      this.channelComboBox1.Location = new System.Drawing.Point(393, 16);
      this.channelComboBox1.Margin = new System.Windows.Forms.Padding(4);
      this.channelComboBox1.Name = "channelComboBox1";
      this.channelComboBox1.Size = new System.Drawing.Size(65, 30);
      this.channelComboBox1.TabIndex = 14;
      // 
      // durationLabel
      // 
      this.durationLabel.AutoSize = true;
      this.durationLabel.Location = new System.Drawing.Point(1080, 26);
      this.durationLabel.Name = "durationLabel";
      this.durationLabel.Size = new System.Drawing.Size(75, 13);
      this.durationLabel.TabIndex = 13;
      this.durationLabel.Text = "duration: none";
      // 
      // progressLabel
      // 
      this.progressLabel.AutoSize = true;
      this.progressLabel.Location = new System.Drawing.Point(997, 26);
      this.progressLabel.Name = "progressLabel";
      this.progressLabel.Size = new System.Drawing.Size(77, 13);
      this.progressLabel.TabIndex = 13;
      this.progressLabel.Text = "progress: none";
      // 
      // FindClustersBtn
      // 
      this.FindClustersBtn.Location = new System.Drawing.Point(873, 21);
      this.FindClustersBtn.Name = "FindClustersBtn";
      this.FindClustersBtn.Size = new System.Drawing.Size(107, 23);
      this.FindClustersBtn.TabIndex = 12;
      this.FindClustersBtn.Text = "Кластеризация";
      this.FindClustersBtn.UseVisualStyleBackColor = true;
      this.FindClustersBtn.Click += new System.EventHandler(this.FindClustersBtn_Click);
      // 
      // EvokedCountLbl
      // 
      this.EvokedCountLbl.AutoSize = true;
      this.EvokedCountLbl.Location = new System.Drawing.Point(78, 33);
      this.EvokedCountLbl.Name = "EvokedCountLbl";
      this.EvokedCountLbl.Size = new System.Drawing.Size(91, 13);
      this.EvokedCountLbl.TabIndex = 11;
      this.EvokedCountLbl.Text = "вызванные : n/a";
      // 
      // SpontanousCountLbl
      // 
      this.SpontanousCountLbl.AutoSize = true;
      this.SpontanousCountLbl.Location = new System.Drawing.Point(78, 12);
      this.SpontanousCountLbl.Name = "SpontanousCountLbl";
      this.SpontanousCountLbl.Size = new System.Drawing.Size(94, 13);
      this.SpontanousCountLbl.TabIndex = 10;
      this.SpontanousCountLbl.Text = "спонтанные : n/a";
      // 
      // AnalyseAllDirectBtn
      // 
      this.AnalyseAllDirectBtn.Location = new System.Drawing.Point(198, 19);
      this.AnalyseAllDirectBtn.Name = "AnalyseAllDirectBtn";
      this.AnalyseAllDirectBtn.Size = new System.Drawing.Size(65, 23);
      this.AnalyseAllDirectBtn.TabIndex = 0;
      this.AnalyseAllDirectBtn.Text = "Анализ";
      this.AnalyseAllDirectBtn.UseVisualStyleBackColor = true;
      this.AnalyseAllDirectBtn.Click += new System.EventHandler(this.AnalyseAllDirectBtn_Click);
      // 
      // HistoContainer
      // 
      this.HistoContainer.Controls.Add(this.label7);
      this.HistoContainer.Controls.Add(this.label6);
      this.HistoContainer.Controls.Add(this.ThresholdTB);
      this.HistoContainer.Controls.Add(this.label5);
      this.HistoContainer.Controls.Add(this.zeroHypDisribGraph);
      this.HistoContainer.Controls.Add(this.NormalDistribGraph);
      this.HistoContainer.Location = new System.Drawing.Point(4, 60);
      this.HistoContainer.Name = "HistoContainer";
      this.HistoContainer.Size = new System.Drawing.Size(473, 621);
      this.HistoContainer.TabIndex = 9;
      this.HistoContainer.TabStop = false;
      this.HistoContainer.Text = "Распределения значений";
      // 
      // label7
      // 
      this.label7.AutoSize = true;
      this.label7.Location = new System.Drawing.Point(6, 302);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(99, 13);
      this.label7.TabIndex = 3;
      this.label7.Text = "Нулевая гипотеза";
      // 
      // label6
      // 
      this.label6.AutoSize = true;
      this.label6.Location = new System.Drawing.Point(9, 20);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(93, 13);
      this.label6.TabIndex = 3;
      this.label6.Text = "Характеристики:";
      // 
      // ThresholdTB
      // 
      this.ThresholdTB.Location = new System.Drawing.Point(182, 593);
      this.ThresholdTB.Name = "ThresholdTB";
      this.ThresholdTB.Size = new System.Drawing.Size(72, 20);
      this.ThresholdTB.TabIndex = 2;
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Location = new System.Drawing.Point(5, 596);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(153, 13);
      this.label5.TabIndex = 1;
      this.label5.Text = "Выбранный порог отсечения";
      // 
      // zeroHypDisribGraph
      // 
      this.zeroHypDisribGraph.Location = new System.Drawing.Point(6, 318);
      this.zeroHypDisribGraph.Margin = new System.Windows.Forms.Padding(4);
      this.zeroHypDisribGraph.Name = "zeroHypDisribGraph";
      this.zeroHypDisribGraph.PanButtons = System.Windows.Forms.MouseButtons.Middle;
      this.zeroHypDisribGraph.ScrollGrace = 0D;
      this.zeroHypDisribGraph.ScrollMaxX = 0D;
      this.zeroHypDisribGraph.ScrollMaxY = 0D;
      this.zeroHypDisribGraph.ScrollMaxY2 = 0D;
      this.zeroHypDisribGraph.ScrollMinX = 0D;
      this.zeroHypDisribGraph.ScrollMinY = 0D;
      this.zeroHypDisribGraph.ScrollMinY2 = 0D;
      this.zeroHypDisribGraph.Size = new System.Drawing.Size(464, 269);
      this.zeroHypDisribGraph.TabIndex = 0;
      this.zeroHypDisribGraph.Load += new System.EventHandler(this.zeroHypDisribGraph_Load);
      // 
      // NormalDistribGraph
      // 
      this.NormalDistribGraph.Location = new System.Drawing.Point(6, 36);
      this.NormalDistribGraph.Margin = new System.Windows.Forms.Padding(4);
      this.NormalDistribGraph.Name = "NormalDistribGraph";
      this.NormalDistribGraph.ScrollGrace = 0D;
      this.NormalDistribGraph.ScrollMaxX = 0D;
      this.NormalDistribGraph.ScrollMaxY = 0D;
      this.NormalDistribGraph.ScrollMaxY2 = 0D;
      this.NormalDistribGraph.ScrollMinX = 0D;
      this.NormalDistribGraph.ScrollMinY = 0D;
      this.NormalDistribGraph.ScrollMinY2 = 0D;
      this.NormalDistribGraph.Size = new System.Drawing.Size(464, 263);
      this.NormalDistribGraph.TabIndex = 0;
      this.NormalDistribGraph.Load += new System.EventHandler(this.NormalDistribGraph_Load);
      // 
      // panel1
      // 
      this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.panel1.Controls.Add(this.MainMapPicture);
      this.panel1.Location = new System.Drawing.Point(480, 60);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(881, 667);
      this.panel1.TabIndex = 10;
      // 
      // FBurstAnalyse
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(1362, 728);
      this.Controls.Add(this.panel1);
      this.Controls.Add(this.HistoContainer);
      this.Controls.Add(this.ParamGroupBox);
      this.Name = "FBurstAnalyse";
      this.Text = "FBurstAnalyse";
      this.Load += new System.EventHandler(this.FBurstAnalyse_Load);
      ((System.ComponentModel.ISupportInitialize)(this.MainMapPicture)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.StartTime)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.StopTime)).EndInit();
      this.ParamGroupBox.ResumeLayout(false);
      this.ParamGroupBox.PerformLayout();
      this.HistoContainer.ResumeLayout(false);
      this.HistoContainer.PerformLayout();
      this.panel1.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Button GetCollectionBtn;
    private System.Windows.Forms.PictureBox MainMapPicture;
    private System.Windows.Forms.Label label1;
    private System.ComponentModel.BackgroundWorker backgroundWorker1;
    private System.Windows.Forms.Button DrawButton;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.NumericUpDown StartTime;
    private System.Windows.Forms.NumericUpDown StopTime;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.ComboBox FunctionSelector;
    private System.Windows.Forms.GroupBox ParamGroupBox;
    private System.Windows.Forms.Button AnalyseAllDirectBtn;
    private System.Windows.Forms.Label EvokedCountLbl;
    private System.Windows.Forms.Label SpontanousCountLbl;
    private System.Windows.Forms.Button FindClustersBtn;
    private System.Windows.Forms.GroupBox HistoContainer;
    private ZedGraph.ZedGraphControl zeroHypDisribGraph;
    private ZedGraph.ZedGraphControl NormalDistribGraph;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.TextBox ThresholdTB;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.Label progressLabel;
    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.Label durationLabel;
    private ChannelComboBox channelComboBox1;
    private System.Windows.Forms.Button ShowAllPairsInRangeBtn;
    private System.Windows.Forms.TextBox ToValueTB;
    private System.Windows.Forms.TextBox FromValueTB;
    private System.Windows.Forms.CheckBox ShowClustersCheckBox;
  }
}