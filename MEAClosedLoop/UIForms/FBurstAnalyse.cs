﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using MEAClosedLoop.Algorithms;
using MEAClosedLoop.Common;
using ZedGraph;

namespace MEAClosedLoop.UIForms
{
  public partial class FBurstAnalyse : Form
  {
    private List<ulong> BurstIds = new List<ulong>();
    List<List<double>> allData = new List<List<double>>();
    List<List<double>> corrData = new List<List<double>>();
    List<List<double>> corrdata = new List<List<double>>();
    int _ChNum;


    CClasterBuilder clasterBuilder;

    private Timer UpdateProgressTimer = new Timer();
    Stopwatch durationWatch;

    private static string[] Types = new string[3] { "IntegralFx", "dIntegralFx", "SpikeRate" };
    private CharactType FuncType;
    float rectwidth, rectheight;
    bool DoShuffle = false;
    public FBurstAnalyse()
    {
      InitializeComponent();
      channelComboBox1.OnSelectedChannelsChanged += ChannelComboBox1_OnSelectedChannelsChanged;
      _ChNum = channelComboBox1.chIDList.FirstOrDefault();
    }

    private void ChannelComboBox1_OnSelectedChannelsChanged()
    {
      _ChNum = channelComboBox1.chIDList.First();
    }

    private void FBurstAnalyse_Load(object sender, EventArgs e)
    {
      FunctionSelector.Items.Clear();
      FunctionSelector.Items.AddRange(Types);
      FunctionSelector.SelectedIndex = 0;
      DisableUI(ParamGroupBox);
      EnableUI(GetCollectionBtn);
    }

    private void AnalyseAllCollected_Click(object sender, EventArgs e)
    {
      FBurstSelector dialog = new FBurstSelector();
      switch (dialog.ShowDialog())
      {
        case DialogResult.OK:
          break;
        case DialogResult.Cancel:
          break;
      }
      BurstIds.Clear();
      corrdata.Clear();
      BurstIds.AddRange(dialog.result);
      SpontanousCountLbl.Text = "Спонтанные: " + BurstIds.Where(x => CBurstDataProvider.BurstAdrCollection[x].IsEvoked == false).Count();
      EvokedCountLbl.Text = "Вызванные: " + BurstIds.Where(x => CBurstDataProvider.BurstAdrCollection[x].IsEvoked).Count();
      EnableUI(ParamGroupBox);
    }

    private void AnalyseAllDirectBtn_Click(object sender, EventArgs e)
    {
      durationWatch = new Stopwatch();

      UpdateProgressTimer = new Timer();
      UpdateProgressTimer.Interval = 500;
      UpdateProgressTimer.Enabled = true;
      UpdateProgressTimer.Tick += UpdateProgressTimer_Tick;
      UpdateProgressTimer.Start();
      Task asyncAnalysisTask = new Task(AnalyseSyncAllMethod);

      durationWatch.Stop();
      durationWatch.Reset();
      durationWatch.Start();

      asyncAnalysisTask.Start();
    }

    private void AnalyseSyncAllMethod()
    {
      DisableUI(ParamGroupBox);

      //this.BeginInvoke(new Action<int>(x => x = channelComboBox1.chIDList.First()), chNum);

      clasterBuilder = new CClasterBuilder(BurstIds, FuncType, _ChNum, (int)StartTime.Value, (int)StopTime.Value);
      var clusterView = clasterBuilder.AnalyseSynс();
      LoadHistoSafe();

      BeginInvoke(new Action(() => clusterView.Show()));

      UpdateProgressTimer_Tick(this, null);
      EnableUI(ParamGroupBox);

      DrawButton.BeginInvoke(new Action(() => DrawButton.PerformClick()));
      UpdateProgressTimer.Stop();
      BeginInvoke(new Action(() =>
        {
          if (ShowClustersCheckBox.Checked)
            ShowAllClusters();
        }));
      BeginInvoke(new Action(() =>
      {
        durationWatch.Stop();
        durationLabel.Text = "duration: " + (durationWatch.ElapsedMilliseconds / 1000.0) + " sec";
      }));

    }


    void ShowAllClusters()
    {
      //выведем содержимое кластеров
      foreach (CBurstCluster c in clasterBuilder.Clasters)
      {
        //if (c.innerLeafsCount > 2)
        //{
          List<double[]> inclaster = new List<double[]>();
          foreach (int i in clasterBuilder.GetPositions(c.innerIDS))
          {
            inclaster.Add(clasterBuilder.Descriptions[i]);
          }

          FSingleClusterView form = new FSingleClusterView(c.Description, inclaster);
          form.Show();
        //}
      }
    }
    void UpdateProgressTimer_Tick(object sender, EventArgs e)
    {
      string pr = "progress: none";
      if (clasterBuilder != null)
        pr = "progress: " + ((int)clasterBuilder.Progress) + "%";
      if (progressLabel.InvokeRequired)
        progressLabel.BeginInvoke(new Action(() => progressLabel.Text = pr));
      else
        progressLabel.Text = pr;
    }

    private void MainMapPicture_Click(object sender, EventArgs e)
    {
      MouseEventArgs me = (MouseEventArgs)e;
      Point coordinates = me.Location;
      if (rectheight <= 0 || rectwidth <= 0) return;

      int x = (int)(coordinates.X / rectwidth);
      int y = (int)(coordinates.Y / rectheight);
      try
      {
        new FCompareForm(clasterBuilder.Descriptions[x].ToList(), clasterBuilder.Descriptions[y].ToList(), clasterBuilder.MatrixD[x, y].ToString()).Show();
      }
      catch (Exception ex)
      {

      }
    }

    private void DrawButton_Click(object sender, EventArgs e)
    {
      //Bitmap bmp = CorrelationBitmap.DrawTask(clasterBuilder.MatrixD, MainMapPicture.Width, MainMapPicture.Height, out rectwidth, out rectheight);
      //MainMapPicture.Image = bmp;
      //bmp.Save("debug-" + DateTime.Now.TimeOfDay.TotalMinutes + ".png", ImageFormat.Bmp);
    }

    private void FunctionSelector_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (FunctionSelector.SelectedItem.ToString().Equals(Types[0]))
        FuncType = CharactType.IntegralFx;
      if (FunctionSelector.SelectedItem.ToString().Equals(Types[1]))
        FuncType = CharactType.DIntegralFx;
      if (FunctionSelector.SelectedItem.ToString().Equals(Types[2]))
        FuncType = CharactType.SpikeRate;

    }

    private void LoadHistoSafe()
    {
      HistoContainer.BeginInvoke(new Action(() => LoadHisto()));
    }

    private void LoadHisto()
    {

      GraphPane pane = NormalDistribGraph.GraphPane;
      pane.CurveList.Clear();
      pane.XAxis.Title.IsVisible = false;
      pane.YAxis.Title.IsVisible = false;
      pane.XAxis.Scale.Min = 0;
      pane.XAxis.Scale.Max = 1;
      pane.Title.Text = "Распределение количества корреляций";

      PointPairList list = new PointPairList();

      for (int i = 0; i < clasterBuilder.histoDataNormal.Count; i++)
      {
        list.Add(clasterBuilder.histoDataNormal[i].Item1, clasterBuilder.histoDataNormal[i].Item2);
      }
      //LineItem curve = pane.AddCurve(null, list, Color.Blue, SymbolType.None);
      BarItem bar = pane.AddBar("Histo", clasterBuilder.histoDataNormal.Select(x => x.Item1).ToArray(), clasterBuilder.histoDataNormal.Select(x => x.Item2).ToArray(), Color.Blue);

      NormalDistribGraph.AxisChange();
      NormalDistribGraph.Invalidate();
      //***************
      //

      pane = zeroHypDisribGraph.GraphPane;
      pane.CurveList.Clear();
      pane.XAxis.Title.IsVisible = false;
      pane.YAxis.Title.IsVisible = false;
      pane.XAxis.Scale.Min = 0;
      pane.XAxis.Scale.Max = 1;
      pane.Title.Text = "Распределение случайных";
      list = new PointPairList();

      for (int i = 0; i < clasterBuilder.histoDataShuffl.Count; i++)
      {
        list.Add(clasterBuilder.histoDataShuffl[i].Item1, clasterBuilder.histoDataShuffl[i].Item2);
      }
      //LineItem curve = pane.AddCurve(null, list, Color.Blue, SymbolType.None);
      bar = pane.AddBar("Histo", clasterBuilder.histoDataShuffl.Select(x => x.Item1).ToArray(), clasterBuilder.histoDataShuffl.Select(x => x.Item2).ToArray(), Color.Blue);

      zeroHypDisribGraph.AxisChange();
      zeroHypDisribGraph.Invalidate();
      ThresholdTB.Text = clasterBuilder.threshold.ToString();

    }

    private void BuldHistoBtn_Click(object sender, EventArgs e)
    {
      double step = 0.05;
      int[] data = new int[(int)(1 / step)];
      for (int celli = 0; celli < corrData.Count(); celli++)
        for (int cellj = 0; cellj < corrData[celli].Count; cellj++)
        {
          double x = corrData[celli][cellj];
          if (celli != cellj)
            for (int i = 0; i < data.Length; i++)
            {
              if (x > step * i && x <= step * (i + 1))
              {
                data[i]++;
                break;
              }
            }
        }
      List<Tuple<double, double>> histodata = new List<Tuple<double, double>>();
      for (int i = 0; i < data.Length; i++)
      {
        histodata.Add(new Tuple<double, double>(i * step + step / 2, data[i]));
      }
      FHistoView form = new FHistoView(histodata);
      form.DrawData();
      form.Show();
    }

    private void EnableUI(Control control)
    {
      if (control is GroupBox)
        foreach (Control c in ParamGroupBox.Controls)
        {
          if (c.InvokeRequired)
            c.BeginInvoke(new Action<Control>(x => x.Enabled = true), c);
          else
            c.Enabled = true;
        }
      else
      {
        if (control.InvokeRequired)
          control.BeginInvoke(new Action<Control>(x => x.Enabled = true), control);
        else
          control.Enabled = true;
      }
    }

    private void DisableUI(Control control)
    {
      if (control is GroupBox)
        foreach (Control c in ParamGroupBox.Controls)
        {
          if (c.InvokeRequired)
            c.BeginInvoke(new Action<Control>(x => x.Enabled = false), c);
          else
            c.Enabled = false;
        }
      else
      {
        if (control.InvokeRequired)
          control.BeginInvoke(new Action<Control>(x => x.Enabled = false), control);
        else
          control.Enabled = false;
      }
    }

    private void ShuffleList<T>(List<T> list)
    {
      Random rand = new Random();
      for (int i = 0; i < list.Count; i++)
      {
        T tmp = list[i];
        list.RemoveAt(i);
        list.Insert(rand.Next(0, list.Count), tmp);
      }
    }

    private void FindClustersBtn_Click(object sender, EventArgs e)
    {
      FClusterTree form = new FClusterTree(clasterBuilder.Matrix, BurstIds);
      form.Show();
    }

    private void NormalDistribGraph_Load(object sender, EventArgs e)
    {
      NormalDistribGraph.Click += NormalDistribGraph_Click;

    }

    void NormalDistribGraph_Click(object sender, EventArgs e)
    {

    }

    private void zeroHypDisribGraph_Load(object sender, EventArgs e)
    {
      zeroHypDisribGraph.Click += zeroHypDisribGraph_Click;
    }

    private void ShowAllPairsInRangeBtn_Click(object sender, EventArgs e)
    {
      double from, to;
      if (!double.TryParse(FromValueTB.Text, out from) || !double.TryParse(ToValueTB.Text, out to))
        return;

      for (int i = 0; i < BurstIds.Count; i++)
      {
        for (int j = 0; j < i; j++)
        {
          double value = clasterBuilder.MatrixD[i, j];
          if (value >= from && value <= to)
          {
            string title = "Corr=" + value + "; N_1=" + i + "; N_2=" + j;
            new FCompareForm(clasterBuilder.Descriptions[i].ToList(), clasterBuilder.Descriptions[j].ToList(), title).Show();
          }
        }
      }
    }

    void zeroHypDisribGraph_Click(object sender, EventArgs e)
    {

    }

  }
}
