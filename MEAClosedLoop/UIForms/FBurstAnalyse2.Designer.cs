﻿namespace MEAClosedLoop.UIForms
{
  partial class FBurstAnalyse2
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.DurationZedGraph = new ZedGraph.ZedGraphControl();
      this.IntervalZedGraph = new ZedGraph.ZedGraphControl();
      this.BtnPhasePortret = new System.Windows.Forms.Button();
      this.groupBox3 = new System.Windows.Forms.GroupBox();
      this.RBMiddle = new System.Windows.Forms.RadioButton();
      this.RBRight = new System.Windows.Forms.RadioButton();
      this.RBLeft = new System.Windows.Forms.RadioButton();
      this.groupBox3.SuspendLayout();
      this.SuspendLayout();
      // 
      // DurationZedGraph
      // 
      this.DurationZedGraph.IsAntiAlias = true;
      this.DurationZedGraph.IsEnableSelection = true;
      this.DurationZedGraph.Location = new System.Drawing.Point(10, 11);
      this.DurationZedGraph.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
      this.DurationZedGraph.Name = "DurationZedGraph";
      this.DurationZedGraph.ScrollGrace = 0D;
      this.DurationZedGraph.ScrollMaxX = 0D;
      this.DurationZedGraph.ScrollMaxY = 0D;
      this.DurationZedGraph.ScrollMaxY2 = 0D;
      this.DurationZedGraph.ScrollMinX = 0D;
      this.DurationZedGraph.ScrollMinY = 0D;
      this.DurationZedGraph.ScrollMinY2 = 0D;
      this.DurationZedGraph.Size = new System.Drawing.Size(888, 336);
      this.DurationZedGraph.TabIndex = 4;
      // 
      // IntervalZedGraph
      // 
      this.IntervalZedGraph.IsAntiAlias = true;
      this.IntervalZedGraph.IsEnableSelection = true;
      this.IntervalZedGraph.Location = new System.Drawing.Point(10, 355);
      this.IntervalZedGraph.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
      this.IntervalZedGraph.Name = "IntervalZedGraph";
      this.IntervalZedGraph.ScrollGrace = 0D;
      this.IntervalZedGraph.ScrollMaxX = 0D;
      this.IntervalZedGraph.ScrollMaxY = 0D;
      this.IntervalZedGraph.ScrollMaxY2 = 0D;
      this.IntervalZedGraph.ScrollMinX = 0D;
      this.IntervalZedGraph.ScrollMinY = 0D;
      this.IntervalZedGraph.ScrollMinY2 = 0D;
      this.IntervalZedGraph.Size = new System.Drawing.Size(888, 349);
      this.IntervalZedGraph.TabIndex = 4;
      // 
      // BtnPhasePortret
      // 
      this.BtnPhasePortret.Location = new System.Drawing.Point(5, 75);
      this.BtnPhasePortret.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
      this.BtnPhasePortret.Name = "BtnPhasePortret";
      this.BtnPhasePortret.Size = new System.Drawing.Size(142, 21);
      this.BtnPhasePortret.TabIndex = 5;
      this.BtnPhasePortret.Text = "Фазовый портрет";
      this.BtnPhasePortret.UseVisualStyleBackColor = true;
      this.BtnPhasePortret.Click += new System.EventHandler(this.BtnPhasePortret_Click);
      // 
      // groupBox3
      // 
      this.groupBox3.Controls.Add(this.BtnPhasePortret);
      this.groupBox3.Controls.Add(this.RBMiddle);
      this.groupBox3.Controls.Add(this.RBRight);
      this.groupBox3.Controls.Add(this.RBLeft);
      this.groupBox3.Location = new System.Drawing.Point(903, 11);
      this.groupBox3.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
      this.groupBox3.Name = "groupBox3";
      this.groupBox3.Padding = new System.Windows.Forms.Padding(1, 1, 1, 1);
      this.groupBox3.Size = new System.Drawing.Size(159, 104);
      this.groupBox3.TabIndex = 6;
      this.groupBox3.TabStop = false;
      this.groupBox3.Text = "Тип интервала";
      // 
      // RBMiddle
      // 
      this.RBMiddle.AutoSize = true;
      this.RBMiddle.Location = new System.Drawing.Point(5, 56);
      this.RBMiddle.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
      this.RBMiddle.Name = "RBMiddle";
      this.RBMiddle.Size = new System.Drawing.Size(68, 17);
      this.RBMiddle.TabIndex = 0;
      this.RBMiddle.Text = "Средний";
      this.RBMiddle.UseVisualStyleBackColor = true;
      this.RBMiddle.CheckedChanged += new System.EventHandler(this.RBMiddle_CheckedChanged);
      // 
      // RBRight
      // 
      this.RBRight.AutoSize = true;
      this.RBRight.Checked = true;
      this.RBRight.Location = new System.Drawing.Point(5, 39);
      this.RBRight.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
      this.RBRight.Name = "RBRight";
      this.RBRight.Size = new System.Drawing.Size(65, 17);
      this.RBRight.TabIndex = 0;
      this.RBRight.TabStop = true;
      this.RBRight.Text = "Правый";
      this.RBRight.UseVisualStyleBackColor = true;
      this.RBRight.CheckedChanged += new System.EventHandler(this.RBRight_CheckedChanged);
      // 
      // RBLeft
      // 
      this.RBLeft.AutoSize = true;
      this.RBLeft.Location = new System.Drawing.Point(5, 21);
      this.RBLeft.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
      this.RBLeft.Name = "RBLeft";
      this.RBLeft.Size = new System.Drawing.Size(59, 17);
      this.RBLeft.TabIndex = 0;
      this.RBLeft.Text = "Левый";
      this.RBLeft.UseVisualStyleBackColor = true;
      this.RBLeft.CheckedChanged += new System.EventHandler(this.RBLeft_CheckedChanged);
      // 
      // FBurstAnalyse2
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(1207, 721);
      this.Controls.Add(this.groupBox3);
      this.Controls.Add(this.IntervalZedGraph);
      this.Controls.Add(this.DurationZedGraph);
      this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
      this.Name = "FBurstAnalyse2";
      this.Text = "FBurstAnalyse2";
      this.Load += new System.EventHandler(this.FBurstAnalyse2_Load);
      this.groupBox3.ResumeLayout(false);
      this.groupBox3.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion
    private ZedGraph.ZedGraphControl DurationZedGraph;
    private ZedGraph.ZedGraphControl IntervalZedGraph;
    private System.Windows.Forms.Button BtnPhasePortret;
    private System.Windows.Forms.GroupBox groupBox3;
    private System.Windows.Forms.RadioButton RBMiddle;
    private System.Windows.Forms.RadioButton RBRight;
    private System.Windows.Forms.RadioButton RBLeft;
  }
}