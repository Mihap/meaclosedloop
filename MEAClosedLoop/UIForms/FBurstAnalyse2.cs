﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ZedGraph;
using MEAClosedLoop.UIForms.SubForms;
namespace MEAClosedLoop.UIForms
{
  public partial class FBurstAnalyse2 : Form
  {
    List<ulong> stims = new List<ulong>();
    object stimsLock = new object();
    Timer updateDurationGraph;
    Timer updateIntervalGraph;
    public FBurstAnalyse2()
    {
      InitializeComponent();
    }
    public void RecieveStim(List<ulong> stim)
    {
      lock (stimsLock)
      {
        stims.AddRange(stim);
      }
    }
    public void RedrawGraphsDuration()
    {

    }
    public void RedrawGraphsInterval()
    {

    }

    private void FBurstAnalyse2_Load(object sender, EventArgs e)
    {
      updateDurationGraph = new Timer();
      updateIntervalGraph = new Timer();
      updateDurationGraph.Interval = 2000;
      updateIntervalGraph.Interval = 2000;
      updateDurationGraph.Tick += UpdateDurationGraph_Tick;
      updateIntervalGraph.Tick += UpdateIntervalGraph_Tick;

      updateDurationGraph.Start();
      updateIntervalGraph.Start();
    }

    private void UpdateIntervalGraph_Tick(object sender, EventArgs e)
    {
      GraphPane pane = IntervalZedGraph.GraphPane;
      if (pane == null) return;

      pane.CurveList.Clear();

      PointPairList Intervallist = new PointPairList();
      List<PointPairList> stimlist = new List<PointPairList>();
      //int timeAxis = pane.AddYAxis("time, sec");
      pane.XAxis.Title.Text = "Время, с";
      pane.Title.Text = "Межпачечные интервалы";
      pane.YAxis.IsVisible = true;
      pane.YAxis.Title.Text = "средний интервал между пачками, мс";
      pane.IsFontsScaled = false;
      //pane.BaseDimension = 1.5f;

      for (int i = 0; i < Global.stimsDataProvider.Stims.Count(); i++)
        stimlist.Add(new PointPairList() {
          new PointPair(Global.stimsDataProvider.Stims[i] / 25000.0, 1000),
          new PointPair(Global.stimsDataProvider.Stims[i] / 25000.0, 2000)});
      if (RBRight.Checked)
        for (int i = 0; i < CBurstDataProvider.BurstAdrCollection.Count - 1; i++)
        {
          ulong id = CBurstDataProvider.BurstAdrCollection.Keys.ElementAt(i);
          ulong nextid = CBurstDataProvider.BurstAdrCollection.Keys.ElementAt(i + 1);
          Intervallist.Add(new PointPairList() {
        new PointPair(
          CBurstDataProvider.BurstAdrCollection[nextid].BeginTime/25000.0,
          (CBurstDataProvider.BurstAdrCollection[nextid].BeginTime - CBurstDataProvider.BurstAdrCollection[id].BeginTime)/25)});
        }
      if (RBLeft.Checked)
        for (int i = 1; i < CBurstDataProvider.BurstAdrCollection.Count; i++)
        {
          ulong id = CBurstDataProvider.BurstAdrCollection.Keys.ElementAt(i - 1);
          ulong nextid = CBurstDataProvider.BurstAdrCollection.Keys.ElementAt(i);
          Intervallist.Add(new PointPairList() {
        new PointPair(
          CBurstDataProvider.BurstAdrCollection[id].BeginTime/25000.0,
          (CBurstDataProvider.BurstAdrCollection[nextid].BeginTime - CBurstDataProvider.BurstAdrCollection[id].BeginTime)/25)});
        }
      if (RBMiddle.Checked)
        for (int i = 1; i < CBurstDataProvider.BurstAdrCollection.Count - 1; i++)
        {
          ulong previd = CBurstDataProvider.BurstAdrCollection.Keys.ElementAt(i - 1);
          ulong id = CBurstDataProvider.BurstAdrCollection.Keys.ElementAt(i);
          ulong nextid = CBurstDataProvider.BurstAdrCollection.Keys.ElementAt(i + 1);
          double prevInterval = CBurstDataProvider.BurstAdrCollection[id].BeginTime / 25 - CBurstDataProvider.BurstAdrCollection[previd].BeginTime / 25;
          double postInterval = CBurstDataProvider.BurstAdrCollection[nextid].BeginTime / 25 - CBurstDataProvider.BurstAdrCollection[id].BeginTime / 25;

          Intervallist.Add(new PointPairList() {
        new PointPair(
          CBurstDataProvider.BurstAdrCollection[id].BeginTime/25000.0,
          (postInterval + prevInterval) * 0.5)});
        }
      //LineItem timeCurve = pane.AddCurve("Time, sec", timelist, Color.Blue, SymbolType.Triangle);
      foreach (PointPairList ppl in stimlist)
        pane.AddCurve("", ppl, Color.Black);

      LineItem durationCurve = pane.AddCurve("", Intervallist, Color.Blue, SymbolType.Square);
      //durationCurve.Line.IsSmooth = true;

      pane.YAxis.Scale.Min = 0;

      IntervalZedGraph.AxisChange();
      IntervalZedGraph.Invalidate();
    }

    private void UpdateDurationGraph_Tick(object sender, EventArgs e)
    {
      GraphPane pane = DurationZedGraph.GraphPane;
      if (pane == null) return;

      pane.CurveList.Clear();

      PointPairList Durationlist = new PointPairList();
      List<PointPairList> stimlist = new List<PointPairList>();
      //int timeAxis = pane.AddYAxis("time, sec");
      pane.XAxis.Title.Text = "Время, с";
      pane.Title.Text = "Длительность пачек";
      pane.YAxis.IsVisible = true;
      pane.YAxis.Title.Text = "длительность, мс";
      pane.IsFontsScaled = false;
      //pane.BaseDimension = 1.5f;

      for (int i = 0; i < Global.stimsDataProvider.Stims.Count(); i++)
        stimlist.Add(new PointPairList() {
          new PointPair(Global.stimsDataProvider.Stims[i] / 25000.0, 1000),
          new PointPair(Global.stimsDataProvider.Stims[i] / 25000.0, 2000)});
      for (int i = 0; i < CBurstDataProvider.BurstAdrCollection.Count; i++)
      {
        ulong id = CBurstDataProvider.BurstAdrCollection.Keys.ElementAt(i);
        Durationlist.Add(new PointPairList() {
        new PointPair( CBurstDataProvider.BurstAdrCollection[id].BeginTime/25000.0, CBurstDataProvider.BurstAdrCollection[id].DurationTime/25)});
      }
      //LineItem timeCurve = pane.AddCurve("Time, sec", timelist, Color.Blue, SymbolType.Triangle);
      foreach (PointPairList ppl in stimlist)
        pane.AddCurve("", ppl, Color.Black);

      LineItem durationCurve = pane.AddCurve("", Durationlist, Color.Blue, SymbolType.Square);
      //durationCurve.Line.IsSmooth = true;

      pane.YAxis.Scale.Min = 0;

      DurationZedGraph.AxisChange();
      DurationZedGraph.Invalidate();
    }

    private void RBMiddle_CheckedChanged(object sender, EventArgs e)
    {
      UpdateIntervalGraph_Tick(sender, e);
    }

    private void RBLeft_CheckedChanged(object sender, EventArgs e)
    {
      UpdateIntervalGraph_Tick(sender, e);
    }

    private void RBRight_CheckedChanged(object sender, EventArgs e)
    {
      UpdateIntervalGraph_Tick(sender, e);
    }

    private void BtnPhasePortret_Click(object sender, EventArgs e)
    {
      if (RBLeft.Checked)
        new FPhasePortret(BurstPhaseMode.Left).Show();
      if (RBRight.Checked)
        new FPhasePortret(BurstPhaseMode.Right).Show();
      if (RBMiddle.Checked)
        new FPhasePortret(BurstPhaseMode.Middle).Show();
    }
  }
}
