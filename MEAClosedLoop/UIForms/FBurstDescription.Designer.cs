﻿namespace MEAClosedLoop
{
  partial class FBurstDescription
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
      this.label2 = new System.Windows.Forms.Label();
      this.PlotGraph = new ZedGraph.ZedGraphControl();
      ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
      this.SuspendLayout();
      // 
      // numericUpDown1
      // 
      this.numericUpDown1.Location = new System.Drawing.Point(131, 9);
      this.numericUpDown1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
      this.numericUpDown1.Name = "numericUpDown1";
      this.numericUpDown1.Size = new System.Drawing.Size(87, 22);
      this.numericUpDown1.TabIndex = 3;
      this.numericUpDown1.Value = new decimal(new int[] {
            41,
            0,
            0,
            0});
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(16, 11);
      this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(102, 17);
      this.label2.TabIndex = 1;
      this.label2.Text = "Номер канала";
      // 
      // PlotGraph
      // 
      this.PlotGraph.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.PlotGraph.IsAntiAlias = true;
      this.PlotGraph.Location = new System.Drawing.Point(16, 41);
      this.PlotGraph.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
      this.PlotGraph.Name = "PlotGraph";
      this.PlotGraph.ScrollGrace = 0D;
      this.PlotGraph.ScrollMaxX = 0D;
      this.PlotGraph.ScrollMaxY = 0D;
      this.PlotGraph.ScrollMaxY2 = 0D;
      this.PlotGraph.ScrollMinX = 0D;
      this.PlotGraph.ScrollMinY = 0D;
      this.PlotGraph.ScrollMinY2 = 0D;
      this.PlotGraph.Size = new System.Drawing.Size(1304, 370);
      this.PlotGraph.TabIndex = 2;
      // 
      // FBurstDescription
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(1336, 426);
      this.Controls.Add(this.numericUpDown1);
      this.Controls.Add(this.PlotGraph);
      this.Controls.Add(this.label2);
      this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
      this.Name = "FBurstDescription";
      this.Text = "FBurstDescription";
      this.Load += new System.EventHandler(this.FBurstDescription_Load);
      ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.NumericUpDown numericUpDown1;
    private System.Windows.Forms.Label label2;
    private ZedGraph.ZedGraphControl PlotGraph;
  }
}