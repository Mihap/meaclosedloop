﻿#define NORMALISE
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ZedGraph;
namespace MEAClosedLoop
{
  public partial class FBurstDescription : Form, IRecieveBurst
  {
    object PlotLock = new object();
    public FBurstDescription()
    {
      InitializeComponent();
    }
    void IRecieveBurst.RecieveBurst(CBurst Burst)
    {
      lock (PlotLock)
        try
        {
          GraphPane pane = PlotGraph.GraphPane;
          pane.CurveList.Clear();
          pane.Legend.IsVisible = false;
          pane.Title.Text = "Burst Description";
          pane.XAxis.Title.Text = "Time, ms";
          pane.YAxis.Title.Text = "U, mV";
          PointPairList f1_list = new PointPairList();

          // Создадим список точек для кривой f2(x)
          PointPairList f2_list = new PointPairList();
          // Создадим список точек для кривой f2(x)
          PointPairList f3_list = new PointPairList();
          int ch = (int) numericUpDown1.Value;
          Burst.BuildDescription(0, 1500 * Param.MS, chNum: -1, windowWidth: 20*Param.MS, Decrease:400, normalise: false, type: CharactType.SpikeRate);

          for (int i = 1; i < Burst.description[ch].Length; i++)
          {
            f3_list.Add(i * 20, Burst.description[ch][i] * 5);
          }

          for (int i = 0; i < Burst.Data[ch].Length; i += 1)
          {
            f1_list.Add(i / 25.0, Burst.Data[ch][i]);
          }

          LineItem f2_curve = pane.AddCurve("Integral after Mooveing Avarage", f3_list, Color.Red, SymbolType.None);
          //LineItem f3_curve = pane.AddCurve("Integral", f3_list, Color.Green, SymbolType.None);
          LineItem f1_curve = pane.AddCurve("Raw Burst", f1_list, Color.FromArgb(100, 70,70,70), SymbolType.None);
            
          // Вызываем метод AxisChange (), чтобы обновить данные об осях. 
          // В противном случае на рисунке будет показана только часть графика, 
          // которая умещается в интервалы по осям, установленные по умолчанию
          PlotGraph.AxisChange();

          // Обновляем график
          PlotGraph.Invalidate();
        }
        catch (Exception e)
        {
        }
    }

    private void FBurstDescription_Load(object sender, EventArgs e)
    {
      PlotGraph.GraphPane.YAxis.Scale.Min = 0;
      PlotGraph.GraphPane.YAxis.Scale.Max = 510;
    }

    private void numericUpDown2_ValueChanged(object sender, EventArgs e)
    {
    }

  }
  public enum DecsriptionWay
  {
    Integral,
    MoovingAverage,
    FlowFreq
  }
}
