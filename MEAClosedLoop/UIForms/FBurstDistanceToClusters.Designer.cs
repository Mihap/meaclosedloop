﻿namespace MEAClosedLoop.UIForms
{
  partial class FBurstDistanceToClusters
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.startButton = new System.Windows.Forms.Button();
      this.mainPanel = new System.Windows.Forms.FlowLayoutPanel();
      this.groupBox2 = new System.Windows.Forms.GroupBox();
      this.clusterCntTb = new System.Windows.Forms.TextBox();
      this.burstIdTb = new System.Windows.Forms.TextBox();
      this.groupBox1 = new System.Windows.Forms.GroupBox();
      this.randomBurstButton = new System.Windows.Forms.Button();
      this.burstModeRB = new System.Windows.Forms.RadioButton();
      this.clustModeRB = new System.Windows.Forms.RadioButton();
      this.secondBurstTb = new System.Windows.Forms.TextBox();
      this.groupBox3 = new System.Windows.Forms.GroupBox();
      this.endIndexTb = new System.Windows.Forms.TextBox();
      this.groupBox4 = new System.Windows.Forms.GroupBox();
      this.channelComboBox1 = new MEAClosedLoop.UIForms.ChannelComboBox();
      this.groupBox5 = new System.Windows.Forms.GroupBox();
      this.groupBox2.SuspendLayout();
      this.groupBox1.SuspendLayout();
      this.groupBox3.SuspendLayout();
      this.groupBox4.SuspendLayout();
      this.groupBox5.SuspendLayout();
      this.SuspendLayout();
      // 
      // startButton
      // 
      this.startButton.Location = new System.Drawing.Point(387, 16);
      this.startButton.Name = "startButton";
      this.startButton.Size = new System.Drawing.Size(90, 31);
      this.startButton.TabIndex = 1;
      this.startButton.Text = "Отрисовать";
      this.startButton.UseVisualStyleBackColor = true;
      this.startButton.Click += new System.EventHandler(this.startButton_Click);
      // 
      // mainPanel
      // 
      this.mainPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.mainPanel.AutoScroll = true;
      this.mainPanel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
      this.mainPanel.Location = new System.Drawing.Point(13, 58);
      this.mainPanel.Name = "mainPanel";
      this.mainPanel.Size = new System.Drawing.Size(892, 534);
      this.mainPanel.TabIndex = 5;
      this.mainPanel.WrapContents = false;
      // 
      // groupBox2
      // 
      this.groupBox2.Controls.Add(this.clusterCntTb);
      this.groupBox2.Location = new System.Drawing.Point(237, 7);
      this.groupBox2.Name = "groupBox2";
      this.groupBox2.Size = new System.Drawing.Size(112, 45);
      this.groupBox2.TabIndex = 9;
      this.groupBox2.TabStop = false;
      this.groupBox2.Text = "Кол-во кластеров";
      // 
      // clusterCntTb
      // 
      this.clusterCntTb.Location = new System.Drawing.Point(39, 15);
      this.clusterCntTb.Name = "clusterCntTb";
      this.clusterCntTb.Size = new System.Drawing.Size(31, 20);
      this.clusterCntTb.TabIndex = 0;
      this.clusterCntTb.Text = "10";
      // 
      // burstIdTb
      // 
      this.burstIdTb.Location = new System.Drawing.Point(6, 15);
      this.burstIdTb.Name = "burstIdTb";
      this.burstIdTb.Size = new System.Drawing.Size(127, 20);
      this.burstIdTb.TabIndex = 2;
      // 
      // groupBox1
      // 
      this.groupBox1.Controls.Add(this.burstIdTb);
      this.groupBox1.Controls.Add(this.randomBurstButton);
      this.groupBox1.Location = new System.Drawing.Point(13, 7);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new System.Drawing.Size(218, 45);
      this.groupBox1.TabIndex = 8;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "Номер пачки";
      // 
      // randomBurstButton
      // 
      this.randomBurstButton.Location = new System.Drawing.Point(137, 13);
      this.randomBurstButton.Name = "randomBurstButton";
      this.randomBurstButton.Size = new System.Drawing.Size(75, 23);
      this.randomBurstButton.TabIndex = 6;
      this.randomBurstButton.Text = "Рандом пачка";
      this.randomBurstButton.UseVisualStyleBackColor = true;
      this.randomBurstButton.Click += new System.EventHandler(this.randomBurstButton_Click);
      // 
      // burstModeRB
      // 
      this.burstModeRB.AutoSize = true;
      this.burstModeRB.Location = new System.Drawing.Point(502, 30);
      this.burstModeRB.Name = "burstModeRB";
      this.burstModeRB.Size = new System.Drawing.Size(69, 17);
      this.burstModeRB.TabIndex = 4;
      this.burstModeRB.Text = "с пачкой";
      this.burstModeRB.UseVisualStyleBackColor = true;
      // 
      // clustModeRB
      // 
      this.clustModeRB.AutoSize = true;
      this.clustModeRB.Checked = true;
      this.clustModeRB.Location = new System.Drawing.Point(502, 11);
      this.clustModeRB.Name = "clustModeRB";
      this.clustModeRB.Size = new System.Drawing.Size(95, 17);
      this.clustModeRB.TabIndex = 3;
      this.clustModeRB.TabStop = true;
      this.clustModeRB.Text = "с кластерами";
      this.clustModeRB.UseVisualStyleBackColor = true;
      // 
      // secondBurstTb
      // 
      this.secondBurstTb.Location = new System.Drawing.Point(6, 15);
      this.secondBurstTb.Name = "secondBurstTb";
      this.secondBurstTb.Size = new System.Drawing.Size(132, 20);
      this.secondBurstTb.TabIndex = 0;
      // 
      // groupBox3
      // 
      this.groupBox3.Controls.Add(this.secondBurstTb);
      this.groupBox3.Location = new System.Drawing.Point(603, 7);
      this.groupBox3.Name = "groupBox3";
      this.groupBox3.Size = new System.Drawing.Size(144, 45);
      this.groupBox3.TabIndex = 10;
      this.groupBox3.TabStop = false;
      this.groupBox3.Text = "Номер второй пачки";
      // 
      // endIndexTb
      // 
      this.endIndexTb.Location = new System.Drawing.Point(18, 15);
      this.endIndexTb.Name = "endIndexTb";
      this.endIndexTb.Size = new System.Drawing.Size(41, 20);
      this.endIndexTb.TabIndex = 0;
      this.endIndexTb.Text = "300";
      // 
      // groupBox4
      // 
      this.groupBox4.Controls.Add(this.endIndexTb);
      this.groupBox4.Location = new System.Drawing.Point(753, 7);
      this.groupBox4.Name = "groupBox4";
      this.groupBox4.Size = new System.Drawing.Size(65, 45);
      this.groupBox4.TabIndex = 11;
      this.groupBox4.TabStop = false;
      this.groupBox4.Text = "Стоп, мс";
      // 
      // channelComboBox1
      // 
      this.channelComboBox1.Location = new System.Drawing.Point(6, 13);
      this.channelComboBox1.Name = "channelComboBox1";
      this.channelComboBox1.Padding = new System.Windows.Forms.Padding(3);
      this.channelComboBox1.Size = new System.Drawing.Size(65, 29);
      this.channelComboBox1.TabIndex = 0;
      this.channelComboBox1.OnSelectedChannelsChanged += new MEAClosedLoop.UIForms.OnSelectedChannelsChangedDelegate(this.channelComboBox1_OnSelectedChannelsChanged);
      // 
      // groupBox5
      // 
      this.groupBox5.Controls.Add(this.channelComboBox1);
      this.groupBox5.Location = new System.Drawing.Point(825, 7);
      this.groupBox5.Name = "groupBox5";
      this.groupBox5.Size = new System.Drawing.Size(79, 45);
      this.groupBox5.TabIndex = 12;
      this.groupBox5.TabStop = false;
      this.groupBox5.Text = "Канал";
      // 
      // FBurstDistanceToClusters
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(917, 604);
      this.Controls.Add(this.groupBox5);
      this.Controls.Add(this.groupBox4);
      this.Controls.Add(this.groupBox3);
      this.Controls.Add(this.groupBox2);
      this.Controls.Add(this.groupBox1);
      this.Controls.Add(this.mainPanel);
      this.Controls.Add(this.burstModeRB);
      this.Controls.Add(this.clustModeRB);
      this.Controls.Add(this.startButton);
      this.Name = "FBurstDistanceToClusters";
      this.Text = "FBurstDistanceToClusters";
      this.groupBox2.ResumeLayout(false);
      this.groupBox2.PerformLayout();
      this.groupBox1.ResumeLayout(false);
      this.groupBox1.PerformLayout();
      this.groupBox3.ResumeLayout(false);
      this.groupBox3.PerformLayout();
      this.groupBox4.ResumeLayout(false);
      this.groupBox4.PerformLayout();
      this.groupBox5.ResumeLayout(false);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion
    private System.Windows.Forms.Button startButton;
    private System.Windows.Forms.FlowLayoutPanel mainPanel;
    private System.Windows.Forms.GroupBox groupBox2;
    private System.Windows.Forms.TextBox clusterCntTb;
    private System.Windows.Forms.TextBox burstIdTb;
    private System.Windows.Forms.GroupBox groupBox1;
    private System.Windows.Forms.Button randomBurstButton;
    private System.Windows.Forms.RadioButton burstModeRB;
    private System.Windows.Forms.RadioButton clustModeRB;
    private System.Windows.Forms.TextBox secondBurstTb;
    private System.Windows.Forms.GroupBox groupBox3;
    private System.Windows.Forms.TextBox endIndexTb;
    private System.Windows.Forms.GroupBox groupBox4;
    private ChannelComboBox channelComboBox1;
    private System.Windows.Forms.GroupBox groupBox5;
  }
}