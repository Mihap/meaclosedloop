﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MEAClosedLoop.Algorithms;

namespace MEAClosedLoop.UIForms
{
  public partial class FBurstDistanceToClusters : Form
  {
    private CClasterBuilder ClusterBuilder
    {
      get
      {
        var cb = CClusterProvider.ClasterBuilder;
        if (cb?.Clasters == null || !cb.Clasters.Any() || !cb.BurstIds.Any())
          throw new Exception("Сначала нужно провести кластеризацию");
        return cb;
      }
    }

    private int ClusterCount
    {
      get
      {
        int n;
        if (!int.TryParse(clusterCntTb.Text, out n) || n < 1)
          throw new ArgumentException("Количество кластеров не задано");
        return n;
      }
    }

    private int _chNum = 10;

    public FBurstDistanceToClusters()
    {
      InitializeComponent();
    }

    private void startButton_Click(object sender, EventArgs e)
    {
      try
      {
        ulong id;
        if (!ulong.TryParse(burstIdTb.Text, out id))
          throw new ArgumentException("Номер пачки не введён / введён некорректно");

        var burst = CBurstDataProvider.GetBurst(id);
        if (burst == null)
          throw new Exception("Ошибка при чтении пачки");

        if (clustModeRB.Checked)
        {
          CompareBurstWithClusters(burst, id);
        }
        else
        {
          CompareBurstWithBurst(burst);
        }

      }
      catch (Exception exception)
      {
        MessageBox.Show(exception.Message);
      }
    }

    private void CompareBurstWithBurst(CBurst burst)
    {
      ulong id2;
      if (!ulong.TryParse(secondBurstTb.Text, out id2))
        throw new ArgumentException("Номер ВТОРОЙ пачки не введён / введён некорректно");

      var burst2 = CBurstDataProvider.GetBurst(id2);
      if (burst2 == null)
        throw new Exception("Ошибка при чтении ВТОРОЙ пачки");

      int endIndex;
      if (!int.TryParse(endIndexTb.Text, out endIndex))
        throw new Exception("Длительность введена не корректно");

      CharactType funcType = default(CharactType);
      burst.BuildDescription(0, endIndex * Param.MS, _chNum, 40, 460, true, funcType);
      burst2.BuildDescription(0, endIndex * Param.MS, _chNum, 40, 460, true, funcType);

      mainPanel.Controls.Clear();
      var corr = CorrelationCounter.crossCorrelation(burst.description[_chNum], burst2.description[_chNum],
        ZeroShift: funcType == CharactType.SpikeRate);
      var title = $"Corr={corr:0.0000}";
      var cmpForm = new FCompareForm(burst.description[_chNum], burst2.description[_chNum], title)
      { FormBorderStyle = FormBorderStyle.None, TopLevel = false, Visible = true };
      mainPanel.Controls.Add(cmpForm);
    }

    private void CompareBurstWithClusters(CBurst burst, ulong id)
    {
      //var burstClusterId = ClusterBuilder.Clasters.Single(x => x.innerIDS.Contains(id)).ID;
      burst.BuildDescription(ClusterBuilder.startIndex * Param.MS, ClusterBuilder.endIndex * Param.MS,
        ClusterBuilder.ch, 40, 460, true, ClusterBuilder.FuncType);
      if (ClusterBuilder.FuncType == CharactType.SpikeRate)
        burst.BuildDescription(ClusterBuilder.startIndex * Param.MS, ClusterBuilder.endIndex * Param.MS,
          ClusterBuilder.ch, 20 * Param.MS, 460, true, ClusterBuilder.FuncType);

      mainPanel.Controls.Clear();
      var clusters = ClusterBuilder.Clasters.OrderBy(x => x.innerIDS.Count).Reverse().ToList();
      for (int i = 0; i < ClusterCount; i++)
      {
        var cluster = clusters[i];
        var corr = CorrelationCounter.crossCorrelation(cluster.Description, burst.description[ClusterBuilder.ch],
          ZeroShift: ClusterBuilder.FuncType == CharactType.SpikeRate);

        var title = $"Кластер №{cluster.ID} ({cluster.innerIDS.Count} пс). Corr={corr:0.0000}";
        var cmpForm = new FCompareForm(cluster.Description, burst.description[ClusterBuilder.ch],
            cluster.innerIDS.Contains(id) ? "ТЕКУЩИЙ: " + title : title)
        { FormBorderStyle = FormBorderStyle.None, TopLevel = false, Visible = true };

        mainPanel.Controls.Add(cmpForm);
      }
    }

    private void randomBurstButton_Click(object sender, EventArgs e)
    {
      try
      {
        //Случайная пачка из первых N кластеров по кол-ву
        List<ulong> burstIds = new List<ulong>();
        var clusters = ClusterBuilder.Clasters.OrderBy(x => x.innerIDS.Count).Reverse().ToList();
        for (int i = 0; i < ClusterCount; i++)
        {
          var cluster = clusters[i];
          burstIds.AddRange(cluster.innerIDS);
        }
        burstIdTb.Text = burstIds[new Random().Next(burstIds.Count)].ToString();
      }
      catch (Exception exception)
      {
        MessageBox.Show(exception.Message);
      }
    }

    private void channelComboBox1_OnSelectedChannelsChanged()
    {
      _chNum = channelComboBox1.chIDList.First();
    }
  }
}
