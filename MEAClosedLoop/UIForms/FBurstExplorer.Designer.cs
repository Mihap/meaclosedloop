﻿namespace MEAClosedLoop.UIForms
{
  partial class FBurstExplorer
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.dataGraphControl = new ZedGraph.ZedGraphControl();
      this.Amplitude = new System.Windows.Forms.ComboBox();
      this.label1 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.prevBurstBtn = new System.Windows.Forms.Button();
      this.nextBurstBtn = new System.Windows.Forms.Button();
      this.BurstSlider = new System.Windows.Forms.TrackBar();
      this.crntBurstTB = new System.Windows.Forms.TextBox();
      this.label3 = new System.Windows.Forms.Label();
      this.drawDescriptionCheckBox = new System.Windows.Forms.CheckBox();
      this.refreshBurstsBtn = new System.Windows.Forms.Button();
      this.PatternGB = new System.Windows.Forms.GroupBox();
      this.sprdRadioButton = new System.Windows.Forms.RadioButton();
      this.freqRadioButton = new System.Windows.Forms.RadioButton();
      this.cancelBtn = new System.Windows.Forms.Button();
      this.SelectBtn = new System.Windows.Forms.Button();
      this.refreshPatternsBtn = new System.Windows.Forms.Button();
      this.patternsComboBox = new System.Windows.Forms.ComboBox();
      this.label4 = new System.Windows.Forms.Label();
      this.singePatternCheckBox = new System.Windows.Forms.CheckBox();
      this.Duration = new System.Windows.Forms.ComboBox();
      this.label5 = new System.Windows.Forms.Label();
      this.label6 = new System.Windows.Forms.Label();
      this.label7 = new System.Windows.Forms.Label();
      this.groupBox1 = new System.Windows.Forms.GroupBox();
      this.BurstNumberInListTB = new System.Windows.Forms.TextBox();
      this.label10 = new System.Windows.Forms.Label();
      this.StartTimeTB = new System.Windows.Forms.TextBox();
      this.DurationTB = new System.Windows.Forms.TextBox();
      this.label9 = new System.Windows.Forms.Label();
      this.label8 = new System.Windows.Forms.Label();
      this.groupBox2 = new System.Windows.Forms.GroupBox();
      this.label12 = new System.Windows.Forms.Label();
      this.label11 = new System.Windows.Forms.Label();
      this.ExportCurrentBtn = new System.Windows.Forms.Button();
      this.DrawVectorsCheckBox = new System.Windows.Forms.CheckBox();
      this.DrawingPictureBox = new System.Windows.Forms.PictureBox();
      this.GradientPictureBox = new System.Windows.Forms.PictureBox();
      this.groupBox3 = new System.Windows.Forms.GroupBox();
      this.maxValueTextBox = new System.Windows.Forms.TextBox();
      this.channelSelector = new MEAClosedLoop.UIForms.ChannelComboBox();
      this.VectorDrawnMethodComboBox = new System.Windows.Forms.ComboBox();
      ((System.ComponentModel.ISupportInitialize)(this.BurstSlider)).BeginInit();
      this.PatternGB.SuspendLayout();
      this.groupBox1.SuspendLayout();
      this.groupBox2.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.DrawingPictureBox)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.GradientPictureBox)).BeginInit();
      this.groupBox3.SuspendLayout();
      this.SuspendLayout();
      // 
      // dataGraphControl
      // 
      this.dataGraphControl.Location = new System.Drawing.Point(12, 12);
      this.dataGraphControl.Margin = new System.Windows.Forms.Padding(6);
      this.dataGraphControl.Name = "dataGraphControl";
      this.dataGraphControl.ScrollGrace = 0D;
      this.dataGraphControl.ScrollMaxX = 0D;
      this.dataGraphControl.ScrollMaxY = 0D;
      this.dataGraphControl.ScrollMaxY2 = 0D;
      this.dataGraphControl.ScrollMinX = 0D;
      this.dataGraphControl.ScrollMinY = 0D;
      this.dataGraphControl.ScrollMinY2 = 0D;
      this.dataGraphControl.Size = new System.Drawing.Size(790, 450);
      this.dataGraphControl.TabIndex = 0;
      // 
      // Amplitude
      // 
      this.Amplitude.FormattingEnabled = true;
      this.Amplitude.Items.AddRange(new object[] {
            "auto",
            "50",
            "100",
            "200",
            "500",
            "1000",
            "1500"});
      this.Amplitude.Location = new System.Drawing.Point(80, 469);
      this.Amplitude.Name = "Amplitude";
      this.Amplitude.Size = new System.Drawing.Size(121, 21);
      this.Amplitude.TabIndex = 1;
      this.Amplitude.SelectedIndexChanged += new System.EventHandler(this.Amplitude_SelectedIndexChanged);
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(12, 472);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(62, 13);
      this.label1.TabIndex = 2;
      this.label1.Text = "Амплитуда";
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(218, 472);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(111, 13);
      this.label2.TabIndex = 2;
      this.label2.Text = "Продолжительность";
      // 
      // prevBurstBtn
      // 
      this.prevBurstBtn.Location = new System.Drawing.Point(864, 425);
      this.prevBurstBtn.Name = "prevBurstBtn";
      this.prevBurstBtn.Size = new System.Drawing.Size(75, 23);
      this.prevBurstBtn.TabIndex = 3;
      this.prevBurstBtn.Text = "<";
      this.prevBurstBtn.UseVisualStyleBackColor = true;
      this.prevBurstBtn.Click += new System.EventHandler(this.prevBurstBtn_Click);
      // 
      // nextBurstBtn
      // 
      this.nextBurstBtn.Location = new System.Drawing.Point(1096, 425);
      this.nextBurstBtn.Name = "nextBurstBtn";
      this.nextBurstBtn.Size = new System.Drawing.Size(75, 23);
      this.nextBurstBtn.TabIndex = 3;
      this.nextBurstBtn.Text = ">";
      this.nextBurstBtn.UseVisualStyleBackColor = true;
      this.nextBurstBtn.Click += new System.EventHandler(this.nextBurstBtn_Click);
      // 
      // BurstSlider
      // 
      this.BurstSlider.Location = new System.Drawing.Point(809, 454);
      this.BurstSlider.Name = "BurstSlider";
      this.BurstSlider.Size = new System.Drawing.Size(410, 45);
      this.BurstSlider.TabIndex = 5;
      this.BurstSlider.Scroll += new System.EventHandler(this.BurstSlider_Scroll);
      // 
      // crntBurstTB
      // 
      this.crntBurstTB.Enabled = false;
      this.crntBurstTB.Location = new System.Drawing.Point(946, 427);
      this.crntBurstTB.Name = "crntBurstTB";
      this.crntBurstTB.Size = new System.Drawing.Size(144, 20);
      this.crntBurstTB.TabIndex = 6;
      this.crntBurstTB.TextChanged += new System.EventHandler(this.crntBurstTB_TextChanged);
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(531, 472);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(38, 13);
      this.label3.TabIndex = 2;
      this.label3.Text = "Канал";
      // 
      // drawDescriptionCheckBox
      // 
      this.drawDescriptionCheckBox.AutoSize = true;
      this.drawDescriptionCheckBox.Location = new System.Drawing.Point(827, 64);
      this.drawDescriptionCheckBox.Name = "drawDescriptionCheckBox";
      this.drawDescriptionCheckBox.Size = new System.Drawing.Size(234, 17);
      this.drawDescriptionCheckBox.TabIndex = 8;
      this.drawDescriptionCheckBox.Text = "Отрисовка характеристической функции";
      this.drawDescriptionCheckBox.UseVisualStyleBackColor = true;
      this.drawDescriptionCheckBox.CheckedChanged += new System.EventHandler(this.drawDescriptionCheckBox_CheckedChanged);
      // 
      // refreshBurstsBtn
      // 
      this.refreshBurstsBtn.Location = new System.Drawing.Point(827, 12);
      this.refreshBurstsBtn.Name = "refreshBurstsBtn";
      this.refreshBurstsBtn.Size = new System.Drawing.Size(142, 23);
      this.refreshBurstsBtn.TabIndex = 3;
      this.refreshBurstsBtn.Text = "Обновить список пачек";
      this.refreshBurstsBtn.UseVisualStyleBackColor = true;
      this.refreshBurstsBtn.Click += new System.EventHandler(this.refreshBurstsBtn_Click);
      // 
      // PatternGB
      // 
      this.PatternGB.Controls.Add(this.sprdRadioButton);
      this.PatternGB.Controls.Add(this.freqRadioButton);
      this.PatternGB.Controls.Add(this.cancelBtn);
      this.PatternGB.Controls.Add(this.SelectBtn);
      this.PatternGB.Controls.Add(this.refreshPatternsBtn);
      this.PatternGB.Controls.Add(this.patternsComboBox);
      this.PatternGB.Controls.Add(this.label4);
      this.PatternGB.Enabled = false;
      this.PatternGB.Location = new System.Drawing.Point(827, 110);
      this.PatternGB.Name = "PatternGB";
      this.PatternGB.Size = new System.Drawing.Size(392, 97);
      this.PatternGB.TabIndex = 9;
      this.PatternGB.TabStop = false;
      this.PatternGB.Text = "Выбор паттерна";
      // 
      // sprdRadioButton
      // 
      this.sprdRadioButton.AutoSize = true;
      this.sprdRadioButton.Location = new System.Drawing.Point(145, 19);
      this.sprdRadioButton.Name = "sprdRadioButton";
      this.sprdRadioButton.Size = new System.Drawing.Size(174, 17);
      this.sprdRadioButton.TabIndex = 0;
      this.sprdRadioButton.Text = "Пространственные паттерны";
      this.sprdRadioButton.UseVisualStyleBackColor = true;
      // 
      // freqRadioButton
      // 
      this.freqRadioButton.AutoSize = true;
      this.freqRadioButton.Checked = true;
      this.freqRadioButton.Location = new System.Drawing.Point(7, 20);
      this.freqRadioButton.Name = "freqRadioButton";
      this.freqRadioButton.Size = new System.Drawing.Size(132, 17);
      this.freqRadioButton.TabIndex = 0;
      this.freqRadioButton.TabStop = true;
      this.freqRadioButton.Text = "Частотные паттерны";
      this.freqRadioButton.UseVisualStyleBackColor = true;
      // 
      // cancelBtn
      // 
      this.cancelBtn.Location = new System.Drawing.Point(296, 68);
      this.cancelBtn.Name = "cancelBtn";
      this.cancelBtn.Size = new System.Drawing.Size(90, 23);
      this.cancelBtn.TabIndex = 3;
      this.cancelBtn.Text = "Отмена";
      this.cancelBtn.UseVisualStyleBackColor = true;
      // 
      // SelectBtn
      // 
      this.SelectBtn.Location = new System.Drawing.Point(200, 68);
      this.SelectBtn.Name = "SelectBtn";
      this.SelectBtn.Size = new System.Drawing.Size(90, 23);
      this.SelectBtn.TabIndex = 3;
      this.SelectBtn.Text = "Применить";
      this.SelectBtn.UseVisualStyleBackColor = true;
      this.SelectBtn.Click += new System.EventHandler(this.SelectBtn_Click);
      // 
      // refreshPatternsBtn
      // 
      this.refreshPatternsBtn.Location = new System.Drawing.Point(7, 43);
      this.refreshPatternsBtn.Name = "refreshPatternsBtn";
      this.refreshPatternsBtn.Size = new System.Drawing.Size(142, 23);
      this.refreshPatternsBtn.TabIndex = 3;
      this.refreshPatternsBtn.Text = "Обновить паттерны";
      this.refreshPatternsBtn.UseVisualStyleBackColor = true;
      this.refreshPatternsBtn.Click += new System.EventHandler(this.refreshPatternsBtn_Click);
      // 
      // patternsComboBox
      // 
      this.patternsComboBox.FormattingEnabled = true;
      this.patternsComboBox.Location = new System.Drawing.Point(265, 45);
      this.patternsComboBox.Name = "patternsComboBox";
      this.patternsComboBox.Size = new System.Drawing.Size(121, 21);
      this.patternsComboBox.TabIndex = 1;
      this.patternsComboBox.SelectedIndexChanged += new System.EventHandler(this.patternsComboBox_SelectedIndexChanged);
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(219, 48);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(40, 13);
      this.label4.TabIndex = 2;
      this.label4.Text = "Выбор";
      // 
      // singePatternCheckBox
      // 
      this.singePatternCheckBox.AutoSize = true;
      this.singePatternCheckBox.Location = new System.Drawing.Point(827, 87);
      this.singePatternCheckBox.Name = "singePatternCheckBox";
      this.singePatternCheckBox.Size = new System.Drawing.Size(144, 17);
      this.singePatternCheckBox.TabIndex = 10;
      this.singePatternCheckBox.Text = "Пачки одного паттерна";
      this.singePatternCheckBox.UseVisualStyleBackColor = true;
      this.singePatternCheckBox.CheckedChanged += new System.EventHandler(this.singePatternCheckBox_CheckedChanged);
      // 
      // Duration
      // 
      this.Duration.DisplayMember = "0";
      this.Duration.FormattingEnabled = true;
      this.Duration.Items.AddRange(new object[] {
            "auto",
            "50",
            "100",
            "150",
            "250",
            "400",
            "600",
            "900",
            "1000",
            "1500",
            "2000",
            "3000",
            "4000"});
      this.Duration.Location = new System.Drawing.Point(335, 469);
      this.Duration.Name = "Duration";
      this.Duration.Size = new System.Drawing.Size(121, 21);
      this.Duration.TabIndex = 1;
      this.Duration.ValueMember = "0";
      this.Duration.SelectedIndexChanged += new System.EventHandler(this.Duration_SelectedIndexChanged);
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Location = new System.Drawing.Point(948, 411);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(85, 13);
      this.label5.TabIndex = 2;
      this.label5.Text = "текущий номер";
      // 
      // label6
      // 
      this.label6.AutoSize = true;
      this.label6.Location = new System.Drawing.Point(884, 409);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(31, 13);
      this.label6.TabIndex = 2;
      this.label6.Text = "пред";
      // 
      // label7
      // 
      this.label7.AutoSize = true;
      this.label7.Location = new System.Drawing.Point(1120, 409);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(31, 13);
      this.label7.TabIndex = 2;
      this.label7.Text = "след";
      // 
      // groupBox1
      // 
      this.groupBox1.Controls.Add(this.BurstNumberInListTB);
      this.groupBox1.Controls.Add(this.label10);
      this.groupBox1.Controls.Add(this.StartTimeTB);
      this.groupBox1.Controls.Add(this.DurationTB);
      this.groupBox1.Controls.Add(this.label9);
      this.groupBox1.Controls.Add(this.label8);
      this.groupBox1.Location = new System.Drawing.Point(827, 211);
      this.groupBox1.Margin = new System.Windows.Forms.Padding(1);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Padding = new System.Windows.Forms.Padding(1);
      this.groupBox1.Size = new System.Drawing.Size(392, 81);
      this.groupBox1.TabIndex = 11;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "Данные";
      // 
      // BurstNumberInListTB
      // 
      this.BurstNumberInListTB.Enabled = false;
      this.BurstNumberInListTB.Location = new System.Drawing.Point(119, 60);
      this.BurstNumberInListTB.Name = "BurstNumberInListTB";
      this.BurstNumberInListTB.Size = new System.Drawing.Size(96, 20);
      this.BurstNumberInListTB.TabIndex = 15;
      this.BurstNumberInListTB.Text = "Номер в списке";
      // 
      // label10
      // 
      this.label10.AutoSize = true;
      this.label10.Location = new System.Drawing.Point(4, 61);
      this.label10.Name = "label10";
      this.label10.Size = new System.Drawing.Size(89, 13);
      this.label10.TabIndex = 14;
      this.label10.Text = "Номер в списке";
      // 
      // StartTimeTB
      // 
      this.StartTimeTB.Enabled = false;
      this.StartTimeTB.Location = new System.Drawing.Point(119, 38);
      this.StartTimeTB.Name = "StartTimeTB";
      this.StartTimeTB.Size = new System.Drawing.Size(96, 20);
      this.StartTimeTB.TabIndex = 13;
      this.StartTimeTB.Text = "-----------------------------";
      // 
      // DurationTB
      // 
      this.DurationTB.Enabled = false;
      this.DurationTB.Location = new System.Drawing.Point(119, 16);
      this.DurationTB.Name = "DurationTB";
      this.DurationTB.Size = new System.Drawing.Size(96, 20);
      this.DurationTB.TabIndex = 12;
      this.DurationTB.Text = "----------------------------";
      // 
      // label9
      // 
      this.label9.AutoSize = true;
      this.label9.Location = new System.Drawing.Point(4, 39);
      this.label9.Name = "label9";
      this.label9.Size = new System.Drawing.Size(78, 13);
      this.label9.TabIndex = 3;
      this.label9.Text = "Время начала";
      // 
      // label8
      // 
      this.label8.AutoSize = true;
      this.label8.Location = new System.Drawing.Point(4, 18);
      this.label8.Name = "label8";
      this.label8.Size = new System.Drawing.Size(111, 13);
      this.label8.TabIndex = 3;
      this.label8.Text = "Продолжительность";
      // 
      // groupBox2
      // 
      this.groupBox2.Controls.Add(this.label12);
      this.groupBox2.Controls.Add(this.label11);
      this.groupBox2.Controls.Add(this.ExportCurrentBtn);
      this.groupBox2.Location = new System.Drawing.Point(827, 298);
      this.groupBox2.Margin = new System.Windows.Forms.Padding(1);
      this.groupBox2.Name = "groupBox2";
      this.groupBox2.Padding = new System.Windows.Forms.Padding(1);
      this.groupBox2.Size = new System.Drawing.Size(392, 98);
      this.groupBox2.TabIndex = 12;
      this.groupBox2.TabStop = false;
      this.groupBox2.Text = "Экспорт";
      this.groupBox2.Enter += new System.EventHandler(this.groupBox2_Enter);
      // 
      // label12
      // 
      this.label12.AutoSize = true;
      this.label12.Location = new System.Drawing.Point(190, 20);
      this.label12.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
      this.label12.Name = "label12";
      this.label12.Size = new System.Drawing.Size(42, 13);
      this.label12.TabIndex = 3;
      this.label12.Text = "Готово";
      // 
      // label11
      // 
      this.label11.AutoSize = true;
      this.label11.Location = new System.Drawing.Point(135, 20);
      this.label11.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
      this.label11.Name = "label11";
      this.label11.Size = new System.Drawing.Size(62, 13);
      this.label11.TabIndex = 2;
      this.label11.Text = "Прогресс: ";
      // 
      // ExportCurrentBtn
      // 
      this.ExportCurrentBtn.Location = new System.Drawing.Point(7, 15);
      this.ExportCurrentBtn.Margin = new System.Windows.Forms.Padding(1);
      this.ExportCurrentBtn.Name = "ExportCurrentBtn";
      this.ExportCurrentBtn.Size = new System.Drawing.Size(103, 23);
      this.ExportCurrentBtn.TabIndex = 1;
      this.ExportCurrentBtn.Text = "Текущий список";
      this.ExportCurrentBtn.UseVisualStyleBackColor = true;
      this.ExportCurrentBtn.Click += new System.EventHandler(this.ExportCurrentBtn_Click);
      // 
      // DrawVectorsCheckBox
      // 
      this.DrawVectorsCheckBox.AutoSize = true;
      this.DrawVectorsCheckBox.Location = new System.Drawing.Point(1088, 64);
      this.DrawVectorsCheckBox.Name = "DrawVectorsCheckBox";
      this.DrawVectorsCheckBox.Size = new System.Drawing.Size(131, 17);
      this.DrawVectorsCheckBox.TabIndex = 13;
      this.DrawVectorsCheckBox.Text = "Отрисовка векторов";
      this.DrawVectorsCheckBox.UseVisualStyleBackColor = true;
      // 
      // DrawingPictureBox
      // 
      this.DrawingPictureBox.BackColor = System.Drawing.Color.White;
      this.DrawingPictureBox.Location = new System.Drawing.Point(1238, 11);
      this.DrawingPictureBox.Margin = new System.Windows.Forms.Padding(2);
      this.DrawingPictureBox.Name = "DrawingPictureBox";
      this.DrawingPictureBox.Size = new System.Drawing.Size(450, 468);
      this.DrawingPictureBox.TabIndex = 14;
      this.DrawingPictureBox.TabStop = false;
      // 
      // GradientPictureBox
      // 
      this.GradientPictureBox.BackColor = System.Drawing.SystemColors.Control;
      this.GradientPictureBox.Location = new System.Drawing.Point(1238, 493);
      this.GradientPictureBox.Name = "GradientPictureBox";
      this.GradientPictureBox.Size = new System.Drawing.Size(450, 70);
      this.GradientPictureBox.TabIndex = 15;
      this.GradientPictureBox.TabStop = false;
      // 
      // groupBox3
      // 
      this.groupBox3.Controls.Add(this.maxValueTextBox);
      this.groupBox3.Location = new System.Drawing.Point(1104, 520);
      this.groupBox3.Margin = new System.Windows.Forms.Padding(2);
      this.groupBox3.Name = "groupBox3";
      this.groupBox3.Padding = new System.Windows.Forms.Padding(2);
      this.groupBox3.Size = new System.Drawing.Size(129, 43);
      this.groupBox3.TabIndex = 16;
      this.groupBox3.TabStop = false;
      this.groupBox3.Text = "Количество цветов";
      // 
      // maxValueTextBox
      // 
      this.maxValueTextBox.Location = new System.Drawing.Point(45, 18);
      this.maxValueTextBox.Margin = new System.Windows.Forms.Padding(2);
      this.maxValueTextBox.Name = "maxValueTextBox";
      this.maxValueTextBox.Size = new System.Drawing.Size(51, 20);
      this.maxValueTextBox.TabIndex = 0;
      this.maxValueTextBox.TextChanged += new System.EventHandler(this.maxValueTextBox_TextChanged);
      // 
      // channelSelector
      // 
      this.channelSelector.Location = new System.Drawing.Point(575, 465);
      this.channelSelector.Margin = new System.Windows.Forms.Padding(6);
      this.channelSelector.Name = "channelSelector";
      this.channelSelector.Padding = new System.Windows.Forms.Padding(3);
      this.channelSelector.Size = new System.Drawing.Size(88, 29);
      this.channelSelector.TabIndex = 7;
      this.channelSelector.OnSelectedChannelsChanged += new MEAClosedLoop.UIForms.OnSelectedChannelsChangedDelegate(this.channelSelector_OnSelectedChannelsChanged);
      // 
      // VectorDrawnMethodComboBox
      // 
      this.VectorDrawnMethodComboBox.FormattingEnabled = true;
      this.VectorDrawnMethodComboBox.Items.AddRange(new object[] {
            "Теплокарта",
            "Поканальная"});
      this.VectorDrawnMethodComboBox.Location = new System.Drawing.Point(1088, 87);
      this.VectorDrawnMethodComboBox.Name = "VectorDrawnMethodComboBox";
      this.VectorDrawnMethodComboBox.Size = new System.Drawing.Size(121, 21);
      this.VectorDrawnMethodComboBox.TabIndex = 17;
      // 
      // FBurstExplorer
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(1702, 576);
      this.Controls.Add(this.VectorDrawnMethodComboBox);
      this.Controls.Add(this.groupBox3);
      this.Controls.Add(this.GradientPictureBox);
      this.Controls.Add(this.DrawingPictureBox);
      this.Controls.Add(this.DrawVectorsCheckBox);
      this.Controls.Add(this.groupBox2);
      this.Controls.Add(this.groupBox1);
      this.Controls.Add(this.singePatternCheckBox);
      this.Controls.Add(this.PatternGB);
      this.Controls.Add(this.drawDescriptionCheckBox);
      this.Controls.Add(this.channelSelector);
      this.Controls.Add(this.crntBurstTB);
      this.Controls.Add(this.BurstSlider);
      this.Controls.Add(this.label7);
      this.Controls.Add(this.label6);
      this.Controls.Add(this.label5);
      this.Controls.Add(this.nextBurstBtn);
      this.Controls.Add(this.refreshBurstsBtn);
      this.Controls.Add(this.prevBurstBtn);
      this.Controls.Add(this.label3);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.Duration);
      this.Controls.Add(this.Amplitude);
      this.Controls.Add(this.dataGraphControl);
      this.Name = "FBurstExplorer";
      this.Text = "FBurstExplorer";
      this.Load += new System.EventHandler(this.FBurstExplorer_Load);
      ((System.ComponentModel.ISupportInitialize)(this.BurstSlider)).EndInit();
      this.PatternGB.ResumeLayout(false);
      this.PatternGB.PerformLayout();
      this.groupBox1.ResumeLayout(false);
      this.groupBox1.PerformLayout();
      this.groupBox2.ResumeLayout(false);
      this.groupBox2.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.DrawingPictureBox)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.GradientPictureBox)).EndInit();
      this.groupBox3.ResumeLayout(false);
      this.groupBox3.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private ZedGraph.ZedGraphControl dataGraphControl;
    private System.Windows.Forms.ComboBox Amplitude;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Button prevBurstBtn;
    private System.Windows.Forms.Button nextBurstBtn;
    private System.Windows.Forms.TrackBar BurstSlider;
    private System.Windows.Forms.TextBox crntBurstTB;
    private ChannelComboBox channelSelector;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.CheckBox drawDescriptionCheckBox;
    private System.Windows.Forms.Button refreshBurstsBtn;
    private System.Windows.Forms.GroupBox PatternGB;
    private System.Windows.Forms.RadioButton sprdRadioButton;
    private System.Windows.Forms.RadioButton freqRadioButton;
    private System.Windows.Forms.CheckBox singePatternCheckBox;
    private System.Windows.Forms.Button refreshPatternsBtn;
    private System.Windows.Forms.Button cancelBtn;
    private System.Windows.Forms.Button SelectBtn;
    private System.Windows.Forms.ComboBox patternsComboBox;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.ComboBox Duration;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.GroupBox groupBox1;
    private System.Windows.Forms.TextBox StartTimeTB;
    private System.Windows.Forms.TextBox DurationTB;
    private System.Windows.Forms.Label label9;
    private System.Windows.Forms.Label label8;
    private System.Windows.Forms.TextBox BurstNumberInListTB;
    private System.Windows.Forms.Label label10;
    private System.Windows.Forms.GroupBox groupBox2;
    private System.Windows.Forms.Button ExportCurrentBtn;
    private System.Windows.Forms.Label label12;
    private System.Windows.Forms.Label label11;
    private System.Windows.Forms.CheckBox DrawVectorsCheckBox;
    private System.Windows.Forms.PictureBox DrawingPictureBox;
    private System.Windows.Forms.PictureBox GradientPictureBox;
    private System.Windows.Forms.GroupBox groupBox3;
    private System.Windows.Forms.TextBox maxValueTextBox;
    private System.Windows.Forms.ComboBox VectorDrawnMethodComboBox;
  }
}