﻿using MEAClosedLoop.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ZedGraph;

namespace MEAClosedLoop.UIForms
{
  public partial class FBurstExplorer : Form
  {
    private int currentIndex = 0;
    private ulong current;
    private List<ulong> burstIds;
    private int currentChannel = 0;
    private int currentPattern = -1;

    private bool ExportPicMode = false;

    private int _colorsCount = 30;

    public List<ulong> BurstIds
    {
      get
      {
        return burstIds;
      }

      set
      {
        burstIds = new List<ulong>();
        burstIds.AddRange(value);
        if (value.Count == 0)
          return;
        CurrentIndex = 0;
        BurstSlider.Maximum = burstIds.Count - 1;
        BurstSlider.Value = 0;
        DrawCurrentBurst();
      }
    }

    private int CurrentIndex
    {
      get
      {
        return currentIndex;
      }
      set
      {
        if (BurstIds.Count > 0)
        {
          if (value >= 0 && value < BurstIds.Count)
          {
            currentIndex = value;
          }
          else if (value < 0)
          {
            currentIndex = BurstIds.Count - 1;
          }
          else
          {
            currentIndex = 0;
          }
          ulong id = BurstIds[currentIndex];
          crntBurstTB.Text = id.ToString();
          BurstSlider.Value = CurrentIndex;
          DurationTB.Text = (CBurstDataProvider.GetBurst(current).Length / Param.MS).ToString() + " мс";
          StartTimeTB.Text = (id / Param.MS / 1000).ToString();
          BurstNumberInListTB.Text = currentIndex.ToString() + " из " + (BurstIds.Count - 1).ToString();
        }
      }
    }

    private ulong CurrentBurstId
    {
      get
      {
        return current;
      }
      set
      {
        if (BurstIds.Contains(value))
        {
          current = value;
          DrawCurrentBurst();
        }
        else if (BurstIds.Count > 0)
          current = BurstIds[0];
      }
    }

    public int DrawAmplitude
    {
      get
      {
        return _drawAmplitude;
      }

      set
      {
        _drawAmplitude = value;
        DrawCurrentBurst();
      }
    }

    private bool _drawDescription = false;

    public int DrawDuration
    {
      get
      {
        return _drawDuration;
      }

      set
      {
        _drawDuration = value;
        DrawCurrentBurst();
      }
    }

    public bool DrawDescription
    {
      get
      {
        return _drawDescription;
      }
      set
      {
        _drawDescription = value;
      }
    }

    private int _drawDuration = 0;
    private int _drawAmplitude = 0;
    private List<Color> _colors;
    private DateTime _dateTimeExport;

    private void DrawCurrentBurst()
    {
      if (current == 0) return;
      CBurst burst = CBurstDataProvider.GetBurst(current);

      GraphPane pane = dataGraphControl.GraphPane;
      pane.CurveList.Clear();

      //      pane.IsFontsScaled = false;

      pane.Title.IsVisible = false;
      pane.XAxis.Title.Text = "t, ms";
      pane.YAxis.Title.Text = "Value, pcs";
      pane.Legend.IsVisible = false;


      if (DrawDuration == 0)
      {
        pane.XAxis.Scale.MaxAuto = true;
        pane.XAxis.Scale.MinAuto = true;
      }
      else
      {
        pane.XAxis.Scale.Max = DrawDuration;
        pane.XAxis.Scale.Min = 0;
      }

      if (DrawAmplitude == 0)
      {
        pane.YAxis.Scale.MaxAuto = true;
        pane.YAxis.Scale.MinAuto = true;
      }
      else
      {
        pane.YAxis.Scale.Max = DrawAmplitude;
        pane.YAxis.Scale.Min = -DrawAmplitude;
      }




      PointPairList dataPoints = new PointPairList();
      //ограничение длительности отображения
      int limit = (DrawDuration > 0) ? DrawDuration * Param.MS : burst.Data[currentChannel].Length;
      if (limit > burst.Data[currentChannel].Length)
        limit = burst.Data[currentChannel].Length;

      if (DrawDescription)
      {
        burst.BuildDescription(chNum: currentChannel);
        PointPairList descriptionPoints = new PointPairList();
        for (int i = 0; i < limit / 25 && i < burst.description[currentChannel].Length; i++)
        {
          descriptionPoints.Add(i * 25 / (float)Param.MS, burst.description[currentChannel][i]);
        }
        CurveItem curveDesc = pane.AddCurve("", descriptionPoints, Color.OrangeRed, SymbolType.None);
        (curveDesc as LineItem).Line.Width = 4;
        (curveDesc as LineItem).Line.IsAntiAlias = true;
        (curveDesc as LineItem).Line.IsSmooth = true;
      }

      for (int i = 0; i < limit; i++)
      {
        dataPoints.Add(i / (float)Param.MS, burst.Data[currentChannel][i]);
      }

      CurveItem curveMain = pane.AddCurve("", dataPoints, Color.FromArgb(250, 100, 100, 100), SymbolType.None);
      curveMain.Color = Color.Blue;
      (curveMain as LineItem).Line.Width = 1;
      (curveMain as LineItem).Line.IsAntiAlias = true;
      (curveMain as LineItem).Line.IsSmooth = true;




      dataGraphControl.AxisChange();
      dataGraphControl.Invalidate();


      if (DrawVectorsCheckBox.Checked)
      {
        Vector60 vector;
        if (CSprdClusterProvider.ClusterBuilder == null ||
          !CSprdClusterProvider.ClusterBuilder.BurstIds.Contains(burst.start))
        {
          //MessageBox.Show("Кластеризация по простр характеристике не была проведена");
          // Формируем новый вектор
          vector = new Vector60(burst);
          vector.FillZeroValues();
        }
        else
        {
          // Если кластеризация уже была проведена, то берём оттуда
          // Разница в пустых электродах
          vector = CSprdClusterProvider.ClusterBuilder.VectorDict[burst.Start];
        }

        if (VectorDrawnMethodComboBox.SelectedIndex == 0)
          VectorDrawHelper.DrawVectorHeatMap(vector, DrawingPictureBox, _colors, true);
        else
          VectorDrawHelper.DrawVectorAsElectrodes(vector, DrawingPictureBox, _colors);
      }

      if (this.ExportPicMode)
      {
        string path = "export/bursts_" + _dateTimeExport.ToString("dd.MM.yy hh-mm") + "/";
        path += (currentPattern > -1 ? currentPattern.ToString(): "any") 
          + $" ({BurstIds.Count})/";
        if (!Directory.Exists(path))
        {
          Directory.CreateDirectory(path);
        }

        Image burstBmp = dataGraphControl.GetImage();
        //  path = @"C:\Users\mrmih\Documents\meaclosedloop\MEAClosedLoop\bin\Debug\" + path;
        path += $"burst_ch{currentChannel}_{BurstIds[currentIndex]}.png";

        if (DrawVectorsCheckBox.Checked)
        {
          // Соединяем две картинки в одну
          var vectorBmp = DrawingPictureBox.Image;
          var result = new Bitmap(burstBmp.Width + vectorBmp.Width + 30, Math.Max(burstBmp.Height, vectorBmp.Height + 10));

          using (Graphics g = Graphics.FromImage(result))
          using (FontFamily fontFamily = new FontFamily("Arial"))
          using (Font font = new Font(fontFamily, 15, FontStyle.Bold, GraphicsUnit.Pixel))
          {
            g.Clear(Color.White);
            g.DrawImage(burstBmp, 10, 10);
            g.DrawImage(vectorBmp, burstBmp.Width + 30, 10);
            string s = $"Номер пачки: {burstIds[CurrentIndex]}";
            g.DrawString(s, font, new SolidBrush(Color.Black), new PointF(burstBmp.Width + 15, 5));
          }
          result.Save(path);
        }
        else
        {
          burstBmp.Save(path);
        }

      }
    }

    public FBurstExplorer()
    {
      InitializeComponent();

      var temp = new List<ulong>();
      temp.AddRange(CBurstDataProvider.BurstAdrCollection.Keys);
      BurstIds = temp;
    }

    private void FBurstExplorer_Load(object sender, EventArgs e)
    {
      Duration.SelectedIndex = 0;
      Amplitude.SelectedIndex = 0;
      maxValueTextBox.Text = "30";
      VectorDrawnMethodComboBox.SelectedIndex = 0;
    }

    private void crntBurstTB_TextChanged(object sender, EventArgs e)
    {
      ulong id;
      if (ulong.TryParse(crntBurstTB.Text, out id))
      {
        if (BurstIds.Contains(id))
        {
          CurrentBurstId = id;
        }
        else
        {
          MessageBox.Show("Пачки с таким номером не существует!");
        }
      }
      else
      {
        MessageBox.Show("Введены данные неправильного формата!");
      }
    }

    private void refreshBurstsBtn_Click(object sender, EventArgs e)
    {
      var temp = new List<ulong>();
      temp.AddRange(CBurstDataProvider.BurstAdrCollection.Keys);
      BurstIds = temp;
    }

    private void channelSelector_OnSelectedChannelsChanged()
    {
      if (channelSelector.chIDList.Count > 0)
      {
        currentChannel = channelSelector.chIDList.First();
        DrawCurrentBurst();
      }
    }

    private void prevBurstBtn_Click(object sender, EventArgs e)
    {
      CurrentIndex--;
    }

    private void nextBurstBtn_Click(object sender, EventArgs e)
    {
      CurrentIndex++;
    }

    private void singePatternCheckBox_CheckedChanged(object sender, EventArgs e)
    {
      if (singePatternCheckBox.Checked)
      {
        PatternGB.Enabled = true;
      }
      else
      {
        PatternGB.Enabled = false;
        currentPattern = -1;
      }
    }

    private void refreshPatternsBtn_Click(object sender, EventArgs e)
    {
      if (CClusterProvider.ClasterBuilder.Clasters.Count == 0)
      {
        MessageBox.Show("паттерны не выделены или не загружены");
        return;
      }
      patternsComboBox.Items.Clear();
      patternsComboBox.Items.Add("any");
      foreach (var cluster in CClusterProvider.ClasterBuilder.Clasters
        .Where(x => x.innerIDS.Count >= 5))
      {
        cluster.ClusterNum = CClusterProvider.ClasterBuilder.Clasters.IndexOf(cluster);
        patternsComboBox.Items.Add(cluster);
      }
      //for (int i = CClusterProvider.ClasterBuilder.Clasters.Count - 1; i >= 0; i--)
      //  patternsComboBox.Items.Add(i);
      patternsComboBox.SelectedIndex = 0;
    }

    private void BurstSlider_Scroll(object sender, EventArgs e)
    {
      if (CurrentIndex != BurstSlider.Value)
        CurrentIndex = BurstSlider.Value;
    }

    private void patternsComboBox_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    private void SelectBtn_Click(object sender, EventArgs e)
    {
      var cluster = patternsComboBox.SelectedItem as CBurstCluster;

      if (cluster != null)
      {
        //int n = (int)patternsComboBox.SelectedItem;
        //if (!CTimeLineClustering.BurstDistribution.ContainsKey(n) || CTimeLineClustering.BurstDistribution[n].Count < 2)
        //{
        //  MessageBox.Show("В кластере слишком мало паттернов < 2");
        //  return;
        //}
        //CClusterProvider.ClasterBuilder.Clasters[n].innerIDS.ToList();
        //BurstIds = CTimeLineClustering.BurstDistribution[n].ToList();
        burstIds = cluster.innerIDS;
        currentPattern = cluster.ClusterNum;
      }
      else
      {
        currentPattern = -1;
        var temp = new List<ulong>();
        temp.AddRange(CBurstDataProvider.BurstAdrCollection.Keys);
        BurstIds = temp;
      }
    }


    private void Duration_SelectedIndexChanged(object sender, EventArgs e)
    {
      if ((Duration.SelectedItem as String).Contains("auto"))
      {
        // авто режим
        DrawDuration = 0;
      }
      else
      {
        // конкретное значение
        int i = -1;
        int.TryParse(Duration.SelectedItem.ToString(), out i);
        if (i > 0)
        {
          DrawDuration = i;
        }

      }
    }

    private void Amplitude_SelectedIndexChanged(object sender, EventArgs e)
    {
      if ((Amplitude.SelectedItem as String).Contains("auto"))
      {
        // авто режим
        DrawAmplitude = 0;
      }
      else
      {
        // конкретное значение
        int i = -1;
        int.TryParse(Amplitude.SelectedItem.ToString(), out i);
        if (i > 0)
        {
          DrawAmplitude = i;
        }

      }
    }

    private void drawDescriptionCheckBox_CheckedChanged(object sender, EventArgs e)
    {
      if (drawDescriptionCheckBox.Checked)
        DrawDescription = true;
      else
        DrawDescription = false;
    }

    private void groupBox2_Enter(object sender, EventArgs e)
    {

    }

    private void ExportCurrentBtn_Click(object sender, EventArgs e)
    {
      if (BurstIds.Count < 1)
      {
        MessageBox.Show("Текущий список пачек пуст");
        return;
      }

      ExportPicMode = true;
      _dateTimeExport = DateTime.Now;
      if (patternsComboBox.Items.Count > 0)
      {
        foreach (var item in patternsComboBox.Items)
        {
          var cluster = item as CBurstCluster;
          if (cluster == null)
            continue;

          burstIds = cluster.innerIDS;
          currentPattern = cluster.ClusterNum;
          CurrentIndex = 0;
          for (int i = 1; i < BurstIds.Count; i++)
          {
            CurrentIndex++;
          }
        }
      }
      else
      {
        CurrentIndex = 0;
        for (int i = 1; i < BurstIds.Count; i++)
        {
          CurrentIndex++;
        }
      }
      ExportPicMode = false;

    }

    private void maxValueTextBox_TextChanged(object sender, EventArgs e)
    {
      if (!int.TryParse(maxValueTextBox.Text, out _colorsCount) || _colorsCount <= 0)
      {
        maxValueTextBox.Text = "30";
        return;
      }

      _colors = VectorDrawHelper.CalculateColors(_colorsCount);
      VectorDrawHelper.DrawGradient(GradientPictureBox, _colors);
    }
  }
}
