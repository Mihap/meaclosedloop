﻿namespace MEAClosedLoop.UIForms
{
  partial class FBurstFormDisplay
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.zedPlotter = new ZedGraph.ZedGraphControl();
      this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
      this.SuspendLayout();
      // 
      // zedPlotter
      // 
      this.zedPlotter.Location = new System.Drawing.Point(13, 13);
      this.zedPlotter.Name = "zedPlotter";
      this.zedPlotter.ScrollGrace = 0D;
      this.zedPlotter.ScrollMaxX = 0D;
      this.zedPlotter.ScrollMaxY = 0D;
      this.zedPlotter.ScrollMaxY2 = 0D;
      this.zedPlotter.ScrollMinX = 0D;
      this.zedPlotter.ScrollMinY = 0D;
      this.zedPlotter.ScrollMinY2 = 0D;
      this.zedPlotter.Size = new System.Drawing.Size(692, 326);
      this.zedPlotter.TabIndex = 0;
      // 
      // flowLayoutPanel1
      // 
      this.flowLayoutPanel1.Location = new System.Drawing.Point(712, 13);
      this.flowLayoutPanel1.Name = "flowLayoutPanel1";
      this.flowLayoutPanel1.Size = new System.Drawing.Size(254, 326);
      this.flowLayoutPanel1.TabIndex = 1;
      // 
      // FBurstFormDisplay
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(978, 351);
      this.Controls.Add(this.flowLayoutPanel1);
      this.Controls.Add(this.zedPlotter);
      this.Name = "FBurstFormDisplay";
      this.Text = "FBurstFormDisplay";
      this.Load += new System.EventHandler(this.FBurstFormDisplay_Load);
      this.ResumeLayout(false);

    }

    #endregion

    private ZedGraph.ZedGraphControl zedPlotter;
    private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;

  }
}