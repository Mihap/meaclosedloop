﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ZedGraph;
namespace MEAClosedLoop.UIForms
{
  public partial class FBurstFormDisplay : Form
  {
    private int CurrentCh;
    private List<ulong> BurstIDs = new List<ulong>();
    public FBurstFormDisplay()
    {
      InitializeComponent();
    }
    public FBurstFormDisplay(List<ulong> IDs, int Ch) : this()
    {
      this.BurstIDs = IDs;
      CurrentCh = Ch;
    }

    private void FBurstFormDisplay_Load(object sender, EventArgs e)
    {
      this.DrawBursts();
    }
    private void DrawBursts()
    {
      GraphPane pane = zedPlotter.GraphPane;
      //pane.Legend.IsVisible = false;
      // Очистим список кривых на тот случай, если до этого сигналы уже были нарисованы
      pane.CurveList.Clear();
      // Создадим список точек
      foreach (ulong burstID in BurstIDs)
      {
        CBurst b = CBurstDataProvider.GetBurst(burstID);
        b.BuildDescription();

        PointPairList list = new PointPairList();
        for(int i = 0; i < b.description[0].Count(); i++)
        {
          list.Add(i, b.description[CurrentCh][i]);
        }
        CurveItem curve = pane.AddCurve("", list, Color.FromArgb(50, 100, 100, 100));
        
      }
      zedPlotter.AxisChange();

      // Обновляем график
      zedPlotter.Invalidate();
    }
  }
}
