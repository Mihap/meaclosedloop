﻿namespace MEAClosedLoop.UIForms
{
  partial class FBurstSelector
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.AvalibleBurstLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
      this.label1 = new System.Windows.Forms.Label();
      this.SelectedBurstLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
      this.label2 = new System.Windows.Forms.Label();
      this.UpdateAvalibleBtn = new System.Windows.Forms.Button();
      this.AddButton = new System.Windows.Forms.Button();
      this.RemoveBtn = new System.Windows.Forms.Button();
      this.GetSelectionBtn = new System.Windows.Forms.Button();
      this.CancelBtn = new System.Windows.Forms.Button();
      this.groupBox2 = new System.Windows.Forms.GroupBox();
      this.TotalTimeAvalibleNUD = new System.Windows.Forms.NumericUpDown();
      this.EndTimeNUD = new System.Windows.Forms.NumericUpDown();
      this.BeginTimeNUD = new System.Windows.Forms.NumericUpDown();
      this.label12 = new System.Windows.Forms.Label();
      this.ApplyFiltersBtn = new System.Windows.Forms.Button();
      this.label7 = new System.Windows.Forms.Label();
      this.label8 = new System.Windows.Forms.Label();
      this.label11 = new System.Windows.Forms.Label();
      this.label9 = new System.Windows.Forms.Label();
      this.label10 = new System.Windows.Forms.Label();
      this.EvokedBurstCBox = new System.Windows.Forms.CheckBox();
      this.SpontanousBurstCBox = new System.Windows.Forms.CheckBox();
      this.SelectAllAvBtn = new System.Windows.Forms.Button();
      this.DeselectAllBtn = new System.Windows.Forms.Button();
      this.SelectAllSelBtn = new System.Windows.Forms.Button();
      this.deSelectAllSelBtn = new System.Windows.Forms.Button();
      this.button1 = new System.Windows.Forms.Button();
      this.groupBox2.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.TotalTimeAvalibleNUD)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.EndTimeNUD)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.BeginTimeNUD)).BeginInit();
      this.SuspendLayout();
      // 
      // AvalibleBurstLayoutPanel
      // 
      this.AvalibleBurstLayoutPanel.AutoScroll = true;
      this.AvalibleBurstLayoutPanel.BackColor = System.Drawing.SystemColors.ControlLightLight;
      this.AvalibleBurstLayoutPanel.Location = new System.Drawing.Point(12, 34);
      this.AvalibleBurstLayoutPanel.Name = "AvalibleBurstLayoutPanel";
      this.AvalibleBurstLayoutPanel.Size = new System.Drawing.Size(300, 400);
      this.AvalibleBurstLayoutPanel.TabIndex = 0;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(13, 13);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(115, 13);
      this.label1.TabIndex = 1;
      this.label1.Text = "Все доступные пачки";
      // 
      // SelectedBurstLayoutPanel
      // 
      this.SelectedBurstLayoutPanel.BackColor = System.Drawing.SystemColors.ControlLightLight;
      this.SelectedBurstLayoutPanel.Location = new System.Drawing.Point(577, 37);
      this.SelectedBurstLayoutPanel.Name = "SelectedBurstLayoutPanel";
      this.SelectedBurstLayoutPanel.Size = new System.Drawing.Size(300, 400);
      this.SelectedBurstLayoutPanel.TabIndex = 0;
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(526, 13);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(66, 13);
      this.label2.TabIndex = 1;
      this.label2.Text = "Выбранные";
      // 
      // UpdateAvalibleBtn
      // 
      this.UpdateAvalibleBtn.Location = new System.Drawing.Point(237, 8);
      this.UpdateAvalibleBtn.Name = "UpdateAvalibleBtn";
      this.UpdateAvalibleBtn.Size = new System.Drawing.Size(75, 23);
      this.UpdateAvalibleBtn.TabIndex = 3;
      this.UpdateAvalibleBtn.Text = "обновить";
      this.UpdateAvalibleBtn.UseVisualStyleBackColor = true;
      this.UpdateAvalibleBtn.Click += new System.EventHandler(this.UpdateAvalibleBtn_Click);
      // 
      // AddButton
      // 
      this.AddButton.Location = new System.Drawing.Point(318, 270);
      this.AddButton.Name = "AddButton";
      this.AddButton.Size = new System.Drawing.Size(116, 23);
      this.AddButton.TabIndex = 4;
      this.AddButton.Text = ">>";
      this.AddButton.UseVisualStyleBackColor = true;
      this.AddButton.Click += new System.EventHandler(this.AddButton_Click);
      // 
      // RemoveBtn
      // 
      this.RemoveBtn.Location = new System.Drawing.Point(455, 270);
      this.RemoveBtn.Name = "RemoveBtn";
      this.RemoveBtn.Size = new System.Drawing.Size(116, 23);
      this.RemoveBtn.TabIndex = 5;
      this.RemoveBtn.Text = "<<";
      this.RemoveBtn.UseVisualStyleBackColor = true;
      this.RemoveBtn.Click += new System.EventHandler(this.RemoveBtn_Click);
      // 
      // GetSelectionBtn
      // 
      this.GetSelectionBtn.DialogResult = System.Windows.Forms.DialogResult.OK;
      this.GetSelectionBtn.Location = new System.Drawing.Point(608, 8);
      this.GetSelectionBtn.Name = "GetSelectionBtn";
      this.GetSelectionBtn.Size = new System.Drawing.Size(75, 23);
      this.GetSelectionBtn.TabIndex = 6;
      this.GetSelectionBtn.Text = "Выбрать";
      this.GetSelectionBtn.UseVisualStyleBackColor = true;
      this.GetSelectionBtn.Click += new System.EventHandler(this.GetSelectionBtn_Click);
      // 
      // CancelBtn
      // 
      this.CancelBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.CancelBtn.Location = new System.Drawing.Point(699, 8);
      this.CancelBtn.Name = "CancelBtn";
      this.CancelBtn.Size = new System.Drawing.Size(75, 23);
      this.CancelBtn.TabIndex = 6;
      this.CancelBtn.Text = "Отмена";
      this.CancelBtn.UseVisualStyleBackColor = true;
      // 
      // groupBox2
      // 
      this.groupBox2.Controls.Add(this.TotalTimeAvalibleNUD);
      this.groupBox2.Controls.Add(this.EndTimeNUD);
      this.groupBox2.Controls.Add(this.BeginTimeNUD);
      this.groupBox2.Controls.Add(this.label12);
      this.groupBox2.Controls.Add(this.ApplyFiltersBtn);
      this.groupBox2.Controls.Add(this.label7);
      this.groupBox2.Controls.Add(this.label8);
      this.groupBox2.Controls.Add(this.label11);
      this.groupBox2.Controls.Add(this.label9);
      this.groupBox2.Controls.Add(this.label10);
      this.groupBox2.Controls.Add(this.EvokedBurstCBox);
      this.groupBox2.Controls.Add(this.SpontanousBurstCBox);
      this.groupBox2.Location = new System.Drawing.Point(318, 34);
      this.groupBox2.Name = "groupBox2";
      this.groupBox2.Size = new System.Drawing.Size(253, 172);
      this.groupBox2.TabIndex = 2;
      this.groupBox2.TabStop = false;
      this.groupBox2.Text = "Фильтры";
      // 
      // TotalTimeAvalibleNUD
      // 
      this.TotalTimeAvalibleNUD.Enabled = false;
      this.TotalTimeAvalibleNUD.Location = new System.Drawing.Point(72, 120);
      this.TotalTimeAvalibleNUD.Name = "TotalTimeAvalibleNUD";
      this.TotalTimeAvalibleNUD.Size = new System.Drawing.Size(115, 20);
      this.TotalTimeAvalibleNUD.TabIndex = 4;
      // 
      // EndTimeNUD
      // 
      this.EndTimeNUD.Location = new System.Drawing.Point(72, 94);
      this.EndTimeNUD.Name = "EndTimeNUD";
      this.EndTimeNUD.Size = new System.Drawing.Size(115, 20);
      this.EndTimeNUD.TabIndex = 4;
      this.EndTimeNUD.ValueChanged += new System.EventHandler(this.EndTimeNUD_ValueChanged);
      // 
      // BeginTimeNUD
      // 
      this.BeginTimeNUD.Location = new System.Drawing.Point(72, 68);
      this.BeginTimeNUD.Name = "BeginTimeNUD";
      this.BeginTimeNUD.Size = new System.Drawing.Size(115, 20);
      this.BeginTimeNUD.TabIndex = 4;
      this.BeginTimeNUD.ValueChanged += new System.EventHandler(this.BeginTimeNUD_ValueChanged);
      // 
      // label12
      // 
      this.label12.AutoSize = true;
      this.label12.Location = new System.Drawing.Point(193, 122);
      this.label12.Name = "label12";
      this.label12.Size = new System.Drawing.Size(25, 13);
      this.label12.TabIndex = 2;
      this.label12.Text = "сек";
      // 
      // ApplyFiltersBtn
      // 
      this.ApplyFiltersBtn.Location = new System.Drawing.Point(72, 145);
      this.ApplyFiltersBtn.Name = "ApplyFiltersBtn";
      this.ApplyFiltersBtn.Size = new System.Drawing.Size(75, 23);
      this.ApplyFiltersBtn.TabIndex = 3;
      this.ApplyFiltersBtn.Text = "Применить";
      this.ApplyFiltersBtn.UseVisualStyleBackColor = true;
      this.ApplyFiltersBtn.Click += new System.EventHandler(this.ApplyFiltersBtn_Click);
      // 
      // label7
      // 
      this.label7.AutoSize = true;
      this.label7.Location = new System.Drawing.Point(194, 96);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(25, 13);
      this.label7.TabIndex = 2;
      this.label7.Text = "сек";
      // 
      // label8
      // 
      this.label8.AutoSize = true;
      this.label8.Location = new System.Drawing.Point(194, 70);
      this.label8.Name = "label8";
      this.label8.Size = new System.Drawing.Size(25, 13);
      this.label8.TabIndex = 2;
      this.label8.Text = "сек";
      // 
      // label11
      // 
      this.label11.AutoSize = true;
      this.label11.Location = new System.Drawing.Point(6, 122);
      this.label11.Name = "label11";
      this.label11.Size = new System.Drawing.Size(21, 13);
      this.label11.TabIndex = 2;
      this.label11.Text = "Из";
      // 
      // label9
      // 
      this.label9.AutoSize = true;
      this.label9.Location = new System.Drawing.Point(6, 96);
      this.label9.Name = "label9";
      this.label9.Size = new System.Drawing.Size(22, 13);
      this.label9.TabIndex = 2;
      this.label9.Text = "До";
      // 
      // label10
      // 
      this.label10.AutoSize = true;
      this.label10.Location = new System.Drawing.Point(6, 70);
      this.label10.Name = "label10";
      this.label10.Size = new System.Drawing.Size(60, 13);
      this.label10.TabIndex = 2;
      this.label10.Text = "Начиная С";
      // 
      // EvokedBurstCBox
      // 
      this.EvokedBurstCBox.AutoSize = true;
      this.EvokedBurstCBox.Checked = true;
      this.EvokedBurstCBox.CheckState = System.Windows.Forms.CheckState.Checked;
      this.EvokedBurstCBox.Location = new System.Drawing.Point(7, 44);
      this.EvokedBurstCBox.Name = "EvokedBurstCBox";
      this.EvokedBurstCBox.Size = new System.Drawing.Size(85, 17);
      this.EvokedBurstCBox.TabIndex = 1;
      this.EvokedBurstCBox.Text = "Вызванные";
      this.EvokedBurstCBox.UseVisualStyleBackColor = true;
      // 
      // SpontanousBurstCBox
      // 
      this.SpontanousBurstCBox.AutoSize = true;
      this.SpontanousBurstCBox.Checked = true;
      this.SpontanousBurstCBox.CheckState = System.Windows.Forms.CheckState.Checked;
      this.SpontanousBurstCBox.Location = new System.Drawing.Point(7, 20);
      this.SpontanousBurstCBox.Name = "SpontanousBurstCBox";
      this.SpontanousBurstCBox.Size = new System.Drawing.Size(88, 17);
      this.SpontanousBurstCBox.TabIndex = 0;
      this.SpontanousBurstCBox.Text = "Спонтанные";
      this.SpontanousBurstCBox.UseVisualStyleBackColor = true;
      // 
      // SelectAllAvBtn
      // 
      this.SelectAllAvBtn.Location = new System.Drawing.Point(318, 212);
      this.SelectAllAvBtn.Name = "SelectAllAvBtn";
      this.SelectAllAvBtn.Size = new System.Drawing.Size(116, 23);
      this.SelectAllAvBtn.TabIndex = 7;
      this.SelectAllAvBtn.Text = "Выделить все";
      this.SelectAllAvBtn.UseVisualStyleBackColor = true;
      this.SelectAllAvBtn.Click += new System.EventHandler(this.SelectAllAvBtn_Click);
      // 
      // DeselectAllBtn
      // 
      this.DeselectAllBtn.Location = new System.Drawing.Point(318, 241);
      this.DeselectAllBtn.Name = "DeselectAllBtn";
      this.DeselectAllBtn.Size = new System.Drawing.Size(116, 23);
      this.DeselectAllBtn.TabIndex = 7;
      this.DeselectAllBtn.Text = "Снять выделение";
      this.DeselectAllBtn.UseVisualStyleBackColor = true;
      this.DeselectAllBtn.Click += new System.EventHandler(this.DeselectAllBtn_Click);
      // 
      // SelectAllSelBtn
      // 
      this.SelectAllSelBtn.Location = new System.Drawing.Point(455, 212);
      this.SelectAllSelBtn.Name = "SelectAllSelBtn";
      this.SelectAllSelBtn.Size = new System.Drawing.Size(116, 23);
      this.SelectAllSelBtn.TabIndex = 7;
      this.SelectAllSelBtn.Text = "Выделить все";
      this.SelectAllSelBtn.UseVisualStyleBackColor = true;
      this.SelectAllSelBtn.Click += new System.EventHandler(this.SelectAllSelBtn_Click);
      // 
      // deSelectAllSelBtn
      // 
      this.deSelectAllSelBtn.Location = new System.Drawing.Point(455, 241);
      this.deSelectAllSelBtn.Name = "deSelectAllSelBtn";
      this.deSelectAllSelBtn.Size = new System.Drawing.Size(116, 23);
      this.deSelectAllSelBtn.TabIndex = 7;
      this.deSelectAllSelBtn.Text = "Снять выделение";
      this.deSelectAllSelBtn.UseVisualStyleBackColor = true;
      this.deSelectAllSelBtn.Click += new System.EventHandler(this.deSelectAllSelBtn_Click);
      // 
      // button1
      // 
      this.button1.DialogResult = System.Windows.Forms.DialogResult.OK;
      this.button1.Location = new System.Drawing.Point(318, 411);
      this.button1.Name = "button1";
      this.button1.Size = new System.Drawing.Size(171, 23);
      this.button1.TabIndex = 6;
      this.button1.Text = "Выбрать все доступные";
      this.button1.UseVisualStyleBackColor = true;
      this.button1.Click += new System.EventHandler(this.button1_Click);
      // 
      // FBurstSelector
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(889, 446);
      this.Controls.Add(this.deSelectAllSelBtn);
      this.Controls.Add(this.DeselectAllBtn);
      this.Controls.Add(this.SelectAllSelBtn);
      this.Controls.Add(this.SelectAllAvBtn);
      this.Controls.Add(this.CancelBtn);
      this.Controls.Add(this.button1);
      this.Controls.Add(this.GetSelectionBtn);
      this.Controls.Add(this.RemoveBtn);
      this.Controls.Add(this.AddButton);
      this.Controls.Add(this.UpdateAvalibleBtn);
      this.Controls.Add(this.groupBox2);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.SelectedBurstLayoutPanel);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.AvalibleBurstLayoutPanel);
      this.Name = "FBurstSelector";
      this.Text = "FBurstSelector";
      this.Load += new System.EventHandler(this.FBurstSelector_Load);
      this.groupBox2.ResumeLayout(false);
      this.groupBox2.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.TotalTimeAvalibleNUD)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.EndTimeNUD)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.BeginTimeNUD)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.FlowLayoutPanel AvalibleBurstLayoutPanel;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.FlowLayoutPanel SelectedBurstLayoutPanel;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Button UpdateAvalibleBtn;
    private System.Windows.Forms.Button AddButton;
    private System.Windows.Forms.Button RemoveBtn;
    private System.Windows.Forms.Button GetSelectionBtn;
    private System.Windows.Forms.Button CancelBtn;
    private System.Windows.Forms.GroupBox groupBox2;
    private System.Windows.Forms.Label label12;
    private System.Windows.Forms.Button ApplyFiltersBtn;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.Label label8;
    private System.Windows.Forms.Label label11;
    private System.Windows.Forms.Label label9;
    private System.Windows.Forms.Label label10;
    private System.Windows.Forms.CheckBox EvokedBurstCBox;
    private System.Windows.Forms.CheckBox SpontanousBurstCBox;
    private System.Windows.Forms.NumericUpDown TotalTimeAvalibleNUD;
    private System.Windows.Forms.NumericUpDown EndTimeNUD;
    private System.Windows.Forms.NumericUpDown BeginTimeNUD;
    private System.Windows.Forms.Button SelectAllAvBtn;
    private System.Windows.Forms.Button DeselectAllBtn;
    private System.Windows.Forms.Button SelectAllSelBtn;
    private System.Windows.Forms.Button deSelectAllSelBtn;
    private System.Windows.Forms.Button button1;
  }
}