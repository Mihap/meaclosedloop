﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MEAClosedLoop.UIForms.SubView;
using System.IO;
namespace MEAClosedLoop.UIForms
{
  public partial class FBurstSelector : Form
  {
    List<CBurstContainer> fullCollection = new List<CBurstContainer>();
    List<FBurstShortData> AvalibleCollection = new List<FBurstShortData>();
    List<FBurstShortData> SelectedCollection = new List<FBurstShortData>();
    
    List<ulong> AvalibleSelected = new List<ulong>();
    List<ulong> SelectdSelected = new List<ulong>();

    ulong _BeginTime;
    ulong BeginTime
    {
      get
      {
        return _BeginTime;
      }
      set
      {
        _BeginTime = value;
        BeginTimeNUD.Value = _BeginTime / Param.MS;
      }
    }
    ulong _EndTime;
    ulong EndTime
    {
      get
      {
        return _EndTime;
      }
      set
      {
        _EndTime = value;
        EndTimeNUD.Value = _EndTime / Param.MS;
      }
    }
    ulong _TotalTime;
    ulong TotalTime
    {
      get
      {
        return _TotalTime;
      }
      set
      {
        _TotalTime = value;
        if (BeginTime > _TotalTime)
          BeginTime = 0;
        if (_EndTime > TotalTime)
          _EndTime = TotalTime;
        TotalTimeAvalibleNUD.Maximum = _TotalTime / Param.MS;
        BeginTimeNUD.Maximum = TotalTimeAvalibleNUD.Maximum;
        EndTimeNUD.Maximum = TotalTimeAvalibleNUD.Maximum;
        TotalTimeAvalibleNUD.Value = _TotalTime / Param.MS;
      }
    }

    public List<ulong> result = new List<ulong>();

    public FBurstSelector()
    {
      InitializeComponent();
    }

    private void FBurstSelector_Load(object sender, EventArgs e)
    {
      if(TotalTime < 500000)  UpdateAvalible();
      EndTime = TotalTime;
      this.ApplyFilters();
    }

    private void UpdateAvalibleBtn_Click(object sender, EventArgs e)
    {
      UpdateAvalible();
    }

    void UpdateAvalible()
    {
      fullCollection.Clear();
      AvalibleCollection.Clear();
      SelectedCollection.Clear();
      List<ulong> IDs = new List<ulong>();
      if(CBurstDataProvider.BurstAdrCollection == null || CBurstDataProvider.BurstAdrCollection.Keys.Count == 0)
      {
        MessageBox.Show("Нету пачек в базе", "Ошибка");
        this.Close();
        return;
      }
      IDs.AddRange(CBurstDataProvider.BurstAdrCollection.Keys);
      foreach (ulong id in IDs)
        fullCollection.Add(CBurstDataProvider.BurstAdrCollection[id]);
      ulong LastID = IDs.Max();
      TotalTime = LastID;
      ApplyFilters();
      refreshPanels();
    }

    void ApplyFilters()
    {
      AvalibleCollection.Clear();
      AvalibleCollection = (from b in fullCollection
                            where b.BeginTime >= _BeginTime
                             && b.BeginTime < EndTime
                             && (EvokedBurstCBox.Checked && b.IsEvoked || SpontanousBurstCBox.Checked && !b.IsEvoked)
                            select new FBurstShortData(b.BeginTime, b.DurationTime, b.IsEvoked, false)
                            ).ToList();
      this.refreshPanels();
    }
    void refreshPanels()
    {
      AvalibleBurstLayoutPanel.Controls.Clear();
      SelectedBurstLayoutPanel.Controls.Clear();
      if (AvalibleCollection.Count > 600 || SelectedCollection.Count > 600) return;
      foreach (FBurstShortData burst in AvalibleCollection)
      {
        //отобразим все доступные на левой панели
        AvalibleBurstLayoutPanel.Controls.Add(burst);
      }
      foreach (FBurstShortData burst in SelectedCollection)
      {
        //отобразим все выбранные на правой панели
        SelectedBurstLayoutPanel.Controls.Add(burst);
      }
    }
    void MoveSelected()
    {
      List<FBurstShortData> toMove = AvalibleCollection.Where(x => x.IsSelected).ToList();
      SelectedCollection.AddRange(toMove);
      AvalibleCollection.RemoveAll(x => x.IsSelected);
    }
    void RemoveSelected()
    {
      List<FBurstShortData> toMove = SelectedCollection.Where(x => x.IsSelected).ToList();
      AvalibleCollection.AddRange(toMove);
      SelectedCollection.RemoveAll(x => x.IsSelected);
    }

    private void GetSelectionBtn_Click(object sender, EventArgs e)
    {
      //очистим результирующий список
      result.Clear();
      foreach (Control ctrl in SelectedBurstLayoutPanel.Controls)
      {
        //добавим в него из панели выбранных элементов
        FBurstShortData burst = ctrl as FBurstShortData;
        if (burst != null)
        {
          //только из тех, которые FBurstShortData
          //выбирая StartTime в качества иентификатора
          result.Add(burst.StartTime);
        }
      }
    }

    private void BeginTimeNUD_ValueChanged(object sender, EventArgs e)
    {
      this.BeginTime = (ulong)BeginTimeNUD.Value * 25;
    }

    private void EndTimeNUD_ValueChanged(object sender, EventArgs e)
    {
      this.EndTime = (ulong)EndTimeNUD.Value * 25;
    }

    private void ApplyFiltersBtn_Click(object sender, EventArgs e)
    {
      this.ApplyFilters();
    }

    private void SelectAllAvBtn_Click(object sender, EventArgs e)
    {
      AvalibleCollection.ForEach(x => x.IsSelected = true);
      refreshPanels();
    }

    private void DeselectAllBtn_Click(object sender, EventArgs e)
    {
      AvalibleCollection.ForEach(x => x.IsSelected = false);
      refreshPanels();
    }

    private void AddButton_Click(object sender, EventArgs e)
    {
      MoveSelected();
      refreshPanels();
    }

    private void SelectAllSelBtn_Click(object sender, EventArgs e)
    {
      SelectedCollection.ForEach(x => x.IsSelected = true);
      refreshPanels();
    }

    private void deSelectAllSelBtn_Click(object sender, EventArgs e)
    {
      SelectedCollection.ForEach(x => x.IsSelected = false);
      refreshPanels();
    }

    private void RemoveBtn_Click(object sender, EventArgs e)
    {
      RemoveSelected();
      refreshPanels();
    }

    private void button1_Click(object sender, EventArgs e)
    {
      //очистим результирующий список
      result.Clear();
      //foreach (ulong ctrl in )
      //{
      //  //добавим в него из панели выбранных элементов
      //  FBurstShortData burst = ctrl as FBurstShortData;
      //  if (burst != null)
      //  {
      //    //только из тех, которые FBurstShortData
      //    //выбирая StartTime в качества иентификатора
      //    result.Add(burst.StartTime);
      //  }
      //}
      result.AddRange(CBurstDataProvider.BurstAdrCollection.Select(x => x.Value.BeginTime));
      
      // вывод в файл всех пачек
      string fileName = "bursts.txt";
      using (StreamWriter sw = new StreamWriter(fileName, false))
      {
        sw.Write("0 ");
        foreach (ulong id in result)
        {
          //double time = (float)CBurstDataProvider.GetBurst(id).Start;
         // sw.Write(time + ";"); 
        }
      }
    }
  }
}
