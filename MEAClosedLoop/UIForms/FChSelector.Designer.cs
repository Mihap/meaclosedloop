﻿namespace MEAClosedLoop.UIForms
{
  partial class FChSelector
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.panel = new System.Windows.Forms.FlowLayoutPanel();
      this.ExitButton = new System.Windows.Forms.Button();
      this.SuspendLayout();
      // 
      // panel
      // 
      this.panel.BackColor = System.Drawing.SystemColors.Control;
      this.panel.Location = new System.Drawing.Point(12, 12);
      this.panel.Name = "panel";
      this.panel.Size = new System.Drawing.Size(216, 216);
      this.panel.TabIndex = 0;
      // 
      // ExitButton
      // 
      this.ExitButton.BackColor = System.Drawing.SystemColors.Control;
      this.ExitButton.Location = new System.Drawing.Point(83, 234);
      this.ExitButton.Name = "ExitButton";
      this.ExitButton.Size = new System.Drawing.Size(75, 23);
      this.ExitButton.TabIndex = 1;
      this.ExitButton.Text = "OK";
      this.ExitButton.UseVisualStyleBackColor = false;
      this.ExitButton.Click += new System.EventHandler(this.ExitButton_Click);
      // 
      // FChSelector
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.SystemColors.ControlDark;
      this.ClientSize = new System.Drawing.Size(241, 265);
      this.Controls.Add(this.ExitButton);
      this.Controls.Add(this.panel);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
      this.Name = "FChSelector";
      this.Text = "FChSelector";
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.FlowLayoutPanel panel;
    private System.Windows.Forms.Button ExitButton;
  }
}