﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MEAClosedLoop.UIForms
{
  public partial class FChSelector : Form
  {
    private List<int> _chNameList = new List<int>();
    public List<int> ChNameList
    {
      get { return _chNameList; }
      set
      {
        _chNameList = value;
        chNameListToIDList();
        refreshTextBox();
      }
    }
    public List<int> ChIDList = new List<int>();

    TextBox idsTextBox;
    channelSelectMode selectMode = channelSelectMode.Multi;

    public FChSelector(TextBox textBox, channelSelectMode csm)
    {
      InitializeComponent();
      selectMode = csm;
      idsTextBox = textBox;
      buttonsConstructor();
    }

    private void chNameListToIDList()
    {
      ChIDList.Clear();
      foreach (int id in ChNameList)
      {
        ChIDList.Add(MEA.NAME2IDX[id]);
      }
    }

    private void buttonsConstructor()
    {
      for (int i = 1; i <= 8; i++)
      {
        for (int j = 1; j <= 8; j++)
        {
          Button b = new Button();
          int ID = j * 10 + i;
          b.Text = ID.ToString();
          b.Font = new System.Drawing.Font("Microsoft Sans Serif", 7);
          b.Size = new Size(27, 27);
          b.Name = "button" + ID;
          b.Margin = new System.Windows.Forms.Padding(0);
          panel.Controls.Add(b);


          if (ID == 11 || ID == 18 || ID == 81 || ID == 88)
            b.Enabled = false;
          else
          {
            b.Click += new EventHandler(new Action<object, EventArgs>((sender, e) =>

            {
              int id = ID;
              Button button = b;
              channelSelectMode csm = selectMode;

              if (!ChNameList.Contains(id))
              {
                ChNameList.Add(id);
                b.BackColor = Color.CadetBlue;
              }
              else
              {
                ChNameList.Remove(id);
                button.BackColor = Color.FromName("Control");
              }
              refreshTextBox();
              if (csm == channelSelectMode.Single)
              {
                closePanel();
              }
            }));
          }

        }
      }
    }

    void refreshTextBox()
    {
      string s = "";
      int n = 0;
      foreach (int id in ChNameList)
      {
        n++;
        if (n == ChNameList.Count)
        {
          s += id;
        }
        else
        {
          s += id + ",";
        }
      }
      idsTextBox.Text = s;
    }

    private void closePanel()
    {
      chNameListToIDList();
      this.Close();
    }

    private void ExitButton_Click(object sender, EventArgs e)
    {
      closePanel();
    }
  }

  public enum channelSelectMode
  {
    Single,
    Multi
  }

}

