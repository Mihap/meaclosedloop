﻿namespace MEAClosedLoop.UIForms
{
  partial class FChannelClusterTree
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.matrixDiagramControl = new ZedGraph.ZedGraphControl();
      this.ThresholdBar = new System.Windows.Forms.TrackBar();
      this.Main8x8ClusterPicture = new System.Windows.Forms.PictureBox();
      this.TreeRebuiltButton = new System.Windows.Forms.Button();
      this.ThresholdNumericUpDown = new System.Windows.Forms.NumericUpDown();
      ((System.ComponentModel.ISupportInitialize)(this.ThresholdBar)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.Main8x8ClusterPicture)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.ThresholdNumericUpDown)).BeginInit();
      this.SuspendLayout();
      // 
      // matrixDiagramControl
      // 
      this.matrixDiagramControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.matrixDiagramControl.Location = new System.Drawing.Point(12, 12);
      this.matrixDiagramControl.Name = "matrixDiagramControl";
      this.matrixDiagramControl.ScrollGrace = 0D;
      this.matrixDiagramControl.ScrollMaxX = 0D;
      this.matrixDiagramControl.ScrollMaxY = 0D;
      this.matrixDiagramControl.ScrollMaxY2 = 0D;
      this.matrixDiagramControl.ScrollMinX = 0D;
      this.matrixDiagramControl.ScrollMinY = 0D;
      this.matrixDiagramControl.ScrollMinY2 = 0D;
      this.matrixDiagramControl.Size = new System.Drawing.Size(591, 474);
      this.matrixDiagramControl.TabIndex = 1;
      // 
      // ThresholdBar
      // 
      this.ThresholdBar.BackColor = System.Drawing.SystemColors.ScrollBar;
      this.ThresholdBar.Location = new System.Drawing.Point(1044, 12);
      this.ThresholdBar.Maximum = 1000;
      this.ThresholdBar.Name = "ThresholdBar";
      this.ThresholdBar.Orientation = System.Windows.Forms.Orientation.Vertical;
      this.ThresholdBar.Size = new System.Drawing.Size(45, 474);
      this.ThresholdBar.TabIndex = 2;
      this.ThresholdBar.Value = 400;
      this.ThresholdBar.Scroll += new System.EventHandler(this.ThresholdBar_Scroll);
      // 
      // Main8x8ClusterPicture
      // 
      this.Main8x8ClusterPicture.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.Main8x8ClusterPicture.BackColor = System.Drawing.SystemColors.AppWorkspace;
      this.Main8x8ClusterPicture.Location = new System.Drawing.Point(609, 12);
      this.Main8x8ClusterPicture.Name = "Main8x8ClusterPicture";
      this.Main8x8ClusterPicture.Size = new System.Drawing.Size(429, 429);
      this.Main8x8ClusterPicture.TabIndex = 11;
      this.Main8x8ClusterPicture.TabStop = false;
      // 
      // TreeRebuiltButton
      // 
      this.TreeRebuiltButton.BackColor = System.Drawing.SystemColors.ScrollBar;
      this.TreeRebuiltButton.Location = new System.Drawing.Point(610, 448);
      this.TreeRebuiltButton.Name = "TreeRebuiltButton";
      this.TreeRebuiltButton.Size = new System.Drawing.Size(117, 23);
      this.TreeRebuiltButton.TabIndex = 12;
      this.TreeRebuiltButton.Text = "Отрисовать дерево";
      this.TreeRebuiltButton.UseVisualStyleBackColor = false;
      this.TreeRebuiltButton.Click += new System.EventHandler(this.TreeRebuiltButton_Click);
      // 
      // ThresholdNumericUpDown
      // 
      this.ThresholdNumericUpDown.Location = new System.Drawing.Point(958, 447);
      this.ThresholdNumericUpDown.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      this.ThresholdNumericUpDown.Name = "ThresholdNumericUpDown";
      this.ThresholdNumericUpDown.Size = new System.Drawing.Size(80, 20);
      this.ThresholdNumericUpDown.TabIndex = 13;
      this.ThresholdNumericUpDown.Value = new decimal(new int[] {
            400,
            0,
            0,
            0});
      this.ThresholdNumericUpDown.ValueChanged += new System.EventHandler(this.ThresholdNumericUpDown_ValueChanged);
      // 
      // FChannelClusterTree
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(1101, 498);
      this.Controls.Add(this.ThresholdNumericUpDown);
      this.Controls.Add(this.TreeRebuiltButton);
      this.Controls.Add(this.Main8x8ClusterPicture);
      this.Controls.Add(this.ThresholdBar);
      this.Controls.Add(this.matrixDiagramControl);
      this.Name = "FChannelClusterTree";
      this.Text = "FChannelClusterTree";
      this.Load += new System.EventHandler(this.FChannelClusterTree_Load);
      ((System.ComponentModel.ISupportInitialize)(this.ThresholdBar)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.Main8x8ClusterPicture)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.ThresholdNumericUpDown)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private ZedGraph.ZedGraphControl matrixDiagramControl;
    private System.Windows.Forms.TrackBar ThresholdBar;
    private System.Windows.Forms.PictureBox Main8x8ClusterPicture;
    private System.Windows.Forms.Button TreeRebuiltButton;
    private System.Windows.Forms.NumericUpDown ThresholdNumericUpDown;
  }
}