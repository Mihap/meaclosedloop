﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ZedGraph;
using MEAClosedLoop.Common;
using MEAClosedLoop;
using MEAClosedLoop.Algorithms;
using MEAClosedLoop.UIForms;

namespace MEAClosedLoop.UIForms
{
  public partial class FChannelClusterTree : Form
  {
    private List<List<double>> corrCollection = new List<List<double>>();
    private List<List<double>> routes = new List<List<double>>();
    private List<int> ChannelIDs = new List<int>();

    float rectwidth = 0, rectheight = 0;
    double[,] CrChMatrix60x60;
    int MaxChNumber = 59;

    public FChannelClusterTree()
    {
      InitializeComponent();
    }

    public FChannelClusterTree(List<List<double>> initCollection, List<int> IDs, double[,] _CrChMatrix60x60)
      : this()
    {
      ChannelIDs = IDs;
      corrCollection = initCollection;
      CrChMatrix60x60 = _CrChMatrix60x60;
    }

    private void FChannelClusterTree_Load(object sender, EventArgs e)
    {
      CChannelCluster MainCluster = CChannelClusterisation.BuildBinTree(corrCollection, ChannelIDs);
      List<CChannelCluster>[] allRoutes = CChannelCluster.BuildRoutesToTop(MainCluster);
      CChannelClusterisation.GetClusters(MainCluster, (double)(ThresholdBar.Value / 1000.0)); // Проверть правильность работы и поиска
      DrawRoutes(allRoutes);
    }

    private void DrawRoutes(List<CChannelCluster>[] allRoutes)
    {
      GraphPane pane = matrixDiagramControl.GraphPane;
      pane.Title.Text = "Поканальная кластеризация пачечной активности культуры";
      pane.XAxis.IsVisible = false;
      pane.XAxis.Title.Text = "Схожесть каналов";
      pane.YAxis.Title.Text = "расстояние (у.е)";

      pane.Legend.IsVisible = false;
      foreach (List<CChannelCluster> data in allRoutes)
      {
        PointPairList dataList = new PointPairList();
        for (int i = 0; i < data.Count; i++)
        {
          if (i == data.Count - 1)
            dataList.Add(data[i].Coord.X, data[i].Coord.Y);
          else
          {
            dataList.Add(data[i].Coord.X, data[i].Coord.Y);
            dataList.Add(data[i].Coord.X, data[i + 1].Coord.Y);
          }
        }
        pane.AddCurve("", dataList, Color.Aquamarine, SymbolType.None);
      }
      matrixDiagramControl.AxisChange();
      matrixDiagramControl.Invalidate();
    }

    private void TreeRebuiltButton_Click(object sender, EventArgs e)
    {
      CChannelCluster MainCluster = CChannelClusterisation.BuildBinTree(corrCollection, ChannelIDs);
      List<CChannelCluster>[] allRoutes = CChannelCluster.BuildRoutesToTop(MainCluster);
      List<CChannelCluster> Clusters = CChannelClusterisation.GetClusters(MainCluster, (double)(ThresholdBar.Value / 1000.0)); // Проверть правильность работы и поиска
      DrawRoutes(allRoutes);

      Bitmap bmp = CorrelationBitmap.DrawTask(MatrixCreate(Clusters), Main8x8ClusterPicture.Width, Main8x8ClusterPicture.Height, out rectwidth, out rectheight);
      using (Graphics G = Graphics.FromImage(bmp))
      {
        float imgWidth = (float)Main8x8ClusterPicture.Width / 8f;
        float imgHeight = (float)Main8x8ClusterPicture.Height / 8f;
        float textX = 0, textY = 0;
        #region ch numbers draw
        for (int i = 0; i < 8; i++)
        {
          for (int j = 0; j < 8; j++)
          {
            if ((i == 0 || i == 7) && (j == 0 || j == 7))
            {
              textX += imgWidth;
              continue;
            }
            Point pt = new Point((int)textX, (int)textY);
            G.DrawString(((j + 1) * 10 + i + 1).ToString(), new Font("Tahoma", 12), Brushes.Black, pt);
            textX += imgWidth;
          }
          textX = 0;
          textY += imgHeight;
        }
        #endregion
        for(int i = 0; i < Clusters.Count(); i++)
        {
          int x, y;
          foreach(int id in Clusters[i].innerIDS)
          {
            y = id % 10;
            x = id / 10;
            G.DrawString(Clusters[i].innerIDS.Min().ToString(), new Font("Tahoma", 12), Brushes.Black, new PointF((x - 1) * imgWidth + 20, (y - 1) * imgHeight + 20));
          }
        }
      }
      Main8x8ClusterPicture.Image = bmp;
      bmp.Save("ChannelClusterTreeMtx-" + DateTime.Now.TimeOfDay.TotalMinutes.ToString() + ".bmp", System.Drawing.Imaging.ImageFormat.Bmp);
    }

    private double[,] MatrixCreate(List<CChannelCluster> Clusters)
    {
      double[,] Matrix8x8 = new double[8, 8];
      for (int i = 0; i < Clusters.Count(); i++)
      {
        int x, y;
        foreach (int id in Clusters[i].innerIDS)
        {
          x = id % 10;
          y = id / 10;
          Matrix8x8[x - 1, y - 1] = Clusters[i].innerIDS.Min() / 60.0;
        }
      }
      return Matrix8x8;
    }

    private void ThresholdBar_Scroll(object sender, EventArgs e)
    {
      ThresholdNumericUpDown.Value = ThresholdBar.Value;
    }

    private void ThresholdNumericUpDown_ValueChanged(object sender, EventArgs e)
    {
      ThresholdBar.Value = (int)ThresholdNumericUpDown.Value;
    }
  }
}
