﻿namespace MEAClosedLoop.UIForms
{
  partial class FClasterTimeLine
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.zedGraphControl1 = new ZedGraph.ZedGraphControl();
      this.groupBox1 = new System.Windows.Forms.GroupBox();
      this.UpdateClaster = new System.Windows.Forms.Button();
      this.ClusterButtonsFLP = new System.Windows.Forms.FlowLayoutPanel();
      this.ParamGroupBox = new System.Windows.Forms.GroupBox();
      this.LoadAutoChB = new System.Windows.Forms.CheckBox();
      this.progressLabel = new System.Windows.Forms.Label();
      this.EvokedCountLbl = new System.Windows.Forms.Label();
      this.SpontanousCountLbl = new System.Windows.Forms.Label();
      this.AnalyseAllDirectBtn = new System.Windows.Forms.Button();
      this.GetCollectionBtn = new System.Windows.Forms.Button();
      this.groupBox2 = new System.Windows.Forms.GroupBox();
      this.ExportBtn = new System.Windows.Forms.Button();
      this.label4 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.label1 = new System.Windows.Forms.Label();
      this.toNUD = new System.Windows.Forms.NumericUpDown();
      this.fromNUD = new System.Windows.Forms.NumericUpDown();
      this.AltExportBtn = new System.Windows.Forms.Button();
      this.groupBox1.SuspendLayout();
      this.ParamGroupBox.SuspendLayout();
      this.groupBox2.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.toNUD)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.fromNUD)).BeginInit();
      this.SuspendLayout();
      // 
      // zedGraphControl1
      // 
      this.zedGraphControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.zedGraphControl1.IsAntiAlias = true;
      this.zedGraphControl1.IsEnableSelection = true;
      this.zedGraphControl1.Location = new System.Drawing.Point(12, 65);
      this.zedGraphControl1.Name = "zedGraphControl1";
      this.zedGraphControl1.ScrollGrace = 0D;
      this.zedGraphControl1.ScrollMaxX = 0D;
      this.zedGraphControl1.ScrollMaxY = 0D;
      this.zedGraphControl1.ScrollMaxY2 = 0D;
      this.zedGraphControl1.ScrollMinX = 0D;
      this.zedGraphControl1.ScrollMinY = 0D;
      this.zedGraphControl1.ScrollMinY2 = 0D;
      this.zedGraphControl1.Size = new System.Drawing.Size(1118, 653);
      this.zedGraphControl1.TabIndex = 0;
      // 
      // groupBox1
      // 
      this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.groupBox1.Controls.Add(this.UpdateClaster);
      this.groupBox1.Controls.Add(this.ClusterButtonsFLP);
      this.groupBox1.Location = new System.Drawing.Point(1136, 12);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new System.Drawing.Size(154, 712);
      this.groupBox1.TabIndex = 1;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "Кластеры";
      // 
      // UpdateClaster
      // 
      this.UpdateClaster.Location = new System.Drawing.Point(7, 18);
      this.UpdateClaster.Name = "UpdateClaster";
      this.UpdateClaster.Size = new System.Drawing.Size(75, 23);
      this.UpdateClaster.TabIndex = 1;
      this.UpdateClaster.Text = "Обновить";
      this.UpdateClaster.UseVisualStyleBackColor = true;
      this.UpdateClaster.Click += new System.EventHandler(this.UpdateClaster_Click);
      // 
      // ClusterButtonsFLP
      // 
      this.ClusterButtonsFLP.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.ClusterButtonsFLP.AutoScroll = true;
      this.ClusterButtonsFLP.BackColor = System.Drawing.SystemColors.ControlDarkDark;
      this.ClusterButtonsFLP.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
      this.ClusterButtonsFLP.Location = new System.Drawing.Point(6, 121);
      this.ClusterButtonsFLP.Margin = new System.Windows.Forms.Padding(3, 150, 3, 3);
      this.ClusterButtonsFLP.Name = "ClusterButtonsFLP";
      this.ClusterButtonsFLP.Size = new System.Drawing.Size(142, 585);
      this.ClusterButtonsFLP.TabIndex = 0;
      // 
      // ParamGroupBox
      // 
      this.ParamGroupBox.Controls.Add(this.LoadAutoChB);
      this.ParamGroupBox.Controls.Add(this.progressLabel);
      this.ParamGroupBox.Controls.Add(this.EvokedCountLbl);
      this.ParamGroupBox.Controls.Add(this.SpontanousCountLbl);
      this.ParamGroupBox.Controls.Add(this.AnalyseAllDirectBtn);
      this.ParamGroupBox.Controls.Add(this.GetCollectionBtn);
      this.ParamGroupBox.Location = new System.Drawing.Point(13, 5);
      this.ParamGroupBox.Name = "ParamGroupBox";
      this.ParamGroupBox.Size = new System.Drawing.Size(673, 54);
      this.ParamGroupBox.TabIndex = 2;
      this.ParamGroupBox.TabStop = false;
      this.ParamGroupBox.Text = "Параметры";
      // 
      // LoadAutoChB
      // 
      this.LoadAutoChB.AutoSize = true;
      this.LoadAutoChB.Location = new System.Drawing.Point(438, 23);
      this.LoadAutoChB.Name = "LoadAutoChB";
      this.LoadAutoChB.Size = new System.Drawing.Size(234, 17);
      this.LoadAutoChB.TabIndex = 19;
      this.LoadAutoChB.Text = "Подгружать новые пачки автоматически";
      this.LoadAutoChB.UseVisualStyleBackColor = true;
      // 
      // progressLabel
      // 
      this.progressLabel.AutoSize = true;
      this.progressLabel.Location = new System.Drawing.Point(355, 24);
      this.progressLabel.Name = "progressLabel";
      this.progressLabel.Size = new System.Drawing.Size(77, 13);
      this.progressLabel.TabIndex = 18;
      this.progressLabel.Text = "progress: none";
      // 
      // EvokedCountLbl
      // 
      this.EvokedCountLbl.AutoSize = true;
      this.EvokedCountLbl.Location = new System.Drawing.Point(187, 24);
      this.EvokedCountLbl.Name = "EvokedCountLbl";
      this.EvokedCountLbl.Size = new System.Drawing.Size(91, 13);
      this.EvokedCountLbl.TabIndex = 17;
      this.EvokedCountLbl.Text = "вызванные : n/a";
      // 
      // SpontanousCountLbl
      // 
      this.SpontanousCountLbl.AutoSize = true;
      this.SpontanousCountLbl.Location = new System.Drawing.Point(87, 24);
      this.SpontanousCountLbl.Name = "SpontanousCountLbl";
      this.SpontanousCountLbl.Size = new System.Drawing.Size(94, 13);
      this.SpontanousCountLbl.TabIndex = 16;
      this.SpontanousCountLbl.Text = "спонтанные : n/a";
      // 
      // AnalyseAllDirectBtn
      // 
      this.AnalyseAllDirectBtn.Location = new System.Drawing.Point(284, 19);
      this.AnalyseAllDirectBtn.Name = "AnalyseAllDirectBtn";
      this.AnalyseAllDirectBtn.Size = new System.Drawing.Size(65, 23);
      this.AnalyseAllDirectBtn.TabIndex = 14;
      this.AnalyseAllDirectBtn.Text = "Анализ";
      this.AnalyseAllDirectBtn.UseVisualStyleBackColor = true;
      this.AnalyseAllDirectBtn.Click += new System.EventHandler(this.AnalyseAllDirectBtn_Click);
      // 
      // GetCollectionBtn
      // 
      this.GetCollectionBtn.Location = new System.Drawing.Point(16, 19);
      this.GetCollectionBtn.Name = "GetCollectionBtn";
      this.GetCollectionBtn.Size = new System.Drawing.Size(65, 23);
      this.GetCollectionBtn.TabIndex = 15;
      this.GetCollectionBtn.Text = "выборка";
      this.GetCollectionBtn.UseVisualStyleBackColor = true;
      this.GetCollectionBtn.Click += new System.EventHandler(this.SelectBursts_Click);
      // 
      // groupBox2
      // 
      this.groupBox2.Controls.Add(this.AltExportBtn);
      this.groupBox2.Controls.Add(this.ExportBtn);
      this.groupBox2.Controls.Add(this.label4);
      this.groupBox2.Controls.Add(this.label2);
      this.groupBox2.Controls.Add(this.label3);
      this.groupBox2.Controls.Add(this.label1);
      this.groupBox2.Controls.Add(this.toNUD);
      this.groupBox2.Controls.Add(this.fromNUD);
      this.groupBox2.Location = new System.Drawing.Point(693, 5);
      this.groupBox2.Name = "groupBox2";
      this.groupBox2.Size = new System.Drawing.Size(444, 54);
      this.groupBox2.TabIndex = 3;
      this.groupBox2.TabStop = false;
      this.groupBox2.Text = "Экспорт";
      // 
      // ExportBtn
      // 
      this.ExportBtn.Location = new System.Drawing.Point(260, 20);
      this.ExportBtn.Name = "ExportBtn";
      this.ExportBtn.Size = new System.Drawing.Size(75, 23);
      this.ExportBtn.TabIndex = 2;
      this.ExportBtn.Text = "Экспорт";
      this.ExportBtn.UseVisualStyleBackColor = true;
      this.ExportBtn.Click += new System.EventHandler(this.ExportBtn_Click);
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(226, 25);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(28, 13);
      this.label4.TabIndex = 1;
      this.label4.Text = "сек ";
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(96, 25);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(28, 13);
      this.label2.TabIndex = 1;
      this.label2.Text = "сек ";
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(130, 26);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(21, 13);
      this.label3.TabIndex = 1;
      this.label3.Text = "По";
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(7, 25);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(14, 13);
      this.label1.TabIndex = 1;
      this.label1.Text = "С";
      // 
      // toNUD
      // 
      this.toNUD.Location = new System.Drawing.Point(157, 22);
      this.toNUD.Maximum = new decimal(new int[] {
            1410065408,
            2,
            0,
            0});
      this.toNUD.Name = "toNUD";
      this.toNUD.Size = new System.Drawing.Size(63, 20);
      this.toNUD.TabIndex = 0;
      // 
      // fromNUD
      // 
      this.fromNUD.Location = new System.Drawing.Point(27, 22);
      this.fromNUD.Maximum = new decimal(new int[] {
            1410065408,
            2,
            0,
            0});
      this.fromNUD.Name = "fromNUD";
      this.fromNUD.Size = new System.Drawing.Size(63, 20);
      this.fromNUD.TabIndex = 0;
      // 
      // AltExportBtn
      // 
      this.AltExportBtn.Location = new System.Drawing.Point(341, 20);
      this.AltExportBtn.Name = "AltExportBtn";
      this.AltExportBtn.Size = new System.Drawing.Size(75, 23);
      this.AltExportBtn.TabIndex = 2;
      this.AltExportBtn.Text = "Экспорт 2";
      this.AltExportBtn.UseVisualStyleBackColor = true;
      this.AltExportBtn.Click += new System.EventHandler(this.AltExportBtn_Click);
      // 
      // FClasterTimeLine
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(1306, 730);
      this.Controls.Add(this.groupBox2);
      this.Controls.Add(this.ParamGroupBox);
      this.Controls.Add(this.groupBox1);
      this.Controls.Add(this.zedGraphControl1);
      this.Name = "FClasterTimeLine";
      this.Text = "FClasterTimeLine";
      this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FClasterTimeLine_FormClosed);
      this.groupBox1.ResumeLayout(false);
      this.ParamGroupBox.ResumeLayout(false);
      this.ParamGroupBox.PerformLayout();
      this.groupBox2.ResumeLayout(false);
      this.groupBox2.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.toNUD)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.fromNUD)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private ZedGraph.ZedGraphControl zedGraphControl1;
    private System.Windows.Forms.GroupBox groupBox1;
    private System.Windows.Forms.GroupBox ParamGroupBox;
    private System.Windows.Forms.FlowLayoutPanel ClusterButtonsFLP;
    private System.Windows.Forms.CheckBox LoadAutoChB;
    private System.Windows.Forms.Label progressLabel;
    private System.Windows.Forms.Label EvokedCountLbl;
    private System.Windows.Forms.Label SpontanousCountLbl;
    private System.Windows.Forms.Button AnalyseAllDirectBtn;
    private System.Windows.Forms.Button GetCollectionBtn;
    private System.Windows.Forms.Button UpdateClaster;
    private System.Windows.Forms.GroupBox groupBox2;
    private System.Windows.Forms.Button ExportBtn;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.NumericUpDown toNUD;
    private System.Windows.Forms.NumericUpDown fromNUD;
    private System.Windows.Forms.Button AltExportBtn;
  }
}