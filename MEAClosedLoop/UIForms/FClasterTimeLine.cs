﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

using ZedGraph;

using MEAClosedLoop.Common;
namespace MEAClosedLoop.UIForms
{
  public partial class FClasterTimeLine : Form, IRecieveBurst
  {
    private List<ulong> BurstIds = new List<ulong>();

    Timer refresher = new Timer();
    public FClasterTimeLine()
    {
      InitializeComponent();
      DisableUI(ParamGroupBox);
    }

    private void SelectBursts_Click(object sender, EventArgs e)
    {
      FBurstSelector dialog = new FBurstSelector();
      switch (dialog.ShowDialog())
      {
        case System.Windows.Forms.DialogResult.OK:
          break;
        case System.Windows.Forms.DialogResult.Cancel:
          break;
      }
      BurstIds.Clear();
      BurstIds.AddRange(dialog.result);
      this.SpontanousCountLbl.Text = "Спонтанные: " + BurstIds.Where(x => CBurstDataProvider.BurstAdrCollection[x].IsEvoked == false).Count().ToString();
      this.EvokedCountLbl.Text = "Вызванные: " + BurstIds.Where(x => CBurstDataProvider.BurstAdrCollection[x].IsEvoked == true).Count().ToString();
      this.EnableUI(ParamGroupBox);
    }

    private void EnableUI(Control control)
    {
      if (control is GroupBox)
        foreach (Control c in ParamGroupBox.Controls)
        {
          if (c.InvokeRequired)
            c.BeginInvoke(new Action<Control>(x => x.Enabled = true), c);
          else
            c.Enabled = true;
        }
      else
      {
        if (control.InvokeRequired)
          control.BeginInvoke(new Action<Control>(x => x.Enabled = true), control);
        else
          control.Enabled = true;
      }
    }

    private void DisableUI(Control control)
    {
      if (control is GroupBox)
        foreach (Control c in ParamGroupBox.Controls)
        {
          if (c.InvokeRequired)
            c.BeginInvoke(new Action<Control>(x => x.Enabled = false), c);
          else
            c.Enabled = false;
        }
      else
      {
        if (control.InvokeRequired)
          control.BeginInvoke(new Action<Control>(x => x.Enabled = false), control);
        else
          control.Enabled = false;
      }
    }

    private void UpdateClaster_Click(object sender, EventArgs e)
    {
      CTimeLineClustering.Init();
      EnableUI(ParamGroupBox);
      ClusterButtonsFLP.Controls.Clear();
      for (int i = CClusterProvider.ClasterBuilder.Clasters.Count - 1; i >= 0; i--)
      {
        CBurstCluster cbc = CClusterProvider.ClasterBuilder.Clasters[i];
        Button butt = new Button();
        butt.Text = "Открыть " + i;
        int id_to_load = i;
        butt.Click += new EventHandler(delegate (object s, EventArgs args)
        {
          this.BeginInvoke(new Action(() => this.OpenClusterView(id_to_load)));
        });
        ClusterButtonsFLP.Controls.Add(butt);
      }
    }

    private void OpenClusterView(int id)
    {
      FSingleClusterView view = new FSingleClusterView(id, true);
      view.Show();
    }

    void IRecieveBurst.RecieveBurst(CBurst Burst)
    {
      if (this.IsDisposed) return;
      ulong id = Burst.Start;
      this.BeginInvoke(new Action(() => { pushId(id); }));
    }
    /// <summary>
    /// Переотрисовка всех данных о распределении пачек по кластерам
    /// </summary>
    private void refresh()
    {
      progressLabel.Text = "progress:" + (int)CTimeLineClustering.Passed + "%";
      drawClusterMap();
    }
    private void pushId(ulong id)
    {
      if (LoadAutoChB.Checked)
        CTimeLineClustering.AddBurst(id);
    }

    private void AnalyseAllDirectBtn_Click(object sender, EventArgs e)
    {
      CTimeLineClustering.AddBurst(BurstIds);
      CTimeLineClustering.StartSorting();
      refresher.Tick += Refresher_Tick;
      refresher.Interval = 1000;
      refresher.Enabled = true;
      refresher.Start();
    }

    private void Refresher_Tick(object sender, EventArgs e)
    {
      refresh();
    }

    private void FClasterTimeLine_FormClosed(object sender, FormClosedEventArgs e)
    {
      refresher.Stop();
      CTimeLineClustering.StopSorting();
    }

    private void drawClusterMap()
    {
      GraphPane pane = zedGraphControl1.GraphPane;
      pane.IsFontsScaled = false;
      pane.Title.Text = "Распределение кластеров по времени";
      pane.XAxis.Title.Text = "t, sec";
      pane.YAxis.Title.Text = "номер кластера";
      pane.Legend.IsVisible = false;
      pane.YAxis.Scale.Min = 0;
      pane.YAxis.Scale.Max = CTimeLineClustering.BurstDistribution.Keys.Count + 1;
      // Очистим список кривых на тот случай, если до этого сигналы уже были нарисованы
      pane.CurveList.Clear();
      // Создадим список точек
      for (int i = 0; i < CTimeLineClustering.BurstDistribution.Keys.Count; i++)
      {
        List<ulong> current = CTimeLineClustering.BurstDistribution[i];
        for (int j = 0; j < current.Count; j++)
        {
          PointPairList eventline = new PointPairList();
          eventline.Add(current[j] / 25000, i);
          eventline.Add(current[j] / 25000, i + 1);
          pane.AddCurve("", eventline, Color.Black, SymbolType.None);
        }
      }
      for (int j = 0; j < CTimeLineClustering.UnsortedBursts.Count; j++)
      {
        PointPairList eventline = new PointPairList();
        eventline.Add(CTimeLineClustering.UnsortedBursts[j] / 25000, CTimeLineClustering.BurstDistribution.Keys.Count);
        eventline.Add(CTimeLineClustering.UnsortedBursts[j] / 25000, CTimeLineClustering.BurstDistribution.Keys.Count + 1);
        pane.AddCurve("", eventline, Color.Red, SymbolType.None);
      }
      //CurveItem curveMain = pane.AddCurve("", main, Color.FromArgb(250, 100, 100, 100), SymbolType.None);
      zedGraphControl1.AxisChange();

      // Обновляем график
      zedGraphControl1.Invalidate();
    }

    private void ExportBtn_Click(object sender, EventArgs e)
    {
      ulong from = (ulong)fromNUD.Value * Param.MS * 1000;
      ulong to = (ulong)toNUD.Value * Param.MS * 1000;
      //номер кластера - количество вхождений
      Dictionary<int, int> counters = new Dictionary<int, int>();

      foreach (int ClusterN in CTimeLineClustering.BurstDistribution.Keys)
      {
        int N = CTimeLineClustering.BurstDistribution[ClusterN].Where(x => x >= from && x <= to).Count();
        counters.Add(ClusterN, N);
      }
      SaveFileDialog dialog = new SaveFileDialog();
      if(dialog.ShowDialog() == DialogResult.OK)
      {
        using (StreamWriter sw = new StreamWriter(dialog.FileName))
        {
          foreach (int ClusterN in counters.Keys)
          {
            sw.WriteLine(ClusterN + "\t" + counters[ClusterN]);
          }
        }
      }
    }

    private void AltExportBtn_Click(object sender, EventArgs e)
    {
      ulong from = (ulong)fromNUD.Value * Param.MS * 1000;
      ulong to = (ulong)toNUD.Value * Param.MS * 1000;
      //номер кластера - количество вхождений
      Dictionary<int, int> counters = new Dictionary<int, int>();

    
      SaveFileDialog dialog = new SaveFileDialog();
      if (dialog.ShowDialog() == DialogResult.OK)
      {
        using (StreamWriter sw = new StreamWriter(dialog.FileName))
        {
          foreach (int ClusterN in CTimeLineClustering.BurstDistribution.Keys)
          {
            List<ulong> Bsts = CTimeLineClustering.BurstDistribution[ClusterN].Where(x => x >= from && x <= to).ToList();
            sw.Write(ClusterN + " ");
            for (int i = 0; i < Bsts.Count; i++)
            {
              sw.Write(Math.Round(Bsts[i] / (double)Param.DAQ_FREQ, 2).ToString() + ";");
            }
            sw.WriteLine("");
          }
        }
      }
    }
  }
}
