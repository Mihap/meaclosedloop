﻿namespace MEAClosedLoop.UIForms
{
  partial class FClusterDataSource
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.SaveBtn = new System.Windows.Forms.Button();
      this.LoadBtn = new System.Windows.Forms.Button();
      this.BurstCountLbl = new System.Windows.Forms.Label();
      this.ClusterCountLbl = new System.Windows.Forms.Label();
      this.groupBox1 = new System.Windows.Forms.GroupBox();
      this.groupBox1.SuspendLayout();
      this.SuspendLayout();
      // 
      // SaveBtn
      // 
      this.SaveBtn.Location = new System.Drawing.Point(245, 47);
      this.SaveBtn.Name = "SaveBtn";
      this.SaveBtn.Size = new System.Drawing.Size(75, 23);
      this.SaveBtn.TabIndex = 0;
      this.SaveBtn.Text = "Сохранить";
      this.SaveBtn.UseVisualStyleBackColor = true;
      this.SaveBtn.Click += new System.EventHandler(this.SaveBtn_Click);
      // 
      // LoadBtn
      // 
      this.LoadBtn.Location = new System.Drawing.Point(245, 18);
      this.LoadBtn.Name = "LoadBtn";
      this.LoadBtn.Size = new System.Drawing.Size(75, 23);
      this.LoadBtn.TabIndex = 1;
      this.LoadBtn.Text = "Загрузить";
      this.LoadBtn.UseVisualStyleBackColor = true;
      this.LoadBtn.Click += new System.EventHandler(this.LoadBtn_Click);
      // 
      // BurstCountLbl
      // 
      this.BurstCountLbl.AutoSize = true;
      this.BurstCountLbl.Location = new System.Drawing.Point(6, 16);
      this.BurstCountLbl.Name = "BurstCountLbl";
      this.BurstCountLbl.Size = new System.Drawing.Size(167, 13);
      this.BurstCountLbl.TabIndex = 2;
      this.BurstCountLbl.Text = "Количество пачек в анализе: 0 ";
      // 
      // ClusterCountLbl
      // 
      this.ClusterCountLbl.AutoSize = true;
      this.ClusterCountLbl.Location = new System.Drawing.Point(6, 35);
      this.ClusterCountLbl.Name = "ClusterCountLbl";
      this.ClusterCountLbl.Size = new System.Drawing.Size(200, 13);
      this.ClusterCountLbl.TabIndex = 3;
      this.ClusterCountLbl.Text = "Количество выявленных кластеров: 0";
      // 
      // groupBox1
      // 
      this.groupBox1.Controls.Add(this.BurstCountLbl);
      this.groupBox1.Controls.Add(this.ClusterCountLbl);
      this.groupBox1.Location = new System.Drawing.Point(12, 12);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new System.Drawing.Size(227, 58);
      this.groupBox1.TabIndex = 4;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "Текущий набор кластеров";
      // 
      // FClusterDataSource
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(326, 81);
      this.Controls.Add(this.groupBox1);
      this.Controls.Add(this.LoadBtn);
      this.Controls.Add(this.SaveBtn);
      this.Name = "FClusterDataSource";
      this.Text = "FClusterDataSource";
      this.groupBox1.ResumeLayout(false);
      this.groupBox1.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Button SaveBtn;
    private System.Windows.Forms.Button LoadBtn;
    private System.Windows.Forms.Label BurstCountLbl;
    private System.Windows.Forms.Label ClusterCountLbl;
    private System.Windows.Forms.GroupBox groupBox1;
  }
}