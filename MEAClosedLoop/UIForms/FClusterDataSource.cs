﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
namespace MEAClosedLoop.UIForms
{
  public partial class FClusterDataSource : Form
  {
    public FClusterDataSource()
    {
      InitializeComponent();
      updateClustersInfo();
    }

    private void LoadBtn_Click(object sender, EventArgs e)
    {
      OpenFileDialog dialog = new OpenFileDialog();
      if (dialog.ShowDialog() == DialogResult.OK)
      {
        CClusterProvider.Import(dialog.FileName);
      }
      updateClustersInfo();
    }

    private void SaveBtn_Click(object sender, EventArgs e)
    {
      SaveFileDialog dialog = new SaveFileDialog();
      if (dialog.ShowDialog() == DialogResult.OK)
      {
        CClusterProvider.Export(dialog.FileName);
      }
    }
    private void updateClustersInfo()
    {
      if (CClusterProvider.ClasterBuilder != null)
      {
        BurstCountLbl.Text = "Количество пачек в анализе: " + CClusterProvider.ClasterBuilder.Descriptions.Count;
        ClusterCountLbl.Text = "Количество выявленных кластеров: " + CClusterProvider.ClasterBuilder.Clasters.Count;
      }
    }
  }
}
