﻿namespace MEAClosedLoop.UIForms
{
  partial class FClusterTree
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.matrixDiagramControl = new ZedGraph.ZedGraphControl();
      this.SuspendLayout();
      // 
      // matrixDiagramControl
      // 
      this.matrixDiagramControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.matrixDiagramControl.Location = new System.Drawing.Point(3, 12);
      this.matrixDiagramControl.Name = "matrixDiagramControl";
      this.matrixDiagramControl.ScrollGrace = 0D;
      this.matrixDiagramControl.ScrollMaxX = 0D;
      this.matrixDiagramControl.ScrollMaxY = 0D;
      this.matrixDiagramControl.ScrollMaxY2 = 0D;
      this.matrixDiagramControl.ScrollMinX = 0D;
      this.matrixDiagramControl.ScrollMinY = 0D;
      this.matrixDiagramControl.ScrollMinY2 = 0D;
      this.matrixDiagramControl.Size = new System.Drawing.Size(918, 357);
      this.matrixDiagramControl.TabIndex = 0;
      // 
      // FClusterTree
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(933, 381);
      this.Controls.Add(this.matrixDiagramControl);
      this.Name = "FClusterTree";
      this.Text = "FClusterTree";
      this.Load += new System.EventHandler(this.FClusterTree_Load);
      this.ResumeLayout(false);

    }

    #endregion

    private ZedGraph.ZedGraphControl matrixDiagramControl;
  }
}