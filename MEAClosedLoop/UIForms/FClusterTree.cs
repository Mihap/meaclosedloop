﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ZedGraph;
using MEAClosedLoop.Common;
namespace MEAClosedLoop.UIForms
{
  public partial class FClusterTree : Form
  {
    private List<List<double>> corrCollection = new List<List<double>>();
    private List<List<double>> routes = new List<List<double>>();
    private List<ulong> BurstIDs = new List<ulong>();

    public FClusterTree()
    {
      InitializeComponent();
    }
    public FClusterTree(List<List<double>> initCollection, List<ulong> iDs)
      : this()
    {
      BurstIDs = iDs;
      corrCollection = initCollection;
    }

    private void FClusterTree_Load(object sender, EventArgs e)
    {
      CBurstCluster MainCluster = CClusterisation.BuildBinTree(corrCollection, BurstIDs);
      List<CBurstCluster>[] allRoutes = CBurstCluster.BuildRoutesToTop(MainCluster);
      CClusterisation.GetClusters(MainCluster, 0.22);
      DrawRoutes(allRoutes);
    }
    private void DrawRoutes(List<CBurstCluster>[] allRoutes)
    {
      GraphPane pane = matrixDiagramControl.GraphPane;
      pane.Title.Text = "кластеризация пачечной активности культуры";
      pane.XAxis.IsVisible = false;
      pane.XAxis.Title.Text = "Спонтанная активность";
      pane.YAxis.Title.Text = "расстояние (у.е)";

      pane.Legend.IsVisible = false;
      foreach (List<CBurstCluster> data in allRoutes)
      {
        PointPairList dataList = new PointPairList();
        for (int i = 0; i < data.Count; i++)
        {
          if (i == data.Count - 1)
            dataList.Add(data[i].Coord.X, data[i].Coord.Y);
          else
          {
            dataList.Add(data[i].Coord.X, data[i].Coord.Y);
            dataList.Add(data[i].Coord.X, data[i + 1].Coord.Y);
          }
        }
        if (CBurstDataProvider.BurstAdrCollection[data[0].ID].IsEvoked)
          pane.AddCurve("", dataList, Color.Red, SymbolType.None);
        else
          pane.AddCurve("", dataList, Color.Blue, SymbolType.None);
      }
      matrixDiagramControl.AxisChange();
      matrixDiagramControl.Invalidate();
    }
  }
}
