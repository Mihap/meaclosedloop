﻿namespace MEAClosedLoop.UIForms
{
    partial class FCompareForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
      this.components = new System.ComponentModel.Container();
      this.zedGraphPlot = new ZedGraph.ZedGraphControl();
      this.SuspendLayout();
      // 
      // zedGraphPlot
      // 
      this.zedGraphPlot.Dock = System.Windows.Forms.DockStyle.Fill;
      this.zedGraphPlot.EditModifierKeys = System.Windows.Forms.Keys.None;
      this.zedGraphPlot.IsAntiAlias = true;
      this.zedGraphPlot.Location = new System.Drawing.Point(0, 0);
      this.zedGraphPlot.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
      this.zedGraphPlot.Name = "zedGraphPlot";
      this.zedGraphPlot.ScrollGrace = 0D;
      this.zedGraphPlot.ScrollMaxX = 0D;
      this.zedGraphPlot.ScrollMaxY = 0D;
      this.zedGraphPlot.ScrollMaxY2 = 0D;
      this.zedGraphPlot.ScrollMinX = 0D;
      this.zedGraphPlot.ScrollMinY = 0D;
      this.zedGraphPlot.ScrollMinY2 = 0D;
      this.zedGraphPlot.Size = new System.Drawing.Size(781, 374);
      this.zedGraphPlot.TabIndex = 2;
      // 
      // FCompareForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(781, 374);
      this.Controls.Add(this.zedGraphPlot);
      this.Margin = new System.Windows.Forms.Padding(2);
      this.Name = "FCompareForm";
      this.Text = "FCompareForm";
      this.Load += new System.EventHandler(this.FCompareForm_Load);
      this.ResumeLayout(false);

        }

        #endregion

        private ZedGraph.ZedGraphControl zedGraphPlot;
    }
}