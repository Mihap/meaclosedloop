﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ZedGraph;

namespace MEAClosedLoop.UIForms
{
  public partial class FCompareForm : Form
  {
    private GraphPane pane;
    private List<double> Burst1, Burst2;

    public FCompareForm(IEnumerable<double> list1, IEnumerable<double> list2, string title)
    {
      InitializeComponent();
      Burst1 = list1.ToList();
      Burst2 = list2.ToList();
      pane = zedGraphPlot.GraphPane;
      pane.Title.Text = title + "";
    }

    private void FCompareForm_Load(object sender, EventArgs e)
    {
      Draw_Compare();
    }

    private void Draw_Compare()
    {
      pane.CurveList.Clear();
      pane.XAxis.Title.IsVisible = false;
      pane.YAxis.Title.IsVisible = false;

      PointPairList list = new PointPairList();
      for (int i = 0; i < Burst1.Count; i++)
      {

        list.Add(i, Burst1[i]);

      }
      LineItem curve = pane.AddCurve(null, list, Color.Blue, SymbolType.None);
      curve.Line.Width = 5;
      curve.Line.IsSmooth = true;


      list = new PointPairList();
      for (int i = 0; i < Burst2.Count; i++)
      {

        list.Add(i, Burst2[i]);

      }
      curve = pane.AddCurve(null, list, Color.Violet, SymbolType.None);
      curve.Line.Width = 5;
      curve.Line.IsSmooth = true;


      zedGraphPlot.AxisChange();
      zedGraphPlot.Invalidate();

    }


  }


}
