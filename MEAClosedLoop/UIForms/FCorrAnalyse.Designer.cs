﻿namespace MEAClosedLoop.UIForms
{
  partial class FCorrAnalyse
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.matrixDiagramControl = new ZedGraph.ZedGraphControl();
      this.SuspendLayout();
      // 
      // matrixDiagramControl
      // 
      this.matrixDiagramControl.Location = new System.Drawing.Point(13, 14);
      this.matrixDiagramControl.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
      this.matrixDiagramControl.Name = "matrixDiagramControl";
      this.matrixDiagramControl.ScrollGrace = 0D;
      this.matrixDiagramControl.ScrollMaxX = 0D;
      this.matrixDiagramControl.ScrollMaxY = 0D;
      this.matrixDiagramControl.ScrollMaxY2 = 0D;
      this.matrixDiagramControl.ScrollMinX = 0D;
      this.matrixDiagramControl.ScrollMinY = 0D;
      this.matrixDiagramControl.ScrollMinY2 = 0D;
      this.matrixDiagramControl.Size = new System.Drawing.Size(1336, 633);
      this.matrixDiagramControl.TabIndex = 0;
      // 
      // FCorrAnalyse
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(1362, 655);
      this.Controls.Add(this.matrixDiagramControl);
      this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
      this.Name = "FCorrAnalyse";
      this.Text = "FCorrAnalyse";
      this.Load += new System.EventHandler(this.FCorrAnalyse_Load);
      this.ResumeLayout(false);

    }

    #endregion

    private ZedGraph.ZedGraphControl matrixDiagramControl;
  }
}