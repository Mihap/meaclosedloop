﻿namespace MEAClosedLoop.UIForms
{
  partial class FCrossChClustering
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.ParamGroupBox = new System.Windows.Forms.GroupBox();
      this.Average_label = new System.Windows.Forms.Label();
      this.dispersion_button = new System.Windows.Forms.Button();
      this.BuildBinTreeBtn = new System.Windows.Forms.Button();
      this.channelComboBox2 = new MEAClosedLoop.UIForms.ChannelComboBox();
      this.label1 = new System.Windows.Forms.Label();
      this.exportButton = new System.Windows.Forms.Button();
      this.button1 = new System.Windows.Forms.Button();
      this.progressLabel = new System.Windows.Forms.Label();
      this.EvokedCountLbl = new System.Windows.Forms.Label();
      this.SpontanousCountLbl = new System.Windows.Forms.Label();
      this.AnalyseAllDirectBtn = new System.Windows.Forms.Button();
      this.GetCollectionBtn = new System.Windows.Forms.Button();
      this.FunctionSelector = new System.Windows.Forms.ComboBox();
      this.StopTime = new System.Windows.Forms.NumericUpDown();
      this.StartTime = new System.Windows.Forms.NumericUpDown();
      this.DrawButton = new System.Windows.Forms.Button();
      this.label4 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.MainMapPicture = new System.Windows.Forms.PictureBox();
      this.Load60x60Button = new System.Windows.Forms.Button();
      this.ParamGroupBox.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.StopTime)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.StartTime)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.MainMapPicture)).BeginInit();
      this.SuspendLayout();
      // 
      // ParamGroupBox
      // 
      this.ParamGroupBox.Controls.Add(this.Load60x60Button);
      this.ParamGroupBox.Controls.Add(this.Average_label);
      this.ParamGroupBox.Controls.Add(this.dispersion_button);
      this.ParamGroupBox.Controls.Add(this.BuildBinTreeBtn);
      this.ParamGroupBox.Controls.Add(this.channelComboBox2);
      this.ParamGroupBox.Controls.Add(this.label1);
      this.ParamGroupBox.Controls.Add(this.exportButton);
      this.ParamGroupBox.Controls.Add(this.button1);
      this.ParamGroupBox.Controls.Add(this.progressLabel);
      this.ParamGroupBox.Controls.Add(this.EvokedCountLbl);
      this.ParamGroupBox.Controls.Add(this.SpontanousCountLbl);
      this.ParamGroupBox.Controls.Add(this.AnalyseAllDirectBtn);
      this.ParamGroupBox.Controls.Add(this.GetCollectionBtn);
      this.ParamGroupBox.Controls.Add(this.FunctionSelector);
      this.ParamGroupBox.Controls.Add(this.StopTime);
      this.ParamGroupBox.Controls.Add(this.StartTime);
      this.ParamGroupBox.Controls.Add(this.DrawButton);
      this.ParamGroupBox.Controls.Add(this.label4);
      this.ParamGroupBox.Controls.Add(this.label2);
      this.ParamGroupBox.Controls.Add(this.label3);
      this.ParamGroupBox.Location = new System.Drawing.Point(12, 12);
      this.ParamGroupBox.Name = "ParamGroupBox";
      this.ParamGroupBox.Size = new System.Drawing.Size(1366, 70);
      this.ParamGroupBox.TabIndex = 9;
      this.ParamGroupBox.TabStop = false;
      this.ParamGroupBox.Text = "Параметры";
      // 
      // Average_label
      // 
      this.Average_label.AutoSize = true;
      this.Average_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
      this.Average_label.Location = new System.Drawing.Point(908, 38);
      this.Average_label.Name = "Average_label";
      this.Average_label.Size = new System.Drawing.Size(157, 18);
      this.Average_label.TabIndex = 22;
      this.Average_label.Text = "Среднее и дисперсия";
      // 
      // dispersion_button
      // 
      this.dispersion_button.Location = new System.Drawing.Point(1179, 12);
      this.dispersion_button.Name = "dispersion_button";
      this.dispersion_button.Size = new System.Drawing.Size(75, 23);
      this.dispersion_button.TabIndex = 20;
      this.dispersion_button.Text = "Дисперсия";
      this.dispersion_button.UseVisualStyleBackColor = true;
      this.dispersion_button.Click += new System.EventHandler(this.dispersion_button_Click);
      // 
      // BuildBinTreeBtn
      // 
      this.BuildBinTreeBtn.BackColor = System.Drawing.SystemColors.ActiveCaption;
      this.BuildBinTreeBtn.Location = new System.Drawing.Point(900, 12);
      this.BuildBinTreeBtn.Name = "BuildBinTreeBtn";
      this.BuildBinTreeBtn.Size = new System.Drawing.Size(107, 23);
      this.BuildBinTreeBtn.TabIndex = 19;
      this.BuildBinTreeBtn.Text = "Дерево каналов";
      this.BuildBinTreeBtn.UseVisualStyleBackColor = false;
      this.BuildBinTreeBtn.Click += new System.EventHandler(this.BuildBinTreeBtn_Click);
      // 
      // channelComboBox2
      // 
      this.channelComboBox2.Location = new System.Drawing.Point(227, 6);
      this.channelComboBox2.Name = "channelComboBox2";
      this.channelComboBox2.Size = new System.Drawing.Size(77, 29);
      this.channelComboBox2.TabIndex = 18;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(178, 12);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(46, 13);
      this.label1.TabIndex = 17;
      this.label1.Text = "Ch-s out";
      // 
      // exportButton
      // 
      this.exportButton.Location = new System.Drawing.Point(1111, 12);
      this.exportButton.Name = "exportButton";
      this.exportButton.Size = new System.Drawing.Size(62, 23);
      this.exportButton.TabIndex = 15;
      this.exportButton.Text = "Экспорт";
      this.exportButton.UseVisualStyleBackColor = true;
      this.exportButton.Click += new System.EventHandler(this.exportButton_Click);
      // 
      // button1
      // 
      this.button1.Location = new System.Drawing.Point(1013, 12);
      this.button1.Name = "button1";
      this.button1.Size = new System.Drawing.Size(92, 23);
      this.button1.TabIndex = 14;
      this.button1.Text = "Отрисовка 8х8";
      this.button1.UseVisualStyleBackColor = true;
      this.button1.Click += new System.EventHandler(this.button1_Click);
      // 
      // progressLabel
      // 
      this.progressLabel.AutoSize = true;
      this.progressLabel.Location = new System.Drawing.Point(792, 12);
      this.progressLabel.Name = "progressLabel";
      this.progressLabel.Size = new System.Drawing.Size(77, 13);
      this.progressLabel.TabIndex = 13;
      this.progressLabel.Text = "progress: none";
      // 
      // EvokedCountLbl
      // 
      this.EvokedCountLbl.AutoSize = true;
      this.EvokedCountLbl.Location = new System.Drawing.Point(78, 33);
      this.EvokedCountLbl.Name = "EvokedCountLbl";
      this.EvokedCountLbl.Size = new System.Drawing.Size(91, 13);
      this.EvokedCountLbl.TabIndex = 11;
      this.EvokedCountLbl.Text = "вызванные : n/a";
      // 
      // SpontanousCountLbl
      // 
      this.SpontanousCountLbl.AutoSize = true;
      this.SpontanousCountLbl.Location = new System.Drawing.Point(78, 12);
      this.SpontanousCountLbl.Name = "SpontanousCountLbl";
      this.SpontanousCountLbl.Size = new System.Drawing.Size(94, 13);
      this.SpontanousCountLbl.TabIndex = 10;
      this.SpontanousCountLbl.Text = "спонтанные : n/a";
      // 
      // AnalyseAllDirectBtn
      // 
      this.AnalyseAllDirectBtn.Location = new System.Drawing.Point(310, 10);
      this.AnalyseAllDirectBtn.Name = "AnalyseAllDirectBtn";
      this.AnalyseAllDirectBtn.Size = new System.Drawing.Size(65, 23);
      this.AnalyseAllDirectBtn.TabIndex = 0;
      this.AnalyseAllDirectBtn.Text = "Анализ";
      this.AnalyseAllDirectBtn.UseVisualStyleBackColor = true;
      this.AnalyseAllDirectBtn.Click += new System.EventHandler(this.AnalyseAllDirectBtn_Click);
      // 
      // GetCollectionBtn
      // 
      this.GetCollectionBtn.Location = new System.Drawing.Point(6, 19);
      this.GetCollectionBtn.Name = "GetCollectionBtn";
      this.GetCollectionBtn.Size = new System.Drawing.Size(65, 23);
      this.GetCollectionBtn.TabIndex = 0;
      this.GetCollectionBtn.Text = "выборка";
      this.GetCollectionBtn.UseVisualStyleBackColor = true;
      this.GetCollectionBtn.Click += new System.EventHandler(this.AnalyseAllCollected_Click);
      // 
      // FunctionSelector
      // 
      this.FunctionSelector.FormattingEnabled = true;
      this.FunctionSelector.Items.AddRange(new object[] {
            "Fx",
            "dFx"});
      this.FunctionSelector.Location = new System.Drawing.Point(718, 9);
      this.FunctionSelector.Name = "FunctionSelector";
      this.FunctionSelector.Size = new System.Drawing.Size(68, 21);
      this.FunctionSelector.TabIndex = 7;
      // 
      // StopTime
      // 
      this.StopTime.Location = new System.Drawing.Point(627, 10);
      this.StopTime.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      this.StopTime.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
      this.StopTime.Name = "StopTime";
      this.StopTime.Size = new System.Drawing.Size(52, 20);
      this.StopTime.TabIndex = 6;
      this.StopTime.Value = new decimal(new int[] {
            150,
            0,
            0,
            0});
      // 
      // StartTime
      // 
      this.StartTime.Location = new System.Drawing.Point(517, 10);
      this.StartTime.Maximum = new decimal(new int[] {
            980,
            0,
            0,
            0});
      this.StartTime.Name = "StartTime";
      this.StartTime.Size = new System.Drawing.Size(52, 20);
      this.StartTime.TabIndex = 6;
      // 
      // DrawButton
      // 
      this.DrawButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
      this.DrawButton.Location = new System.Drawing.Point(381, 10);
      this.DrawButton.Name = "DrawButton";
      this.DrawButton.Size = new System.Drawing.Size(75, 23);
      this.DrawButton.TabIndex = 4;
      this.DrawButton.Text = "Отрисовать";
      this.DrawButton.UseVisualStyleBackColor = true;
      this.DrawButton.Click += new System.EventHandler(this.DrawButton_Click);
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(685, 12);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(27, 13);
      this.label4.TabIndex = 5;
      this.label4.Text = "type";
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(462, 12);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(49, 13);
      this.label2.TabIndex = 5;
      this.label2.Text = "start, ms ";
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(575, 12);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(46, 13);
      this.label3.TabIndex = 5;
      this.label3.Text = "stop, ms";
      // 
      // MainMapPicture
      // 
      this.MainMapPicture.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.MainMapPicture.BackColor = System.Drawing.SystemColors.AppWorkspace;
      this.MainMapPicture.Location = new System.Drawing.Point(12, 88);
      this.MainMapPicture.Name = "MainMapPicture";
      this.MainMapPicture.Size = new System.Drawing.Size(1273, 473);
      this.MainMapPicture.TabIndex = 10;
      this.MainMapPicture.TabStop = false;
      this.MainMapPicture.Click += new System.EventHandler(this.MainMapPicture_Click);
      // 
      // Load60x60Button
      // 
      this.Load60x60Button.Location = new System.Drawing.Point(310, 41);
      this.Load60x60Button.Name = "Load60x60Button";
      this.Load60x60Button.Size = new System.Drawing.Size(94, 23);
      this.Load60x60Button.TabIndex = 23;
      this.Load60x60Button.Text = "Загрузка 60х60";
      this.Load60x60Button.UseVisualStyleBackColor = true;
      this.Load60x60Button.Click += new System.EventHandler(this.Load60x60Button_Click);
      // 
      // FCrossChClustering
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(1301, 573);
      this.Controls.Add(this.MainMapPicture);
      this.Controls.Add(this.ParamGroupBox);
      this.Name = "FCrossChClustering";
      this.Text = "FCrossChClustering";
      this.ParamGroupBox.ResumeLayout(false);
      this.ParamGroupBox.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.StopTime)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.StartTime)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.MainMapPicture)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.GroupBox ParamGroupBox;
    private System.Windows.Forms.Label progressLabel;
    private System.Windows.Forms.Button AnalyseAllDirectBtn;
    private System.Windows.Forms.ComboBox FunctionSelector;
    private System.Windows.Forms.NumericUpDown StopTime;
    private System.Windows.Forms.NumericUpDown StartTime;
    private System.Windows.Forms.Button DrawButton;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Label EvokedCountLbl;
    private System.Windows.Forms.Label SpontanousCountLbl;
    private System.Windows.Forms.Button GetCollectionBtn;
    private System.Windows.Forms.PictureBox MainMapPicture;
    private System.Windows.Forms.Button button1;
    private System.Windows.Forms.Button exportButton;
    private System.Windows.Forms.Label label1;
    private ChannelComboBox channelComboBox2;
    private System.Windows.Forms.Button BuildBinTreeBtn;
    private System.Windows.Forms.Button dispersion_button;
    private System.Windows.Forms.Label Average_label;
    private System.Windows.Forms.Button Load60x60Button;
  }
}