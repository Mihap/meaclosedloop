﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.Text;
using MEAClosedLoop;
using MEAClosedLoop.Algorithms;
using MEAClosedLoop.UIForms;
using System.IO;
using ZedGraph;
using MEAClosedLoop.Common;
using System.Runtime.Serialization.Formatters.Binary;

namespace MEAClosedLoop.UIForms
{
  [Serializable]
  public partial class FCrossChClustering : Form
  {
    object FileWriteLock = new object();
    bool UseLoad60x60 = false;
    private List<ulong> BurstIds = new List<ulong>();
    List<List<double>> allData = new List<List<double>>();

    //Для матрицы дисперсии
    double[] _disp = new double[60], _Aver = new double[60];
    double[,] _DispMatrix8x8 = new double[8, 8];
    double AverDispersion_60, AverAverage_60;

    public List<int> BinTreeOutChannels = new List<int>();
    List<CClasterBuilder> CrossChClusterList = new List<CClasterBuilder>();

    CClasterBuilder clusterBuilder = null;

    private Timer UpdateProgressTimer = new Timer();
    Stopwatch durationWatch;

    private static string[] Types = new string[2] { "IntegralFx", "dIntegralFx" };
    private CharactType FuncType;
    float rectwidth = 0, rectheight = 0;

    /// <summary>
    ///Матрица отрисовки кросскорреляции 
    /// </summary>
    public double[,] CrChMatrix60x60 = new double[60, 60];
    public int[] ChannelMatxID;

    /// <summary>
    /// Не используемые каналы при анализе
    /// </summary>
    private List<int> chNumbers = new List<int>();
    int CURRENT_MAX_CHANNEL;
    public char plusMinus = (char)177;
    public string StatString;

    public FCrossChClustering()
    {
      InitializeComponent();
      channelComboBox2.OnSelectedChannelsChanged += ChannelComboBox2_OnSelectedChannelsChanged;
    }
    private void ChannelComboBox2_OnSelectedChannelsChanged()
    {
      chNumbers = channelComboBox2.chIDList;
      CURRENT_MAX_CHANNEL = 60 - chNumbers.Count();
    }

    private void FCrossCHClustering_Load(object sender, EventArgs e)
    {
      FunctionSelector.Items.Clear();
      FunctionSelector.Items.AddRange(Types);
      FunctionSelector.SelectedIndex = 0;
      this.DisableUI(ParamGroupBox);
      this.EnableUI(GetCollectionBtn);
    }

    private void AnalyseAllCollected_Click(object sender, EventArgs e)
    {
      FBurstSelector dialog = new FBurstSelector();
      switch (dialog.ShowDialog())
      {
        case System.Windows.Forms.DialogResult.OK:
          break;
        case System.Windows.Forms.DialogResult.Cancel:
          break;
      }
      BurstIds.Clear();
      BurstIds.AddRange(dialog.result);
      this.SpontanousCountLbl.Text = "Спонтанные: " + BurstIds.Where(x => CBurstDataProvider.BurstAdrCollection[x].IsEvoked == false).Count().ToString();
      this.EvokedCountLbl.Text = "Вызванные: " + BurstIds.Where(x => CBurstDataProvider.BurstAdrCollection[x].IsEvoked == true).Count().ToString();
      this.EnableUI(ParamGroupBox);
    }

    private void AnalyseAllDirectBtn_Click(object sender, EventArgs e)
    {
      if (BurstIds.Count() != 0 || UseLoad60x60)
      {
        durationWatch = new Stopwatch();

        UpdateProgressTimer = new Timer();
        UpdateProgressTimer.Interval = 500;
        UpdateProgressTimer.Enabled = true;
        Task asyncAnalysisTask = new Task(AnalyseSyncAllMethod);

        durationWatch.Stop();
        durationWatch.Reset();
        durationWatch.Start();

        asyncAnalysisTask.Start();
      }
    }


    //Запускается в отдельном потоке
    //Внутри себя имеет вызов многопоточной реализации CClasterBuilder.AnalyseSync(), создающей 8 потоков => многопоточность идёт лесом
    private void AnalyseSyncAllMethod()
    {
      DisableUI(ParamGroupBox);
      //60 каналов, для каждого, КРОМЕ КАНАЛОВ ИЗ СПИСКА, считаем AnalyseSync()
      if (UseLoad60x60)
      {
        this.BeginInvoke(new Action(() => progressLabel.Text = "Loading Matrix..."));
        CrossChMatrixCreating();
        EnableUI(ParamGroupBox);
        //Отрисовываем матрицу кросскорелляции
        DrawButton.BeginInvoke(new Action(() => DrawButton.PerformClick()));
        this.BeginInvoke(new Action(() => progressLabel.Text = "Progress: " + (60 - CURRENT_MAX_CHANNEL).ToString() + " of " + (60 - CURRENT_MAX_CHANNEL).ToString()));
      }
      else
      {
        for (int ChNum = 0; ChNum < 60; ChNum++)
        {
          UpdateProgressTimer.Start();
          this.BeginInvoke(new Action(() =>
          {
            progressLabel.Text = "Progress: " + (ChNum + 1).ToString() + " of " + (60 - CURRENT_MAX_CHANNEL).ToString();
          }));

          CrossChClusterList.Add(new CClasterBuilder(BurstIds, FuncType, ChNum, (int)StartTime.Value, (int)StopTime.Value));
          CrossChClusterList.Last().AnalyseSynс(false, false, true);
        }
        this.BeginInvoke(new Action(() => progressLabel.Text = "Creating Matrix..."));
        CrossChMatrixCreating();
        EnableUI(ParamGroupBox);
        this.BeginInvoke(new Action(() => progressLabel.Text = "Progress: " + (60 - CURRENT_MAX_CHANNEL).ToString() + " of " + (60 - CURRENT_MAX_CHANNEL).ToString()));

        //Отрисовываем матрицу кросскорелляции
        DrawButton.BeginInvoke(new Action(() => DrawButton.PerformClick()));

        // Сохранение матрицы корр
        string path = "matrix60x60/" + DateTime.Now.TimeOfDay.TotalHours.ToString() + DateTime.Now.TimeOfDay.TotalMinutes.ToString();
        byte[] bytearr;
        MemoryStream ms = new MemoryStream();
        BinaryFormatter formatter = new BinaryFormatter();
        formatter.Serialize(ms, CrChMatrix60x60);
        bytearr = ms.ToArray();

        lock (FileWriteLock)
        {
          Directory.CreateDirectory("matrix60x60/");
          using (BinaryWriter sw = new BinaryWriter(File.Open(path, FileMode.CreateNew, FileAccess.Write)))
          {
            sw.Write(bytearr);
          }
        }
      }

    }


    // 4 вложенных for по каналам и кластерам внутри каналов и линейное перечисление
    // CrChMatrix60x60 - конечная матрица

    private void CrossChMatrixCreating()
    {
      if (UseLoad60x60)
      {
        ChannelMatxID = new int[60];
        for (int rowCh = 0; rowCh < 60; rowCh++)
        {
          ChannelMatxID[rowCh] = MEA.IDX2NAME[rowCh];
        }
        UseLoad60x60 = false;
      }
      else
      {
        ChannelMatxID = new int[60];                            //Матричные индексы рисунка
        List<ulong> CurrentConcatIDs = new List<ulong>();
        long rowPos = 0, colPos = 0;
        int innerRowIDsNumber = 0;
        int innerColIDsNumber = 0;

        for (int rowCh = 0; rowCh < 60; rowCh++)
        {

          ChannelMatxID[rowCh] = MEA.IDX2NAME[rowCh];
          for (int colCh = 0; colCh < 60; colCh++)
          {
            if (rowCh == colCh)
              ;
            int Clusters1 = CrossChClusterList[rowCh].Clasters.Count();
            int Clusters2 = CrossChClusterList[colCh].Clasters.Count();

            double summ = 0;
            double union = 0;
            // идем по кластерам принадлежащим каналам rowch & colch
            // считаем объединение
            List<ulong> UnitedIds = new List<ulong>();

            foreach (List<ulong> ids in CrossChClusterList[rowCh].Clasters.Select(x => x.innerIDS).Union(CrossChClusterList[colCh].Clasters.Select(x => x.innerIDS)))
            {
              foreach (ulong id in ids)
                if (!UnitedIds.Contains(id))
                  UnitedIds.Add(id);
            }
            union = UnitedIds.Count();
            for (int rowClust = 0; rowClust < Clusters1; rowClust++)
            {
              innerRowIDsNumber = CrossChClusterList[rowCh].Clasters[rowClust].innerIDS.Count();
              for (int colClust = 0; colClust < Clusters2; colClust++)
              {
                innerColIDsNumber = CrossChClusterList[colCh].Clasters[colClust].innerIDS.Count();


                int MinClustersNumber = CrossChClusterList[rowCh].Clasters[rowClust].innerIDS.Where(x => CrossChClusterList[colCh].Clasters[colClust].innerIDS.Contains(x)).Count();
                //Долго, но надёжно считаем нормализацию(1)
                CurrentConcatIDs.AddRange(CrossChClusterList[rowCh].Clasters[rowClust].innerIDS.Where(x => !CurrentConcatIDs.Contains(x)));
                CurrentConcatIDs.AddRange(CrossChClusterList[colCh].Clasters[colClust].innerIDS.Where(x => !CurrentConcatIDs.Contains(x)));
                //Добавляем элементы в первоначальнуютаблицу и в конечную таблицу
                List<ulong> intersected = new List<ulong>();
                foreach (ulong id in CrossChClusterList[colCh].Clasters[colClust].innerIDS)
                {
                  if (CrossChClusterList[rowCh].Clasters[rowClust].innerIDS.Contains(id))
                    intersected.Add(id);
                }
                summ += intersected.Count();
              }
            }
            //нормализация
            CrChMatrix60x60[rowCh, colCh] = summ / union;
            if (Double.IsNaN(CrChMatrix60x60[rowCh, colCh]))
              CrChMatrix60x60[rowCh, colCh] = 0;
            colPos += CrossChClusterList[colCh].Clasters.Count();
          }
          colPos = 0;
          rowPos += CrossChClusterList[rowCh].Clasters.Count();
        }
      }
      DispMatrCreating(CrChMatrix60x60);
    }

    private void DispMatrCreating(double[,] Data)
    {
      Average stat = new Average();
      Average[] ch_stat = new Average[60];
      // Расчёт коэффициентов дисперсии
      double[] disp = new double[60], Aver = new double[60];

      for (int i = 0; i < 60; i++)
        ch_stat[i] = new Average();
      for (int i = 0; i < 60; i++)
      {
        for (int j = 0; j < 60; j++)
        {
          if (i == j) continue;
          Aver[i] += Data[i, j];
          ch_stat[i].AddValueElem(Data[i, j]);
          if (Data[i, j] > 0)
            stat.AddValueElem(Data[i, j]);
        }
        Aver[i] /= 59;
        AverAverage_60 += Aver[i];
      }

      AverAverage_60 /= 60;
      stat.Calc();
      foreach (Average st in ch_stat) st.Calc();

      this.BeginInvoke(new Action(() =>
      {
        StatString = Math.Round(stat.Value, 3).ToString() + " " + plusMinus + " " + Math.Round(stat.Sigma, 3).ToString();
        Average_label.Text = "Дисперсия " + plusMinus + " среднее: " + StatString;
      }));

      for (int i = 0; i < 60; i++)
      {
        for (int j = 0; j < 60; j++)
        {
          if (i == j) continue;
          disp[i] += Math.Pow(CrChMatrix60x60[i, j] - Aver[i], 2);
        }
        disp[i] /= 59;
        AverDispersion_60 += disp[i];//Сумма квадратов
        disp[i] = Math.Sqrt(disp[i]);
      }
      AverDispersion_60 = Math.Sqrt(AverDispersion_60 / 60);//Корень из суммы квадратов
      //this.BeginInvoke(new Action(() =>
      //{
      //  dispersion_label.Text = "Dispersion: " + (Math.Round(AverDispersion_60, 3)).ToString();
      //}));

      double[,] DispMatrix8x8 = new double[8, 8];

      for (int i = 0; i < 60; i++)
      {
        int x = MEA.IDX2NAME[i] / 10 - 1;
        int y = MEA.IDX2NAME[i] % 10 - 1;
        DispMatrix8x8[x, y] = disp[i];
      }

      _DispMatrix8x8 = DispMatrix8x8;
      _Aver = Aver;
    }

    void UpdateProgressTimer_Tick(object sender, EventArgs e)
    {
      string pr = "progress: none";
      if (clusterBuilder != null)
        pr = "progress: " + ((int)clusterBuilder.Progress).ToString() + "%";
      if (progressLabel.InvokeRequired)
        progressLabel.BeginInvoke(new Action(() => progressLabel.Text = pr));
      else
        progressLabel.Text = pr;
    }

    private void DrawButton_Click(object sender, EventArgs e)
    {
      //Ошибка нулевого размера изображения
      if (MainMapPicture.Width == 0 && MainMapPicture.Height == 0)
      {
        MainMapPicture.Width = 1200;
        MainMapPicture.Height = 1200;
      }
      Bitmap bmp = CorrelationBitmap.DrawTask(CrChMatrix60x60,
        (int)(MainMapPicture.Width * 60 / 61f),
        (int)(MainMapPicture.Height * 60 / 61f),
        out rectwidth,
        out rectheight);
      Bitmap background = new Bitmap(MainMapPicture.Width, MainMapPicture.Height);
      using (Graphics G = Graphics.FromImage(background))
      {
        G.FillPolygon(new SolidBrush(Color.FromArgb(255, 255, 255, 255)),
          new Point[]
          {
            new Point(0 ,0),
            new Point(background.Width, 0),
            new Point(background.Width, background.Height),
            new Point(0, background.Height)
          });
        float imgWidth = (float)MainMapPicture.Width / 61f;
        float imgHeight = (float)MainMapPicture.Height / 61f;
        float textX = imgWidth, textY = 0, text2X = 0, text2Y = imgHeight;
        G.DrawImage((Image)bmp, imgWidth, imgHeight);

        for (int i = 1; i < 61; i++)
        {
          Point pt = new Point((int)textX, (int)textY);
          Point pt2 = new Point((int)text2X, (int)text2Y);
          G.DrawString(ChannelMatxID[i - 1].ToString(), new Font("Tahoma", 8), Brushes.Black, pt);
          G.DrawString(ChannelMatxID[i - 1].ToString(), new Font("Tahoma", 8), Brushes.Black, pt2);
          textX += imgWidth;
          text2Y += imgHeight;
        }
      }
      MainMapPicture.Image = background;
      bmp.Save(StatString + "_" + DateTime.Now.TimeOfDay.TotalHours.ToString() + ".bmp", System.Drawing.Imaging.ImageFormat.Bmp);
    }

    private void FunctionSelector_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (FunctionSelector.SelectedItem.ToString().Equals(Types[0]))
        FuncType = CharactType.IntegralFx;
      if (FunctionSelector.SelectedItem.ToString().Equals(Types[1]))
        FuncType = CharactType.DIntegralFx;
    }

    private void EnableUI(Control control)
    {
      if (control is GroupBox)
        foreach (Control c in ParamGroupBox.Controls)
        {
          if (c.InvokeRequired)
            c.BeginInvoke(new Action<Control>(x => x.Enabled = true), c);
          else
            c.Enabled = true;
        }
      else
      {
        if (control.InvokeRequired)
          control.BeginInvoke(new Action<Control>(x => x.Enabled = true), control);
        else
          control.Enabled = true;
      }
    }

    private void DisableUI(Control control)
    {
      if (control is GroupBox)
        foreach (Control c in ParamGroupBox.Controls)
        {
          if (c.InvokeRequired)
            c.BeginInvoke(new Action<Control>(x => x.Enabled = false), c);
          else
            c.Enabled = false;
        }
      else
      {
        if (control.InvokeRequired)
          control.BeginInvoke(new Action<Control>(x => x.Enabled = false), control);
        else
          control.Enabled = false;
      }
    }

    private void button1_Click(object sender, EventArgs e)
    {
      //Построение матрицы поканальной схожести в отдельной форме
      if (CrChMatrix60x60 != null && ChannelMatxID != null)
      {
        FSimilarityCh form = new FSimilarityCh(CrChMatrix60x60, ChannelMatxID, BurstIds);
        form.DrawImage();
        form.Show();
      }
      else
        Console.WriteLine("\b");
    }

    private void MainMapPicture_Click(object sender, EventArgs e)
    {

    }

    private void exportButton_Click(object sender, EventArgs e)
    {
      string folderName;
      using (FolderBrowserDialog dialog = new FolderBrowserDialog())
      {
        switch (dialog.ShowDialog())
        {
          case DialogResult.OK:
            break;
          default:
            return;
        }
        folderName = dialog.SelectedPath;
      }
      foreach (CClasterBuilder cb in CrossChClusterList)
      {
        string path = Path.Combine(folderName, "ch_" + cb.ch + ".txt");

        using (StreamWriter sw = new StreamWriter(path, false))
        {
          for (int i = 0; i < cb.Clasters.Count; i++)
          {

            List<ulong> Bsts = cb.Clasters[i].innerIDS.OrderBy(time => time).ToList();
            sw.Write(i + " ");
            for (int j = 0; j < Bsts.Count; j++)
            {
              sw.Write(Math.Round( Bsts[j]/(double)Param.DAQ_FREQ, 2).ToString() + ";");
            }
            sw.WriteLine("");

          }
        }
      }
    }

    private void Load60x60Button_Click(object sender, EventArgs e)
    {
      UseLoad60x60 = true;
      lock (FileWriteLock)
      {
        string fileName;
        using (OpenFileDialog dialog = new OpenFileDialog())
        {
          switch (dialog.ShowDialog())
          {
            case DialogResult.OK:
              break;
            default:
              return;
          }
          fileName = dialog.FileName;
        }
        using (FileStream fs = new FileStream(fileName, FileMode.OpenOrCreate))
        {
          BinaryFormatter formatter = new BinaryFormatter();
          CrChMatrix60x60 = (double[,])formatter.Deserialize(fs);
        }
      }
      AnalyseAllDirectBtn.PerformClick();
    }

    private void BuildBinTreeBtn_Click(object sender, EventArgs e)
    {
      int k = 0;//Сдвиговый итератор
      //Build List<List..>> for Data and ChannelNumber
      List<List<double>> TreeData = new List<List<double>>();
      for (int i = 0; (i + k) < 60; i++)
      {
        if (BinTreeOutChannels.Contains(MEA.IDX2NAME[i + k]))
        {
          k++;
        }
        TreeData.Add(new List<double>());
        for (int j = 0; j < 60; j++)
        {
          //Выкидываем нулевые каналы из данных
          if (CrChMatrix60x60[i + k, j] != 0.0)
            TreeData[i].Add(CrChMatrix60x60[i + k, j]);
          else if (i == 0)
            BinTreeOutChannels.Add(MEA.IDX2NAME[j]);
        }
      }
      List<int> ChannelIDs = new List<int>();
      for (int i = 0; i < 60; i++)
      {
        if (!(BinTreeOutChannels.Contains(MEA.IDX2NAME[i])))
          ChannelIDs.Add(MEA.IDX2NAME[i]);
      }
      FChannelClusterTree form = new FChannelClusterTree(TreeData, ChannelIDs, CrChMatrix60x60);
      form.Show();
    }

    private void dispersion_button_Click(object sender, EventArgs e)
    {
      FDispersionMatrix form = new FDispersionMatrix(_DispMatrix8x8, _Aver);
      form.DrawDisperseImage();
      form.Show();
    }

    private void FindClustersBtn_Click(object sender, EventArgs e)
    {
      FClusterTree form = new FClusterTree(clusterBuilder.Matrix, BurstIds);
      form.Show();
    }
  }
}
