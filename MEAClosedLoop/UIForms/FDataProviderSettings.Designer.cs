﻿namespace MEAClosedLoop.UIForms
{
    partial class FDataProviderSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.isFltWritingOnLabel = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.fltWritingButton = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.OpenBtn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.rawDataLabel = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.rawWritingButton = new System.Windows.Forms.Button();
            this.openRawFileButton = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.OpenBtn);
            this.groupBox1.Controls.Add(this.isFltWritingOnLabel);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.fltWritingButton);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(433, 120);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Flt Data";
            // 
            // isFltWritingOnLabel
            // 
            this.isFltWritingOnLabel.AutoSize = true;
            this.isFltWritingOnLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Bold);
            this.isFltWritingOnLabel.Location = new System.Drawing.Point(152, 25);
            this.isFltWritingOnLabel.Name = "isFltWritingOnLabel";
            this.isFltWritingOnLabel.Size = new System.Drawing.Size(91, 17);
            this.isFltWritingOnLabel.TabIndex = 3;
            this.isFltWritingOnLabel.Text = "выключена";
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox1.Enabled = false;
            this.textBox1.Location = new System.Drawing.Point(6, 50);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(343, 20);
            this.textBox1.TabIndex = 2;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.label1.Location = new System.Drawing.Point(6, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(140, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Запись Filtered Data";
            // 
            // fltWritingButton
            // 
            this.fltWritingButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.fltWritingButton.Location = new System.Drawing.Point(134, 76);
            this.fltWritingButton.Name = "fltWritingButton";
            this.fltWritingButton.Size = new System.Drawing.Size(128, 33);
            this.fltWritingButton.TabIndex = 0;
            this.fltWritingButton.Text = "Включить";
            this.fltWritingButton.UseVisualStyleBackColor = true;
            this.fltWritingButton.Click += new System.EventHandler(this.fltWritingButton_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.openRawFileButton);
            this.groupBox2.Controls.Add(this.rawWritingButton);
            this.groupBox2.Controls.Add(this.textBox2);
            this.groupBox2.Controls.Add(this.rawDataLabel);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Location = new System.Drawing.Point(12, 137);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(430, 120);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Raw Data";
            this.groupBox2.Enter += new System.EventHandler(this.groupBox2_Enter);
            // 
            // OpenBtn
            // 
            this.OpenBtn.Location = new System.Drawing.Point(355, 48);
            this.OpenBtn.Name = "OpenBtn";
            this.OpenBtn.Size = new System.Drawing.Size(75, 23);
            this.OpenBtn.TabIndex = 4;
            this.OpenBtn.Text = "Открыть";
            this.OpenBtn.UseVisualStyleBackColor = true;
            this.OpenBtn.Click += new System.EventHandler(this.OpenBtn_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.label2.Location = new System.Drawing.Point(6, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Запись Raw Data";
            // 
            // rawDataLabel
            // 
            this.rawDataLabel.AutoSize = true;
            this.rawDataLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Bold);
            this.rawDataLabel.Location = new System.Drawing.Point(152, 27);
            this.rawDataLabel.Name = "rawDataLabel";
            this.rawDataLabel.Size = new System.Drawing.Size(91, 17);
            this.rawDataLabel.TabIndex = 4;
            this.rawDataLabel.Text = "выключена";
            // 
            // textBox2
            // 
            this.textBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox2.Enabled = false;
            this.textBox2.Location = new System.Drawing.Point(9, 61);
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(343, 20);
            this.textBox2.TabIndex = 5;
            // 
            // rawWritingButton
            // 
            this.rawWritingButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.rawWritingButton.Location = new System.Drawing.Point(134, 81);
            this.rawWritingButton.Name = "rawWritingButton";
            this.rawWritingButton.Size = new System.Drawing.Size(128, 33);
            this.rawWritingButton.TabIndex = 6;
            this.rawWritingButton.Text = "Включить";
            this.rawWritingButton.UseVisualStyleBackColor = true;
            this.rawWritingButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // openRawFileButton
            // 
            this.openRawFileButton.Location = new System.Drawing.Point(355, 58);
            this.openRawFileButton.Name = "openRawFileButton";
            this.openRawFileButton.Size = new System.Drawing.Size(75, 23);
            this.openRawFileButton.TabIndex = 7;
            this.openRawFileButton.Text = "Открыть";
            this.openRawFileButton.UseVisualStyleBackColor = true;
            this.openRawFileButton.Click += new System.EventHandler(this.openRawFileButton_Click);
            // 
            // FDataProviderSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(457, 272);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "FDataProviderSettings";
            this.Text = "FDataProviderSettings";
            this.Load += new System.EventHandler(this.FDataProviderSettings_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
    private System.Windows.Forms.Label isFltWritingOnLabel;
    private System.Windows.Forms.TextBox textBox1;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Button fltWritingButton;
        private System.Windows.Forms.Button OpenBtn;
        private System.Windows.Forms.Button openRawFileButton;
        private System.Windows.Forms.Button rawWritingButton;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label rawDataLabel;
        private System.Windows.Forms.Label label2;
    }
}