﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MEAClosedLoop.UIForms
{
  public partial class FDataProviderSettings : Form
  {
    string[] status = { "выключена", "включена" };
    string[] buttonText = { "Включить", "Выключить" };
    public FDataProviderSettings()
    {
      InitializeComponent();
      textBox1.Text = CFltDataProvider.defaultPath;
    }

    private void groupBox2_Enter(object sender, EventArgs e)
    {

    }

    private void FDataProviderSettings_Load(object sender, EventArgs e)
    {
      if (Global.dw.isFltDataWriting)
      {
        isFltWritingOnLabel.Text = status[1];
        fltWritingButton.Text = buttonText[1];
      }
      else
      {
        isFltWritingOnLabel.Text = status[0];
        fltWritingButton.Text = buttonText[0];
      }
    }

    private void fltWritingButton_Click(object sender, EventArgs e)
    {
      Global.dw.isFltDataWriting = !Global.dw.isFltDataWriting;
      if(Global.dw.isFltDataWriting)
      {
        isFltWritingOnLabel.Text = status[1];
        fltWritingButton.Text = buttonText[1];
      }
      else
      {
        isFltWritingOnLabel.Text = status[0];
        fltWritingButton.Text = buttonText[0];
      }
    }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void OpenBtn_Click(object sender, EventArgs e)
        {
            string FilePath = "";
            SaveFileDialog open_db_file = new SaveFileDialog();
            open_db_file.Filter = "raw| *.raw";

            switch (open_db_file.ShowDialog())
            {
                case System.Windows.Forms.DialogResult.OK:
                    if (open_db_file.FileNames.Count() > 1)
                    {
                        MessageBox.Show("необходимо выбрать только 1 файл", "ошибка");
                        return;
                    }
                    FilePath = open_db_file.FileName;
                    CFltDataProvider.defaultPath = FilePath;
                    textBox1.Text = FilePath;
                    break;
                case System.Windows.Forms.DialogResult.Cancel:
                    return;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Global.dw.isRawDataWriting = !Global.dw.isRawDataWriting;
            if (Global.dw.isRawDataWriting)
            {
                rawDataLabel.Text = status[1];
                rawWritingButton.Text = buttonText[1];
            }
            else
            {
                rawDataLabel.Text = status[0];
                rawWritingButton.Text = buttonText[0];
            }
        }

        private void openRawFileButton_Click(object sender, EventArgs e)
        {
            string FilePath = "";
            SaveFileDialog open_db_file = new SaveFileDialog();
            open_db_file.Filter = "raw| *.raw";

            switch (open_db_file.ShowDialog())
            {
                case System.Windows.Forms.DialogResult.OK:
                    if (open_db_file.FileNames.Count() > 1)
                    {
                        MessageBox.Show("необходимо выбрать только 1 файл", "ошибка");
                        return;
                    }
                    FilePath = open_db_file.FileName;
                    CRawDataProvider.defaultPath = FilePath;
                    textBox2.Text = FilePath;
                    break;
                case System.Windows.Forms.DialogResult.Cancel:
                    return;
            }
        }
    }
}
