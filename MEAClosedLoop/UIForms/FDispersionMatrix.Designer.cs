﻿namespace MEAClosedLoop.UIForms
{
  partial class FDispersionMatrix
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.dispersion_pictureBox = new System.Windows.Forms.PictureBox();
      ((System.ComponentModel.ISupportInitialize)(this.dispersion_pictureBox)).BeginInit();
      this.SuspendLayout();
      // 
      // dispersion_pictureBox
      // 
      this.dispersion_pictureBox.BackColor = System.Drawing.SystemColors.ActiveCaption;
      this.dispersion_pictureBox.Location = new System.Drawing.Point(12, 12);
      this.dispersion_pictureBox.Name = "dispersion_pictureBox";
      this.dispersion_pictureBox.Size = new System.Drawing.Size(640, 480);
      this.dispersion_pictureBox.TabIndex = 0;
      this.dispersion_pictureBox.TabStop = false;
      // 
      // FDispersionMatrix
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(664, 507);
      this.Controls.Add(this.dispersion_pictureBox);
      this.Name = "FDispersionMatrix";
      this.Text = "Dispersion Matrix";
      ((System.ComponentModel.ISupportInitialize)(this.dispersion_pictureBox)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.PictureBox dispersion_pictureBox;
  }
}