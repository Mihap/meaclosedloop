﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using MEAClosedLoop.Algorithms;

namespace MEAClosedLoop.UIForms
{
  public partial class FDispersionMatrix : Form
  {
    private double[,] _Mtx = new double[8, 8];
    private List<List<double>> _Matrix8x8 = new List<List<double>>();
    double[] _Aver = new double[60];
    float rectwidth = 0, rectheight = 0;

    public FDispersionMatrix(double[,] Matrix8x8, double[] Average)
    {
      InitializeComponent();
      for (int i = 0; i < 8; i++)
      {
        _Matrix8x8.Add(new List<double>());
        for (int j = 0; j < 8; j++)
          _Matrix8x8[i].Add(Matrix8x8[i, j]);

      }
      _Aver = Average;
    }

    public List<List<double>> MatrixCreate()
    {
      //Построение матрицы дисперсий
      List<List<double>> DispMatrix = new List<List<double>>();
      double average = new double();
      for (int i = 0; i < 8; i++)
        for (int j = 0; j < 8; j++)
          average += _Matrix8x8[i][j];
      average /= 60;
      for (int i = 0; i < 8; i++)
      {
        DispMatrix.Add(new List<double>());
        for (int j = 0; j < 8; j++)
        {
          if (_Matrix8x8[j][i] == 0.0)
            DispMatrix[i].Add(0.0);
          else
            DispMatrix[i].Add(Math.Sqrt(Math.Abs(Math.Pow(_Matrix8x8[j][i], 2) - average * average)));
        }
      }
      DispMatrix[0][0] = DispMatrix[0][7] = DispMatrix[7][0] = DispMatrix[7][7] = 0.0;
      return DispMatrix;
    }

    public void DrawImage()
    {
      Bitmap bmp = CorrelationBitmap.DrawTask(_Matrix8x8, dispersion_pictureBox.Width, dispersion_pictureBox.Height, out rectwidth, out rectheight);
      using (Graphics G = Graphics.FromImage(bmp))
      {
        float imgWidth = (float)dispersion_pictureBox.Width / 8f;
        float imgHeight = (float)dispersion_pictureBox.Height / 8f;
        float textX = 0, textY = 0;
        for (int i = 0; i < 8; i++)
        {
          for (int j = 0; j < 8; j++)
          {
            if ((i == 0 || i == 7) && (j == 0 || j == 7))
            {
              textX += imgWidth;
              continue;
            }
            Point pt = new Point((int)textX, (int)textY);
            G.DrawString(((j + 1) * 10 + i + 1).ToString(), new Font("Tahoma", 12), Brushes.Black, pt);
            textX += imgWidth;
          }
          textX = 0;
          textY += imgHeight;
        }
      }
      dispersion_pictureBox.Image = bmp;
      bmp.Save("DispChan-" + DateTime.Now.Hour.ToString() + ".bmp", System.Drawing.Imaging.ImageFormat.Bmp);
    }

    public void DrawDisperseImage()
    {
      Bitmap bmp = CorrelationBitmap.DrawTask(MatrixCreate(), dispersion_pictureBox.Width, dispersion_pictureBox.Height, out rectwidth, out rectheight);
      using (Graphics G = Graphics.FromImage(bmp))
      {
        float imgWidth = (float)dispersion_pictureBox.Width / 8f;
        float imgHeight = (float)dispersion_pictureBox.Height / 8f;
        float textX = 0, textY = 0;
        for (int i = 0; i < 8; i++)
        {
          for (int j = 0; j < 8; j++)
          {
            if ((i == 0 || i == 7) && (j == 0 || j == 7))
            {
              textX += imgWidth;
              continue;
            }
            Point pt = new Point((int)textX, (int)textY);
            G.DrawString(((j + 1) * 10 + i + 1).ToString(), new Font("Tahoma", 12), Brushes.Black, pt);
            textX += imgWidth;
          }
          textX = 0;
          textY += imgHeight;
        }
      }
      dispersion_pictureBox.Image = bmp;
      bmp.Save("debug-DispDispChan-" + /*DateTime.Now.Date.ToString() +*/ DateTime.Now.TimeOfDay.TotalMinutes.ToString() + ".bmp", System.Drawing.Imaging.ImageFormat.Bmp);
    }
  }
}
