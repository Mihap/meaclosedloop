﻿namespace MEAClosedLoop.UIForms
{
  partial class FFileIOTest
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.button1 = new System.Windows.Forms.Button();
      this.BurstCountTB = new System.Windows.Forms.TextBox();
      this.GetAllBurstData = new System.Windows.Forms.Button();
      this.LoadDurationTB = new System.Windows.Forms.TextBox();
      this.getFltDataCountBtn = new System.Windows.Forms.Button();
      this.getAllPacketsDataBtn = new System.Windows.Forms.Button();
      this.DataCountTB = new System.Windows.Forms.TextBox();
      this.DataLoadDurationTB = new System.Windows.Forms.TextBox();
      this.SuspendLayout();
      // 
      // button1
      // 
      this.button1.Location = new System.Drawing.Point(13, 13);
      this.button1.Name = "button1";
      this.button1.Size = new System.Drawing.Size(139, 23);
      this.button1.TabIndex = 0;
      this.button1.Text = "get Burst Count in File";
      this.button1.UseVisualStyleBackColor = true;
      this.button1.Click += new System.EventHandler(this.button1_Click);
      // 
      // BurstCountTB
      // 
      this.BurstCountTB.Location = new System.Drawing.Point(169, 15);
      this.BurstCountTB.Name = "BurstCountTB";
      this.BurstCountTB.Size = new System.Drawing.Size(100, 20);
      this.BurstCountTB.TabIndex = 1;
      // 
      // GetAllBurstData
      // 
      this.GetAllBurstData.Enabled = false;
      this.GetAllBurstData.Location = new System.Drawing.Point(12, 42);
      this.GetAllBurstData.Name = "GetAllBurstData";
      this.GetAllBurstData.Size = new System.Drawing.Size(140, 23);
      this.GetAllBurstData.TabIndex = 0;
      this.GetAllBurstData.Text = "get All Burst Data ";
      this.GetAllBurstData.UseVisualStyleBackColor = true;
      this.GetAllBurstData.Click += new System.EventHandler(this.GetAllBurstData_Click);
      // 
      // LoadDurationTB
      // 
      this.LoadDurationTB.Location = new System.Drawing.Point(169, 44);
      this.LoadDurationTB.Name = "LoadDurationTB";
      this.LoadDurationTB.Size = new System.Drawing.Size(100, 20);
      this.LoadDurationTB.TabIndex = 1;
      // 
      // getFltDataCountBtn
      // 
      this.getFltDataCountBtn.Location = new System.Drawing.Point(13, 83);
      this.getFltDataCountBtn.Name = "getFltDataCountBtn";
      this.getFltDataCountBtn.Size = new System.Drawing.Size(139, 23);
      this.getFltDataCountBtn.TabIndex = 0;
      this.getFltDataCountBtn.Text = "get Packet Count in File";
      this.getFltDataCountBtn.UseVisualStyleBackColor = true;
      this.getFltDataCountBtn.Click += new System.EventHandler(this.getFltDataCountBtn_Click);
      // 
      // getAllPacketsDataBtn
      // 
      this.getAllPacketsDataBtn.Enabled = false;
      this.getAllPacketsDataBtn.Location = new System.Drawing.Point(12, 112);
      this.getAllPacketsDataBtn.Name = "getAllPacketsDataBtn";
      this.getAllPacketsDataBtn.Size = new System.Drawing.Size(140, 23);
      this.getAllPacketsDataBtn.TabIndex = 0;
      this.getAllPacketsDataBtn.Text = "get All Burst Data ";
      this.getAllPacketsDataBtn.UseVisualStyleBackColor = true;
      this.getAllPacketsDataBtn.Click += new System.EventHandler(this.getAllPacketsDataBtn_Click);
      // 
      // DataCountTB
      // 
      this.DataCountTB.Location = new System.Drawing.Point(169, 85);
      this.DataCountTB.Name = "DataCountTB";
      this.DataCountTB.Size = new System.Drawing.Size(100, 20);
      this.DataCountTB.TabIndex = 1;
      // 
      // DataLoadDurationTB
      // 
      this.DataLoadDurationTB.Location = new System.Drawing.Point(169, 114);
      this.DataLoadDurationTB.Name = "DataLoadDurationTB";
      this.DataLoadDurationTB.Size = new System.Drawing.Size(100, 20);
      this.DataLoadDurationTB.TabIndex = 1;
      // 
      // FFileIOTest
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(619, 202);
      this.Controls.Add(this.DataLoadDurationTB);
      this.Controls.Add(this.LoadDurationTB);
      this.Controls.Add(this.DataCountTB);
      this.Controls.Add(this.BurstCountTB);
      this.Controls.Add(this.getAllPacketsDataBtn);
      this.Controls.Add(this.GetAllBurstData);
      this.Controls.Add(this.getFltDataCountBtn);
      this.Controls.Add(this.button1);
      this.Name = "FFileIOTest";
      this.Text = "FFileIOTest";
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Button button1;
    private System.Windows.Forms.TextBox BurstCountTB;
    private System.Windows.Forms.Button GetAllBurstData;
    private System.Windows.Forms.TextBox LoadDurationTB;
    private System.Windows.Forms.Button getFltDataCountBtn;
    private System.Windows.Forms.Button getAllPacketsDataBtn;
    private System.Windows.Forms.TextBox DataCountTB;
    private System.Windows.Forms.TextBox DataLoadDurationTB;
  }
}