﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.Threading.Tasks;

namespace MEAClosedLoop.UIForms
{

  using TFltDataPacket = Dictionary<int, System.Double[]>;
  public partial class FFileIOTest : Form
  {
    private List<ulong> dataIds = new List<ulong>();
    private int DataCount = 0;
    public FFileIOTest()
    {
      InitializeComponent();
    }

    private void button1_Click(object sender, EventArgs e)
    {
      dataIds.Clear();
      dataIds.AddRange(CBurstDataProvider.BurstAdrCollection.Keys);
      this.BurstCountTB.Text = dataIds.Count.ToString();

      GetAllBurstData.Enabled = true;
    }

    private void GetAllBurstData_Click(object sender, EventArgs e)
    {
      Task asyncTest = new Task(loadAsyc);
      asyncTest.Start();
    }
    private void loadAsyc()
    {
      Stopwatch sw = new Stopwatch();
      sw.Start();
      for (int i = 0; i < dataIds.Count; i++)
      {
        CBurst temp = CBurstDataProvider.GetBurst(dataIds[i]);
      }
      sw.Stop();
      LoadDurationTB.BeginInvoke(new Action<string>(s => LoadDurationTB.Text = s), sw.ElapsedMilliseconds.ToString() + " ms");
    }

    private void getFltDataCountBtn_Click(object sender, EventArgs e)
    {

      DataCount = CFltDataProvider.FltDataCollection.Count();
      DataCountTB.Text = DataCount.ToString();
      if (DataCount > 0)
        getAllPacketsDataBtn.Enabled = true;
    }

    private void getAllPacketsDataBtn_Click(object sender, EventArgs e)
    {
      Stopwatch sw = new Stopwatch();
      sw.Start();
      List<ulong> Keys = CFltDataProvider.FltDataCollection.Keys.Take(DataCount).ToList();
      for (int i = 0; i < DataCount; i++)
      {
        TFltDataPacket packet = CFltDataProvider.GetFltData(CFltDataProvider.FltDataCollection[Keys[i]].Begin);
      }
      sw.Stop();
      LoadDurationTB.BeginInvoke(new Action<string>(s => LoadDurationTB.Text = s), sw.ElapsedMilliseconds.ToString() + " ms");
    }
  }
}
