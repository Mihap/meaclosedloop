﻿namespace MEAClosedLoop.UIForms
{
  partial class FHistoView
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.MainPlot = new ZedGraph.ZedGraphControl();
      this.SuspendLayout();
      // 
      // MainPlot
      // 
      this.MainPlot.Dock = System.Windows.Forms.DockStyle.Fill;
      this.MainPlot.Location = new System.Drawing.Point(0, 0);
      this.MainPlot.Name = "MainPlot";
      this.MainPlot.ScrollGrace = 0D;
      this.MainPlot.ScrollMaxX = 0D;
      this.MainPlot.ScrollMaxY = 0D;
      this.MainPlot.ScrollMaxY2 = 0D;
      this.MainPlot.ScrollMinX = 0D;
      this.MainPlot.ScrollMinY = 0D;
      this.MainPlot.ScrollMinY2 = 0D;
      this.MainPlot.Size = new System.Drawing.Size(712, 345);
      this.MainPlot.TabIndex = 0;
      // 
      // FHistoView
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(712, 345);
      this.Controls.Add(this.MainPlot);
      this.Name = "FHistoView";
      this.Text = "FHistoView";
      this.ResumeLayout(false);

    }

    #endregion

    private ZedGraph.ZedGraphControl MainPlot;
  }
}