﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ZedGraph;
namespace MEAClosedLoop.UIForms
{
  public partial class FHistoView : Form
  {
    public List<Tuple<double, double>> histoData = new List<Tuple<double,double>>();
    public FHistoView()
    {
      InitializeComponent();
    }
    public FHistoView(List<Tuple<double, double>> data)
    {
      InitializeComponent();
      histoData = data;
    }
    public void DrawData()
    {
      GraphPane pane = MainPlot.GraphPane;
      pane.CurveList.Clear();
      pane.XAxis.Title.IsVisible = false;
      pane.YAxis.Title.IsVisible = false;

      PointPairList list = new PointPairList();
      for (int i = 0; i < histoData.Count; i++)
      {
        list.Add(histoData[i].Item1, histoData[i].Item2);
      }
      //LineItem curve = pane.AddCurve(null, list, Color.Blue, SymbolType.None);
      BarItem bar = pane.AddBar("Histo", histoData.Select(x => x.Item1).ToArray(), histoData.Select(x => x.Item2).ToArray(), Color.Blue);

      MainPlot.AxisChange();
      MainPlot.Invalidate();
    }
  }
}
