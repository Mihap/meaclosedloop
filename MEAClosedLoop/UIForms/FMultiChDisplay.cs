﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using ZedGraph;

namespace MEAClosedLoop.UIForms
{
  using TData = System.Double;
  using TTime = System.UInt64;
  using TStimIndex = System.Int16;
  using TAbsStimIndex = System.UInt64;
  using TRawDataPacket = Dictionary<int, ushort[]>;
  using TFltDataPacket = Dictionary<int, System.Double[]>;
  public delegate void OnPacketRecievedDelegate(TFltDataPacket packet);
  public partial class FMultiChDisplay : Form, IRecieveFltData
  {
    private event OnPacketRecievedDelegate onPacketRecieved;
    //private Queue<> dataQueue = new Queue<double>();
    private Dictionary<int, List<double>> mchDataQueue = new Dictionary<int, List<TData>>();
    private Dictionary<int, List<double>> mchRawDataQueue = new Dictionary<int, List<TData>>();
    private Queue<TFltDataPacket> unpackedFltDataQueue = new Queue<TFltDataPacket>();
    private object DataQueueLock = new object();

    Timer plotUpdater;
    const uint updateInterval = 1000;
    int currentChNum = 0;
    Task updatePlotTask;

    uint _Duration2 = Param.MS * 1000;
    uint _Amplitude = 800;
    uint Amplitude2
    {
      get
      {
        return _Amplitude;
      }
      set
      {
        if (value > 10 && value < 3000)
          _Amplitude = value;
      }
    }
    uint Duration2
    {
      get { return _Duration2; }
      set { if (value >= Param.MS * 1000) _Duration2 = value; }
    }

    public event OnDisconnectDelegate OnDisconnect;

    public FMultiChDisplay()
    {
      InitializeComponent();
      initPlot();
    }
    void IRecieveFltData.RecieveFltData(TFltDataPacket packet)
    {
      if (onPacketRecieved != null) onPacketRecieved(packet);
      if (plotUpdater != null && plotUpdater.Enabled)
        lock (DataQueueLock)
        {
          unpackedFltDataQueue.Enqueue(packet);
          if (unpackedFltDataQueue.Select(x => x[0].Length).Sum() > Param.MS * 5000)
          {
            unpackedFltDataQueue.Dequeue();
          }
        }
    }

    private void StartButton_Click(object sender, EventArgs e)
    {
      start();
    }

    void plotUpdater_Tick(object sender, EventArgs e)
    {
      if (updatePlotTask != null)
      {
        if (updatePlotTask.IsCompleted)
        {
          updatePlotTask = new Task(this.updatePlot);
          updatePlotTask.Start();
        }
        else if (updatePlotTask.Status == TaskStatus.Running)
        {
          updatePlotTask.Wait();
          updatePlotTask = new Task(this.updatePlot);
          updatePlotTask.Start();
        }
      }
      else
      {
        updatePlotTask = new Task(this.updatePlot);
        updatePlotTask.Start();
      }
    }
    void initPlot()
    {
      // Создаем экземпляр класса MasterPane, который представляет собой область, 
      // на которйо "лежат" все графики (экземпляры класса GraphPane)

      ZedGraph.MasterPane masterPane = zedGraphPlot.MasterPane;
      // По умолчанию в MasterPane содержится один экземпляр класса GraphPane 
      // (который можно получить из свойства zedGraph.GraphPane)
      // Очистим этот список, так как потом мы будем создавать графики вручную
      masterPane.PaneList.Clear();
      masterPane.Margin.All = 0;
      masterPane.Border.IsVisible = false;
      masterPane.InnerPaneGap = 0;
      masterPane.IsAntiAlias = false;
      zedGraphPlot.IsSynchronizeXAxes = true;
      zedGraphPlot.IsSynchronizeYAxes = true;
      for (int i = 0; i < 64; i++)
      {
        // Создаем экземпляр класса GraphPane, представляющий собой один график
        GraphPane pane = new GraphPane();

        pane.Margin.Top = 0;
        pane.Margin.Left = 1;
        pane.Margin.Right = 1;
        pane.Margin.Bottom = 0;
        pane.XAxis.Scale.IsSkipFirstLabel = true;
        pane.XAxis.Scale.IsSkipLastLabel = true;
        pane.YAxis.Scale.IsSkipFirstLabel = true;
        pane.YAxis.Scale.IsSkipLastLabel = true;
        pane.XAxis.Scale.FontSpec.Size = 9;
        pane.YAxis.Scale.FontSpec.Size = 9;
        //pane.XAxis.Scale.MajorStep = 0.5;
        //pane.XAxis.Scale.MinorStep = 0.5;
        //pane.YAxis.Scale.MajorStep = 0.5;
        //pane.YAxis.Scale.MinorStep = 0.5;
        pane.YAxis.Scale.Align = AlignP.Inside;
        pane.XAxis.Scale.IsVisible = false;
        pane.YAxis.Scale.AlignH = AlignH.Right;
        pane.Title.IsVisible = false;

        //pane.XAxis.Scale.AlignH = AlignH.Left;
        //pane.XAxis.Scale.Align = AlignP.Inside;

        //pane.XAxis.Cross = +Amplitude2;
        pane.X2Axis.IsVisible = false;
        pane.XAxis.Title.IsVisible = false;
        pane.BaseDimension = 1.5f;
        //pane.XAxis.IsVisible = false;
        pane.Y2Axis.IsVisible = false;
        pane.YAxis.IsVisible = false;
        //if (i < 8)
        //{
        //  pane.XAxis.IsVisible = true;
        //}
        ////pane.Margin.Left = -15;
        //if (i % 8 == 0)
        //{
        //  pane.Margin.Left = 0;
        //  pane.YAxis.IsVisible = true;
        ////}

        int chj = (i / 8 + 1);
        int chi = (i % 8 + 1);
        var text = new TextObj(chj.ToString() + chi.ToString(), 0.05, 0.1, CoordType.PaneFraction);
        pane.GraphObjList.Add(text);

        pane.Legend.IsVisible = false;

        pane.Title.IsVisible = false;

        //pane.TitleGap = 0;
        // Добавим новый график в MasterPane
        masterPane.Add(pane);
      }
      // Будем размещать добавленные графики в MasterPane
      using (Graphics g = CreateGraphics())
      {
        // Графики будут размещены в три строки.
        // В первой будет 4 столбца,
        // Во второй - 2
        // В третей - 3
        // Если бы второй аргумент был равен false, то массив из третьего аргумента
        // задавал бы не количество столбцов в каждой строке,
        // а количество строк в каждом столбце
        masterPane.SetLayout(g, false, new int[] { 8, 8, 8, 8, 8, 8, 8, 8 });
      }

      // Обновим оси и перерисуем график
      zedGraphPlot.AxisChange();
      zedGraphPlot.Invalidate();
    }

    private void updatePlot()
    {
      // если окно закрыто
      if (this.IsDisposed)
      {
        //останавливаем таймер, завершая отрисовку
        plotUpdater.Stop();
        return;
      }
      try
      {
        Stopwatch sw1 = new Stopwatch();
        Stopwatch sw2 = new Stopwatch();
        sw2.Start();
        int PartsLength;
        int PartsCount;

        if (mchDataQueue.Keys.Count != 60) return;
        lock (DataQueueLock)
        {
          // вынимаем необработанные данные из очереди поступивших пакетов
          while (unpackedFltDataQueue.Count > 0)
          {
            TFltDataPacket currentPacket = unpackedFltDataQueue.Dequeue();
            foreach (int key in mchDataQueue.Keys)
            {
              double[] Data = currentPacket[key];
              mchRawDataQueue[key].AddRange(currentPacket[key]);

            }
          }
          // удаляем лишние элементы из очереди данных
          // расчет удаляемой длины
          int length = 0;
          int dequeueCount = 0;
          length = mchRawDataQueue[0].Count;
          dequeueCount = length - (int)Duration2;
          // удаляем со всех каналов
          if (dequeueCount > 0)
            foreach (int key in mchDataQueue.Keys)
            {
              // removeRange работает быстрее (вроде бы, но не факт!, т.к. информация не подтверждена  )
              mchRawDataQueue[key].RemoveRange(0, dequeueCount);
            }

          //парсим графические данные
          //Размеры окон для создания пары min/max 
          if (zedGraphPlot.Width == 0) return;
          PartsLength = (mchRawDataQueue[0].Count > 0) ? (int)Duration2 * 8 / zedGraphPlot.Width : 0;
          PartsCount = (PartsLength > 0) ? mchRawDataQueue[0].Count / PartsLength : 0;
          // вычисляем для всех каналов

          foreach (int key in mchRawDataQueue.Keys)
          {
            mchDataQueue[key].Clear();
            double min = double.MaxValue;
            double max = double.MinValue;
            // ссылка на лист для оптимизации
            List<double> Data = mchRawDataQueue[key];
            for (int i = 0; i <= PartsCount; i++)
            {
              //нач значения
              min = double.MaxValue;
              max = double.MinValue;
              // обновляем для текущего окна
              for (int ii = 0; ii < PartsLength && i * PartsLength + ii < Data.Count; ii++)
              {
                if (Data[i * PartsLength + ii] > max) max = Data[i * PartsLength + ii];
                if (Data[i * PartsLength + ii] < min) min = Data[i * PartsLength + ii];
              }
              //заносим в общий массив
              mchDataQueue[key].Add(min);
              mchDataQueue[key].Add(max);
            }
          }
        }


        sw2.Stop();
        sw1.Start();

        PaneList paneList = zedGraphPlot.MasterPane.PaneList;

        double[] xValueArray = new double[mchDataQueue[0].Count()];
        for (int i = 0; i < xValueArray.Length; i += 2)
        {
          xValueArray[i] = i * PartsLength / 50.0;
          xValueArray[i + 1] = xValueArray[i];
        }

        foreach (int ch in mchDataQueue.Keys)
        {
          int j = (MEA.IDX2NAME[ch] / 10 - 1);
          int i = (MEA.IDX2NAME[ch] % 10 - 1);
          var pane = paneList[j * 8 + i];
          //var text = new TextObj((i+1).ToString()+"."+(j+1).ToString(), 0, 0,CoordType.PaneFraction, AlignH.Left, AlignV.Bottom);
          //text.IsClippedToChartRect = false;
          //pane.GraphObjList.Add(text);
          pane.CurveList.Clear();
          pane.XAxis.Scale.Min = 0;
          pane.XAxis.Scale.Max = Duration2 / 25.0;
          pane.YAxis.Scale.Min = -Amplitude2;
          pane.YAxis.Scale.Max = +Amplitude2;

          double[] yValueArray = mchDataQueue[ch].ToArray();
          LineItem f1_curve = pane.AddCurve(MEA.IDX2NAME[ch].ToString(), xValueArray, yValueArray, Color.Blue, SymbolType.None);
          f1_curve.Label.IsVisible = true;
        }
        zedGraphPlot.AxisChange();
        // Обновляем график
        zedGraphPlot.Invalidate();
        sw1.Stop();
        string s = "post: " + (sw1.ElapsedMilliseconds).ToString() + " pre " + sw2.ElapsedMilliseconds.ToString() + " ms";
        if (UpdateTimeLabel.InvokeRequired)
          UpdateTimeLabel.BeginInvoke(new Action<System.Windows.Forms.Label>((lab) => lab.Text = s), UpdateTimeLabel);
        else
          UpdateTimeLabel.Text = s;
      }
      catch(Exception e)
      {

      }
    }

    private void start()
    {
      plotUpdater = new Timer();
      plotUpdater.Interval = 1000;
      plotUpdater.Tick += plotUpdater_Tick;
      plotUpdater.Start();
    }

    private void stop()
    {
      plotUpdater.Stop();
    }

    private void stopButton_Click(object sender, EventArgs e)
    {
      stop();
    }

    private void AmplitudeChecker_ValueChanged(object sender, EventArgs e)
    {

      Amplitude2 = (uint)(sender as NumericUpDown).Value;
    }

    private void DurationChecker_ValueChanged(object sender, EventArgs e)
    {
      Duration2 = (uint)(sender as NumericUpDown).Value * Param.MS * 1000;
    }

    private void ChNumChecker_ValueChanged(object sender, EventArgs e)
    {
      lock (DataQueueLock)
      {
        //dataQueue.Clear();
        currentChNum = (int)(sender as NumericUpDown).Value;
      }
    }

    private void FMultiChDisplay_Load(object sender, EventArgs e)
    {
      onPacketRecieved += initCollection;
    }

    private void initCollection(TFltDataPacket packet)
    {
      foreach (int key in packet.Keys)
      {
        mchDataQueue.Add(key, new List<double>());
        mchRawDataQueue.Add(key, new List<double>());
      }
      onPacketRecieved -= initCollection;
    }

    private void zedGraphPlot_Resize(object sender, EventArgs e)
    {
      lock (DataQueueLock)
      {
        foreach (int key in mchDataQueue.Keys)
        {
          mchDataQueue[key].Clear();
          mchRawDataQueue[key].Clear();
        }
      }
    }

    private bool zedGraphPlot_DoubleClickEvent(ZedGraphControl sender, MouseEventArgs e)
    {
      if (mchDataQueue.Count > 0 && plotUpdater != null)
      {
        int coord_w = (int)Math.Truncate(e.X / zedGraphPlot.MasterPane[0].Rect.Width);
        int coord_h = (int)Math.Truncate(e.Y / zedGraphPlot.MasterPane[0].Rect.Height);
        FSingleChDisplay display = new FSingleChDisplay();
        FMainWindow mainwindow = (FMainWindow)this.Owner;
        if (mainwindow != null)
        {
          mainwindow.dataFlowController.AddConsumer(display);
          display.Show();
          display.currentChNum = (coord_w + 1) * 10 + (coord_h + 1);
          display.start();
        }
      }
      return default(bool);
    }

    private void FMultiChDisplay_FormClosing(object sender, FormClosingEventArgs e)
    {
      mchRawDataQueue.Clear();
      unpackedFltDataQueue.Clear();
      zedGraphPlot.Dispose();
    }
  }
}
