﻿namespace MEAClosedLoop.UIForms
{
  partial class FMultiClusterView
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.groupBox2 = new System.Windows.Forms.GroupBox();
      this.clusterCntTb = new System.Windows.Forms.TextBox();
      this.mainPanel = new System.Windows.Forms.FlowLayoutPanel();
      this.startButton = new System.Windows.Forms.Button();
      this.DrawCircleCheckBox = new System.Windows.Forms.CheckBox();
      this.groupBox2.SuspendLayout();
      this.SuspendLayout();
      // 
      // groupBox2
      // 
      this.groupBox2.Controls.Add(this.clusterCntTb);
      this.groupBox2.Location = new System.Drawing.Point(12, 3);
      this.groupBox2.Name = "groupBox2";
      this.groupBox2.Size = new System.Drawing.Size(112, 45);
      this.groupBox2.TabIndex = 18;
      this.groupBox2.TabStop = false;
      this.groupBox2.Text = "Кол-во кластеров";
      // 
      // clusterCntTb
      // 
      this.clusterCntTb.Location = new System.Drawing.Point(39, 15);
      this.clusterCntTb.Name = "clusterCntTb";
      this.clusterCntTb.Size = new System.Drawing.Size(31, 20);
      this.clusterCntTb.TabIndex = 0;
      this.clusterCntTb.Text = "10";
      // 
      // mainPanel
      // 
      this.mainPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.mainPanel.AutoScroll = true;
      this.mainPanel.Location = new System.Drawing.Point(12, 54);
      this.mainPanel.Name = "mainPanel";
      this.mainPanel.Size = new System.Drawing.Size(1144, 608);
      this.mainPanel.TabIndex = 16;
      // 
      // startButton
      // 
      this.startButton.Location = new System.Drawing.Point(130, 12);
      this.startButton.Name = "startButton";
      this.startButton.Size = new System.Drawing.Size(90, 31);
      this.startButton.TabIndex = 13;
      this.startButton.Text = "Отрисовать";
      this.startButton.UseVisualStyleBackColor = true;
      this.startButton.Click += new System.EventHandler(this.startButton_Click);
      // 
      // DrawCircleCheckBox
      // 
      this.DrawCircleCheckBox.AutoSize = true;
      this.DrawCircleCheckBox.Enabled = false;
      this.DrawCircleCheckBox.Location = new System.Drawing.Point(282, 18);
      this.DrawCircleCheckBox.Name = "DrawCircleCheckBox";
      this.DrawCircleCheckBox.Size = new System.Drawing.Size(122, 17);
      this.DrawCircleCheckBox.TabIndex = 19;
      this.DrawCircleCheckBox.Text = "Круги с векторами";
      this.DrawCircleCheckBox.UseVisualStyleBackColor = true;
      // 
      // FMultiClusterView
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(1168, 674);
      this.Controls.Add(this.DrawCircleCheckBox);
      this.Controls.Add(this.groupBox2);
      this.Controls.Add(this.mainPanel);
      this.Controls.Add(this.startButton);
      this.Name = "FMultiClusterView";
      this.Text = "FMultiClusterView";
      this.Load += new System.EventHandler(this.FMultiClusterView_Load);
      this.groupBox2.ResumeLayout(false);
      this.groupBox2.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion
    private System.Windows.Forms.GroupBox groupBox2;
    private System.Windows.Forms.TextBox clusterCntTb;
    private System.Windows.Forms.FlowLayoutPanel mainPanel;
    private System.Windows.Forms.Button startButton;
    private System.Windows.Forms.CheckBox DrawCircleCheckBox;
  }
}