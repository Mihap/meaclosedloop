﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MEAClosedLoop.Common;
using MEAClosedLoop.Algorithms;
using MEAClosedLoop.UIForms.SubView;
using ZedGraph;

namespace MEAClosedLoop.UIForms
{
  public partial class FMultiClusterView : Form
  {
    private CClasterBuilder _clusterBuilder;
    private CSprdClusterBuilder _sprdClusterBuilder;
    private Dictionary<ulong, Vector60> vectorDict;
    private double[][] data;
    private int[] labels;

    public FMultiClusterView()
    {
      InitializeComponent();
    }

    public FMultiClusterView(int clustersNum, CClasterBuilder clasterBuilder, double[][] data) : this()
    {
      clusterCntTb.Text = clustersNum.ToString();
      this._clusterBuilder = clasterBuilder;
      this.data = data;
      DrawCircleCheckBox.Enabled = true;
    }

    public FMultiClusterView(int clustersNum, CSprdClusterBuilder sprdClusterBuilder, double[][] data, int[] labels) : this()
    {
      this.labels = labels;
      clusterCntTb.Text = clustersNum.ToString();
      _sprdClusterBuilder = sprdClusterBuilder;
      this.data = data;
    }


    private void ReCompute()
    {
      int clustersNum;
      if (!int.TryParse(clusterCntTb.Text, out clustersNum))
        return;

      _clusterBuilder.Clasters.Clear();
      var model = AccordClustering.kmeans(data, clustersNum);
      var labels = model.Decide(data);
      for (int i = 0; i < clustersNum; i++)
      {
        var idsInCluster = labels.Select((x, ind) => new { x, ind })
          .Where(x => x.x == i)
          .Select(x => x.ind);

        var burstIdsInCluster = _clusterBuilder.BurstIds
          .Where((x, index) => idsInCluster.Contains(index)).ToList();
        var cluster = new CBurstCluster(burstIdsInCluster, i);
        cluster.Description = model.Clusters.Single(x => x.Index == i).Centroid;
        _clusterBuilder.Clasters.Add(cluster);
      }
    }

    private void ReComputeSprd()
    {
      int clustersNum;
      if (!int.TryParse(clusterCntTb.Text, out clustersNum))
        return;

      _sprdClusterBuilder.Clusters.Clear();
      var model = AccordClustering.kmeans(data, clustersNum);
      labels = model.Decide(data);

      for (int i = 0; i < clustersNum; i++)
      {
        var idsInCluster = labels.Select((x, ind) => new { x, ind })
          .Where(x => x.x == i)
          .Select(x => x.ind);

        var burstIdsInCluster = _sprdClusterBuilder.BurstIds
          .Where((x, index) => idsInCluster.Contains(index)).ToList();
        var cluster = new CSprdBurstCluster(burstIdsInCluster, i);
        var vector = new Vector60();
        var centroid = model.Clusters.Single(x => x.Index == i).Centroid;
        for (int j = 0; j < _sprdClusterBuilder.VectorDict.First().Value.Elements.Count; j++)
          vector.Elements.Add(_sprdClusterBuilder.VectorDict.First().Value.Elements.ElementAt(j).Key, (int)centroid[j]);

        cluster.Vector = vector;
        _sprdClusterBuilder.Clusters.Add(cluster);
      }
    }

    private void DrawSprd()
    {
      int j = 0;
      //выведем содержимое кластеров
      mainPanel.Controls.Clear();
      foreach (var cluster in _sprdClusterBuilder.Clusters)
      {
        var view = new ClusterVectorView(cluster) { Margin = new Padding(10) };

        //var box = new GroupBox() { Margin = new Padding(10), Width = view.Width, Height = view.Height };
        //box.Controls.Add(view);
        mainPanel.Controls.Add(view);

      }
    }

    private void Draw()
    {
      int j = 0;
      bool vectorsInited = vectorDict != null && vectorDict.Count > 0;
      if (!vectorsInited)
        vectorDict = new Dictionary<ulong, Vector60>();
      var locker = new object();

      //выведем содержимое кластеров
      mainPanel.Controls.Clear();
      foreach (var cluster in _clusterBuilder.Clasters)
      {
        List<double[]> inclaster = new List<double[]>();
        foreach (int i in _clusterBuilder.GetPositions(cluster.innerIDS))
        {
          inclaster.Add(data[i]);
        }

        var form = new FSingleClusterView(cluster.Description, inclaster, cluster.ToString())
        {
          FormBorderStyle = FormBorderStyle.None,
          TopLevel = false,
          Visible = true,
        };

        var box = new GroupBox() { Margin = new Padding(10), Width = form.Width, Height = form.Height };
        box.Controls.Add(form);
        mainPanel.Controls.Add(box);

        if (DrawCircleCheckBox.Checked)
        {
          var pictureBox = new PictureBox()
          {
            Height = box.Height,
            Width = box.Height,
            Margin = new Padding(10)
          };

          var vectorList = new List<Vector60>();

          if (vectorsInited)
          {
            foreach (var burstId in cluster.innerIDS)
            {
              vectorList.Add(vectorDict[burstId]);
            }
          }
          else
          {
            cluster.innerIDS.AsParallel().ForAll(burstId =>
            {
              var burst = CBurstDataProvider.GetBurst(burstId);
              var vector = new Vector60(burst);

              lock (locker)
              {
                vectorList.Add(vector);
                if (!vectorDict.ContainsKey(burstId))
                  vectorDict.Add(burstId, vector);
              }
            });
          }

          VectorDrawHelper.DrawVectorsCircle(vectorList, pictureBox);
          mainPanel.Controls.Add(pictureBox);
        }

      }
    }

    private void Debug()
    {
      int j = 0;
      //выведем содержимое кластеров
      mainPanel.Controls.Clear();
      for (int i = 0; i < int.Parse(clusterCntTb.Text); i++)
      {
        List<double[]> inclaster = new List<double[]>();
        for (j = 0; j < labels.Length; j++)
        {
          if (labels[j] == i)
            inclaster.Add(data[j]);
        }
        var centr = _sprdClusterBuilder.Clusters[i].Vector.Elements.Select(x => (double)x.Value).ToArray();
        var form = new FSingleClusterView(centr, inclaster)
        {
          FormBorderStyle = FormBorderStyle.None,
          TopLevel = false,
          Visible = true,
        };

        var box = new GroupBox() { Margin = new Padding(10), Width = form.Width, Height = form.Height };
        box.Controls.Add(form);
        mainPanel.Controls.Add(box);

      }
    }

    private void startButton_Click(object sender, EventArgs e)
    {
      if (_sprdClusterBuilder != null)
      {
        ReComputeSprd();
        //Debug();
        DrawSprd();
      }
      else if (_clusterBuilder != null)
      {
        ReCompute();
        Draw();
      }
    }

    private void FMultiClusterView_Load(object sender, EventArgs e)
    {
      //Debug();
      //return;

      if (_sprdClusterBuilder != null)
        DrawSprd();
      else if (_clusterBuilder != null)
        Draw();
    }
  }
}
