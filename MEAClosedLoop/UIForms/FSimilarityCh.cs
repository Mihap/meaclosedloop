﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using MEAClosedLoop.Algorithms;

namespace MEAClosedLoop.UIForms
{
  public partial class FSimilarityCh : Form
  {
    private List<List<double>> _Matrix8x8 = new List<List<double>>();
    private List<ulong> BurstIds = new List<ulong>();
    float rectwidth = 0, rectheight = 0;
    double[,] CrChMatrix60x60;
    int[] ChannelMatxID;

    int MaxChNumber = 59;

    public FSimilarityCh(double[,] _CrChMatrix60x60, int[] _ChannelMatxID, List<ulong> _BurstIds)
    {
      InitializeComponent();
      CrChMatrix60x60 = _CrChMatrix60x60;
      ChannelMatxID = _ChannelMatxID;
      BurstIds = _BurstIds;
    }

    private double[,] MatrixCreate()
    {
      double[] TempMatr = new double[MaxChNumber + 1], TempMatr2 = new double[MaxChNumber + 1];
      double[,] Matrix8x8 = new double[8, 8];

      // 1. Идем по всем каналам
      for (int i = 0; i < 60; i++)
      {
        double ordinary = 0;
        // 2. Для каждого канала считаем сумму связности со всеми остальными
        for (int j = 0; j < 60; j++)
        {
          ordinary += CrChMatrix60x60[i, j];
        }
        // 2.1 Нормируем на число каналов
        ordinary /= 60.0;
        // 3. Получаем координаты канала в матрице
        int x = MEA.IDX2NAME[i] / 10 - 1;
        int y = MEA.IDX2NAME[i] % 10 - 1;
        // 4. Заполняем матрицу 8x8
        Matrix8x8[y, x] = ordinary;
      }

      //Обнуление вершин
      Matrix8x8[0, 0] = 0.0;
      Matrix8x8[7, 0] = 0.0;
      Matrix8x8[0, 7] = 0.0;
      Matrix8x8[7, 7] = 0.0;

      for (int i = 0; i < 8; i++)
      {
        _Matrix8x8.Add(new List<double>());
        for (int j = 0; j < 8; j++)
          _Matrix8x8[i].Add(Matrix8x8[i, j]);
      }
      return Matrix8x8;
    }

    private void button1_Click(object sender, EventArgs e)
    {
      FClusterTree form = new FClusterTree(_Matrix8x8, BurstIds);
      form.Show();
    }

    public void DrawImage()
    {
      Bitmap bmp = CorrelationBitmap.DrawTask(MatrixCreate(), pictureBox1.Width, pictureBox1.Height, out rectwidth, out rectheight);
      using (Graphics G = Graphics.FromImage(bmp))
      {
        float imgWidth = (float)pictureBox1.Width / 8f;
        float imgHeight = (float)pictureBox1.Height / 8f;
        float textX = 0, textY = 0;
        for (int i = 0; i < 8; i++)
        {
          for (int j = 0; j < 8; j++)
          {
            if ((i == 0 || i == 7) && (j == 0 || j == 7))
            {
              textX += imgWidth;
              continue;
            }
            Point pt = new Point((int)textX, (int)textY);
            G.DrawString(((j + 1) * 10 + i + 1).ToString(), new Font("Tahoma", 12), Brushes.Black, pt);
            textX += imgWidth;
            //g.Flush();
          }
          textX = 0;
          textY += imgHeight;
        }
      }
      pictureBox1.Image = bmp;
      bmp.Save("simil-" + DateTime.Now.TimeOfDay.TotalMinutes.ToString() + ".bmp", System.Drawing.Imaging.ImageFormat.Bmp);
    }
  }
}
