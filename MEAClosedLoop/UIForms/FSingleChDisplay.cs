﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Text;
using System.Windows.Forms;

using System.Diagnostics;
using ZedGraph;

namespace MEAClosedLoop.UIForms
{
  using TData = System.Double;
  using TTime = System.UInt64;
  using TStimIndex = System.Int16;
  using TAbsStimIndex = System.UInt64;
  using TRawDataPacket = Dictionary<int, ushort[]>;
  using TFltDataPacket = Dictionary<int, System.Double[]>;

  public partial class FSingleChDisplay : Form, IRecieveFltData
  {
    //Отрисовка пачек
    Queue<TAbsStimIndex> FoundStimData;

    private Queue<double> dataQueue = new Queue<double>();
    private Queue<TFltDataPacket> unpackedFltDataQueue = new Queue<TFltDataPacket>();
    private object DataQueueLock = new object();
    Timer plotUpdater;

    const uint updateInterval = 200;      //1000
    public int currentChNum = 12;
    uint _Duration2 = Param.MS * 1000;
    uint _Amplitude = 100;
    uint Amplitude2
    {
      get
      {
        return _Amplitude;
      }
      set
      {
        if (value > 10 && value < 3000)
        {
          _Amplitude = value;
          AmplitudeChecker.Value = value;
        }
      }
    }
    uint Duration2
    {
      get { return _Duration2; }
      set { if (value >= Param.MS * 1000)  _Duration2 = value; }
    }

    public FSingleChDisplay()
    {
      InitializeComponent();
      initPlot();
    }
    public FSingleChDisplay(int ChNum)
    {
      InitializeComponent();
    }

    public event OnDisconnectDelegate OnDisconnect;

    void IRecieveFltData.RecieveFltData(TFltDataPacket packet)
    {
      lock (DataQueueLock)
      {
        unpackedFltDataQueue.Enqueue(packet);
        while (unpackedFltDataQueue.Select(x => x[0].Length).Sum() > Param.MS * 3000)
        {
          unpackedFltDataQueue.Dequeue();
        }
      }
    }

    private void StartButton_Click(object sender, EventArgs e)
    {
      start();
    }

    void plotUpdater_Tick(object sender, EventArgs e)
    {
      Task updatetask = new Task(this.updatePlot);
      updatetask.Start();

    }
    
    private void initPlot()
    {
      GraphPane pane = zedGraphPlot.GraphPane;
      zedGraphPlot.MasterPane.Border = new Border(false, Color.White, 0);
      zedGraphPlot.MasterPane.InnerPaneGap = 0;
      //zedGraphPlot.MasterPane.BaseDimension = 3;
      pane.Margin.Top = 0;
      pane.Margin.Left = 0;
      pane.Margin.Right = 0;
      pane.Margin.Bottom = 0;
      pane.XAxis.Title.IsVisible = false;
      pane.YAxis.Title.IsVisible = false;
      pane.Legend.IsVisible = false;
      pane.Title.IsVisible = false;
      pane.TitleGap = 0;
      pane.CurveList.Clear();
      pane.XAxis.IsVisible = false;
      pane.X2Axis.Cross = +Amplitude2;
      pane.BaseDimension = 9;
      pane.X2Axis.IsVisible = true;
    }
    
    private void updatePlot()
    {
      object UpdatePlotLock = new object();

      lock (UpdatePlotLock)
      {
        Stopwatch sw = new Stopwatch();
        sw.Start();
        if (this.IsDisposed)
        {
          plotUpdater.Stop();
          return;
        }
        //int debug = Environment.TickCount;
        GraphPane pane = zedGraphPlot.GraphPane;

        PointPairList f1_list = new PointPairList();
        PointPairList f2_list = new PointPairList();

        //data prepare;
        double[] Data;
        lock (DataQueueLock)
        {
          while (unpackedFltDataQueue.Count > 0)
          {
            double[] data = unpackedFltDataQueue.Dequeue()[MEA.NAME2IDX[currentChNum]];
            for (int i = 0; i < data.Length; i++)
              dataQueue.Enqueue(data[i]);
            while (dataQueue.Count > Duration2)
            {
              dataQueue.Dequeue();
            }
          }
        }
        Data = dataQueue.ToArray();
        TData[] x = new TData[Data.Length];
        TData[] y = new TData[Data.Length];

        for (int i = 0; i < Data.Length; i++)
        {
          //f1_list.Add(i / 25.0, Data[i]);
          x[i] = i / 25.0;
          y[i] = Data[i];
        }
        int PartsLength;
        PartsLength = (Data.Length > 0) ? Data.Length / zedGraphPlot.Width : 0;
        int PartsCount = (PartsLength > 0) ? Data.Length / PartsLength : 0;
        double min = double.MaxValue;
        double max = double.MinValue;

        ulong CurrentTime = GlobalTimeStamp.TimeStamp - (ulong)Data.Length;
        ulong Dur2Key = 0;
        bool foolflag = false, PackFlag = false;                  //Только если 2 пачки влезут в 1 часть

        if (Duration2 > 3 * Param.MS * 1000)                      // Если длина окна > 4 сек
        {
          ulong counter = 0;
          for (int i = 0; i < PartsCount; i++)                    // Проход по частям
          {
            foolflag = false;
            min = double.MaxValue;
            max = double.MinValue;
            for (int ii = 0; ii < PartsLength; ii++)              // Проход по элементам частей: ищем пачки активности и мин\макс
            {
              if (CBurstDataProvider.BurstAdrCollection.Keys.Contains(CurrentTime + (ulong)(i * PartsLength + ii)))
              {
                PackFlag = true;
                Dur2Key = CurrentTime + (ulong)(i * PartsLength + ii);
                if (foolflag) throw new Exception("Too many seconds on the pane => pack draw trouble");
                foolflag = true;
              }
              if (Data[i * PartsLength + ii] > max) max = Data[i * PartsLength + ii];
              if (Data[i * PartsLength + ii] < min) min = Data[i * PartsLength + ii];
            }
            f1_list.Add(i * PartsLength / 25.0, min);             // Добавляем мин\макс элементы
            f1_list.Add(i * PartsLength / 25.0, max);
            if (PackFlag)                                         // Если нашли пачки
            {
              ulong LongPartsLength = (ulong)PartsLength;
              ulong ThisDurationTime = CBurstDataProvider.BurstAdrCollection[Dur2Key].DurationTime;
              if (counter * LongPartsLength <= ThisDurationTime)   // Если мы в середине пачки
              {
                f2_list.Add(i * PartsLength / 25.0, min);
                f2_list.Add(i * PartsLength / 25.0, max);
                counter++;
              }
              else PackFlag = false;
            }
          }
        }
        else
        {
          for (ulong i = 0; i < (ulong)Data.Length; i++)
          {
            if (CBurstDataProvider.BurstAdrCollection.Keys.Contains(CurrentTime + i))
            {
              ulong j = 0;
              ulong ThisDurationTime = CBurstDataProvider.BurstAdrCollection[CurrentTime + i].DurationTime;
              for (; j < ThisDurationTime; j++)
              {
                f2_list.Add((i + j) / 25.0, Data[i + j]);
              }
            }
            f1_list.Add(i / 25.0, Data[i]);
          }
        }

        pane.XAxis.Scale.Min = 0;
        pane.XAxis.Scale.Max = Duration2 / 25.0;
        pane.YAxis.Scale.Min = -Amplitude2;
        pane.YAxis.Scale.Max = +Amplitude2;
        pane.CurveList.Clear();

        LineItem f1_curve = pane.AddCurve("Neuronal Activity", f1_list, Color.Blue, SymbolType.None);
        LineItem f2_curve = pane.AddCurve("Pack", f2_list, Color.Red, SymbolType.Circle);
        // Вызываем метод AxisChange (), чтобы обновить данные об осях. 
        // В противном случае на рисунке будет показана только часть графика, 
        // которая умещается в интервалы по осям, установленные по умолчанию
        
        PointPairList testlist1 = new PointPairList();
        PointPairList testlist2 = new PointPairList();
        testlist1 = f1_list;
        testlist2 = f2_list;
        zedGraphPlot.AxisChange();


        // Обновляем график
        zedGraphPlot.Invalidate();
        //filteredList = new FilteredPointList(new double[0], new double[0]);

        sw.Stop();
        string s = sw.ElapsedMilliseconds.ToString() + " ms";
        if (UpdateTimeLabel.InvokeRequired)
          UpdateTimeLabel.BeginInvoke(new Action<System.Windows.Forms.Label>((lab) => lab.Text = s), UpdateTimeLabel);
        else
          UpdateTimeLabel.Text = s;
      }
    }
   
    public void start()
    {
      plotUpdater = new Timer();
      plotUpdater.Interval = 1000;
      plotUpdater.Tick += plotUpdater_Tick;
      plotUpdater.Start();
    }
    
    private void stop()
    {
      plotUpdater.Stop();
    }

    private void stopButton_Click(object sender, EventArgs e)
    {
      stop();
    }

    private void AmplitudeChecker_ValueChanged(object sender, EventArgs e)
    {
      Amplitude2 = (uint)(sender as NumericUpDown).Value;
    }

    private void DurationChecker_ValueChanged(object sender, EventArgs e)
    {
      Duration2 = (uint)(sender as NumericUpDown).Value * Param.MS * 1000;
    }

    private void ChNumChecker_ValueChanged(object sender, EventArgs e)
    {
      lock (DataQueueLock)
      {
        dataQueue.Clear();
        currentChNum = (int)(sender as NumericUpDown).Value;
      }
    }

    private void FSingleChDisplay_Load(object sender, EventArgs e)
    {
      Amplitude2 = 800;
      zedGraphPlot.GraphPane.XAxis.Title.IsVisible = false;
      zedGraphPlot.GraphPane.YAxis.Title.IsVisible = false;
    }

    private void FSingleChDisplay_FormClosing(object sender, FormClosingEventArgs e)
    {
      if (OnDisconnect != null) OnDisconnect(this);
      zedGraphPlot.Dispose();
    }
  }
}
