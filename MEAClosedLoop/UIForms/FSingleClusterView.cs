﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using ZedGraph;

namespace MEAClosedLoop.UIForms
{
  public partial class FSingleClusterView : Form
  {
    private double[] Main;
    private List<double[]> Sub = new List<double[]>();
    private int _id = -1;
    private string _clusterName;

    public FSingleClusterView()
    {
      InitializeComponent();
    }
    public FSingleClusterView(double[] Main, List<double[]> Sub, string name = null)
      : this()
    {
      _clusterName = name;
      this.Main = Main;
      this.Sub = Sub;
    }

    public FSingleClusterView(int id, bool DrawSubs = false)
      : this()
    {
      Main = CClusterProvider.ClasterBuilder.Clasters[id].Description;
      if (DrawSubs)
      {
        Sub.Clear();
        foreach (ulong key in CTimeLineClustering.BurstDistribution[id])
        {
          Sub.Add(CTimeLineClustering.Descriptors[key]);
        }
      }

    }

    private void FSingleClusterView_Load(object sender, EventArgs e)
    {
      this.Height = 350;
      this.Width = 500;
      this.Width = Screen.AllScreens[0].WorkingArea.Width / 3;
      this.Height = Screen.AllScreens[0].WorkingArea.Height / 3;

      DrawGraph();
    }

    private void DrawGraph()
    {

      GraphPane pane = zedGraphControl1.GraphPane;
      pane.IsFontsScaled = false;
      if (_id > -1)
        pane.Title.Text = "Кластер " + _id;
      else if (_clusterName != null)
        pane.Title.Text = "Кластер " + _clusterName;
      else
        pane.Title.IsVisible = false;
      pane.XAxis.Title.Text = "t, ms";
      pane.YAxis.Title.Text = "Value, pcs";
      pane.Legend.IsVisible = false;

      //pane.Legend.IsVisible = false;
      // Очистим список кривых на тот случай, если до этого сигналы уже были нарисованы
      pane.CurveList.Clear();
      // Создадим список точек
      PointPairList main = new PointPairList();
      List<PointPairList> subs = new List<PointPairList>();
      for (int i = 0; i < Main.Length; i++)
      {
        main.Add(i, Main[i]);
      }
      CurveItem curveMain = pane.AddCurve("", main, Color.FromArgb(250, 100, 100, 100), SymbolType.None);
      curveMain.Color = Color.Blue;
      (curveMain as LineItem).Line.Width = 2;
      (curveMain as LineItem).Line.IsAntiAlias = true;
      (curveMain as LineItem).Line.IsSmooth = true;

      foreach (double[] currentSub in Sub)
      {

        PointPairList list = new PointPairList();
        for (int i = 0; i < currentSub.Length; i++)
        {
          list.Add(i, currentSub[i]);
        }
        CurveItem curve = pane.AddCurve("", list, Color.FromArgb(50, 100, 100, 100), SymbolType.None);
        (curveMain as LineItem).Line.IsAntiAlias = true;
        (curveMain as LineItem).Line.IsSmooth = true;
      }
      zedGraphControl1.AxisChange();

      // Обновляем график
      zedGraphControl1.Invalidate();
              zedGraphControl1.Width = 600;
      zedGraphControl1.Height = 300;
      var bitmap = new Bitmap(zedGraphControl1.Width, zedGraphControl1.Height);
      //var bitmap = new Bitmap(pictureBox1.Width, pictureBox1.Height);

      //zedGraphControl1.Size = new Size(600, 250);
      zedGraphControl1.DrawToBitmap(bitmap, new Rectangle(0, 0, zedGraphControl1.Width, zedGraphControl1.Height));

      var debugFolder = "BurstClusters";
      if (!System.IO.Directory.Exists(debugFolder))
        System.IO.Directory.CreateDirectory(debugFolder);

      var fileName = System.IO.Path.Combine(debugFolder, $"{DateTime.Now.Minute * DateTime.Now.Second}_{_clusterName}.png");

      bitmap.Save(fileName, System.Drawing.Imaging.ImageFormat.Png);

      zedGraphControl1.GraphPane = null;
      zedGraphControl1.Visible = false;
      zedGraphControl1.Dispose();

      mainPictureBox.SizeMode = PictureBoxSizeMode.Zoom;
      mainPictureBox.Image = bitmap;
    }
  }
}
