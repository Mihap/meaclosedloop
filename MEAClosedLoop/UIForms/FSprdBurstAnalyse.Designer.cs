﻿namespace MEAClosedLoop.UIForms
{
  partial class FSprdBurstAnalyse
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.ParamGroupBox = new System.Windows.Forms.GroupBox();
      this.DrawShuffleButton = new System.Windows.Forms.Button();
      this.drawTreeButton = new System.Windows.Forms.Button();
      this.clusterCountLabel = new System.Windows.Forms.Label();
      this.vectorCountLabel = new System.Windows.Forms.Label();
      this.openGraphFormButton = new System.Windows.Forms.Button();
      this.FindClustersBtn = new System.Windows.Forms.Button();
      this.EvokedCountLbl = new System.Windows.Forms.Label();
      this.SpontanousCountLbl = new System.Windows.Forms.Label();
      this.AnalyseAllDirectBtn = new System.Windows.Forms.Button();
      this.GetCollectionBtn = new System.Windows.Forms.Button();
      this.DrawButton = new System.Windows.Forms.Button();
      this.HistoContainer = new System.Windows.Forms.GroupBox();
      this.ThresholdTB = new System.Windows.Forms.TextBox();
      this.label5 = new System.Windows.Forms.Label();
      this.NormalDistribGraph = new ZedGraph.ZedGraphControl();
      this.MainMapPicture = new System.Windows.Forms.PictureBox();
      this.DrawClusterBtn = new System.Windows.Forms.Button();
      this.ClusterNumCB = new System.Windows.Forms.ComboBox();
      this.ParamGroupBox.SuspendLayout();
      this.HistoContainer.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.MainMapPicture)).BeginInit();
      this.SuspendLayout();
      // 
      // ParamGroupBox
      // 
      this.ParamGroupBox.Controls.Add(this.ClusterNumCB);
      this.ParamGroupBox.Controls.Add(this.DrawClusterBtn);
      this.ParamGroupBox.Controls.Add(this.DrawShuffleButton);
      this.ParamGroupBox.Controls.Add(this.drawTreeButton);
      this.ParamGroupBox.Controls.Add(this.clusterCountLabel);
      this.ParamGroupBox.Controls.Add(this.vectorCountLabel);
      this.ParamGroupBox.Controls.Add(this.openGraphFormButton);
      this.ParamGroupBox.Controls.Add(this.FindClustersBtn);
      this.ParamGroupBox.Controls.Add(this.EvokedCountLbl);
      this.ParamGroupBox.Controls.Add(this.SpontanousCountLbl);
      this.ParamGroupBox.Controls.Add(this.AnalyseAllDirectBtn);
      this.ParamGroupBox.Controls.Add(this.GetCollectionBtn);
      this.ParamGroupBox.Controls.Add(this.DrawButton);
      this.ParamGroupBox.Location = new System.Drawing.Point(12, 12);
      this.ParamGroupBox.Name = "ParamGroupBox";
      this.ParamGroupBox.Size = new System.Drawing.Size(1338, 52);
      this.ParamGroupBox.TabIndex = 9;
      this.ParamGroupBox.TabStop = false;
      this.ParamGroupBox.Text = "Параметры";
      // 
      // DrawShuffleButton
      // 
      this.DrawShuffleButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
      this.DrawShuffleButton.Location = new System.Drawing.Point(350, 19);
      this.DrawShuffleButton.Name = "DrawShuffleButton";
      this.DrawShuffleButton.Size = new System.Drawing.Size(86, 23);
      this.DrawShuffleButton.TabIndex = 17;
      this.DrawShuffleButton.Text = "Перешанные";
      this.DrawShuffleButton.UseVisualStyleBackColor = true;
      this.DrawShuffleButton.Click += new System.EventHandler(this.DrawShuffleButton_Click);
      // 
      // drawTreeButton
      // 
      this.drawTreeButton.Location = new System.Drawing.Point(897, 19);
      this.drawTreeButton.Name = "drawTreeButton";
      this.drawTreeButton.Size = new System.Drawing.Size(139, 23);
      this.drawTreeButton.TabIndex = 16;
      this.drawTreeButton.Text = "Отрисовать дерево";
      this.drawTreeButton.UseVisualStyleBackColor = true;
      this.drawTreeButton.Click += new System.EventHandler(this.drawTreeButton_Click);
      // 
      // clusterCountLabel
      // 
      this.clusterCountLabel.AutoSize = true;
      this.clusterCountLabel.Location = new System.Drawing.Point(474, 29);
      this.clusterCountLabel.Name = "clusterCountLabel";
      this.clusterCountLabel.Size = new System.Drawing.Size(125, 13);
      this.clusterCountLabel.TabIndex = 15;
      this.clusterCountLabel.Text = "Количество кластеров:";
      // 
      // vectorCountLabel
      // 
      this.vectorCountLabel.AutoSize = true;
      this.vectorCountLabel.Location = new System.Drawing.Point(474, 12);
      this.vectorCountLabel.Name = "vectorCountLabel";
      this.vectorCountLabel.Size = new System.Drawing.Size(119, 13);
      this.vectorCountLabel.TabIndex = 14;
      this.vectorCountLabel.Text = "Количество векторов:";
      // 
      // openGraphFormButton
      // 
      this.openGraphFormButton.Location = new System.Drawing.Point(671, 19);
      this.openGraphFormButton.Name = "openGraphFormButton";
      this.openGraphFormButton.Size = new System.Drawing.Size(85, 23);
      this.openGraphFormButton.TabIndex = 13;
      this.openGraphFormButton.Text = "График";
      this.openGraphFormButton.UseVisualStyleBackColor = true;
      this.openGraphFormButton.Click += new System.EventHandler(this.openGraphFormButton_Click);
      // 
      // FindClustersBtn
      // 
      this.FindClustersBtn.Location = new System.Drawing.Point(773, 19);
      this.FindClustersBtn.Name = "FindClustersBtn";
      this.FindClustersBtn.Size = new System.Drawing.Size(107, 23);
      this.FindClustersBtn.TabIndex = 12;
      this.FindClustersBtn.Text = "Экспорт";
      this.FindClustersBtn.UseVisualStyleBackColor = true;
      this.FindClustersBtn.Click += new System.EventHandler(this.ExportClustersBtn_Click);
      // 
      // EvokedCountLbl
      // 
      this.EvokedCountLbl.AutoSize = true;
      this.EvokedCountLbl.Location = new System.Drawing.Point(78, 33);
      this.EvokedCountLbl.Name = "EvokedCountLbl";
      this.EvokedCountLbl.Size = new System.Drawing.Size(91, 13);
      this.EvokedCountLbl.TabIndex = 11;
      this.EvokedCountLbl.Text = "вызванные : n/a";
      // 
      // SpontanousCountLbl
      // 
      this.SpontanousCountLbl.AutoSize = true;
      this.SpontanousCountLbl.Location = new System.Drawing.Point(78, 12);
      this.SpontanousCountLbl.Name = "SpontanousCountLbl";
      this.SpontanousCountLbl.Size = new System.Drawing.Size(94, 13);
      this.SpontanousCountLbl.TabIndex = 10;
      this.SpontanousCountLbl.Text = "спонтанные : n/a";
      // 
      // AnalyseAllDirectBtn
      // 
      this.AnalyseAllDirectBtn.Location = new System.Drawing.Point(198, 19);
      this.AnalyseAllDirectBtn.Name = "AnalyseAllDirectBtn";
      this.AnalyseAllDirectBtn.Size = new System.Drawing.Size(65, 23);
      this.AnalyseAllDirectBtn.TabIndex = 0;
      this.AnalyseAllDirectBtn.Text = "Анализ";
      this.AnalyseAllDirectBtn.UseVisualStyleBackColor = true;
      this.AnalyseAllDirectBtn.Click += new System.EventHandler(this.AnalyseAllDirectBtn_Click);
      // 
      // GetCollectionBtn
      // 
      this.GetCollectionBtn.Location = new System.Drawing.Point(6, 19);
      this.GetCollectionBtn.Name = "GetCollectionBtn";
      this.GetCollectionBtn.Size = new System.Drawing.Size(65, 23);
      this.GetCollectionBtn.TabIndex = 0;
      this.GetCollectionBtn.Text = "выборка";
      this.GetCollectionBtn.UseVisualStyleBackColor = true;
      this.GetCollectionBtn.Click += new System.EventHandler(this.GetCollectionBtn_Click);
      // 
      // DrawButton
      // 
      this.DrawButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
      this.DrawButton.Location = new System.Drawing.Point(269, 19);
      this.DrawButton.Name = "DrawButton";
      this.DrawButton.Size = new System.Drawing.Size(75, 23);
      this.DrawButton.TabIndex = 4;
      this.DrawButton.Text = "Отрисовать";
      this.DrawButton.UseVisualStyleBackColor = true;
      this.DrawButton.Click += new System.EventHandler(this.DrawButton_Click);
      // 
      // HistoContainer
      // 
      this.HistoContainer.Controls.Add(this.ThresholdTB);
      this.HistoContainer.Controls.Add(this.label5);
      this.HistoContainer.Controls.Add(this.NormalDistribGraph);
      this.HistoContainer.Location = new System.Drawing.Point(12, 70);
      this.HistoContainer.Name = "HistoContainer";
      this.HistoContainer.Size = new System.Drawing.Size(473, 621);
      this.HistoContainer.TabIndex = 10;
      this.HistoContainer.TabStop = false;
      this.HistoContainer.Text = "Распределения значений";
      // 
      // ThresholdTB
      // 
      this.ThresholdTB.Location = new System.Drawing.Point(182, 593);
      this.ThresholdTB.Name = "ThresholdTB";
      this.ThresholdTB.Size = new System.Drawing.Size(72, 20);
      this.ThresholdTB.TabIndex = 2;
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Location = new System.Drawing.Point(5, 596);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(153, 13);
      this.label5.TabIndex = 1;
      this.label5.Text = "Выбранный порог отсечения";
      // 
      // NormalDistribGraph
      // 
      this.NormalDistribGraph.Location = new System.Drawing.Point(12, 20);
      this.NormalDistribGraph.Margin = new System.Windows.Forms.Padding(4);
      this.NormalDistribGraph.Name = "NormalDistribGraph";
      this.NormalDistribGraph.ScrollGrace = 0D;
      this.NormalDistribGraph.ScrollMaxX = 0D;
      this.NormalDistribGraph.ScrollMaxY = 0D;
      this.NormalDistribGraph.ScrollMaxY2 = 0D;
      this.NormalDistribGraph.ScrollMinX = 0D;
      this.NormalDistribGraph.ScrollMinY = 0D;
      this.NormalDistribGraph.ScrollMinY2 = 0D;
      this.NormalDistribGraph.Size = new System.Drawing.Size(454, 530);
      this.NormalDistribGraph.TabIndex = 0;
      // 
      // MainMapPicture
      // 
      this.MainMapPicture.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.MainMapPicture.BackColor = System.Drawing.SystemColors.AppWorkspace;
      this.MainMapPicture.Location = new System.Drawing.Point(491, 70);
      this.MainMapPicture.Name = "MainMapPicture";
      this.MainMapPicture.Size = new System.Drawing.Size(868, 621);
      this.MainMapPicture.TabIndex = 11;
      this.MainMapPicture.TabStop = false;
      this.MainMapPicture.Click += new System.EventHandler(this.MainMapPicture_Click);
      // 
      // DrawClusterBtn
      // 
      this.DrawClusterBtn.Enabled = false;
      this.DrawClusterBtn.Location = new System.Drawing.Point(1144, 19);
      this.DrawClusterBtn.Name = "DrawClusterBtn";
      this.DrawClusterBtn.Size = new System.Drawing.Size(85, 23);
      this.DrawClusterBtn.TabIndex = 14;
      this.DrawClusterBtn.Text = "Вид кластера";
      this.DrawClusterBtn.UseVisualStyleBackColor = true;
      this.DrawClusterBtn.Click += new System.EventHandler(this.DrawClusterBtn_Click);
      // 
      // ClusterNumCB
      // 
      this.ClusterNumCB.Enabled = false;
      this.ClusterNumCB.FormattingEnabled = true;
      this.ClusterNumCB.Location = new System.Drawing.Point(1235, 19);
      this.ClusterNumCB.Name = "ClusterNumCB";
      this.ClusterNumCB.Size = new System.Drawing.Size(46, 21);
      this.ClusterNumCB.TabIndex = 18;
      // 
      // FSprdBurstAnalyse
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(1369, 701);
      this.Controls.Add(this.MainMapPicture);
      this.Controls.Add(this.HistoContainer);
      this.Controls.Add(this.ParamGroupBox);
      this.Name = "FSprdBurstAnalyse";
      this.Text = "FSprdBurstAnalyse";
      this.ParamGroupBox.ResumeLayout(false);
      this.ParamGroupBox.PerformLayout();
      this.HistoContainer.ResumeLayout(false);
      this.HistoContainer.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.MainMapPicture)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.GroupBox ParamGroupBox;
    private System.Windows.Forms.Button FindClustersBtn;
    private System.Windows.Forms.Label EvokedCountLbl;
    private System.Windows.Forms.Label SpontanousCountLbl;
    private System.Windows.Forms.Button AnalyseAllDirectBtn;
    private System.Windows.Forms.Button GetCollectionBtn;
    private System.Windows.Forms.Button DrawButton;
    private System.Windows.Forms.GroupBox HistoContainer;
    private System.Windows.Forms.TextBox ThresholdTB;
    private System.Windows.Forms.Label label5;
    private ZedGraph.ZedGraphControl NormalDistribGraph;
    private System.Windows.Forms.PictureBox MainMapPicture;
    private System.Windows.Forms.Label clusterCountLabel;
    private System.Windows.Forms.Label vectorCountLabel;
    private System.Windows.Forms.Button openGraphFormButton;
    private System.Windows.Forms.Button drawTreeButton;
    private System.Windows.Forms.Button DrawShuffleButton;
    private System.Windows.Forms.Button DrawClusterBtn;
    private System.Windows.Forms.ComboBox ClusterNumCB;
  }
}