﻿using MEAClosedLoop.Algorithms;
using MEAClosedLoop.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZedGraph;

namespace MEAClosedLoop.UIForms
{
  public partial class FSprdBurstAnalyse : Form
  {
    private double defaultTh;
    private int defaultClustNum;

    private List<ulong> BurstIds = new List<ulong>();
    List<List<double>> allData = new List<List<double>>();
    List<List<double>> corrData = new List<List<double>>();
    List<List<double>> corrdata = new List<List<double>>();

    float rectwidth = 0, rectheight = 0;

    CSprdClusterBuilder clusterBuilder = null;

    public FSprdBurstAnalyse()
    {
      InitializeComponent();
    }

    private void GetCollectionBtn_Click(object sender, EventArgs e)
    {
      FBurstSelector dialog = new FBurstSelector();
      switch (dialog.ShowDialog())
      {
        case System.Windows.Forms.DialogResult.OK:
          break;
        case System.Windows.Forms.DialogResult.Cancel:
          break;
      }
      BurstIds.Clear();
      corrdata.Clear();
      BurstIds.AddRange(dialog.result);
      this.SpontanousCountLbl.Text = "Спонтанные: " + BurstIds.Where(x => CBurstDataProvider.BurstAdrCollection[x].IsEvoked == false).Count().ToString();
      this.EvokedCountLbl.Text = "Вызванные: " + BurstIds.Where(x => CBurstDataProvider.BurstAdrCollection[x].IsEvoked == true).Count().ToString();
      this.EnableUI(ParamGroupBox);
    }


    private void EnableUI(Control control)
    {
      if (control is GroupBox)
        foreach (Control c in ParamGroupBox.Controls)
        {
          if (c.InvokeRequired)
            c.BeginInvoke(new Action<Control>(x => x.Enabled = true), c);
          else
            c.Enabled = true;
        }
      else
      {
        if (control.InvokeRequired)
          control.BeginInvoke(new Action<Control>(x => x.Enabled = true), control);
        else
          control.Enabled = true;
      }
    }

    private void DisableUI(Control control)
    {
      if (control is GroupBox)
        foreach (Control c in ParamGroupBox.Controls)
        {
          if (c.InvokeRequired)
            c.BeginInvoke(new Action<Control>(x => x.Enabled = false), c);
          else
            c.Enabled = false;
        }
      else
      {
        if (control.InvokeRequired)
          control.BeginInvoke(new Action<Control>(x => x.Enabled = false), control);
        else
          control.Enabled = false;
      }
    }

    private void AnalyseAllDirectBtn_Click(object sender, EventArgs e)
    {
      //Task asyncAnalysisTask = new Task(AnalyseSyncAllMethod);
      AnalyseSyncAllMethod();
    }

    private void AnalyseSyncAllMethod()
    {
      DisableUI(ParamGroupBox);
      CSprdClusterBuilder scb = new CSprdClusterBuilder(BurstIds);
      var clustersForm = scb.Analyse();
      this.clusterBuilder = scb;
      LoadHisto();
      EnableUI(ParamGroupBox);
      ClusterNumCB.Enabled = true;
      DrawClusterBtn.Enabled = true;
      ClusterNumCB.Items.Clear();
      for(int i = 0; i < scb.Clusters.Count; i++)
      {
        ClusterNumCB.Items.Add(i.ToString());
      }
      ClusterNumCB.SelectedItem = 0.ToString();
      clusterCountLabel.Text = "Количество кластеров: " + scb.Clusters.Count;
      vectorCountLabel.Text = "Количество векторов: " + scb.VectorDict.Count;
      defaultClustNum = scb.clustNum;
      defaultTh = scb.threshold;

      BeginInvoke(new Action(() => clustersForm.Show()));
    }

    private void DrawButton_Click(object sender, EventArgs e)
    {
      Bitmap bmp = CorrelationBitmap.DrawTask(this.clusterBuilder.corrMatrix, MainMapPicture.Width, MainMapPicture.Height, out rectwidth, out rectheight);
      MainMapPicture.Image = bmp;
      bmp.Save("debug-" + DateTime.Now.TimeOfDay.TotalMinutes.ToString() + ".png", System.Drawing.Imaging.ImageFormat.Bmp);
    }

    private void LoadHisto()
    {

      GraphPane pane = NormalDistribGraph.GraphPane;
      pane.CurveList.Clear();
      pane.XAxis.Title.IsVisible = false;
      pane.YAxis.Title.IsVisible = false;
      pane.XAxis.Scale.Min = 0;
      pane.XAxis.Scale.Max = 1;
      pane.Title.Text = "Распределение количества корреляций";

      PointPairList list = new PointPairList();

      for (int i = 0; i < clusterBuilder.histoDataNormal.Count; i++)
      {
        list.Add(clusterBuilder.histoDataNormal[i].Item1, clusterBuilder.histoDataNormal[i].Item2);
      }
      LineItem curve1 = pane.AddCurve("Реальные данные", list, Color.FromArgb(50, Color.Blue), SymbolType.None);
      curve1.Line.IsSmooth = true;
      curve1.Line.SmoothTension = 0.1F;
      curve1.Line.Fill = new Fill(Color.FromArgb(50, Color.Blue));
      //BarItem bar = pane.AddBar("Histo", clusterBuilder.histoDataNormal.Select(x => x.Item1).ToArray(), clusterBuilder.histoDataNormal.Select(x => x.Item2).ToArray(), Color.Blue);

      //NormalDistribGraph.AxisChange();
      //NormalDistribGraph.Invalidate();
      //***************
      //

      //pane = zeroHypDisribGraph.GraphPane;
      //pane.CurveList.Clear();
      //pane.XAxis.Title.IsVisible = false;
      //pane.YAxis.Title.IsVisible = false;
      //pane.XAxis.Scale.Min = 0;
      //pane.XAxis.Scale.Max = 1;
      //pane.Title.Text = "Распределение случайных";
      list = new PointPairList();

      for (int i = 0; i < clusterBuilder.histoDataShuffl.Count; i++)
      {
        list.Add(clusterBuilder.histoDataShuffl[i].Item1, clusterBuilder.histoDataShuffl[i].Item2);
      }
      LineItem curve2 = pane.AddCurve("Перемешанные данные в векторах", list, Color.FromArgb(50, Color.Red), SymbolType.None);
      curve2.Line.Fill = new Fill(Color.FromArgb(50, Color.Red));
      curve2.Line.IsSmooth = true;
      curve2.Line.SmoothTension = 0.1F;
      //bar = pane.AddBar("Histo", clusterBuilder.histoDataShuffl.Select(x => x.Item1).ToArray(), clusterBuilder.histoDataShuffl.Select(x => x.Item2).ToArray(), Color.Red);

      //zeroHypDisribGraph.AxisChange();
      //zeroHypDisribGraph.Invalidate();
      NormalDistribGraph.AxisChange();
      NormalDistribGraph.Invalidate();
      ThresholdTB.Text = clusterBuilder.threshold.ToString();

    }

    private void thresholdChanged(double val, int n)
    {
      clusterCountLabel.Text = "Количество кластеров: " + (n + 2);
      ThresholdTB.Text = val.ToString();
      clusterBuilder.ChangeThreshold(val, n);
      ClusterNumCB.Items.Clear();
      for (int i = 0; i < clusterBuilder.Clusters.Count; i++)
      {
        ClusterNumCB.Items.Add(i.ToString());
      }
      ClusterNumCB.SelectedItem = 0.ToString();
    }

    private void MainMapPicture_Click(object sender, EventArgs e)
    {
      MouseEventArgs me = (MouseEventArgs)e;
      Point coordinates = me.Location;
      if (rectheight <= 0 || rectwidth <= 0) return;

      int x = (int)(coordinates.X / (float)rectwidth);
      int y = (int)(coordinates.Y / (float)rectheight);
      try
      {
        new FSpreadClusterCompare(clusterBuilder.BurstIds[x], clusterBuilder.BurstIds[y], clusterBuilder.corrMatrix[x, y]).Show();
       // new FCompareForm(clusterBuilder.Descriptions[x].ToList(), clasterBuilder.Descriptions[y].ToList(), clasterBuilder.MatrixD[x, y]).Show();
      }
      catch (Exception ex)
      {

      }
    }

    private void openGraphFormButton_Click(object sender, EventArgs e)
    {
      FThresholdGraph graphForm = new FThresholdGraph(clusterBuilder, defaultClustNum, defaultTh);
      graphForm.thresholdChanged += thresholdChanged;
      graphForm.Show();
    }

    private void drawTreeButton_Click(object sender, EventArgs e)
    {
      FSprdClusterTree form = new FSprdClusterTree(clusterBuilder.Head, clusterBuilder.threshold);
      form.Show();
    }

    private void ExportClustersBtn_Click(object sender, EventArgs e)
    {
      string fileName;
      using (SaveFileDialog dialog = new SaveFileDialog())
      {
        switch (dialog.ShowDialog())
        {
          case DialogResult.OK:
            break;
          default:
            return;
        }
        fileName = dialog.FileName;
      }

      using (StreamWriter sw = new StreamWriter(fileName, false))
      {
        int i = 0;
        foreach (CSprdBurstCluster cluster in clusterBuilder.Clusters)
        {
          List<ulong> Bsts = cluster.innerIDS.OrderBy(time => time).ToList();
          sw.Write(i++ + " ");
          for (int j = 0; j < Bsts.Count; j++)
          {
            sw.Write(Math.Round(Bsts[j] / (double)Param.DAQ_FREQ, 2).ToString() + ";");
          }
          sw.WriteLine("");
        }
      }

    }

    private void DrawClusterBtn_Click(object sender, EventArgs e)
    {
      int num = int.Parse((string)ClusterNumCB.SelectedItem);
      FSprdVector form = new FSprdVector();
      var v = clusterBuilder.Clusters[num].Vector;
      form.DrawnVector = v;
      form.Show();
    }

    private void DrawShuffleButton_Click(object sender, EventArgs e)
    {
      Bitmap bmp = CorrelationBitmap.DrawTask(this.clusterBuilder.shCorrMatrix, MainMapPicture.Width, MainMapPicture.Height, out rectwidth, out rectheight);
      MainMapPicture.Image = bmp;
      bmp.Save("debug-" + DateTime.Now.TimeOfDay.TotalMinutes.ToString() + ".png", System.Drawing.Imaging.ImageFormat.Bmp);
    }
  }
}