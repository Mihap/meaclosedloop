﻿using MEAClosedLoop.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ZedGraph;

namespace MEAClosedLoop.UIForms
{
  public partial class FSprdClusterTree : Form
  {
    public FSprdClusterTree(CSprdBurstCluster mainClust, double th)
    {
      InitializeComponent();
      List<CSprdBurstCluster>[] allRoutes = CSprdBurstCluster.BuildRoutesToTop(mainClust);
      DrawTree(allRoutes, th);
    }

    private void DrawTree(List<CSprdBurstCluster>[] allRoutes, double th)
    {
      GraphPane pane = graphControl.GraphPane;
      pane.Title.Text = "кластеризация пачечной активности культуры";
      pane.XAxis.IsVisible = false;
      pane.XAxis.Title.Text = "Спонтанная активность";
      pane.YAxis.Title.Text = "расстояние (у.е)";

      pane.Legend.IsVisible = false;
      foreach (List<CSprdBurstCluster> data in allRoutes)
      {
        PointPairList dataList = new PointPairList();
        for (int i = 0; i < data.Count; i++)
        {
          if (i == data.Count - 1)
            dataList.Add(data[i].Coord.X, data[i].Coord.Y);
          else
          {
            dataList.Add(data[i].Coord.X, data[i].Coord.Y);
            dataList.Add(data[i].Coord.X, data[i + 1].Coord.Y);
          }
        }
        if (CBurstDataProvider.BurstAdrCollection[data[0].ID].IsEvoked)
          pane.AddCurve("", dataList, Color.Red, SymbolType.None);
        else
          pane.AddCurve("", dataList, Color.Blue, SymbolType.None);
      }
      graphControl.AxisChange();

      LineObj line = new LineObj(pane.XAxis.Scale.Min, th, pane.XAxis.Scale.Max, th);
      line.Line.Color = Color.Red;
      line.Line.Style = System.Drawing.Drawing2D.DashStyle.Dash;
      pane.GraphObjList.Add(line);

      graphControl.Invalidate();
    }
  }
}
