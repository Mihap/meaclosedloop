﻿namespace MEAClosedLoop.UIForms
{
  partial class FSprdVector
  {
    /// <summary>
    /// Обязательная переменная конструктора.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Освободить все используемые ресурсы.
    /// </summary>
    /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Код, автоматически созданный конструктором форм Windows

    /// <summary>
    /// Требуемый метод для поддержки конструктора — не изменяйте 
    /// содержимое этого метода с помощью редактора кода.
    /// </summary>
    private void InitializeComponent()
    {
      this.DrawingPictureBox = new System.Windows.Forms.PictureBox();
      this.button1 = new System.Windows.Forms.Button();
      this.GradientPictureBox = new System.Windows.Forms.PictureBox();
      this.button2 = new System.Windows.Forms.Button();
      this.groupBox1 = new System.Windows.Forms.GroupBox();
      this.maxValueTextBox = new System.Windows.Forms.TextBox();
      this.button3 = new System.Windows.Forms.Button();
      ((System.ComponentModel.ISupportInitialize)(this.DrawingPictureBox)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.GradientPictureBox)).BeginInit();
      this.groupBox1.SuspendLayout();
      this.SuspendLayout();
      // 
      // DrawingPictureBox
      // 
      this.DrawingPictureBox.BackColor = System.Drawing.SystemColors.AppWorkspace;
      this.DrawingPictureBox.Location = new System.Drawing.Point(14, 9);
      this.DrawingPictureBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
      this.DrawingPictureBox.Name = "DrawingPictureBox";
      this.DrawingPictureBox.Size = new System.Drawing.Size(450, 468);
      this.DrawingPictureBox.TabIndex = 0;
      this.DrawingPictureBox.TabStop = false;
      // 
      // button1
      // 
      this.button1.Location = new System.Drawing.Point(86, 498);
      this.button1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
      this.button1.Name = "button1";
      this.button1.Size = new System.Drawing.Size(84, 19);
      this.button1.TabIndex = 1;
      this.button1.Text = "Method 1";
      this.button1.UseVisualStyleBackColor = true;
      this.button1.Click += new System.EventHandler(this.button1_Click);
      // 
      // GradientPictureBox
      // 
      this.GradientPictureBox.BackColor = System.Drawing.SystemColors.Control;
      this.GradientPictureBox.Location = new System.Drawing.Point(469, 57);
      this.GradientPictureBox.Name = "GradientPictureBox";
      this.GradientPictureBox.Size = new System.Drawing.Size(128, 420);
      this.GradientPictureBox.TabIndex = 2;
      this.GradientPictureBox.TabStop = false;
      // 
      // button2
      // 
      this.button2.Location = new System.Drawing.Point(288, 498);
      this.button2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
      this.button2.Name = "button2";
      this.button2.Size = new System.Drawing.Size(84, 19);
      this.button2.TabIndex = 3;
      this.button2.Text = "Method 2";
      this.button2.UseVisualStyleBackColor = true;
      this.button2.Click += new System.EventHandler(this.button2_Click);
      // 
      // groupBox1
      // 
      this.groupBox1.Controls.Add(this.maxValueTextBox);
      this.groupBox1.Location = new System.Drawing.Point(468, 9);
      this.groupBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
      this.groupBox1.Size = new System.Drawing.Size(129, 43);
      this.groupBox1.TabIndex = 4;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "Количество цветов";
      // 
      // maxValueTextBox
      // 
      this.maxValueTextBox.Location = new System.Drawing.Point(45, 18);
      this.maxValueTextBox.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
      this.maxValueTextBox.Name = "maxValueTextBox";
      this.maxValueTextBox.Size = new System.Drawing.Size(51, 20);
      this.maxValueTextBox.TabIndex = 0;
      // 
      // button3
      // 
      this.button3.Location = new System.Drawing.Point(457, 498);
      this.button3.Margin = new System.Windows.Forms.Padding(2);
      this.button3.Name = "button3";
      this.button3.Size = new System.Drawing.Size(84, 19);
      this.button3.TabIndex = 5;
      this.button3.Text = "Method 21";
      this.button3.UseVisualStyleBackColor = true;
      this.button3.Click += new System.EventHandler(this.button3_Click);
      // 
      // FSprdVector
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(609, 548);
      this.Controls.Add(this.button3);
      this.Controls.Add(this.groupBox1);
      this.Controls.Add(this.button2);
      this.Controls.Add(this.GradientPictureBox);
      this.Controls.Add(this.button1);
      this.Controls.Add(this.DrawingPictureBox);
      this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
      this.Name = "FSprdVector";
      this.Text = "Визуализация кластера пространственной характеристики";
      ((System.ComponentModel.ISupportInitialize)(this.DrawingPictureBox)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.GradientPictureBox)).EndInit();
      this.groupBox1.ResumeLayout(false);
      this.groupBox1.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.PictureBox DrawingPictureBox;
    private System.Windows.Forms.Button button1;
    private System.Windows.Forms.PictureBox GradientPictureBox;
    private System.Windows.Forms.Button button2;
    private System.Windows.Forms.GroupBox groupBox1;
    private System.Windows.Forms.TextBox maxValueTextBox;
    private System.Windows.Forms.Button button3;
  }
}

