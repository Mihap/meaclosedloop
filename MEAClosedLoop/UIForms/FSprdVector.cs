﻿using MEAClosedLoop.Common;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace MEAClosedLoop.UIForms
{
  public partial class FSprdVector : Form
  {
    Graphics g;
    public Vector60 DrawnVector;

    public FSprdVector()
    {
      InitializeComponent();
      maxValueTextBox.Text = "30";
    }

    public void Draw21(Vector60 vector)
    {
      Draw2(vector);
      Draw1(vector);
    }

    public void Draw1(Vector60 vector)
    {
      g = DrawingPictureBox.CreateGraphics();
      // Отрисовка линий
      Pen myPen = new Pen(Color.Black);
      var height = DrawingPictureBox.Height;
      var width = DrawingPictureBox.Width;
      float h = height / 9;
      float w = width / 9;
      for (int i = 1; i < 9; i++)
      {
        g.DrawLine(myPen, 0, h * i, width - 1, h * i);
        g.DrawLine(myPen, w * i, 0, w * i, height - 1);
      }

      // Набор цветов (30)
      ColorHeatMap heatMap = new ColorHeatMap();
      List<Color> colors = new List<Color>();
      int a;
      int maxVal = int.TryParse(maxValueTextBox.Text, out a) && a > 0 ? a : 30;
      for (int i = 0; i < maxVal; i++)
      {
        //Color col = Color.FromArgb(0, 255 - i * 255 / 15, i * 255 / 10);
        Color col = heatMap.GetColorForValue(i, maxVal);
        colors.Add(col);
      }
      //for (int i = 0; i < 10; i++)
      //{
      //  Color col = Color.FromArgb(i * 255 / 10, 0, 255 - i * 255 / 10);
      //  colors.Add(col);
      //}

      // Отрисовка кругов
      float diam = Math.Min(h, w) / 2;
      FontFamily fontFamily = new FontFamily("Arial");
      Font font = new Font(fontFamily, 11, FontStyle.Italic, GraphicsUnit.Pixel);
      Pen redPen = new Pen(Color.Red, 2);
      //Pen ellPen = new Pen(Color.Black);
      for (int i = 1; i < 9; i++)
      {
        for (int j = 1; j < 9; j++)
        {
          if ((i == 1 || i == 8) && (j == 1 || j == 8))
            continue;

          int val = -1;
          int chId = NAME2IDX[10 * i + j];
          if (vector.Elements.ContainsKey(chId))
          {
            val = vector.Elements[chId] / 25;
          }

          Color brushColor;
          if (val != -1)
          {
            if (val > maxVal - 1)
            {
              brushColor = colors[maxVal - 1];
            }
            else
            {
              brushColor = colors[val];
            }
            SolidBrush ellBrush = new SolidBrush(brushColor);
            g.FillEllipse(ellBrush, w * j - diam / 2, h * i - diam / 2, diam, diam);
            //g.DrawString((10 * i + j).ToString(), font, new SolidBrush(Color.Black), new PointF(w * j + diam / 4, h * i + diam / 4));
            g.DrawString(val.ToString(), font, new SolidBrush(Color.Black), new PointF(w * j + diam / 4, h * i + diam / 4));
          }
          else
          {
            g.FillEllipse(new SolidBrush(Color.Black), w * j - diam / 2, h * i - diam / 2, diam, diam);
            g.DrawLine(redPen, w * j - diam / 2, h * i - diam / 2, w * j + diam / 2, h * i + diam / 2);
            g.DrawLine(redPen, w * j - diam / 2, h * i + diam / 2, w * j + diam / 2, h * i - diam / 2);
          }

          //g.DrawEllipse(ellPen, w * i - diam / 2, h * j - diam / 2, diam, diam);
          //g.FillEllipse(ellBrush, w * j - diam / 2, h * i - diam / 2, diam, diam);
          //if (val != -1)
          //  g.DrawString(val.ToString(), font, new SolidBrush(Color.Black), new PointF(w * j + diam / 4, h * i + diam / 4));
        }
      }

      var arrowPoints = vector.GetGradientPoints();
      using (Pen p = new Pen(Color.FromArgb(100, Color.Black), 20))
      {
        p.EndCap = System.Drawing.Drawing2D.LineCap.ArrowAnchor;

        g.DrawLine(p, 
          arrowPoints.Item1 % 10 * w - w / 2, arrowPoints.Item1 / 10 * h - h / 2,
          arrowPoints.Item2 % 10 * w + w / 2, arrowPoints.Item2 / 10 * h + h / 2);
      }

      // Отрисовка градиента с подписью
      height = GradientPictureBox.Height;
      width = GradientPictureBox.Width;
      h = height / maxVal - 1;
      w = width / 3;
      var gradGraphics = GradientPictureBox.CreateGraphics();
      gradGraphics.FillRectangle(new SolidBrush(Color.White), new Rectangle(0, 0, width, height));
      for (int i = 0; i < maxVal; i++)
      {
        gradGraphics.FillRectangle(new SolidBrush(colors[i]), w, i * h, w, h);
        gradGraphics.DrawString(i.ToString(), font, new SolidBrush(Color.Black), new PointF(2 * w, h * i));
      }
      //for (int i = 0; i < 10; i++)
      //{

      //  gradGraphics.DrawString((i + 10).ToString(), font, new SolidBrush(Color.Black), new PointF((i + 10) * w, h / 2));
      //  gradGraphics.FillRectangle(new SolidBrush(colors[i + 10]), (10 + i) * w, h, w, h);
      //}

      gradGraphics.Dispose();
      g.Dispose();
      myPen.Dispose();
    }

    public void Draw2(Vector60 vector)
    {
      g = DrawingPictureBox.CreateGraphics();
      g.Clear(Color.White);
      // Отрисовка линий
      Pen myPen = new Pen(Color.Black);
      var height = DrawingPictureBox.Height;
      var width = DrawingPictureBox.Width;
      float h = height / 9;
      float w = width / 9;


      // Набор цветов (maxVal - количество)
      ColorHeatMap heatMap = new ColorHeatMap();
      List<Color> colors = new List<Color>();
      int a = 0;
      int maxVal = int.TryParse(maxValueTextBox.Text, out a) && a > 0 ? a : 30;
      for (int i = 0; i < maxVal; i++)
      {
        Color col = heatMap.GetColorForValue(i, maxVal);
        colors.Add(col);
      }

      // Отрисовка квадратов
      FontFamily fontFamily = new FontFamily("Arial");
      Font font = new Font(fontFamily, 13, FontStyle.Bold, GraphicsUnit.Pixel);
      for (int i = 1; i < 9; i++)
      {
        for (int j = 1; j < 9; j++)
        {
          if ((i == 1 || i == 8) && (j == 1 || j == 8))
            continue;

          int val = -1;
          int chId = NAME2IDX[10 * i + j];
          if (vector.Elements.ContainsKey(chId))
          {
            val = vector.Elements[chId] / 25;
          }

          Color brushColor;
          if (val != -1)
          {
            if (val > maxVal - 1)
            {
              brushColor = colors[maxVal - 1];
            }
            else
            {
              brushColor = colors[val];
            }
            SolidBrush ellBrush = new SolidBrush(brushColor);
            g.FillRectangle(ellBrush, w * j, h * i, w, h);
          }
          else
          {
            //g.FillRectangle(new SolidBrush(Color.Black), w * j, h * i, w, h);
          }
        }
      }

      // Отрисовка градиента с подписью
      height = GradientPictureBox.Height;
      width = GradientPictureBox.Width;
      h = height / maxVal - 1;
      w = width / 3;
      var gradGraphics = GradientPictureBox.CreateGraphics();
      gradGraphics.Clear(Color.White);
      for (int i = 0; i < maxVal; i++)
      {
        gradGraphics.FillRectangle(new SolidBrush(colors[i]), w, i * h, w, h);
        gradGraphics.DrawString(i.ToString(), font, new SolidBrush(Color.Black), new PointF(2 * w, h * i));
      }

      gradGraphics.Dispose();
      g.Dispose();
      myPen.Dispose();
    }

    private void button3_Click(object sender, EventArgs e)
    {
      Draw21(DrawnVector);
    }

    private void button1_Click(object sender, EventArgs e)
    {
      Draw1(DrawnVector);
    }

    private void button2_Click(object sender, EventArgs e)
    {
      Draw2(DrawnVector);
    }


    #region NAME/ID matrix

    public const int MAX_CHANNELS = 60;

    // Range of correct electrode names. But some names are invalid within this range (19, 20, 29, etc.)
    public const int ELECTRODE_FIRST = 12;
    public const int ELECTRODE_LAST = 87;

    public static readonly int[] NAME2IDX = { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,   // 0
                                              -1, -1, 20, 18, 15, 14, 11,  9, -1, -1,   // 1
                                              -1, 23, 21, 19, 16, 13, 10,  8,  6, -1,   // 2
                                              -1, 25, 24, 22, 17, 12,  7,  5,  4, -1,   // 3
                                              -1, 28, 29, 27, 26,  3,  2,  0,  1, -1,   // 4
                                              -1, 31, 30, 32, 33, 56, 57, 59, 58, -1,   // 5
                                              -1, 34, 35, 37, 42, 47, 52, 54, 55, -1,   // 6
                                              -1, 36, 38, 40, 43, 46, 49, 51, 53, -1,   // 7
                                              -1, -1, 39, 41, 44, 45, 48, 50, -1, -1 }; // 8


    public static readonly int[] IDX2NAME = {     47, 48, 46, 45, 38, 37,
                                              28, 36, 27, 17, 26, 16, 35, 25,
                                              15, 14, 24, 34, 13, 23, 12, 22,
                                              33, 21, 32, 31, 44, 43, 41, 42,
                                              52, 51, 53, 54, 61, 62, 71, 63,
                                              72, 82, 73, 83, 64, 74, 84, 85,
                                              75, 65, 86, 76, 87, 77, 66, 78,
                                                  67, 68, 55, 56, 58, 57 };
  }
  #endregion
}

