﻿namespace MEAClosedLoop.UIForms
{
  partial class FSpreadClusterCompare
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.groupBox1 = new System.Windows.Forms.GroupBox();
      this.TBCorrValue = new System.Windows.Forms.TextBox();
      this.label1 = new System.Windows.Forms.Label();
      this.RightVector = new MEAClosedLoop.UIForms.SubView.SpreadVectorView();
      this.LeftVector = new MEAClosedLoop.UIForms.SubView.SpreadVectorView();
      this.groupBox1.SuspendLayout();
      this.SuspendLayout();
      // 
      // groupBox1
      // 
      this.groupBox1.Controls.Add(this.TBCorrValue);
      this.groupBox1.Controls.Add(this.label1);
      this.groupBox1.Location = new System.Drawing.Point(6, 6);
      this.groupBox1.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Padding = new System.Windows.Forms.Padding(1, 1, 1, 1);
      this.groupBox1.Size = new System.Drawing.Size(518, 45);
      this.groupBox1.TabIndex = 2;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "схожесть";
      // 
      // TBCorrValue
      // 
      this.TBCorrValue.Location = new System.Drawing.Point(120, 15);
      this.TBCorrValue.Margin = new System.Windows.Forms.Padding(1, 1, 1, 1);
      this.TBCorrValue.Name = "TBCorrValue";
      this.TBCorrValue.Size = new System.Drawing.Size(80, 20);
      this.TBCorrValue.TabIndex = 1;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(3, 17);
      this.label1.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(112, 13);
      this.label1.TabIndex = 0;
      this.label1.Text = "корреляция пирсона";
      // 
      // RightVector
      // 
      this.RightVector.BurstID = ((ulong)(0ul));
      this.RightVector.Location = new System.Drawing.Point(265, 49);
      this.RightVector.Name = "RightVector";
      this.RightVector.Size = new System.Drawing.Size(258, 644);
      this.RightVector.TabIndex = 1;
      // 
      // LeftVector
      // 
      this.LeftVector.BurstID = ((ulong)(0ul));
      this.LeftVector.Location = new System.Drawing.Point(1, 49);
      this.LeftVector.Name = "LeftVector";
      this.LeftVector.Size = new System.Drawing.Size(258, 644);
      this.LeftVector.TabIndex = 0;
      // 
      // FSpreadClusterCompare
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(525, 698);
      this.Controls.Add(this.groupBox1);
      this.Controls.Add(this.RightVector);
      this.Controls.Add(this.LeftVector);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
      this.Name = "FSpreadClusterCompare";
      this.Text = "FSpreadCluasterCompare";
      this.Load += new System.EventHandler(this.FSpreadCluasterCompare_Load);
      this.groupBox1.ResumeLayout(false);
      this.groupBox1.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private SubView.SpreadVectorView LeftVector;
    private SubView.SpreadVectorView RightVector;
    private System.Windows.Forms.GroupBox groupBox1;
    private System.Windows.Forms.TextBox TBCorrValue;
    private System.Windows.Forms.Label label1;
  }
}