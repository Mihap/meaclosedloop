﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MEAClosedLoop.UIForms.SubView;
namespace MEAClosedLoop.UIForms
{
  public partial class FSpreadClusterCompare : Form
  {
    ulong LeftID = 0, RightID = 0;
    double Correlation;
    public FSpreadClusterCompare()
    {
      InitializeComponent();
    }
    public FSpreadClusterCompare(ulong LeftID, ulong RightID, double Correlation) : this()
    {
      this.LeftID = LeftID;
      this.RightID = RightID;
      this.Correlation = Correlation;
    }

    private void FSpreadCluasterCompare_Load(object sender, EventArgs e)
    {
      LeftVector.BurstID = LeftID;
      RightVector.BurstID = RightID;
      TBCorrValue.Text = Correlation.ToString();
    }
  }
}
