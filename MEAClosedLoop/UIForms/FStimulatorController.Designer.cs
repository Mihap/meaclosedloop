﻿namespace MEAClosedLoop.UIForms
{
  partial class FStimulatorController
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.CyclicStimRBtn = new System.Windows.Forms.RadioButton();
      this.AdaptiveStimRBtn = new System.Windows.Forms.RadioButton();
      this.groupBox1 = new System.Windows.Forms.GroupBox();
      this.groupBox2 = new System.Windows.Forms.GroupBox();
      this.label1 = new System.Windows.Forms.Label();
      this.NUDStimInterval = new System.Windows.Forms.NumericUpDown();
      this.groupBox3 = new System.Windows.Forms.GroupBox();
      this.BtnRunStim = new System.Windows.Forms.Button();
      this.TBStimStatus = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.groupBox1.SuspendLayout();
      this.groupBox2.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.NUDStimInterval)).BeginInit();
      this.groupBox3.SuspendLayout();
      this.SuspendLayout();
      // 
      // CyclicStimRBtn
      // 
      this.CyclicStimRBtn.AutoSize = true;
      this.CyclicStimRBtn.Location = new System.Drawing.Point(12, 17);
      this.CyclicStimRBtn.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
      this.CyclicStimRBtn.Name = "CyclicStimRBtn";
      this.CyclicStimRBtn.Size = new System.Drawing.Size(104, 17);
      this.CyclicStimRBtn.TabIndex = 2;
      this.CyclicStimRBtn.TabStop = true;
      this.CyclicStimRBtn.Text = "Периодическая";
      this.CyclicStimRBtn.UseVisualStyleBackColor = true;
      this.CyclicStimRBtn.CheckedChanged += new System.EventHandler(this.CyclicStimRBtn_CheckedChanged);
      // 
      // AdaptiveStimRBtn
      // 
      this.AdaptiveStimRBtn.AutoSize = true;
      this.AdaptiveStimRBtn.Location = new System.Drawing.Point(12, 37);
      this.AdaptiveStimRBtn.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
      this.AdaptiveStimRBtn.Name = "AdaptiveStimRBtn";
      this.AdaptiveStimRBtn.Size = new System.Drawing.Size(85, 17);
      this.AdaptiveStimRBtn.TabIndex = 3;
      this.AdaptiveStimRBtn.TabStop = true;
      this.AdaptiveStimRBtn.Text = "Адаптивная";
      this.AdaptiveStimRBtn.UseVisualStyleBackColor = true;
      this.AdaptiveStimRBtn.CheckedChanged += new System.EventHandler(this.AdaptiveStimRBtn_CheckedChanged);
      // 
      // groupBox1
      // 
      this.groupBox1.Controls.Add(this.CyclicStimRBtn);
      this.groupBox1.Controls.Add(this.AdaptiveStimRBtn);
      this.groupBox1.Location = new System.Drawing.Point(11, 10);
      this.groupBox1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
      this.groupBox1.Size = new System.Drawing.Size(134, 59);
      this.groupBox1.TabIndex = 4;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "Тип стимуляции";
      // 
      // groupBox2
      // 
      this.groupBox2.Controls.Add(this.label1);
      this.groupBox2.Controls.Add(this.NUDStimInterval);
      this.groupBox2.Location = new System.Drawing.Point(159, 10);
      this.groupBox2.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
      this.groupBox2.Name = "groupBox2";
      this.groupBox2.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
      this.groupBox2.Size = new System.Drawing.Size(111, 59);
      this.groupBox2.TabIndex = 4;
      this.groupBox2.TabStop = false;
      this.groupBox2.Text = "Интервал";
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(88, 19);
      this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(21, 13);
      this.label1.TabIndex = 1;
      this.label1.Text = "мс";
      // 
      // NUDStimInterval
      // 
      this.NUDStimInterval.Location = new System.Drawing.Point(5, 18);
      this.NUDStimInterval.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
      this.NUDStimInterval.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
      this.NUDStimInterval.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
      this.NUDStimInterval.Name = "NUDStimInterval";
      this.NUDStimInterval.Size = new System.Drawing.Size(79, 20);
      this.NUDStimInterval.TabIndex = 0;
      this.NUDStimInterval.Value = new decimal(new int[] {
            1500,
            0,
            0,
            0});
      this.NUDStimInterval.ValueChanged += new System.EventHandler(this.StimIntervalNUD_ValueChanged);
      // 
      // groupBox3
      // 
      this.groupBox3.Controls.Add(this.BtnRunStim);
      this.groupBox3.Controls.Add(this.TBStimStatus);
      this.groupBox3.Controls.Add(this.label2);
      this.groupBox3.Location = new System.Drawing.Point(280, 10);
      this.groupBox3.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
      this.groupBox3.Name = "groupBox3";
      this.groupBox3.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
      this.groupBox3.Size = new System.Drawing.Size(150, 59);
      this.groupBox3.TabIndex = 5;
      this.groupBox3.TabStop = false;
      this.groupBox3.Text = "Стимуляция";
      // 
      // BtnRunStim
      // 
      this.BtnRunStim.Location = new System.Drawing.Point(7, 34);
      this.BtnRunStim.Margin = new System.Windows.Forms.Padding(2);
      this.BtnRunStim.Name = "BtnRunStim";
      this.BtnRunStim.Size = new System.Drawing.Size(101, 22);
      this.BtnRunStim.TabIndex = 1;
      this.BtnRunStim.Text = "Включить";
      this.BtnRunStim.UseVisualStyleBackColor = true;
      this.BtnRunStim.Click += new System.EventHandler(this.BtnRunStim_Click);
      // 
      // TBStimStatus
      // 
      this.TBStimStatus.AutoSize = true;
      this.TBStimStatus.Location = new System.Drawing.Point(47, 19);
      this.TBStimStatus.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
      this.TBStimStatus.Name = "TBStimStatus";
      this.TBStimStatus.Size = new System.Drawing.Size(61, 13);
      this.TBStimStatus.TabIndex = 0;
      this.TBStimStatus.Text = "отключена";
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(4, 19);
      this.label2.Margin = new System.Windows.Forms.Padding(1, 0, 1, 0);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(43, 13);
      this.label2.TabIndex = 0;
      this.label2.Text = "статус:";
      // 
      // FStimulatorController
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(445, 75);
      this.Controls.Add(this.groupBox3);
      this.Controls.Add(this.groupBox2);
      this.Controls.Add(this.groupBox1);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
      this.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
      this.Name = "FStimulatorController";
      this.Text = "FStimulatorController";
      this.Load += new System.EventHandler(this.FStimulatorController_Load);
      this.groupBox1.ResumeLayout(false);
      this.groupBox1.PerformLayout();
      this.groupBox2.ResumeLayout(false);
      this.groupBox2.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.NUDStimInterval)).EndInit();
      this.groupBox3.ResumeLayout(false);
      this.groupBox3.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.RadioButton CyclicStimRBtn;
    private System.Windows.Forms.RadioButton AdaptiveStimRBtn;
    private System.Windows.Forms.GroupBox groupBox1;
    private System.Windows.Forms.GroupBox groupBox2;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.NumericUpDown NUDStimInterval;
    private System.Windows.Forms.GroupBox groupBox3;
    private System.Windows.Forms.Label TBStimStatus;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Button BtnRunStim;
  }
}