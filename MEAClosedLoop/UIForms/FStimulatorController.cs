﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace MEAClosedLoop.UIForms
{
  public partial class FStimulatorController : Form
  {
    public FStimulatorController()
    {
      InitializeComponent();
    }

    private void FStimulatorController_Load(object sender, EventArgs e)
    {
      if (Global.lp != null)
      {
        Global.lp.OnDoStimChanged += Lp_OnDoStimChanged;
        Global.lp.OnStimIntervalChanged += Lp_OnStimIntervalChanged;
        LoadSettings();
      } 
      else
      { 
        MessageBox.Show("Не запущен цикл чтения данных");
        this.BeginInvoke(new Action(() => { Thread.Sleep(500); this.Close(); }));
      }
    }

    private void Lp_OnStimIntervalChanged(int Interval)
    {
      NUDStimInterval.BeginInvoke(new Action(() => NUDStimInterval.Value = Interval/Param.MS));
    }

    private void Lp_OnDoStimChanged(bool DoStim)
    {
      string Text;
      if (DoStim)
        Text = "включена";
      else
        Text = "отключена";
      if (DoStim)
        BtnRunStim.Text = "остановить";
      else
        BtnRunStim.Text = "запустить";

      TBStimStatus.BeginInvoke(new Action(() => TBStimStatus.Text = Text));
  }
    private void CyclicStimRBtn_CheckedChanged(object sender, EventArgs e)
    {
      if (CyclicStimRBtn.Checked)
        Global.lp.StimType = StimType.Сyclical;
    }

    private void AdaptiveStimRBtn_CheckedChanged(object sender, EventArgs e)
    {
      if (AdaptiveStimRBtn.Checked)
        Global.lp.StimType = StimType.Adaptive;
    }

    private void StimIntervalNUD_ValueChanged(object sender, EventArgs e)
    {
      Global.lp.ReceivedStimShift = (int)NUDStimInterval.Value * Param.MS; //in counts
    }

    private void LoadSettings()
    {
      if (Global.lp.DoStim)
        TBStimStatus.Text = "включена";
      else
        TBStimStatus.Text = "отключена";
      if (Global.lp.DoStim)
        BtnRunStim.Text = "остановить";
      else
        BtnRunStim.Text = "запустить";

      if (Global.lp.StimType == StimType.Adaptive)
        AdaptiveStimRBtn.Checked = true;
      else
        CyclicStimRBtn.Checked = true;
      //if loop is not inited
      if (Global.lp.ReceivedStimShift == 0)
        Global.lp.ReceivedStimShift = 40000;//counts
      NUDStimInterval.Value = Global.lp.ReceivedStimShift/Param.MS;
    }

    private void BtnRunStim_Click(object sender, EventArgs e)
    {
      Global.lp.DoStim = !Global.lp.DoStim;
    }
  }
}
