﻿namespace MEAClosedLoop.UIForms
{
  partial class FThresholdGraph
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.graphControl = new ZedGraph.ZedGraphControl();
      this.label1 = new System.Windows.Forms.Label();
      this.thTextBox = new System.Windows.Forms.TextBox();
      this.leftArrButton = new System.Windows.Forms.Button();
      this.rightArrButton = new System.Windows.Forms.Button();
      this.clustCountLabel = new System.Windows.Forms.Label();
      this.setDefaultButton = new System.Windows.Forms.Button();
      this.saveButton = new System.Windows.Forms.Button();
      this.SuspendLayout();
      // 
      // graphControl
      // 
      this.graphControl.Location = new System.Drawing.Point(1, 1);
      this.graphControl.Name = "graphControl";
      this.graphControl.ScrollGrace = 0D;
      this.graphControl.ScrollMaxX = 0D;
      this.graphControl.ScrollMaxY = 0D;
      this.graphControl.ScrollMaxY2 = 0D;
      this.graphControl.ScrollMinX = 0D;
      this.graphControl.ScrollMinY = 0D;
      this.graphControl.ScrollMinY2 = 0D;
      this.graphControl.Size = new System.Drawing.Size(1086, 537);
      this.graphControl.TabIndex = 0;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(12, 558);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(93, 13);
      this.label1.TabIndex = 1;
      this.label1.Text = "Порог отсечения";
      // 
      // thTextBox
      // 
      this.thTextBox.Enabled = false;
      this.thTextBox.Location = new System.Drawing.Point(111, 554);
      this.thTextBox.Name = "thTextBox";
      this.thTextBox.Size = new System.Drawing.Size(134, 20);
      this.thTextBox.TabIndex = 2;
      // 
      // leftArrButton
      // 
      this.leftArrButton.Location = new System.Drawing.Point(510, 553);
      this.leftArrButton.Name = "leftArrButton";
      this.leftArrButton.Size = new System.Drawing.Size(75, 23);
      this.leftArrButton.TabIndex = 3;
      this.leftArrButton.UseVisualStyleBackColor = true;
      this.leftArrButton.Click += new System.EventHandler(this.leftArrButton_Click);
      // 
      // rightArrButton
      // 
      this.rightArrButton.Location = new System.Drawing.Point(591, 553);
      this.rightArrButton.Name = "rightArrButton";
      this.rightArrButton.Size = new System.Drawing.Size(75, 23);
      this.rightArrButton.TabIndex = 4;
      this.rightArrButton.UseVisualStyleBackColor = true;
      this.rightArrButton.Click += new System.EventHandler(this.rightArrButton_Click);
      // 
      // clustCountLabel
      // 
      this.clustCountLabel.AutoSize = true;
      this.clustCountLabel.Location = new System.Drawing.Point(273, 558);
      this.clustCountLabel.Name = "clustCountLabel";
      this.clustCountLabel.Size = new System.Drawing.Size(83, 13);
      this.clustCountLabel.TabIndex = 5;
      this.clustCountLabel.Text = "clustCountLabel";
      // 
      // setDefaultButton
      // 
      this.setDefaultButton.Location = new System.Drawing.Point(738, 552);
      this.setDefaultButton.Name = "setDefaultButton";
      this.setDefaultButton.Size = new System.Drawing.Size(99, 23);
      this.setDefaultButton.TabIndex = 6;
      this.setDefaultButton.Text = "По умолчанию";
      this.setDefaultButton.UseVisualStyleBackColor = true;
      this.setDefaultButton.Click += new System.EventHandler(this.setDefaultButton_Click);
      // 
      // saveButton
      // 
      this.saveButton.Location = new System.Drawing.Point(997, 552);
      this.saveButton.Name = "saveButton";
      this.saveButton.Size = new System.Drawing.Size(77, 23);
      this.saveButton.TabIndex = 7;
      this.saveButton.Text = "Сохранить";
      this.saveButton.UseVisualStyleBackColor = true;
      this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
      // 
      // FThresholdGraph
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(1086, 579);
      this.Controls.Add(this.saveButton);
      this.Controls.Add(this.setDefaultButton);
      this.Controls.Add(this.clustCountLabel);
      this.Controls.Add(this.rightArrButton);
      this.Controls.Add(this.leftArrButton);
      this.Controls.Add(this.thTextBox);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.graphControl);
      this.Name = "FThresholdGraph";
      this.Text = "FThresholdGraph";
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private ZedGraph.ZedGraphControl graphControl;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.TextBox thTextBox;
    private System.Windows.Forms.Button leftArrButton;
    private System.Windows.Forms.Button rightArrButton;
    private System.Windows.Forms.Label clustCountLabel;
    private System.Windows.Forms.Button setDefaultButton;
    private System.Windows.Forms.Button saveButton;
  }
}