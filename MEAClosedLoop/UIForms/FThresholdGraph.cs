﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ZedGraph;

namespace MEAClosedLoop.UIForms
{
  public delegate void ThresholdChangedDelagate(double th, int clustNum);

  public partial class FThresholdGraph : Form
  {
    public event ThresholdChangedDelagate thresholdChanged;
    private List<int> maxList;
    private int currLinePoint;
    private Dictionary<int, double> clustDict;
    private double defTh;
    private int defClustNum;

    public FThresholdGraph(CSprdClusterBuilder scb, int k, double th)
    {
      InitializeComponent();
      leftArrButton.Text = char.ConvertFromUtf32(0x2190);
      rightArrButton.Text = char.ConvertFromUtf32(0x2192);

      defTh = th;
      defClustNum = k;
      maxList = scb.MaxList;
      clustDict = scb.ClustDict;
      currLinePoint = maxList.IndexOf(scb.clustNum);
      if (currLinePoint == -1)
      {
        currLinePoint = 0;
      }

      GraphPane pane = graphControl.GraphPane;
      pane.CurveList.Clear();
      pane.Title.Text = "Выбор порога отсечения";
      pane.YAxis.Title.Text = "Расстояние между кластерами";
      pane.XAxis.Title.Text = "Количество кластеров";
      PointPairList ppList1 = new PointPairList();
      PointPairList ppList2 = new PointPairList();
      foreach (KeyValuePair<int, double> kvp in clustDict)
      {
        ppList1.Add(new PointPair(kvp.Key, kvp.Value));
      }
      foreach (KeyValuePair<int, double> kvp in scb.Diff2Dict)
      {
        ppList2.Add(new PointPair(kvp.Key + 2, kvp.Value * 6));
      }

      LineItem l1 = new LineItem("Кластеры", ppList1, Color.Blue, SymbolType.None);
      LineItem l2 = new LineItem("Вторая производная", ppList2, Color.Red, SymbolType.None);
      pane.CurveList.Add(l1);
      pane.CurveList.Add(l2);
      thTextBox.Text = scb.threshold.ToString();
      clustCountLabel.Text = "Количество кластеров: " + (scb.clustNum + 2);
      graphControl.AxisChange();

      SetLine(scb.clustNum + 2, scb.threshold);
    }

    private void SetLine(int clustNum, double th)
    {
      GraphPane pane = graphControl.GraphPane;
      pane.GraphObjList.Clear();

      LineObj line = new LineObj(clustNum, pane.YAxis.Scale.Min, clustNum, pane.YAxis.Scale.Max);
      line.Line.Style = System.Drawing.Drawing2D.DashStyle.Dash;
      pane.GraphObjList.Add(line);

      ArrowObj arrow = new ArrowObj(clustNum + 22, th, clustNum, th);
      pane.GraphObjList.Add(arrow);

      TextObj text = new TextObj("threshold", clustNum + 31, th);
      text.FontSpec.Border.IsVisible = false;
      pane.GraphObjList.Add(text);

      graphControl.Invalidate();

    }

    private void leftArrButton_Click(object sender, EventArgs e)
    {
      if (currLinePoint < maxList.Count - 1)
      {
        int k = maxList[++currLinePoint] + 2;
        SetLine(k, clustDict[k]);
        thTextBox.Text = clustDict[k].ToString();
        clustCountLabel.Text = "Количество кластеров: " + k;
      }
    }

    private void rightArrButton_Click(object sender, EventArgs e)
    {
      if (currLinePoint > 0)
      {
        int k = maxList[--currLinePoint] + 2;
        SetLine(k, clustDict[k]);
        thTextBox.Text = clustDict[k].ToString();
        clustCountLabel.Text = "Количество кластеров: " + k;
      }
    }

    private void setDefaultButton_Click(object sender, EventArgs e)
    {
      currLinePoint = maxList.IndexOf(defClustNum);
      if (currLinePoint == -1)
      {
        currLinePoint = 0;
      }
      SetLine(defClustNum + 2, defTh);
      clustCountLabel.Text = "Количество кластеров: " + (defClustNum + 2);
      thTextBox.Text = defTh.ToString();
    }

    private void saveButton_Click(object sender, EventArgs e)
    {
      if (thresholdChanged != null)
      {
        thresholdChanged(clustDict[maxList[currLinePoint] + 2], maxList[currLinePoint]);
      }

      this.Close();
    }
  }
}
