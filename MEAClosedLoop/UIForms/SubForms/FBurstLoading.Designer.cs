﻿namespace MEAClosedLoop.UIForms.SubForms
{
    partial class FBurstLoading
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
      this.label1 = new System.Windows.Forms.Label();
      this.parsingProgressBar = new System.Windows.Forms.ProgressBar();
      this.ParsedLbl = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.label4 = new System.Windows.Forms.Label();
      this.FoundBurstLbl = new System.Windows.Forms.Label();
      this.label6 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.FileNamelbl = new System.Windows.Forms.Label();
      this.SuspendLayout();
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(12, 34);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(68, 13);
      this.label1.TabIndex = 0;
      this.label1.Text = "Обработано";
      // 
      // parsingProgressBar
      // 
      this.parsingProgressBar.Location = new System.Drawing.Point(12, 50);
      this.parsingProgressBar.Name = "parsingProgressBar";
      this.parsingProgressBar.Size = new System.Drawing.Size(525, 23);
      this.parsingProgressBar.TabIndex = 1;
      // 
      // ParsedLbl
      // 
      this.ParsedLbl.AutoSize = true;
      this.ParsedLbl.Location = new System.Drawing.Point(86, 34);
      this.ParsedLbl.Name = "ParsedLbl";
      this.ParsedLbl.Size = new System.Drawing.Size(19, 13);
      this.ParsedLbl.TabIndex = 0;
      this.ParsedLbl.Text = "00";
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(111, 34);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(50, 13);
      this.label3.TabIndex = 0;
      this.label3.Text = "% файла";
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(219, 34);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(62, 13);
      this.label4.TabIndex = 0;
      this.label4.Text = "Загружено";
      // 
      // FoundBurstLbl
      // 
      this.FoundBurstLbl.AutoSize = true;
      this.FoundBurstLbl.Location = new System.Drawing.Point(287, 34);
      this.FoundBurstLbl.Name = "FoundBurstLbl";
      this.FoundBurstLbl.Size = new System.Drawing.Size(19, 13);
      this.FoundBurstLbl.TabIndex = 0;
      this.FoundBurstLbl.Text = "00";
      // 
      // label6
      // 
      this.label6.AutoSize = true;
      this.label6.Location = new System.Drawing.Point(312, 34);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(36, 13);
      this.label6.TabIndex = 0;
      this.label6.Text = "пачек";
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(12, 9);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(36, 13);
      this.label2.TabIndex = 2;
      this.label2.Text = "Файл";
      // 
      // FileNamelbl
      // 
      this.FileNamelbl.AutoSize = true;
      this.FileNamelbl.Location = new System.Drawing.Point(54, 9);
      this.FileNamelbl.Name = "FileNamelbl";
      this.FileNamelbl.Size = new System.Drawing.Size(0, 13);
      this.FileNamelbl.TabIndex = 2;
      // 
      // FBurstLoading
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(549, 76);
      this.Controls.Add(this.FileNamelbl);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.parsingProgressBar);
      this.Controls.Add(this.FoundBurstLbl);
      this.Controls.Add(this.ParsedLbl);
      this.Controls.Add(this.label6);
      this.Controls.Add(this.label3);
      this.Controls.Add(this.label4);
      this.Controls.Add(this.label1);
      this.Name = "FBurstLoading";
      this.Text = "FBurstLoading";
      this.Load += new System.EventHandler(this.FBurstLoading_Load);
      this.ResumeLayout(false);
      this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ProgressBar parsingProgressBar;
        private System.Windows.Forms.Label ParsedLbl;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label FoundBurstLbl;
        private System.Windows.Forms.Label label6;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label FileNamelbl;
  }
}