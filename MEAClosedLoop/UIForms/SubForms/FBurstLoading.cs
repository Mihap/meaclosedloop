﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MEAClosedLoop.UIForms.SubForms
{
  public partial class FBurstLoading : Form
  {
    public FBurstLoading()
    {
      InitializeComponent();
    }
    public void Init()
    {
      
    }
    public void UpdateProgress(int progress, int burstscount, string FileName)
    {
      this.BeginInvoke(new Action(() =>
      {
        ParsedLbl.Text = progress.ToString();
        FoundBurstLbl.Text = burstscount.ToString();
        FileNamelbl.Text = FileName;
        if (progress >= 0 && progress <= 100)
          parsingProgressBar.Value = progress;
      }));
    }

    private void FBurstLoading_Load(object sender, EventArgs e)
    {
      this.BeginInvoke(new Action(() =>
      {
        try
        {
          ParsedLbl.Text = "00";
          FoundBurstLbl.Text = "00";
          parsingProgressBar.Value = 0;
        }
        catch { }
      }));
    }
    public void SafeClose()
    {
      //this.BeginInvoke(new Action(() => this.Close()));
    }
  }
}
