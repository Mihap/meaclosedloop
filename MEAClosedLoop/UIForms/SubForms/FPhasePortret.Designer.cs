﻿namespace MEAClosedLoop.UIForms.SubForms
{
  partial class FPhasePortret
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.PhaseZedGraph = new ZedGraph.ZedGraphControl();
      this.SuspendLayout();
      // 
      // PhaseZedGraph
      // 
      this.PhaseZedGraph.Dock = System.Windows.Forms.DockStyle.Fill;
      this.PhaseZedGraph.Location = new System.Drawing.Point(0, 0);
      this.PhaseZedGraph.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
      this.PhaseZedGraph.Name = "PhaseZedGraph";
      this.PhaseZedGraph.ScrollGrace = 0D;
      this.PhaseZedGraph.ScrollMaxX = 0D;
      this.PhaseZedGraph.ScrollMaxY = 0D;
      this.PhaseZedGraph.ScrollMaxY2 = 0D;
      this.PhaseZedGraph.ScrollMinX = 0D;
      this.PhaseZedGraph.ScrollMinY = 0D;
      this.PhaseZedGraph.ScrollMinY2 = 0D;
      this.PhaseZedGraph.Size = new System.Drawing.Size(1885, 1352);
      this.PhaseZedGraph.TabIndex = 0;
      // 
      // FPhasePortret
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(14F, 29F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(1885, 1352);
      this.Controls.Add(this.PhaseZedGraph);
      this.Name = "FPhasePortret";
      this.Text = "FPhasePortret";
      this.Load += new System.EventHandler(this.FPhasePortret_Load);
      this.ResumeLayout(false);

    }

    #endregion

    private ZedGraph.ZedGraphControl PhaseZedGraph;
  }
}