﻿using MEAClosedLoop.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ZedGraph;
namespace MEAClosedLoop.UIForms.SubForms
{
  public partial class FPhasePortret : Form
  {
    Timer updateGraph;
    BurstPhaseMode burstPhaseMode;
    public FPhasePortret()
    {
      InitializeComponent();
    }
    public FPhasePortret(BurstPhaseMode mode) : this()
    {
      burstPhaseMode = mode;
      updateGraph = new Timer();
      updateGraph.Interval = 2000;
      updateGraph.Tick += UpdateGraph_Tick; ;
    }

    private void UpdateGraph_Tick(object sender, EventArgs e)
    {
      GraphPane pane = PhaseZedGraph.GraphPane;
      if (pane == null) return;

      pane.CurveList.Clear();

      PointPairList Intervallist = new PointPairList();
      List<PointPairList> stimlist = new List<PointPairList>();
      //int timeAxis = pane.AddYAxis("time, sec");
      pane.XAxis.Title.Text = "Длительность, мс";
      pane.Title.Text = "Фазовый портрет";
      pane.YAxis.IsVisible = true;
      pane.YAxis.Title.Text = "средний интервал между пачками, мс";
      pane.IsFontsScaled = false;
      double n = CBurstDataProvider.BurstAdrCollection.Count;
      //pane.BaseDimension = 1.5f;
      if (burstPhaseMode == BurstPhaseMode.Right)
        for (int i = 0; i < CBurstDataProvider.BurstAdrCollection.Count - 1; i++)
        {
          ulong id = CBurstDataProvider.BurstAdrCollection.Keys.ElementAt(i);
          ulong nextid = CBurstDataProvider.BurstAdrCollection.Keys.ElementAt(i + 1);
         
          int[] rgb = Colors.valueToRGB(i / n);
          Color result = Color.FromArgb(255, rgb[0], rgb[1], rgb[2]);
          //LineItem durationCurve = pane.AddCurve("", Intervallist, result, SymbolType.Diamond);
          LineItem durationCurve = pane.AddCurve("", new PointPairList() {new PointPair( CBurstDataProvider.BurstAdrCollection[id].DurationTime/25.0,
              (CBurstDataProvider.BurstAdrCollection[nextid].BeginTime - CBurstDataProvider.BurstAdrCollection[id].BeginTime)/25) }, result, SymbolType.Diamond);
          // durationCurve.Line.IsVisible = false;
          // Цвет заполнения отметок (ромбиков) - голубой
          durationCurve.Symbol.Fill.Color = result;

          // !!!
          // Тип заполнения - сплошная заливка
          durationCurve.Symbol.Fill.Type = FillType.Solid;

          // !!!
          // Размер ромбиков
          durationCurve.Symbol.Size = 7;

        }
      if (burstPhaseMode == BurstPhaseMode.Left)
        for (int i = 1; i < CBurstDataProvider.BurstAdrCollection.Count; i++)
        {
          ulong id = CBurstDataProvider.BurstAdrCollection.Keys.ElementAt(i - 1);
          ulong nextid = CBurstDataProvider.BurstAdrCollection.Keys.ElementAt(i);
        


          int[] rgb = Colors.valueToRGB(i / n);
          Color result = Color.FromArgb(255, rgb[0], rgb[1], rgb[2]);
          //LineItem durationCurve = pane.AddCurve("", Intervallist, result, SymbolType.Diamond);
          LineItem durationCurve = pane.AddCurve("", new PointPairList() {new PointPair( CBurstDataProvider.BurstAdrCollection[id].DurationTime/25.0,
              (CBurstDataProvider.BurstAdrCollection[nextid].BeginTime - CBurstDataProvider.BurstAdrCollection[id].BeginTime)/25) }, result, SymbolType.Diamond);
          // durationCurve.Line.IsVisible = false;
          // Цвет заполнения отметок (ромбиков) - голубой
          durationCurve.Symbol.Fill.Color = result;

          // !!!
          // Тип заполнения - сплошная заливка
          durationCurve.Symbol.Fill.Type = FillType.Solid;

          // !!!
          // Размер ромбиков
          durationCurve.Symbol.Size = 7;
          Intervallist.Clear();
        }
      if (burstPhaseMode == BurstPhaseMode.Middle)
        for (int i = 1; i < CBurstDataProvider.BurstAdrCollection.Count - 1; i++)
        {
          ulong previd = CBurstDataProvider.BurstAdrCollection.Keys.ElementAt(i - 1);
          ulong id = CBurstDataProvider.BurstAdrCollection.Keys.ElementAt(i);
          ulong nextid = CBurstDataProvider.BurstAdrCollection.Keys.ElementAt(i + 1);
          double prevInterval = CBurstDataProvider.BurstAdrCollection[id].BeginTime / 25 - CBurstDataProvider.BurstAdrCollection[previd].BeginTime / 25;
          double postInterval = CBurstDataProvider.BurstAdrCollection[nextid].BeginTime / 25 - CBurstDataProvider.BurstAdrCollection[id].BeginTime / 25;

          int[] rgb = Colors.valueToRGB(i / n);
          Color result = Color.FromArgb(255, rgb[0], rgb[1], rgb[2]);
          //LineItem durationCurve = pane.AddCurve("", Intervallist, result, SymbolType.Diamond);
          LineItem durationCurve = pane.AddCurve("", new PointPairList() {new PointPair( CBurstDataProvider.BurstAdrCollection[id].DurationTime/25.0,
              (CBurstDataProvider.BurstAdrCollection[nextid].BeginTime - CBurstDataProvider.BurstAdrCollection[id].BeginTime)/25) }, result, SymbolType.Diamond);
          // durationCurve.Line.IsVisible = false;
          // Цвет заполнения отметок (ромбиков) - голубой
          durationCurve.Symbol.Fill.Color = result;

          // !!!
          // Тип заполнения - сплошная заливка
          durationCurve.Symbol.Fill.Type = FillType.Solid;

          // !!!
          // Размер ромбиков
          durationCurve.Symbol.Size = 7;
        }

      //int[] rgb = Colors.waveLengthToRGB(0);
      //Color result = Color.FromArgb(255, rgb[0], rgb[1], rgb[2]);
      //LineItem durationCurve = pane.AddCurve("", Intervallist, Color.Blue, SymbolType.Diamond);
      //durationCurve.Line.IsVisible = false;
      //// Цвет заполнения отметок (ромбиков) - голубой
      //durationCurve.Symbol.Fill.Color = Color.Blue;

      //// !!!
      //// Тип заполнения - сплошная заливка
      //durationCurve.Symbol.Fill.Type = FillType.Solid;

      //// !!!
      //// Размер ромбиков
      //durationCurve.Symbol.Size = 7;
      pane.YAxis.Scale.Min = 0;

      PhaseZedGraph.AxisChange();
      PhaseZedGraph.Invalidate();
    }

    private void FPhasePortret_Load(object sender, EventArgs e)
    {
      updateGraph.Start();
    }
  }
  public enum BurstPhaseMode
  {
    Left,
    Right,
    Middle
  }
}
