﻿namespace MEAClosedLoop.UIForms.SubView
{
  partial class FBurstShortData
  {
    /// <summary> 
    /// Требуется переменная конструктора.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Освободить все используемые ресурсы.
    /// </summary>
    /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Код, автоматически созданный конструктором компонентов

    /// <summary> 
    /// Обязательный метод для поддержки конструктора - не изменяйте 
    /// содержимое данного метода при помощи редактора кода.
    /// </summary>
    private void InitializeComponent()
    {
      this.label1 = new System.Windows.Forms.Label();
      this.IsSelectedChekBox = new System.Windows.Forms.CheckBox();
      this.label2 = new System.Windows.Forms.Label();
      this.StartTimeBox = new System.Windows.Forms.TextBox();
      this.DurationTimeBox = new System.Windows.Forms.TextBox();
      this.IsEvokedCheckBox = new System.Windows.Forms.CheckBox();
      this.SuspendLayout();
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(-3, 32);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(114, 13);
      this.label1.TabIndex = 0;
      this.label1.Text = "Продолжительность:";
      // 
      // IsSelectedChekBox
      // 
      this.IsSelectedChekBox.AutoSize = true;
      this.IsSelectedChekBox.Location = new System.Drawing.Point(184, 32);
      this.IsSelectedChekBox.Name = "IsSelectedChekBox";
      this.IsSelectedChekBox.Size = new System.Drawing.Size(70, 17);
      this.IsSelectedChekBox.TabIndex = 1;
      this.IsSelectedChekBox.Text = "выбрана";
      this.IsSelectedChekBox.UseVisualStyleBackColor = true;
      this.IsSelectedChekBox.CheckedChanged += new System.EventHandler(this.IsSelectedChekBox_CheckedChanged);
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(-3, 6);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(77, 13);
      this.label2.TabIndex = 2;
      this.label2.Text = "Время старта";
      // 
      // StartTimeBox
      // 
      this.StartTimeBox.Location = new System.Drawing.Point(117, 3);
      this.StartTimeBox.Name = "StartTimeBox";
      this.StartTimeBox.ReadOnly = true;
      this.StartTimeBox.Size = new System.Drawing.Size(61, 20);
      this.StartTimeBox.TabIndex = 3;
      // 
      // DurationTimeBox
      // 
      this.DurationTimeBox.Location = new System.Drawing.Point(117, 29);
      this.DurationTimeBox.Name = "DurationTimeBox";
      this.DurationTimeBox.ReadOnly = true;
      this.DurationTimeBox.Size = new System.Drawing.Size(61, 20);
      this.DurationTimeBox.TabIndex = 3;
      // 
      // IsEvokedCheckBox
      // 
      this.IsEvokedCheckBox.AutoSize = true;
      this.IsEvokedCheckBox.Enabled = false;
      this.IsEvokedCheckBox.Location = new System.Drawing.Point(184, 5);
      this.IsEvokedCheckBox.Name = "IsEvokedCheckBox";
      this.IsEvokedCheckBox.Size = new System.Drawing.Size(82, 17);
      this.IsEvokedCheckBox.TabIndex = 1;
      this.IsEvokedCheckBox.Text = "вызванная";
      this.IsEvokedCheckBox.UseVisualStyleBackColor = true;
      this.IsEvokedCheckBox.CheckedChanged += new System.EventHandler(this.IsEvokedCheckBox_CheckedChanged);
      // 
      // BurstShortData
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.Controls.Add(this.DurationTimeBox);
      this.Controls.Add(this.StartTimeBox);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.IsEvokedCheckBox);
      this.Controls.Add(this.IsSelectedChekBox);
      this.Controls.Add(this.label1);
      this.Name = "BurstShortData";
      this.Size = new System.Drawing.Size(260, 52);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.CheckBox IsSelectedChekBox;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.TextBox StartTimeBox;
    private System.Windows.Forms.TextBox DurationTimeBox;
    private System.Windows.Forms.CheckBox IsEvokedCheckBox;
  }
}
