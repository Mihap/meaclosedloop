﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MEAClosedLoop.UIForms.SubView
{
  public partial class FBurstShortData : UserControl
  {
    private ulong _StartTime;
    private ulong _Duratinon;
    private bool _IsEvoked;
    private bool _IsSelected;

    #region Имплементация Свойств
    public ulong StartTime
    {
      get
      {
        return _StartTime;
      }
      set
      {
        _StartTime = value;
        StartTimeBox.Text = (_StartTime / (Param.MS * 1000)).ToString() + " сек";
      }
    }
    public ulong Duratinon
    {
      get
      {
        return _Duratinon;
      }
      set
      {
        _Duratinon = value;
        DurationTimeBox.Text = (_Duratinon / (Param.MS )).ToString() + " мс";
      }
    }
    public bool IsEvoked
    {
      get
      {
        return _IsEvoked;
      }
      set
      {
        _IsEvoked = value;
        IsEvokedCheckBox.Checked = value;
      }
    }
    public bool IsSelected
    {
      get
      {
        return _IsSelected;
      }
      set
      {
        _IsSelected = value;
        IsSelectedChekBox.Checked = value;
      }
    }
    #endregion

    public FBurstShortData()
    {
      InitializeComponent();
      StartTime = 0;
      Duratinon = 0;
      IsEvoked = false;
      IsSelected = false;
    }
    public FBurstShortData(ulong startTime, ulong duration, bool isEvoked, bool isSelected)
    {
      InitializeComponent();
      StartTime = startTime;
      Duratinon = duration;
      IsEvoked = isEvoked;
      IsSelected = isSelected;
    }
    private void IsEvokedCheckBox_CheckedChanged(object sender, EventArgs e)
    {
      IsEvoked = IsEvokedCheckBox.Checked;
    }

    private void IsSelectedChekBox_CheckedChanged(object sender, EventArgs e)
    {
      IsSelected = IsSelectedChekBox.Checked;
    }

  }
}
