﻿namespace MEAClosedLoop.UIForms.SubView
{
  partial class ClusterVectorView
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.groupBox1 = new System.Windows.Forms.GroupBox();
      this.TBID = new System.Windows.Forms.TextBox();
      this.TBBurstsCount = new System.Windows.Forms.TextBox();
      this.label3 = new System.Windows.Forms.Label();
      this.label1 = new System.Windows.Forms.Label();
      this.zedGraphControl1 = new ZedGraph.ZedGraphControl();
      this.pictureBox1 = new System.Windows.Forms.PictureBox();
      this.groupBox1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
      this.SuspendLayout();
      // 
      // groupBox1
      // 
      this.groupBox1.Controls.Add(this.TBID);
      this.groupBox1.Controls.Add(this.TBBurstsCount);
      this.groupBox1.Controls.Add(this.label3);
      this.groupBox1.Controls.Add(this.label1);
      this.groupBox1.Location = new System.Drawing.Point(4, 2);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new System.Drawing.Size(311, 45);
      this.groupBox1.TabIndex = 3;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "Информация о кластере";
      // 
      // TBID
      // 
      this.TBID.Location = new System.Drawing.Point(226, 16);
      this.TBID.Name = "TBID";
      this.TBID.Size = new System.Drawing.Size(79, 20);
      this.TBID.TabIndex = 1;
      // 
      // TBBurstsCount
      // 
      this.TBBurstsCount.Location = new System.Drawing.Point(85, 16);
      this.TBBurstsCount.Name = "TBBurstsCount";
      this.TBBurstsCount.Size = new System.Drawing.Size(79, 20);
      this.TBBurstsCount.TabIndex = 1;
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(202, 19);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(18, 13);
      this.label3.TabIndex = 0;
      this.label3.Text = "ID";
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(6, 19);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(73, 13);
      this.label1.TabIndex = 0;
      this.label1.Text = "Кол-во пачек";
      // 
      // zedGraphControl1
      // 
      this.zedGraphControl1.Location = new System.Drawing.Point(4, 48);
      this.zedGraphControl1.Margin = new System.Windows.Forms.Padding(7);
      this.zedGraphControl1.Name = "zedGraphControl1";
      this.zedGraphControl1.ScrollGrace = 0D;
      this.zedGraphControl1.ScrollMaxX = 0D;
      this.zedGraphControl1.ScrollMaxY = 0D;
      this.zedGraphControl1.ScrollMaxY2 = 0D;
      this.zedGraphControl1.ScrollMinX = 0D;
      this.zedGraphControl1.ScrollMinY = 0D;
      this.zedGraphControl1.ScrollMinY2 = 0D;
      this.zedGraphControl1.Size = new System.Drawing.Size(311, 493);
      this.zedGraphControl1.TabIndex = 2;
      // 
      // pictureBox1
      // 
      this.pictureBox1.Location = new System.Drawing.Point(4, 541);
      this.pictureBox1.Name = "pictureBox1";
      this.pictureBox1.Size = new System.Drawing.Size(311, 308);
      this.pictureBox1.TabIndex = 4;
      this.pictureBox1.TabStop = false;
      // 
      // ClusterVectorView
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.pictureBox1);
      this.Controls.Add(this.groupBox1);
      this.Controls.Add(this.zedGraphControl1);
      this.Name = "ClusterVectorView";
      this.Size = new System.Drawing.Size(322, 852);
      this.groupBox1.ResumeLayout(false);
      this.groupBox1.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.GroupBox groupBox1;
    private System.Windows.Forms.TextBox TBID;
    private System.Windows.Forms.TextBox TBBurstsCount;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Label label1;
    private ZedGraph.ZedGraphControl zedGraphControl1;
    private System.Windows.Forms.PictureBox pictureBox1;
  }
}
