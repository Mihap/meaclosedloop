﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MEAClosedLoop.Common;
using ZedGraph;

namespace MEAClosedLoop.UIForms.SubView
{
  public partial class ClusterVectorView : UserControl
  {

    private CSprdBurstCluster _cluster;
    public CSprdBurstCluster Cluster
    {
      get
      {
        return _cluster;
      }

      set
      {
        _cluster = value;

        ReloadCluster();
      }
    }

    public ClusterVectorView()
    {
      InitializeComponent();
    }

    public ClusterVectorView(CSprdBurstCluster cluster) : this()
    {
      Cluster = cluster;
    }

    private void ReloadCluster()
    {
      //обновляем надписи
      TBID.Text = Cluster.ClusterNum.ToString();
      TBBurstsCount.Text = Cluster.innerIDS.Count.ToString();

      //обновляем график
      GraphPane pane = zedGraphControl1.GraphPane;

      pane.CurveList.Clear();
      pane.XAxis.Title.Text = "Начало пачки, мс";
      pane.YAxis.Title.Text = "Номер канала";

      pane.XAxis.Scale.Max = 80;
      pane.YAxis.Scale.Max = 60;
      pane.XAxis.Scale.Min = 0;
      pane.YAxis.Scale.Min = 0;
      // Все векторы кластера
      foreach (var burstId in Cluster.innerIDS)
      {
        if (burstId == 0) continue;

        Vector60 burstVector = CSprdClusterProvider.ClusterBuilder.VectorDict[burstId];
        for (int i = 0; i < 60; i++)
        {
          if (!burstVector.Elements.Keys.Contains(i)) continue;
          PointPairList line = new PointPairList();
          line.Add(burstVector.Elements[i] / 25.0, i);
          line.Add(burstVector.Elements[i] / 25.0, i + 1);
          LineItem curve;
          curve = pane.AddCurve(null, line, Color.FromArgb(20, Color.Black), SymbolType.None);
          curve.Line.Width = 3;
          curve.Line.IsSmooth = true;
        }
      }

      // Отрисовка центроида
      var centroid = Cluster.Vector;
      for (int i = 0; i < 60; i++)
      {
        if (!centroid.Elements.Keys.Contains(i)) continue;
        PointPairList line = new PointPairList();
        line.Add(centroid.Elements[i] / 25.0, i);
        line.Add(centroid.Elements[i] / 25.0, i + 1);
        LineItem curve;
        curve = pane.AddCurve(null, line, Color.Blue, SymbolType.None);
        curve.Line.Width = 6;
        curve.Line.IsSmooth = true;
      }

      zedGraphControl1.AxisChange();
      zedGraphControl1.Invalidate();

      // рисуем картинку
      var colors = VectorDrawHelper.CalculateColors(25);
      VectorDrawHelper.DrawComplexMap(centroid, pictureBox1, colors, true);
      
    }

  }
}
