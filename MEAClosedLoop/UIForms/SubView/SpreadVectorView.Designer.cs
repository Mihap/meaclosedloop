﻿namespace MEAClosedLoop.UIForms.SubView
{
  partial class SpreadVectorView
  {
    /// <summary> 
    /// Обязательная переменная конструктора.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Освободить все используемые ресурсы.
    /// </summary>
    /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Код, автоматически созданный конструктором компонентов

    /// <summary> 
    /// Требуемый метод для поддержки конструктора — не изменяйте 
    /// содержимое этого метода с помощью редактора кода.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.zedGraphControl1 = new ZedGraph.ZedGraphControl();
      this.groupBox1 = new System.Windows.Forms.GroupBox();
      this.TBID = new System.Windows.Forms.TextBox();
      this.TBDuration = new System.Windows.Forms.TextBox();
      this.TBStartTime = new System.Windows.Forms.TextBox();
      this.label3 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.label1 = new System.Windows.Forms.Label();
      this.groupBox1.SuspendLayout();
      this.SuspendLayout();
      // 
      // zedGraphControl1
      // 
      this.zedGraphControl1.Location = new System.Drawing.Point(9, 265);
      this.zedGraphControl1.Margin = new System.Windows.Forms.Padding(16);
      this.zedGraphControl1.Name = "zedGraphControl1";
      this.zedGraphControl1.ScrollGrace = 0D;
      this.zedGraphControl1.ScrollMaxX = 0D;
      this.zedGraphControl1.ScrollMaxY = 0D;
      this.zedGraphControl1.ScrollMaxY2 = 0D;
      this.zedGraphControl1.ScrollMinX = 0D;
      this.zedGraphControl1.ScrollMinY = 0D;
      this.zedGraphControl1.ScrollMinY2 = 0D;
      this.zedGraphControl1.Size = new System.Drawing.Size(585, 1171);
      this.zedGraphControl1.TabIndex = 0;
      // 
      // groupBox1
      // 
      this.groupBox1.Controls.Add(this.TBID);
      this.groupBox1.Controls.Add(this.TBDuration);
      this.groupBox1.Controls.Add(this.TBStartTime);
      this.groupBox1.Controls.Add(this.label3);
      this.groupBox1.Controls.Add(this.label2);
      this.groupBox1.Controls.Add(this.label1);
      this.groupBox1.Location = new System.Drawing.Point(9, 9);
      this.groupBox1.Margin = new System.Windows.Forms.Padding(7);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Padding = new System.Windows.Forms.Padding(7);
      this.groupBox1.Size = new System.Drawing.Size(585, 223);
      this.groupBox1.TabIndex = 1;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "Информация о пачке";
      // 
      // TBID
      // 
      this.TBID.Location = new System.Drawing.Point(282, 161);
      this.TBID.Margin = new System.Windows.Forms.Padding(7);
      this.TBID.Name = "TBID";
      this.TBID.Size = new System.Drawing.Size(180, 35);
      this.TBID.TabIndex = 1;
      // 
      // TBDuration
      // 
      this.TBDuration.Location = new System.Drawing.Point(282, 103);
      this.TBDuration.Margin = new System.Windows.Forms.Padding(7);
      this.TBDuration.Name = "TBDuration";
      this.TBDuration.Size = new System.Drawing.Size(180, 35);
      this.TBDuration.TabIndex = 1;
      // 
      // TBStartTime
      // 
      this.TBStartTime.Location = new System.Drawing.Point(282, 42);
      this.TBStartTime.Margin = new System.Windows.Forms.Padding(7);
      this.TBStartTime.Name = "TBStartTime";
      this.TBStartTime.Size = new System.Drawing.Size(180, 35);
      this.TBStartTime.TabIndex = 1;
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(14, 167);
      this.label3.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(36, 29);
      this.label3.TabIndex = 0;
      this.label3.Text = "ID";
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(14, 109);
      this.label2.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(252, 29);
      this.label2.TabIndex = 0;
      this.label2.Text = "продолжительность";
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(14, 51);
      this.label1.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(171, 29);
      this.label1.TabIndex = 0;
      this.label1.Text = "Время старта";
      // 
      // SpreadVectorView
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(14F, 29F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.groupBox1);
      this.Controls.Add(this.zedGraphControl1);
      this.Margin = new System.Windows.Forms.Padding(7);
      this.Name = "SpreadVectorView";
      this.Size = new System.Drawing.Size(602, 1437);
      this.Load += new System.EventHandler(this.SpreadClusterView_Load);
      this.groupBox1.ResumeLayout(false);
      this.groupBox1.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private ZedGraph.ZedGraphControl zedGraphControl1;
    private System.Windows.Forms.GroupBox groupBox1;
    private System.Windows.Forms.TextBox TBID;
    private System.Windows.Forms.TextBox TBDuration;
    private System.Windows.Forms.TextBox TBStartTime;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label1;
  }
}
