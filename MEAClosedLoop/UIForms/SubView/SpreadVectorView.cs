﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ZedGraph;
using MEAClosedLoop.Common;

namespace MEAClosedLoop.UIForms.SubView
{
  public partial class SpreadVectorView : UserControl
  {
    private ulong _BurstID;
    public ulong BurstID
    {
      get
      {
        return _BurstID;
      }

      set
      {
        _BurstID = value;
        
        ReloadBurst();
      }
    }
    public SpreadVectorView()
    {
      InitializeComponent();
    }

    public SpreadVectorView(ulong BurstID) : this()
    {
      this.BurstID = BurstID;
    }

    private void SpreadClusterView_Load(object sender, EventArgs e)
    {
     
    }
    private void ReloadBurst()
    {
      if (BurstID == 0) return;
      //обновляем надписи
      TBID.Text = BurstID.ToString();
      TBStartTime.Text = Math.Round(BurstID / ((double)Param.DAQ_FREQ), 3).ToString() + " с";
      if(CBurstDataProvider.BurstAdrCollection.ContainsKey(BurstID))
        TBDuration.Text = Math.Round(CBurstDataProvider.BurstAdrCollection[BurstID].DurationTime / ((double)Param.DAQ_FREQ), 3).ToString() + " с";
      //обновляем график

      GraphPane pane = zedGraphControl1.GraphPane;
      pane.CurveList.Clear();
      pane.XAxis.Title.Text = "Начало пачки, мс";
      pane.YAxis.Title.Text = "Номер канала";
      Vector60 burstVector = CSprdClusterProvider.ClusterBuilder.VectorDict[BurstID];
      List<PointPairList> channels = new List<PointPairList>();
      for (int i = 0; i < 60; i++)
      {
        if (!burstVector.Elements.Keys.Contains(i)) continue;
        PointPairList line = new PointPairList();
        line.Add(burstVector.Elements[i] / 25.0, i);
        line.Add(burstVector.Elements[i] / 25.0, i + 1);
        LineItem curve;
        if (burstVector.ChWithZeroElList.Contains(i))
        {
          curve = pane.AddCurve(null, line, Color.Red, SymbolType.None);
        }
        else
        {
          curve = pane.AddCurve(null, line, Color.Blue, SymbolType.None);
        }
        curve.Line.Width = 3;
        curve.Line.IsSmooth = true;
      }
      zedGraphControl1.AxisChange();
      zedGraphControl1.Invalidate();
    }
  }
}
