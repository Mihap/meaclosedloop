﻿#define channelmethod
#define channeldata
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Mcs.Usb;
using System.Threading;
using System.Diagnostics;

namespace MeaCapturer
{
  /// <summary>
  /// Логика взаимодействия для MainWindow.xaml
  /// </summary>
  public partial class MainWindow : Window
  {
    private StreamControlUI[] streamContols = new StreamControlUI[4];

    private readonly CMcsUsbListNet usblist = new CMcsUsbListNet();
    private CMeaDeviceNet device;

    int channelblocksize;
    int mChannelHandles;
    public MainWindow()
    {
      InitializeComponent();
    }


    private void Window_Loaded(object sender, RoutedEventArgs e)
    {
      //Добавляем все контролы на общую таблицу
      for (int i = 0; i < 4; i++)
      {
        streamContols[i] = new StreamControlUI(i);
        SrcGrid.Children.Add(streamContols[i]);
        Grid.SetColumn(streamContols[i], i);
      }
    }

    private void UpdateDeviceList_Click(object sender, RoutedEventArgs e)
    {
      DeviceListComboBox.Items.Clear();
      usblist.Initialize(DeviceEnumNet.MCS_MEA_DEVICE);
      for (uint i = 0; i < usblist.Count; i++)
      {
        DeviceListComboBox.Items.Add(usblist.GetUsbListEntry(i).DeviceName + " / " + usblist.GetUsbListEntry(i).SerialNumber);
      }
      if (DeviceListComboBox.Items.Count > 0)
      {
        DeviceListComboBox.SelectedIndex = 0;
      }
    }
    void OnChannelData(CMcsUsbDacqNet d, int cbHandle, int numSamples)
    {
      int sizeRet;
      int totalchannels, offset, channels;
      device.ChannelBlock_GetChannel(0, 0, out totalchannels, out offset, out channels);
      ushort[] data = device.ChannelBlock_ReadFramesUI16(0, channelblocksize, out sizeRet);

      for (int Plate_N = 0; Plate_N < 4; Plate_N++)
      {
        Dictionary<int, ushort[]> inputData = new Dictionary<int, ushort[]>();
        for (int i = Plate_N * 64; i < Plate_N * 64 + 60; i++)
        {
          ushort[] data1 = new ushort[sizeRet];
          for (int j = 0; j < sizeRet; j++)
          {
            data1[j] = data[j * mChannelHandles + i];
          }
          inputData.Add(i, data1);
          //BeginInvoke(new OnChannelDataDelegate(OnChannelDataLater), new Object[] { data1, i });
        }
        streamContols[Plate_N].AddData(inputData);
      }
    }

    private void DeviceListComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      if (device != null)
      {
        device.StopDacq();

        device.Disconnect();
        device.Dispose();

        device = null;
      }

      uint sel = (uint)DeviceListComboBox.SelectedIndex;
      /* choose one of the following contructors:
       * The first one uses the OnNewData callback and gives you a reference to the raw multiplexed data,
       * this could be used without further initialisation
       * The second uses the more advanced callback which gives you the data for each channel in a callback, but need initialistation
       * for buffers and the selected channels
       */

      if (sel >= 0) // a device is selected, enable the sampling buttons
      {
        StartDeviceBtn.IsEnabled = true;
      }
      else
      {
        StartDeviceBtn.IsEnabled = false;
      }
     // StartDeviceBtn.IsEnabled = false;

      device = new CMeaDeviceNet(usblist.GetUsbListEntry(sel).DeviceId.BusType, OnChannelData, OnError);

      device.Connect(usblist.GetUsbListEntry(sel));

      device.SendStop(); // only to be sure

      DeviceInfo.Content = "";
      DeviceInfo.Content += /*"Serialnumber: " +*/ device.SerialNumber + " ";
      int hwchannels;
      device.HWInfo().GetNumberOfHWADCChannels(out hwchannels);
      DeviceInfo.Content += @"Number of Hardwarechannels: " + hwchannels.ToString("D") + " ";

      if (hwchannels == 0)
      {
        hwchannels = 64;
      }

      // configure MeaDevice: MC_Card or Usb
      device.SetNumberOfChannels(hwchannels);

      const int Samplingrate = 25000; // MC_Card does not support all settings, please see MC_Rack for valid settings
      device.SetSampleRate(Samplingrate, 1, 0);

      int gain = device.GetGain();

      List<CMcsUsbDacqNet.CHWInfo.CVoltageRangeInfoNet> voltageranges;
      device.HWInfo().GetAvailableVoltageRangesInMicroVoltAndStringsInMilliVolt(out voltageranges);
      for (int i = 0; i < voltageranges.Count; i++)
      {
        DeviceInfo.Content += @"(" + i.ToString("D") + @") " + voltageranges[i].VoltageRangeDisplayStringMilliVolt;
      }

      // Set the range acording to the index (only valid for MC_Card)
      // device.SetVoltageRangeInMicroVoltByIndex(0, 0);

      device.EnableDigitalIn(true, 0);

      // Checksum not supported by MC_Card
      device.EnableChecksum(true, 0);


      // Get the layout to know how the data look like that you receive
      int ana, digi, che, tim, block;
      device.GetChannelLayout(out ana, out digi, out che, out tim, out block, 0);

      // or
      block = device.GetChannelsInBlock();

      // set the channel combo box with the channels
      // SetChannelCombo(block);

      channelblocksize = Samplingrate / 10; // good choise for MC_Card

      bool[] selChannels = new bool[block];

      for (int i = 0; i < block; i++)
      {
        selChannels[i] = true; // With true channel i is selected
                               // selChannels[i] = false; // With false the channel i is deselected
      }
      // queue size and threshold should be selected carefully

      device.SetSelectedData(selChannels, 10 * channelblocksize, channelblocksize, SampleSizeNet.SampleSize16Unsigned, block);
      //device.AddSelectedChannelsQueue(10, 2, 10 * channelblocksize, channelblocksize, SampleSizeNet.SampleSize16Unsigned);
      //device.ChannelBlock_SetCommonThreshold(channelblocksize);
      //Alternative call if you want to select all channels
      //device.SetSelectedData(block, 10 * channelblocksize, channelblocksize, CMcsUsbDacqNet.SampleSize.Size16, block);
      mChannelHandles = block; // for this case, if all channels are selected

    }
    void OnError(String msg, int info)
    {
      device.StopDacq();
      //            MessageBox.Show(@"Mea Device Error: " + msg);
    }
    private void DebugBtn_Click(object sender, RoutedEventArgs e)
    {
      DebugBtn.IsEnabled = false;
      Task emulation = new Task(new Action(() =>
      {
        Stopwatch sw = new Stopwatch();
        sw.Start(); 
        int n = 0;
        int N = 1000000;
        Random rand = new Random();
        while (n < N)
        {
          for (int Plate_N = 0; Plate_N < 4; Plate_N++)
          {
            Dictionary<int, ushort[]> inputData = new Dictionary<int, ushort[]>();
            for (int i = Plate_N * 64; i < Plate_N * 64 + 60; i++)
            {
              ushort[] data1 = new ushort[2500];
              for (int j = 0; j < 2500; j++)
              {
                data1[j] = (ushort)( rand.Next(70, 340)* Math.Sin(Math.PI * 2 * j * 25 / 2500.0) + ushort.MaxValue / 2);
              }
              inputData.Add(i - Plate_N * 64, data1);
            }
            streamContols[Plate_N].AddData(inputData);
          }
          n++;
          int delay = (100 - sw.ElapsedMilliseconds > 0) ? (int)(100 - sw.ElapsedMilliseconds) : 0;
          Thread.Sleep(delay);
        }

      }));
      emulation.Start();
    }
    private void StartDeviceBtn_Click(object sender, RoutedEventArgs e)
    {
      device.StartDacq();
      DeviceListComboBox.IsEnabled = false;
      UpdateDeviceList.IsEnabled = false;
      StartDeviceBtn.IsEnabled = false;
      StopDeviceBtn.IsEnabled = true;
    }
    private void StopDeviceBtn_Click(object sender, RoutedEventArgs e)
    {
      device.StopDacq();
      DeviceListComboBox.IsEnabled = true;
      UpdateDeviceList.IsEnabled = true;
      StartDeviceBtn.IsEnabled = true;
      StopDeviceBtn.IsEnabled = false;
    }
  }
}
