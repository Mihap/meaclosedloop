﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
namespace MeaCapturer
{
  using MEAClosedLoop;
  using System.Diagnostics;
  using System.Net;
  using System.Net.Sockets;
  using System.Threading;
  /// <summary>
  /// Логика взаимодействия для StreamControlUI.xaml
  /// </summary>
  using TRawDataPacket = Dictionary<int, ushort[]>;
  public partial class StreamControlUI : UserControl
  {
    private Queue<TRawDataPacket> InputQueue = new Queue<TRawDataPacket>();
    private object QueueLock = new object();
    private bool DoSend = false;
    private bool ClientConnected = false;
    Task SendTask;
    private int _Port;
    public int Port
    {
      get { return _Port; }
      private set
      {
        _Port = value + Block;
        ChannelsLabel.Dispatcher.BeginInvoke(new Action(() => { PortLabel.Content = value + Block; }));
      }
    }
    private int _Block;
    public int Block
    {
      get { return _Block; }
      private set
      {
        _Block = value;
        ChannelsLabel.Dispatcher.BeginInvoke(new Action(() => { ChannelsLabel.Content = value * 64 + " - " + (value * 64 + 59); }));
      }
    }

    public StreamControlUI()
    {
      InitializeComponent();
    }

    public StreamControlUI(int Block) : this()
    {
      this.Block = Block;
      this.Port = 2236;
    }

    public void AddData(TRawDataPacket data)
    {
      if (DoSend && ClientConnected)
        lock (QueueLock)
          InputQueue.Enqueue(data);
      QueueLengthLabel.Dispatcher.BeginInvoke(new Action(() => { QueueLengthLabel.Content = InputQueue.Count; }));
    }
    private void SendFunction()
    {
      // Устанавливаем для сокета локальную конечную точку
      IPHostEntry ipHost = Dns.GetHostEntry("localhost");
      IPAddress ipAddr = ipHost.AddressList[0];
      IPEndPoint ipEndPoint = new IPEndPoint(ipAddr, Port);
      // Создаем сокет Tcp/Ip
      Socket sListener = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
      sListener.Bind(ipEndPoint);
      sListener.Listen(10);
      // Программа приостанавливается, ожидая входящее соединение
      Socket handler = sListener.Accept();

      ClientStatusLabel.Dispatcher.BeginInvoke(new Action(() => { ClientStatusLabel.Content = "подключен";}));
      ClientConnected = true;
      ClientAddrLabel.Dispatcher.BeginInvoke(new Action(() => { ClientAddrLabel.Content = handler.RemoteEndPoint.ToString(); }));
      while (DoSend)
      {
        Stopwatch sw = new Stopwatch();
        sw.Start();
        lock (QueueLock)
          if (InputQueue.Count > 0)
          {
            TRawDataPacket t = InputQueue.Dequeue();
            byte[] data = CRawDataProvider.RawDataToBinary(t);
            try
            {
              handler.Send(data);
              handler.Receive(new byte[1]);
            }
            catch(Exception e)
            {

            }
          }
        QueueLengthLabel.Dispatcher.BeginInvoke(new Action(() => { QueueLengthLabel.Content = InputQueue.Count; }));
        int delay = 1;// (50 - sw.ElapsedMilliseconds > 0) ? (int)(50 - sw.ElapsedMilliseconds) : 0;
        Thread.Sleep(delay);
      }
    }

    private void StartButton_Click(object sender, RoutedEventArgs e)
    {
      SendTask = new Task(SendFunction);
      DoSend = true;
      SendTask.Start();
      StartButton.IsEnabled = false;
    }

    private void StopButton_Click(object sender, RoutedEventArgs e)
    {
      DoSend = false;
      if (SendTask != null && !SendTask.IsCompleted)
        SendTask.Wait();
      StartButton.IsEnabled = true;
    }
  }
}
